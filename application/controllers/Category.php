<?php
class Category extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('T_supsection');
		$this->load->model('T_section');
		if(!$this->session->userdata('id_adm'))
			redirect('auth');
	}
	
	function index($menu=0, $daerah, $id=0)
	{
		$datacontent['title']	= 'category';
		$datacontent['id'] = $id;
		$datacontent['url'] 	= "category/index/".$id;
		$datacontent['daerah']	= $daerah;
		//echo "<pre>";
		//print_r($datacontent['url']);
		//echo "</pre>";
		$datacontent['menuid'] 	= $menu;
		$datacontent['data'] 	= $this->T_supsection->selectadmin($menu);
		
		$datacontent['title'] 		=	$this->T_section->getsection($menu);
		if(empty($this->input->post('search')))
		{
			$datacontent['category'] = $this->T_section->getcategory($menu, $daerah);
		}
		else
		{
			$datacontent['category'] = $this->T_section->search($this->input->post('search'));
		}
		$this->load->view('gen_cnf/category/index',$datacontent);
	}
	
	function add($id=0,$daerah=0)
	{
		$datacontent['menu'] 	=	$this->T_section->getsection($id);
		$datacontent['title']	= 'category';
		//$datacontent['url'] = "category/index";
		$this->load->helper('form');
		$datacontent['action'] 	= 'add';
		$datacontent['id']	 = $id;
		$datacontent['daerah'] = $daerah;
		$this->load->view('gen_cnf/category/add',$datacontent);
	}
	
	function actionadd()
	{
		
		$data = array(
			'id_supsection'		=> $this->input->post('id'),
			'nama_section'   	=> $this->input->post('nama_section'),
			'status'     		=> $this->input->post('status'),
			'keyword'			=> $this->input->post('keyword'),
			'description'		=> $this->input->post('description'),
			'title'				=> $this->input->post('title'),
			'url_title'			=> url_title($this->input->post('nama_section')),
			'daerah'			=> $this->input->post('daerah')
		);
		
		$this->T_section->add($data);
		redirect('category/index/'.$this->input->post('id').'/'.$this->input->post('daerah'));  
	}

	function edit($id=0,$supsec=0,$daerah=0)
	{
		$datacontent['menu'] 	=	$this->T_section->getsection($id);
		$datacontent['title']	= 'category';
		$datacontent['url'] = "category/index";
		$datacontent['action'] 		= 'edit';
		$datacontent['data'] 		= $this->T_section->get($id);
		$datacontent['daerah']		= $daerah;
		$this->load->view('gen_cnf/category/edit',$datacontent);
	}	


	function actionedit()
	{
	
		$data = array(
			'nama_section'   => $this->input->post('nama_section'),
			'status'     			=> $this->input->post('status'),
			'keyword'			=> $this->input->post('keyword'),
			'description'		=> $this->input->post('description'),
			'title'				=> $this->input->post('title'),
			'url_title'			=> url_title($this->input->post('nama_section'))
			
		);
		$this->T_section->update($this->input->post('id'),$data);
		
		redirect('category/index/'.$this->input->post('idsup').'/'.$this->input->post('daerah')); 
	}
	
	
	function actiondelete($id, $idsup, $daerah)
	{
	
		$data = array('id_section' => $id);
		$data2 = array('id_supsection' => $id);
		$this->T_section->delete($data);
		$this->T_section->delete($data2);
		redirect('category/index/'.$idsup.'/'.$daerah);  
	}
	

	function position() 
	{
		$count = $this->T_section->count($this->input->post('supsection'));
		if($this->input->post('position') > $count)
		{
			$this->session->set_flashdata('postwarning', strtoupper('posisi yang anda masukkan melebihi jumlah channel'));
			redirect('category/index/'.$this->input->post('supsection'));
		}
		else
		{
			$check = $this->T_section->checkpos($this->input->post('position'), $this->input->post('supsection'));
			if(!empty($check))
			{
				$data = array('position' => $this->input->post('position'));
				$this->T_section->updatepos($this->input->post('section'), $data);
				$data2 = array('position' => $this->input->post('positionold'));
				$this->T_section->updatepos($check['id_section'], $data2);
				redirect('category/index/'.$this->input->post('supsection'));
			}
			else
			{
				$data = array('position' => $this->input->post('position'));
				$this->T_section->updatepos($this->input->post('section'), $data);
				redirect('category/index/'.$this->input->post('supsection'));
			}
		}
	}
	function toexcel(){
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="Data-Administrator-'.date("Y-m-d/H-i-s").'.xls"');
        $data["rows"] = $this->model_administrator->select();
        $this->load->view("gen_cnf/administrator/xls", $data);
    }
	

}
