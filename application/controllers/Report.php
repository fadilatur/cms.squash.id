<?php
class Report extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('T_artikel');
		$this->load->model('T_section');
		$this->load->model('T_admin');
		if(!$this->session->userdata('id_adm'))
			redirect('auth');
	}
	
	function index()
	{
		$datacontent['title']	= 'report';
		$datacontent['url'] 	= "report/";
		
		if(empty($this->input->post('tgl1')))
		{
			$datacontent['tgl1'] = date('Y-m-d', strtotime("-31 day", strtotime(date("Y-m-d"))));;
		}
		else
		{
			$datacontent['tgl1'] = $this->input->post('tgl1');
		}

		if(empty($this->input->post('tgl2')))
		{
			$datacontent['tgl2'] = date('Y-m-d');
		}
		else
		{
			$datacontent['tgl2'] = $this->input->post('tgl2');
		}

		$datacontent['author'] = $this->T_admin->getauthor();
		//$datacontent['data'] 	= $this->T_artikel->selectreport($tgl1,$tgl2);
		//echo "<pre>";
		//print_r($datacontent['data']);
		//echo "</pre>";
		$this->load->view('gen_cnf/report/index',$datacontent);
	}

	function article()
	{

		$datacontent['title']	= 'report';
		$datacontent['url'] 	= "report/";
		
		if(empty($this->input->post('tgl1')))
		{
			//$datacontent['tgl1'] = date('Y-m-d', strtotime("-31 day", strtotime(date("Y-m-d"))));;
			$datacontent['tgl1']	= date('Y-m-d');
		}
		else
		{
			$datacontent['tgl1'] = $this->input->post('tgl1');
		}

		if(empty($this->input->post('tgl2')))
		{
			$datacontent['tgl2'] = date('Y-m-d');
		}
		else
		{
			$datacontent['tgl2'] = $this->input->post('tgl2');
		}

		$datacontent['author'] = $this->T_admin->getauthor();
		$datacontent['laporan'] = $this->T_artikel->laporan($datacontent['tgl1'], $datacontent['tgl2']);
		//$datacontent['data'] 	= $this->T_artikel->selectreport($tgl1,$tgl2);
		//echo "<pre>";
		//print_r($datacontent['data']);
		//echo "</pre>";
		$this->load->view('gen_cnf/report/article',$datacontent);
	}
	
	

		
}
