<?php 
class Kolom extends ci_controller{

	function __construct()
	{
		parent::__construct();
		$this->load->model('T_kolom');
		$this->load->model('T_admin');
		$this->load->model('T_section');
		$this->load->model('T_artikel');
		date_default_timezone_set('Asia/Jakarta');
		if(!$this->session->userdata('id_adm'))
			redirect('auth');
	}

	function index($daerah=0){

		$data['data']	=	$this->T_kolom->select($daerah);
		$data['daerah']	= 	$daerah;
		$this->load->view ('gen_cnf/kolom/index', $data);
	}

	function add($daerah=0){
		if($daerah == 0) 
		{
			$data['kolom']	=	$this->T_section->select(38);
		}
		else
		{
			$data['kolom']	=	$this->T_section->select(143);
		}
		$data['daerah']		= 	$daerah;
		$this->load->view ('gen_cnf/kolom/add', $data);
	}

	function actionadd(){
		if(!empty($_FILES)){

				//pengecekan folder ada apa tidak
				$uploaddir 		= $this->config->item('upload_images').'images/';
				$datename 		= date("Y"); 
			
				$tahun = $uploaddir.$datename;
				if(is_dir($tahun)){
				}else{
					mkdir($tahun, 0777, true);
					$fp = fopen($tahun.'/index.html', 'w');
					fwrite($fp , 'Bismillaah');
					fclose($fp);
				}


				$datename2 = date("m"); 
			
				$bulan = $uploaddir.$datename."/".$datename2;
				if(is_dir($bulan)){
				}else{
					mkdir($bulan, 0777, true);
					$fp = fopen($bulan.'/index.html', 'w');
					fwrite($fp , 'Bismillaah');
					fclose($fp);
				}


				$datename3 = date("d"); 
			
				$hari = $uploaddir.$datename."/".$datename2."/".$datename3;
				if(is_dir($hari)){
				}else{
					mkdir($hari, 0777, true);
					$fp = fopen($hari.'/index.html', 'w');
					fwrite($fp , 'Bismillaah');
					fclose($fp);
				}

			}

			if(!empty($_FILES['image']['name'])){
				$image_name =  str_replace(' ','_',date('Ymdhis').$_FILES['image']['name']);
				$config['upload_path'] 		= $hari ;
				$config['allowed_types'] 	= 'gif|jpg|png|jpeg';
				$config['file_name'] 		= $image_name;
				$config['min_width'] = '800';
				$config['min_height'] = '450';
				$this->upload->initialize($config);
				$this->upload->do_upload('image');
			}else{
				$image_name = $this->input->post('image');
			}
		$data = array(
				'category_id'				=> $this->input->post('author'),
				'nama'	    			=> $this->input->post('nama'),
				'image'     			=> $image_name,
				'keterangan'    	  	=> $this->input->post('keterangan'),
				'postdate'    	  		=> date('Y-m-d H:i:s'),
				'daerah'				=> $this->input->post('daerah')
				
			);
			$this->T_kolom->add($data);
			$lastid = $this->db->insert_id();
			redirect('kolom/index/'.$this->input->post('daerah'));
	}

	function edit($id,$daerah=0){

		$data['data'] = $this->T_kolom->get_kolom($id);
		if($daerah == 0) 
		{
			$data['kolom']	=	$this->T_section->select(38);
		}
		else
		{
			$data['kolom']	=	$this->T_section->select(143);
		}
		$data['daerah']		= 	$daerah;
		$this->load->view ('gen_cnf/kolom/edit', $data);
	}

	function actionedit(){
		if(!empty($_FILES)){

				//pengecekan folder ada apa tidak
				$uploaddir 		= $this->config->item('upload_images').'images/';
				

				$datapath = explode("/", $this->input->post('path'));

				$datename 		= $datapath[1]; 
			
				$tahun = $uploaddir.$datename;
				if(is_dir($tahun)){
				}else{
					mkdir($tahun, 0777, true);
					$fp = fopen($tahun.'/index.html', 'w');
					fwrite($fp , 'Bismillaah');
					fclose($fp);
				}


				$datename2 = $datapath[2]; 
			
				$bulan = $uploaddir.$datename."/".$datename2;
				if(is_dir($bulan)){
				}else{
					mkdir($bulan, 0777, true);
					$fp = fopen($bulan.'/index.html', 'w');
					fwrite($fp , 'Bismillaah');
					fclose($fp);
				}


				$datename3 = $datapath[3]; 
			
				$hari = $uploaddir.$datename."/".$datename2."/".$datename3;
				if(is_dir($hari)){
				}else{
					mkdir($hari, 0777, true);
					$fp = fopen($hari.'/index.html', 'w');
					fwrite($fp , 'Bismillaah');
					fclose($fp);
				}

			}


		if(!empty($_FILES['image']['name'])){
				$image_name =  str_replace(' ','_',date('Ymdhis').$_FILES['image']['name']);
				$config['upload_path'] 		= $hari;
				$config['allowed_types'] 	= 'gif|jpg|png|jpeg';
				$config['file_name'] 		= $image_name;
				$config['min_width'] = '800';
				$config['min_height'] = '450';
				$this->upload->initialize($config);
				$this->upload->do_upload('image');
			}else{
				$image_name = $this->input->post('image');
			}

		$data = array(
			'category_id'				=> $this->input->post('author'),
			'nama'		 =>	$this->input->post('nama'),
			'image'      => $image_name,
			'keterangan' => $this->input->post('keterangan')

			);
		$this->T_kolom->update($this->input->post('id'),$data);
		
		redirect('kolom/index/'.$this->input->post('daerah'));
	}

	function actiondelete($id,$daerah=0)
	{
		
		$this->T_kolom->delete($id);
		redirect('kolom/index/'.$daerah);  
	}

	function artikel($id)
	{
		$data['article'] = $this->T_artikel->getartikelkolom($id);
		$this->load->view('gen_cnf/kolom/artikel', $data);
	}

	function pilih($id, $section)
	{
		$data = array('hlkolom'	=>	'Y');
		$this->T_artikel->updatehlkolom($data, $id);
		redirect('kolom/artikel/'.$section);
	}

	function unpilih($id, $section)
	{
		$data = array('hlkolom'	=>	'N');
		$this->T_artikel->updatehlkolom($data, $id);
		redirect('kolom/artikel/'.$section);
	}
}