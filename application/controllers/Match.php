<?php 
/**
 * 
 */
class Match extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('T_match');
		$this->load->model('T_player');
		$this->load->model('T_set');
		$this->load->model('T_score');
		$this->load->model('T_group_match');
	}

	function index($parent=0) {
		$data['parent'] = 	$parent;
		if(!empty($parent)) {
			$data['match'] 	=	$this->T_match->getAllByParent($parent);
		} else {
			$data['match'] 	=	$this->T_match->getAll();
		}
		$this->load->view('gen_cnf/match/index', $data);
	}

	function add($parent=0) {
		$data['parent']		=	$parent;
		$this->load->view('gen_cnf/match/add', $data);
	}

	function actionadd($parent=0) {
		$data = array(
			'name'		=>	$this->input->post('name'),
			'parent_id'	=>	$parent,
			'created'	=>	date('Y-m-d H:i:s')
		);
		$this->T_match->add($data);
		if(!empty($parent)) {
			redirect('match/index/'.$parent);
		} else {
			redirect('match');
		}
	}

	function edit($id,$parent=0) {
		$data['parent']	=	$parent;
		$data['match'] 	=	$this->T_match->getById($id);
		$this->load->view('gen_cnf/match/edit', $data);
	}

	function actionedit($id,$parent=0) {
		$data = array(
			'name'		=>	$this->input->post('name')
		);
		$this->T_match->update($data, $id);
		if(!empty($parent)) {
			redirect('match/index/'.$parent);
		} else {
			redirect('match');
		}
	}

	function delete($id,$parent=0) {
		$this->T_match->delete($id);
		if(!empty($parent)) {
			redirect('match/index/'.$parent);
		} else {
			redirect('match');
		}
	}

	function player($id) {
		$data['match']		=	$id;
		$data['player'] 	=	$this->T_player->getAllByMatch($id);
		$this->load->view('gen_cnf/match/playerindex', $data);
	}

	function playeradd($id) {
		$data['match']	=	$id;
		$this->load->view('gen_cnf/match/playeradd', $data);
	}

	function playeractionadd($id) {
		if(!empty($_FILES['flag']['name'])){
			$flag =  str_replace(' ','_',date('Ymdhis').$_FILES['flag']['name']);
			$config['upload_path'] 		= $this->config->item('upload_images').'/player/' ;
			$config['allowed_types'] 	= 'gif|jpg|png|jpeg';
			$config['file_name'] 		= $flag;
			$this->upload->initialize($config);
			$this->upload->do_upload('flag');
		}else{
			$flag = '';
		}

		if(!empty($_FILES['image']['name'])){
			$image_name =  str_replace(' ','_',date('Ymdhis').$_FILES['image']['name']);
			$config['upload_path'] 		= $this->config->item('upload_images').'/player/' ;
			$config['allowed_types'] 	= 'gif|jpg|png|jpeg';
			$config['file_name'] 		= $image_name;
			$this->upload->initialize($config);
			$this->upload->do_upload('image');
		}else{
			$image_name = '';
		}
		$data = array(
			'match_id'	=>	$id,
			'name'		=>	$this->input->post('name'),
			'country'	=>	$this->input->post('country'),
			'flag'		=>	$flag,
			'image'		=>	$image_name,
			'status'	=>	$this->input->post('status'),
			'created'	=>	date('Y-m-d H:i:s')
		);
		$this->T_player->add($data);
		redirect('match/player/'.$id);
	}

	function playeredit($match, $id) {
		$data['match']	=	$match;
		$data['player'] 	=	$this->T_player->getById($id);
		$this->load->view('gen_cnf/match/playeredit', $data);
	}

	function playeractionedit($match, $id) {
		if(!empty($_FILES['flag']['name'])){
			$flag =  str_replace(' ','_',date('Ymdhis').$_FILES['flag']['name']);
			$config['upload_path'] 		= $this->config->item('upload_images').'/player/' ;
			$config['allowed_types'] 	= 'gif|jpg|png|jpeg';
			$config['file_name'] 		= $flag;
			$this->upload->initialize($config);
			$this->upload->do_upload('flag');
		}else{
			$flag = $this->input->post('flag');
		}

		if(!empty($_FILES['image']['name'])){
			$image_name =  str_replace(' ','_',date('Ymdhis').$_FILES['image']['name']);
			$config['upload_path'] 		= $this->config->item('upload_images').'/player/' ;
			$config['allowed_types'] 	= 'gif|jpg|png|jpeg';
			$config['file_name'] 		= $image_name;
			$this->upload->initialize($config);
			$this->upload->do_upload('image');
		}else{
			$image_name = $this->input->post('image');
		}
		$data = array(
			'name'		=>	$this->input->post('name'),
			'country'	=>	$this->input->post('country'),
			'flag'		=>	$flag,
			'image'		=>	$image_name,
			'status'	=>	$this->input->post('status'),
		);
		$this->T_player->update($data, $id);
		redirect('match/player/'.$match);
	}

	function playerdelete($match,$id) {
		$this->T_player->delete($id);
		redirect('match/player/'.$match);
	}

	function set($id) {
		$data['match']	=	$id;
		$data['set']	=	$this->T_set->getAllByMatch($id);
		$this->load->view('gen_cnf/match/setindex', $data);
	}

	function setadd($id) {
		$data['match']	=	$id;
		$this->load->view('gen_cnf/match/setadd', $data);
	}

	function setactionadd($id) {
		$data = array(
			'set'		=>	$this->input->post('set'),
			'match_id'	=>	$id,
			'winner'	=>	0,
			'created'	=>	date('Y-m-d H:i:s')
		);
		$this->T_set->add($data);

		$player 	=	$this->T_player->getAllByMatch($id);
		foreach($player as $player) {
			$array	= array(
				'match_id'	=>	$id,
				'set_id'	=>	$this->input->post('set'),
				'player_id'	=>	$player['id'],
				'score'		=>	0,
				'created'	=>	date('Y-m-d H:i:s')
			);
			$this->T_score->add($array);
		}
		redirect('match/set/'.$id);
	}

	function setedit($match,$id) {
		$data['match']	=	$match;
		$data['set']	=	$this->T_set->getById($id);
		$this->load->view('gen_cnf/match/setedit', $data);
	}

	function setactionedit($match,$id) {
		$data = array(
			'set'	=>	$this->input->post('set')
		);
		$this->T_set->update($data,$id);
		$this->T_score->deleteBySetMatch($this->input->post('set'), $match);
		$this->T_score->deleteBySetMatch($this->input->post('oldset'), $match);
		$player 	=	$this->T_player->getAllByMatch($match);
		foreach($player as $player) {
			$array	= array(
				'match_id'	=>	$match,
				'set_id'	=>	$this->input->post('set'),
				'player_id'	=>	$player['id'],
				'score'		=>	0,
				'created'	=>	date('Y-m-d H:i:s')
			);
			$this->T_score->add($array);
		}
		redirect('match/set/'.$match);
	}

	function setdelete($set,$match,$id) {
		$this->T_set->delete($id);
		$this->T_score->deleteBySetMatch($set, $match);
		redirect('match/set/'.$match);
	}

	function score($match,$set) {
		$data['match']	=	$match;
		$data['set']	=	$set;
		$data['player1']	=	$this->T_player->getAllByMatchPlayer($match,1);
		$data['player2']	=	$this->T_player->getAllByMatchPlayer($match,2);
		$data['score1']		=	$this->T_score->getByeSetMatch($set,$match,$data['player1']['id']);
		$data['score2']		=	$this->T_score->getByeSetMatch($set,$match,$data['player2']['id']);
		$this->load->view('gen_cnf/match/scoreedit', $data);
	}

	function updateScorePlayer($match,$set,$player) {
		$data = array(
			'score'		=>	$this->input->post('score')
		);
		$this->T_score->updateScorePlayer($match,$set,$player,$data);
	}

	function setActive($match,$set) {
		$nonactive = array(
			'status'	=>	0
		);
		$this->T_set->updateByMatch($nonactive,$match);
		$active = array(
			'status'	=>	1
		);
		$this->T_set->updateBySetMatch($active,$match,$set);
		redirect('match/set/'.$match);
	}


	function setWinner($match,$set) {
		$active = array(
			'winner'	=>	$this->input->post('winner')
		);
		$this->T_set->updateBySetMatch($active,$match,$set);
		echo json_encode("Oke");
	}

	function setServe($match,$player) {
		$neutral = array(
			'serve'		=>	0
		);
		$this->T_player->updateByMatch($match,$neutral);
		$serve  = array (
			'serve'		=>	1
		);
		$this->T_player->update($serve,$player);
	}

	function group() {
		$data['group']	=	$this->T_group_match->getAll();
		$this->load->view('gen_cnf/match/groupindex', $data);
	}

	function groupadd() {
		$this->load->view('gen_cnf/match/groupadd');
	}

	function groupactionadd() {
		$data = array(
			'name'		=>	$this->input->post('name'),
			'created'	=>	date('Y-m-d H:i:s')
		);
		$this->T_group_match->add($data);
		redirect('match/group');
	}

	function groupedit($id) {
		$data['id']		=	$id;
		$data['group']	=	$this->T_group_match->getById($id);
		$this->load->view('gen_cnf/match/groupedit', $data);
	}

	function groupactionedit($id) {
		$data = array(
			'name'		=>	$this->input->post('name')
		);
		$this->T_group_match->update($data, $id);
		redirect('match/group');
	}

	function groupdelete($id) {
		$this->T_group_match->delete($id);
		redirect('match/group');
	}


}