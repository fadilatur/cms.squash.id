<?php
class Choice extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('T_choice');
		$this->load->model('T_section');
		if(!$this->session->userdata('id_adm'))
			redirect('auth');
	}
	
	function index($polling=0,$id=0)
	{
		$datacontent['title']	= 'jadwal';
		$datacontent['url'] 	= "jadwal/";
		$datacontent['polling'] 	= $polling;
		if($id==0){
			if(!empty($_POST)){
				$data = array(
				'content'			=> $this->input->post('choice'),
				'polling_id'		=> $this->input->post('polling'),
				);
			$this->T_choice->add($data);

			}
		}else{

			$datacontent['detail'] 	= $this->T_choice->get($id);
			//echo "<pre>";
			//print_r($datacontent['detail']);
			//echo "</pre>";
			if(!empty($_POST)){
				$data = array(
				'content'			=> $this->input->post('choice')
				);
				$this->T_choice->update($id,$data);

			}
		}
		$datacontent['data'] 	= $this->T_choice->select($polling);
		//echo "<pre>";
		//print_r($datacontent['data']);
		//echo "</pre>";
		$this->load->view('gen_cnf/choice/index',$datacontent);
	}
	
	

	function actiondelete($polling=0,$id=0)
	{
		$this->T_choice->delete($id);
		redirect('choice/index/'.$polling);  
	}
		
}

