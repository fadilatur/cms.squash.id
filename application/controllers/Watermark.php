<?php
/**
* 
*/
class Watermark extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('T_watermark');
	}

	function index()
	{
		$data['watermark']	=	$this->T_watermark->get();
		$this->load->view('gen_cnf/watermark/index', $data);
	}

	function add()
	{
		$this->load->view('gen_cnf/watermark/add');
	}

	function addwatermark()
	{
		if(!empty($_FILES['image']['name'])){
			$image_name =  str_replace(' ','_',date('Ymdhis').$_FILES['image']['name']);
			$config['upload_path'] 		= $this->config->item('watermark');
			$config['allowed_types'] 	= 'gif|jpg|png|jpeg';
			$config['file_name'] 		= $image_name;
			//$config['min_width'] = '800';
			//$config['min_height'] = '450';
			$this->upload->initialize($config);
			$this->upload->do_upload('image');
		}else{
			$image_name = $this->input->post('image');
		}

		$data = array(
			'judul'			=>	$this->input->post('judul'),
			'keterangan'	=>	$this->input->post('keterangan'),
			'gambar'		=>	$image_name,
			'publish'		=>	'Y');
		$this->T_watermark->add($data);
		redirect('watermark');
	}

	function edit($id)
	{
		$data['watermark']	=	$this->T_watermark->getid($id);
		$this->load->view('gen_cnf/watermark/edit', $data);
	}

	function editwatermark()
	{
		if(!empty($_FILES['image']['name'])){
			$image_name =  str_replace(' ','_',date('Ymdhis').$_FILES['image']['name']);
			$config['upload_path'] 		= $this->config->item('watermark');
			$config['allowed_types'] 	= 'gif|jpg|png|jpeg';
			$config['file_name'] 		= $image_name;
			//$config['min_width'] = '800';
			//$config['min_height'] = '450';
			$this->upload->initialize($config);
			$this->upload->do_upload('image');
		}else{
			$image_name = $this->input->post('image');
		}

		$data = array(
			'judul'			=>	$this->input->post('judul'),
			'keterangan'	=>	$this->input->post('keterangan'),
			'gambar'		=>	$image_name,);
		$this->T_watermark->edit($data, $this->input->post('id'));
		redirect('watermark');
	}

	function delete($id)
	{
		$this->T_watermark->delete($id);
		redirect('watermark');
	}
}