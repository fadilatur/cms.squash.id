<?php
class Subscriber extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('newsletter');
		if(!$this->session->userdata('id_adm'))
			redirect('auth');
	}

	function index()
	{
		$datacontent['data'] 	= $this->newsletter->select();
		$this->load->view('gen_cnf/subscriber/index',$datacontent);
	}
	
	function actiondelete($id)
	{
		$this->newsletter->delete($id);
		redirect('subscriber');  
	}
}