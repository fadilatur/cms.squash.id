<?php
class Informasi extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('T_informasi');
		$this->load->model('T_section');
		if(!$this->session->userdata('id_adm'))
			redirect('auth');
	}
	
	function index($id)
	{
		$datacontent['title']	= 'informasi';
		$datacontent['url'] 	= "informasi/index";
		$datacontent['action'] 		= 'index';
		$this->load->helper('form');
		$datacontent['user'] 		= $this->T_informasi->get_informasi($id);
		$datacontent['daerah']	= 	$id;
		//echo "<pre>";
		//print_r($datacontent['user']);
		//echo "</pre>";
		
		$this->load->view('gen_cnf/informasi/index',$datacontent);
	}
	
	function actionindex()
	{
		if(!empty($_FILES['image']['name'])){
				$image_name =  str_replace(' ','_',date('Ymdhis').$_FILES['image']['name']);
				$config['upload_path'] 		= $this->config->item('upload_images');
				$config['allowed_types'] 	= 'gif|jpg|png|jpeg';
				$config['file_name'] 		= $image_name;
				$this->upload->initialize($config);
				$this->upload->do_upload('image');
			}else{
				$image_name = $this->input->post('imagex');
			}
	
		$datacontent = array(
			'penerbit'    		 	=> $this->input->post('penerbit'),
			'telepon'     			=> $this->input->post('telepon'),
			'fax'     				=> $this->input->post('fax'),
			'email'    	  		  	=> $this->input->post('email'),
			'alamat'    	  	  	=> $this->input->post('alamat'),
			'facebook'    	  	  	=> $this->input->post('fb'),
			'instagram'    	  	  	=> $this->input->post('ig'),
			'youtube'    	  	  	=> $this->input->post('youtube'),
			'gplus'	    	  	  	=> $this->input->post('gplus'),
			'twitter'    	  	  	=> $this->input->post('tw'),
			'daerah'				=> $this->input->post('parent_id'),
			'images'    	  	  	=> $image_name
		);
		$this->T_informasi->update($this->input->post('id'),$datacontent);
		
		redirect('informasi/index/'.$this->input->post('id')); 
	}
	
	function actiondelete($id)
	{
	
		//log admin
				$activity = "Delete";
				$table = "user";
				$last = $id;
				include_once dirname(__FILE__).'/log.php' ;
		//end log admin
		$this->model_administrator->delete($id);
		redirect('gen_cnf/administrator/');  
	}
	
	function toexcel(){
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="Data-Administrator-'.date("Y-m-d/H-i-s").'.xls"');
        $data["rows"] = $this->model_administrator->select();
        $this->load->view("gen_cnf/administrator/xls", $data);
    }

}
