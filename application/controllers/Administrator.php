<?php
class Administrator extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('T_admin');
		$this->load->model('T_level');
		$this->load->model('T_section');
		//$this->load->model('model_typeuser');
		//$this->load->model('model_group');
		if(!$this->session->userdata('id_adm'))
			redirect('auth');
	}
	
	function index($daerah)
	{
		$datacontent['title']	= 'administrator';
		$datacontent['url'] = "administrator/index";
		$datacontent['data'] 	= $this->T_admin->select($daerah);
		$datacontent['daerah']	= $daerah;
		//echo "<pre>";
		//print_r($datacontent['data']);
		//echo "</pre>";

		$this->load->view('gen_cnf/administrator/index',$datacontent);
	}
	
	function add($daerah)
	{
		$this->output->cache(60);
		$datacontent['title']	= 'administrator';
		$datacontent['url'] = "administrator/index";
		$this->load->helper('form');
		$datacontent['action'] 	= 'add';
		$datacontent['group'] 	= $this->T_level->select();
		$datacontent['daerah']	= $daerah;
		
		//echo "<pre>";
		//print_r($datacontent['group']);
		//echo "</pre>";
		$this->load->view('gen_cnf/administrator/add',$datacontent);
	}
	
	function actionadd()
	{
		$email = $this->input->post('email');
		$cekemail = $this->T_admin->authenticate($email);
		if (!empty($cekemail)) {
			echo 'email sudah tersedia, silahkan menggunakan email lain';
		}else{
			if(!empty($_FILES['image']['name'])){
				$image_name =  str_replace(' ','_',date('Ymdhis').$_FILES['image']['name']);
				$config['upload_path'] 		= $this->config->item('upload_images')."mimin/";
				$config['allowed_types'] 	= 'gif|jpg|png|jpeg';
				$config['file_name'] 		= $image_name;
				$this->upload->initialize($config);
				$this->upload->do_upload('image');
			}else{
				$image_name = '';
			}
		
		$password = md5($this->input->post('password'));
		$data = array(
			'level'    	  	  	=> $this->input->post('group_id'),
			'nama'	    		=> $this->input->post('name'),
			'email'     		=> $this->input->post('email'),
			'aktif'				=> 'Y',
			'des_admin'     	=> $this->input->post('deskripsi'),
			'password'     		=> $password,
			'foto_admin'		=> $image_name,
			'daerah'			=> $this->input->post('daerah'),
			'url_admin'			=> url_title($this->input->post('name'))
			
		);
		
		$this->T_admin->add($data);
		
		
				
		redirect('administrator/index/'.$this->input->post('daerah'));
		}
	}
	
	function cpassword($id)
	{
		$datacontent['title']	= 'administrator';
		$datacontent['url'] = "administrator/index";
		$this->load->helper('form');
		$datacontent['action'] 		= 'cpassword';
		$datacontent['data'] 		= $this->T_admin->get_tadmin($id);
		$this->load->view('gen_cnf/administrator/cpassword',$datacontent);
	}
	
	function actioncpassword()
	{
		$password = md5($this->input->post('password'));
		$data = array(
			'password'     		=> $password,
		);			
		$this->T_admin->update($this->input->post('id'),$data);
		//$this->db->cache_delete('gen_cnf', 'administrator');
		redirect('administrator/'); 
	}
	
	
	function edit($id)
	{
		$datacontent['title']	= 'administrator';
		$datacontent['url'] = "administrator/index";
		$this->load->helper('form');
		$datacontent['action'] 		= 'edit';
		
		if($this->session->userdata('level') == 1) 
		{
			$group = array('1','2','3');
			$datacontent['group'] 		= $this->T_level->selectin($group);
		}
		else
		{
			$datacontent['group'] 		= $this->T_level->select();
		}
		
		//$datacontent['group'] 		= $this->T_level->select();
		$datacontent['data'] 		= $this->T_admin->get_tadmin($id);
		//echo "<pre>";
		//print_r($datacontent['data']);
		//echo "</pre>";

		$this->load->view('gen_cnf/administrator/edit',$datacontent);
	}	
	
	/*
	function actionedit()
	{
	if(!empty($this->input->post('password')) || !empty($this->input->post('repassword')))
	{
		if($this->input->post('repassword') != $this->input->post('password'))
		{
			$this->session->set_flashdata('warning', 'Kata sandi tidak otentik, pastikan anda mengisi kolom Password dan Re-type Password dengan benar.');
			redirect('administrator/edit/'.$this->input->post('id'));
		}
		else
		{
			if(!empty($_FILES['image']['name'])){
				$image_name =  str_replace(' ','_',date('Ymdhis').$_FILES['image']['name']);
				$config['upload_path'] 		= $this->config->item('upload_images')."mimin/";
				$config['allowed_types'] 	= 'gif|jpg|png';
				$config['file_name'] 		= $image_name;
				$this->upload->initialize($config);
				$this->upload->do_upload('image');
			}else{
				$image_name = $this->input->post('imagex');
			}
			
			$password = md5($this->input->post('password'));
			$data = array(
				'level'    	  	  	=> $this->input->post('group_id'),
				'nama'	    		=> $this->input->post('name'),
				'email'     		=> $this->input->post('email'),
				'des_admin'     	=> $this->input->post('deskripsi'),
				'password'     		=> $password,
				'foto_admin'		=> $image_name,
				
			);
			$this->T_admin->update($this->input->post('id'),$data);
			
			redirect('administrator'); 
		}
	}
	else
	{
		if(!empty($_FILES['image']['name'])){
				$image_name =  str_replace(' ','_',date('Ymdhis').$_FILES['image']['name']);
				$config['upload_path'] 		= $this->config->item('upload_images')."mimin/";
				$config['allowed_types'] 	= 'gif|jpg|png';
				$config['file_name'] 		= $image_name;
				$this->upload->initialize($config);
				$this->upload->do_upload('image');
			}else{
				$image_name = $this->input->post('imagex');
			}
	
		$data = array(
			'level'    	  	  	=> $this->input->post('group_id'),
			'nama'	    		=> $this->input->post('name'),
			'email'     		=> $this->input->post('email'),
			'des_admin'     	=> $this->input->post('deskripsi'),
			'foto_admin'		=> $image_name,
			
		);
		$this->T_admin->update($this->input->post('id'),$data);
		
		redirect('administrator'); 
	}
	
	}
	*/

	function actionedit()
	{
		if(!empty($_FILES['image']['name'])){
				$image_name =  str_replace(' ','_',date('Ymdhis').$_FILES['image']['name']);
				$config['upload_path'] 		= $this->config->item('upload_images')."mimin/";
				$config['allowed_types'] 	= 'gif|jpg|png|jpeg';
				$config['file_name'] 		= $image_name;
				$this->upload->initialize($config);
				$this->upload->do_upload('image');
			}else{
				$image_name = $this->input->post('imagex');
			}
			
			$password = md5($this->input->post('password'));

			if(!empty($this->input->post('password')))
			{
				$data = array(
				'level'    	  	  	=> $this->input->post('group_id'),
				'nama'	    		=> $this->input->post('name'),
				'email'     		=> $this->input->post('email'),
				'des_admin'     	=> $this->input->post('deskripsi'),
				'password'     		=> $password,
				'foto_admin'		=> $image_name,
				'url_admin'			=> url_title($this->input->post('name'))
				
				);	
			}
			else
			{
				$data = array(
				'level'    	  	  	=> $this->input->post('group_id'),
				'nama'	    		=> $this->input->post('name'),
				'email'     		=> $this->input->post('email'),
				'des_admin'     	=> $this->input->post('deskripsi'),
				'foto_admin'		=> $image_name,
				'url_admin'			=> url_title($this->input->post('name'))
				
				);	
			}
			
			$this->T_admin->update($this->input->post('id'),$data);
			redirect('administrator/edit/'.$this->input->post('id')); 

	}


	
	
	function actiondelete($id,$daerah)
	{
	
		
		$this->T_admin->delete($id);
		redirect('administrator/index/'.$daerah);  
	}
	
	
	function toexcel(){
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="Data-Administrator-'.date("Y-m-d/H-i-s").'.xls"');
        $data["rows"] = $this->model_administrator->select();
        $this->load->view("gen_cnf/administrator/xls", $data);
    }

}
