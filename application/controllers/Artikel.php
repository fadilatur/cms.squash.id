<?php
class Artikel extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('T_artikel');
		$this->load->model('T_admin');
		$this->load->model('T_section');
		$this->load->model('T_supsection');
		$this->load->model('T_relasi');
		$this->load->model('T_tag');
		date_default_timezone_set('Asia/Jakarta');
		if(!$this->session->userdata('id_adm'))
			redirect('auth');
	}
	
	function index($id=0,$pages=0)
	{

		//echo date('Y-m-d H:i:s');
		$datacontent['title']	= 'artikel';
		$viewperpage 	= 10;
		$datacontent['parent_id'] 	= $id;

		$datacontent['daerah']	= $id;
		$datacontent['tag'] 	= $this->T_tag->select();

		//cek session as GET

/*		

		if(empty($this->input->get('daricheck')))
		{
			if(empty($this->session->userdata('dari')))
			{
				$datacontent['dari'] = date('Y-m-d', strtotime("-7 day", strtotime(date("Y-m-d"))));
			}
			else
			{
				$datacontent['dari'] = $this->session->userdata('dari');
			}
		}
		else
		{
			$datacontent['dari'] = $this->input->get('dari');
		}

		if(empty($this->input->get('sampaicheck')))
		{
			if(empty($this->session->userdata('sampai')))
			{
				$datacontent['sampai'] = date('Y-m-d');
			}
			else
			{
				$datacontent['sampai'] = $this->session->userdata('sampai');
			}
		}
		else
		{
			$datacontent['sampai'] = $this->input->get('sampai');
		}

		if(empty($this->input->get('statuscheck')))
		{
			if(empty($this->session->userdata('status')))
			{
				$datacontent['status'] = 'Y';
			}
			else
			{
				$datacontent['status'] = $this->session->userdata('status');
			}
		}
		else
		{
			$datacontent['status'] = $this->input->get('status');
		}
*/
		/*
		if(empty($this->input->get('judulcheck')))
		{
			if(empty($this->session->userdata('judul')))
			{
				$datacontent['judul'] = '';
			}
			else
			{
				$datacontent['judul'] = $this->session->userdata('judul');
			}
		}
		else
		{
			$datacontent['judul'] = $this->input->get('judul');
			$this->session->set_userdata('judul', $datacontent['judul']);
		}

		if(empty($this->input->get('categorycheck')))
		{
			if(empty($this->session->userdata('category')))
			{
				$datacontent['category'] = '';
			}
			else
			{
				$datacontent['category'] = $this->session->userdata('category');
			}
		}
		else
		{
			$datacontent['category'] = $this->input->get('category');
			$this->session->set_userdata('category', $datacontent['category']);
		}

		if(empty($this->input->get('admincheck')))
		{
			if(empty($this->session->userdata('admin')))
			{
				$datacontent['admin'] = '';
			}
			else
			{
				$datacontent['admin'] = $this->session->userdata('admin');
			}
		}
		else
		{
			$datacontent['admin'] = $this->input->get('admin');
			$this->session->set_userdata('admin', $datacontent['admin']);
		}
		*/
	


/*
		$this->session->set_userdata('dari', $datacontent['dari']);
		$this->session->set_userdata('sampai', $datacontent['sampai']);
		$this->session->set_userdata('status', $datacontent['status']);


if(!empty($this->input->get('judulcheck')) || !empty($this->input->get('categorycheck')) || !empty($this->input->get('admincheck')) || 
	!empty($this->input->get('dari')) || !empty($this->input->get('sampai')) || !empty($this->input->get('status')))
{
	if(!empty($this->input->get('judulcheck')) && !empty($this->input->get('categorycheck')) && !empty($this->input->get('admincheck')))
	{
		$this->session->set_userdata('judul', $this->input->get('judul'));
		$this->session->set_userdata('category', $this->input->get('category'));
		$this->session->set_userdata('admin', $this->input->get('admin'));

		$datacontent['data'] 	= 	$this->T_artikel->selectlengkap($this->session->userdata('category'), $this->session->userdata('admin'),
									$this->session->userdata('judul'), $pages, $viewperpage, $this->session->userdata('dari'), 
									$this->session->userdata('sampai'), $this->session->userdata('status'));

		$datacontent['count'] 	=  	$this->T_artikel->countlengkap($this->session->userdata('category'), $this->session->userdata('admin'),
									$this->session->userdata('judul'),  $this->session->userdata('dari'), $this->session->userdata('sampai'), 
									$this->session->userdata('status'));
	}
	elseif(!empty($this->input->get('judulcheck')) && !empty($this->input->get('categorycheck')))
	{
		$this->session->unset_userdata('admin');
		$this->session->set_userdata('judul', $this->input->get('judul'));
		$this->session->set_userdata('category', $this->input->get('category'));

		if($this->session->userdata('level') == 3)
		{
			$datacontent['data']	=	$this->T_artikel->selectlengkap2author($this->session->userdata('category'), 
										$this->session->userdata('judul'), $pages, $viewperpage, $this->session->userdata('dari'), 
										$this->session->userdata('sampai'), $this->session->userdata('status'), 
										$this->session->userdata('id_adm'));

			$datacontent['count'] 	=  	$this->T_artikel->countlengkap2author($this->session->userdata('category'), 
										$this->session->userdata('judul'), $this->session->userdata('dari'), $this->session->userdata('sampai'), 
										$this->session->userdata('status'), $this->session->userdata('id_adm'));
		}
		else
		{
			$datacontent['data'] 	= 	$this->T_artikel->selectlengkap2($this->session->userdata('category'), $this->session->userdata('judul'),
										$pages, $viewperpage, $this->session->userdata('dari'), $this->session->userdata('sampai'), 
										$this->session->userdata('status'));
			$datacontent['count'] 	=  	$this->T_artikel->countlengkap2($this->session->userdata('category'), $this->session->userdata('judul'), 
										$this->session->userdata('dari'), $this->session->userdata('sampai'), $this->session->userdata('status'));
		}
	}
	elseif(!empty($this->input->get('categorycheck')) && !empty($this->input->get('admincheck')))
	{
		$this->session->unset_userdata('judul');
		$this->session->set_userdata('category', $this->input->get('category'));
		$this->session->set_userdata('admin', $this->input->get('admin'));

		$datacontent['data']	= 	$this->T_artikel->selectlengkap1($this->session->userdata('category'), $this->session->userdata('admin'), 
									$pages, $viewperpage, $this->session->userdata('dari'), $this->session->userdata('sampai'), 
									$this->session->userdata('status'));
		$datacontent['count'] 	=  	$this->T_artikel->countlengkap1($this->session->userdata('category'), $this->session->userdata('admin'), 
									$this->session->userdata('dari'), $this->session->userdata('sampai'), $this->session->userdata('status'));
	}
	elseif(!empty($this->input->get('admincheck')) && !empty($this->input->get('judulcheck')))
	{
		$this->session->unset_userdata('category');
		$this->session->set_userdata('admin', $this->input->get('admin'));
		$this->session->set_userdata('judul', $this->input->get('judul'));

		$datacontent['data'] 	= 	$this->T_artikel->selectlengkap3($this->session->userdata('admin'), $this->session->userdata('judul'), 
									$pages, $viewperpage, $this->session->userdata('dari'), $this->session->userdata('sampai'), 
									$this->session->userdata('status'));
		$datacontent['count'] 	=  	$this->T_artikel->countlengkap3($this->session->userdata('admin'), $this->session->userdata('judul'), 
									$this->session->userdata('dari'), $this->session->userdata('sampai'), $this->session->userdata('status'));

	}
	elseif(!empty($this->input->get('judulcheck')))
	{
		if($this->session->userdata('level') == 3)
		{
			$this->session->unset_userdata('category');
			$this->session->unset_userdata('admin');
			$this->session->set_userdata('judul', $this->input->get('judul'));
			$datacontent['data']	= 	$this->T_artikel->selectjudulauthor($this->session->userdata('judul'), $pages, $viewperpage, 
										$this->session->userdata('dari'), $this->session->userdata('sampai'), $this->session->userdata('status'),
										$this->session->userdata('id_adm'));
			$datacontent['count'] 	=  	$this->T_artikel->countjudulauthor($this->session->userdata('judul'), $this->session->userdata('dari'), 
										$this->session->userdata('sampai'), $this->session->userdata('status'), 
										$this->session->userdata('id_adm'));
		}
		else
		{
			$this->session->unset_userdata('category');
			$this->session->unset_userdata('admin');
			$this->session->set_userdata('judul', $this->input->get('judul'));

			$datacontent['data'] 	= 	$this->T_artikel->selectjudul($this->session->userdata('judul'), $pages, $viewperpage, 
										$this->session->userdata('dari'), $this->session->userdata('sampai'), 
										$this->session->userdata('status'));
			$datacontent['count'] 	=  	$this->T_artikel->countjudul($this->session->userdata('judul'), $this->session->userdata('dari'), 
										$this->session->userdata('sampai'), $this->session->userdata('status'));
		}
	}
	elseif(!empty($this->input->get('categorycheck')))
	{
		$this->session->unset_userdata('admin');
		$this->session->unset_userdata('judul');
		$this->session->set_userdata('category', $this->input->get('category'));
		if($this->session->userdata('level') == 3)
		{
			$datacontent['data']	= 	$this->T_artikel->selectsectionauthor($this->session->userdata('category'), $pages, $viewperpage, 
										$this->session->userdata('dari'), $this->session->userdata('sampai'), $this->session->userdata('status'),
										$this->session->userdata('id_adm'));
			$datacontent['count'] 	=  	$this->T_artikel->countsectionauthor($this->session->userdata('category'), 
										$this->session->userdata('dari'), $this->session->userdata('sampai'), $this->session->userdata('status'),
										$this->session->userdata('id_adm'));
		}
		else
		{
			$datacontent['data'] 	= 	$this->T_artikel->selectsection($this->session->userdata('category'), $pages, $viewperpage, 
										$this->session->userdata('dari'), $this->session->userdata('sampai'), $this->session->userdata('status'));
			$datacontent['count'] 	=  	$this->T_artikel->countsection($this->session->userdata('category'), $this->session->userdata('dari'), 
										$this->session->userdata('sampai'),$this->session->userdata('status'));
		}
	}
	elseif(!empty($this->input->get('admincheck')))
	{
		$this->session->unset_userdata('category');
		$this->session->unset_userdata('judul');
		$this->session->set_userdata('admin', $this->input->get('admin'));

		$datacontent['data']	= 	$this->T_artikel->selectbyadmin($this->session->userdata('admin'), $pages, $viewperpage, 
									$this->session->userdata('dari'), $this->session->userdata('sampai'), $this->session->userdata('status'));
		$datacontent['count'] 	=  	$this->T_artikel->countbyadmin($this->session->userdata('admin'), $this->session->userdata('dari'), 
									$this->session->userdata('sampai'), $this->session->userdata('status'));
	}
	else
	{
		$this->session->unset_userdata('category');
		$this->session->unset_userdata('admin');
		$this->session->unset_userdata('judul');
		if($this->session->userdata('level') == 3)
		{
			$datacontent['data'] 	= 	$this->T_artikel->selectstatusauthor($pages, $viewperpage, $this->session->userdata('dari'), 
										$this->session->userdata('sampai'), $this->session->userdata('status'), 
										$this->session->userdata('id_adm'));
			$datacontent['count']	=  	$this->T_artikel->countstatusauthor($this->session->userdata('dari'), $this->session->userdata('sampai'),
										$this->session->userdata('status'), $this->session->userdata('id_adm'));
		}
		else
		{
			$datacontent['data'] 	= 	$this->T_artikel->selectstatus($pages, $viewperpage, $this->session->userdata('dari'), 
										$this->session->userdata('sampai'), $this->session->userdata('status'));
			$datacontent['count'] 	=  	$this->T_artikel->countstatus($this->session->userdata('dari'), $this->session->userdata('sampai'), 
										$this->session->userdata('status'));
		}
	}
}
else
{
	if(!empty($this->session->userdata('judul')) && !empty($this->session->userdata('category')) && !empty($this->session->userdata('admin')))
	{
		$datacontent['data'] 	= 	$this->T_artikel->selectlengkap($this->session->userdata('category'), $this->session->userdata('admin'),
									$this->session->userdata('judul'), $pages, $viewperpage, $this->session->userdata('dari'), 
									$this->session->userdata('sampai'), $this->session->userdata('status'));

		$datacontent['count'] 	=  	$this->T_artikel->countlengkap($this->session->userdata('category'), $this->session->userdata('admin'),
									$this->session->userdata('judul'),  $this->session->userdata('dari'), $this->session->userdata('sampai'), 
									$this->session->userdata('status'));
	}
	elseif(!empty($this->session->userdata('judul')) && !empty($this->session->userdata('category')))
	{
		$this->session->unset_userdata('admin');

		if($this->session->userdata('level') == 3)
		{
			$datacontent['data']	=	$this->T_artikel->selectlengkap2author($this->session->userdata('category'), 
										$this->session->userdata('judul'), $pages, $viewperpage, $this->session->userdata('dari'), 
										$this->session->userdata('sampai'), $this->session->userdata('status'), 
										$this->session->userdata('id_adm'));

			$datacontent['count'] 	=  	$this->T_artikel->countlengkap2author($this->session->userdata('category'), 
										$this->session->userdata('judul'), $this->session->userdata('dari'), $this->session->userdata('sampai'), 
										$this->session->userdata('status'), $this->session->userdata('id_adm'));
		}
		else
		{
			$datacontent['data'] 	= 	$this->T_artikel->selectlengkap2($this->session->userdata('category'), $this->session->userdata('judul'),
										$pages, $viewperpage, $this->session->userdata('dari'), $this->session->userdata('sampai'), 
										$this->session->userdata('status'));
			$datacontent['count'] 	=  	$this->T_artikel->countlengkap2($this->session->userdata('category'), $this->session->userdata('judul'), 
										$this->session->userdata('dari'), $this->session->userdata('sampai'), $this->session->userdata('status'));
		}
	}
	elseif(!empty($this->session->userdata('category')) && !empty($this->session->userdata('admin')))
	{
		$this->session->unset_userdata('judul');
		$this->session->set_userdata('category', $this->session->userdata('category'));
		$this->session->set_userdata('admin', $this->session->userdata('admin'));

		$datacontent['data']	= 	$this->T_artikel->selectlengkap1($this->session->userdata('category'), $this->session->userdata('admin'), 
									$pages, $viewperpage, $this->session->userdata('dari'), $this->session->userdata('sampai'), 
									$this->session->userdata('status'));
		$datacontent['count'] 	=  	$this->T_artikel->countlengkap1($this->session->userdata('category'), $this->session->userdata('admin'), 
									$this->session->userdata('dari'), $this->session->userdata('sampai'), $this->session->userdata('status'));
	}
	elseif(!empty($this->session->userdata('admin')) && !empty($this->session->userdata('judul')))
	{
		$this->session->unset_userdata('category');
		$this->session->set_userdata('admin', $this->session->userdata('admin'));
		$this->session->set_userdata('judul', $this->session->userdata('judul'));

		$datacontent['data'] 	= 	$this->T_artikel->selectlengkap3($this->session->userdata('admin'), $this->session->userdata('judul'), 
									$pages, $viewperpage, $this->session->userdata('dari'), $this->session->userdata('sampai'), 
									$this->session->userdata('status'));
		$datacontent['count'] 	=  	$this->T_artikel->countlengkap3($this->session->userdata('admin'), $this->session->userdata('judul'), 
									$this->session->userdata('dari'), $this->session->userdata('sampai'), $this->session->userdata('status'));

	}
	elseif(!empty($this->session->userdata('judul')))
	{
		if($this->session->userdata('level') == 3)
		{
			$this->session->unset_userdata('category');
			$this->session->unset_userdata('admin');
			$datacontent['data']	= 	$this->T_artikel->selectjudulauthor($this->session->userdata('judul'), $pages, $viewperpage, 
										$this->session->userdata('dari'), $this->session->userdata('sampai'), $this->session->userdata('status'),
										$this->session->userdata('id_adm'));
			$datacontent['count'] 	=  	$this->T_artikel->countjudulauthor($this->session->userdata('judul'), $this->session->userdata('dari'), 
										$this->session->userdata('sampai'), $this->session->userdata('status'), 
										$this->session->userdata('id_adm'));
		}
		else
		{
			$this->session->unset_userdata('category');
			$this->session->unset_userdata('admin');
			$datacontent['data'] 	= 	$this->T_artikel->selectjudul($this->session->userdata('judul'), $pages, $viewperpage, 
										$this->session->userdata('dari'), $this->session->userdata('sampai'), 
										$this->session->userdata('status'));
			$datacontent['count'] 	=  	$this->T_artikel->countjudul($this->session->userdata('judul'), $this->session->userdata('dari'), 
										$this->session->userdata('sampai'), $this->session->userdata('status'));
		}
	}
	elseif(!empty($this->session->userdata('category')))
	{
		$this->session->unset_userdata('admin');
		$this->session->unset_userdata('judul');
		$this->session->set_userdata('category', $this->session->userdata('category'));
		if($this->session->userdata('level') == 3)
		{
			$datacontent['data']	= 	$this->T_artikel->selectsectionauthor($this->session->userdata('category'), $pages, $viewperpage, 
										$this->session->userdata('dari'), $this->session->userdata('sampai'), $this->session->userdata('status'),
										$this->session->userdata('id_adm'));
			$datacontent['count'] 	=  	$this->T_artikel->countsectionauthor($this->session->userdata('category'), 
										$this->session->userdata('dari'), $this->session->userdata('sampai'), $this->session->userdata('status'),
										$this->session->userdata('id_adm'));
		}
		else
		{
			$datacontent['data'] 	= 	$this->T_artikel->selectsection($this->session->userdata('category'), $pages, $viewperpage, 
										$this->session->userdata('dari'), $this->session->userdata('sampai'), $this->session->userdata('status'));
			$datacontent['count'] 	=  	$this->T_artikel->countsection($this->session->userdata('category'), $this->session->userdata('dari'), 
										$this->session->userdata('sampai'),$this->session->userdata('status'));
		}
	}
	elseif(!empty($this->session->userdata('admin')))
	{
		$this->session->unset_userdata('category');
		$this->session->unset_userdata('judul');
		$this->session->set_userdata('admin', $this->session->userdata('admin'));

		$datacontent['data']	= 	$this->T_artikel->selectbyadmin($this->session->userdata('admin'), $pages, $viewperpage, 
									$this->session->userdata('dari'), $this->session->userdata('sampai'), $this->session->userdata('status'));
		$datacontent['count'] 	=  	$this->T_artikel->countbyadmin($this->session->userdata('admin'), $this->session->userdata('dari'), 
									$this->session->userdata('sampai'), $this->session->userdata('status'));
	}
	else
	{
		$this->session->unset_userdata('category');
		$this->session->unset_userdata('admin');
		$this->session->unset_userdata('judul');
		if(!empty($datacontent['parent_id']))
		{
			if($this->session->userdata('level') == 3)
			{
				$datacontent['data'] 	= 	$this->T_artikel->selectstatusauthorparent($datacontent['parent_id'], $pages, $viewperpage, $this->session->userdata('dari'), 
											$this->session->userdata('sampai'), $this->session->userdata('status'), 
											$this->session->userdata('id_adm'));
				$datacontent['count']	=  	$this->T_artikel->countstatusauthorparent($datacontent['parent_id'], $this->session->userdata('dari'), $this->session->userdata('sampai'),
											$this->session->userdata('status'), $this->session->userdata('id_adm'));
			}
			else
			{
				$datacontent['data'] 	= 	$this->T_artikel->selectstatusparent($datacontent['parent_id'], $pages, $viewperpage, $this->session->userdata('dari'), 
											$this->session->userdata('sampai'), $this->session->userdata('status'));
				$datacontent['count'] 	=  	$this->T_artikel->countstatusparent($datacontent['parent_id'], $this->session->userdata('dari'), $this->session->userdata('sampai'), 
											$this->session->userdata('status'));
			}
		}
		else
		{
			if($this->session->userdata('level') == 3)
			{
				$datacontent['data'] 	= 	$this->T_artikel->selectstatusauthor($pages, $viewperpage, $this->session->userdata('dari'), 
											$this->session->userdata('sampai'), $this->session->userdata('status'), 
											$this->session->userdata('id_adm'));
				$datacontent['count']	=  	$this->T_artikel->countstatusauthor($this->session->userdata('dari'), $this->session->userdata('sampai'),
											$this->session->userdata('status'), $this->session->userdata('id_adm'));
			}
			else
			{
				$datacontent['data'] 	= 	$this->T_artikel->selectstatus($pages, $viewperpage, $this->session->userdata('dari'), 
											$this->session->userdata('sampai'), $this->session->userdata('status'));
				$datacontent['count'] 	=  	$this->T_artikel->countstatus($this->session->userdata('dari'), $this->session->userdata('sampai'), 
											$this->session->userdata('status'));
			}
		}
	}
}		


*/

		$datacontent['search'] = $this->input->post('search');
		$datacontent['status'] = $this->input->post('status');
		
		if($this->session->userdata('level') == 3)
		{

			if(!empty($this->input->post('search') || !empty($this->input->post('status'))))
			{	

				$datacontent['data'] 	= 	$this->T_artikel->selectstatusauthor2search($id,$pages, $viewperpage, 
											$this->session->userdata('id_adm'), $this->input->post('search'), $this->input->post('status'));

				$datacontent['count']	=  	$this->T_artikel->countstatusauthor2search($id,$this->session->userdata('id_adm'), 
											$this->input->post('search'), $this->input->post('status'));
			}
			else
			{

				$datacontent['data'] 	= 	$this->T_artikel->selectstatusauthor2($id, $pages, $viewperpage, $this->session->userdata('id_adm'));
				$datacontent['count']	=  	$this->T_artikel->countstatusauthor2($id, $this->session->userdata('id_adm'));
			}
		}
		else
		{
			
			if(!empty($this->input->post('search') || !empty($this->input->post('status'))))
			{	
				$datacontent['data'] 	= 	$this->T_artikel->selectjudul2($id, $this->input->post('search'), $this->input->post('status'), 
											$pages, $viewperpage);
				$datacontent['count'] 	=  	$this->T_artikel->countjudul2($id, $this->input->post('search'), $this->input->post('status'));
			}
			else
			{
				$datacontent['data'] 	= 	$this->T_artikel->selectstatus2($id, $pages, $viewperpage);
				$datacontent['count'] 	=  	$this->T_artikel->countstatus2($id);
			}
		}
		

		 $datacontent['pages'] = (!empty($pages))?$pages:0;
		$this->load->library('pagination');
            $config['base_url'] 	= site_url('artikel/index/'.$id.'/');
            $config['total_rows'] 	= $datacontent['count'];
            $config['per_page'] 	= $viewperpage;
             $config['first_tag_open'] 	= '<div class="btn btn-info" type="button">';
            $config['first_tag_close'] 	= '</div>';
            $config['last_tag_open'] 	= '<div class="btn btn-info" type="button">';
            $config['last_tag_close'] 	= '</div>';
             $config['prev_tag_open'] 	= '<div class="btn btn-info" type="button">';
            $config['prev_tag_close'] 	= '</div>';
            $config['next_tag_open'] 	= '<div class="btn btn-info" type="button">';
            $config['next_tag_close'] 	= '</div>';
            $config['num_tag_open'] 	= '<div class="btn btn-info" type="button">';
            $config['num_tag_close'] 	= '</div>';
            $config['cur_tag_open'] 	= '<div class="btn btn-info active" type="button">';
            $config['cur_tag_close'] 	= '</div>';
			$this->pagination->initialize($config);
			$datacontent['tag'] 	= $this->T_tag->select();
			$datacontent['paging'] = $this->pagination->create_links();

			$datacontent['member'] 	= $this->T_admin->select();

		
		
		//echo "<pre>";
		//print_r($datacontent['data']);
		//echo "</pre>";
		$this->load->view('gen_cnf/artikel/index',$datacontent);
	}
	
	function add($id=0)
	{
		$this->output->cache(120);
		$datacontent['daerah'] 	= $id;
		$datacontent['tag'] 	= $this->T_tag->select();
		$datacontent['fokus']	= $this->T_artikel->fokus();
		$datacontent['member'] 	= $this->T_admin->select($id);
		$datacontent['editor']	= $this->T_admin->geteditor();
		//$datacontent['kolom']	= $this->T_artikel->kolom();
			$this->load->view('gen_cnf/artikel/add',$datacontent);
		
	}
	
	function actionadd()
	{
		
		if(!empty($_FILES)){

				//pengecekan folder ada apa tidak
				$uploaddir 		= $this->config->item('upload_images').'images/';
				$datename 		= date("Y"); 
			
				$tahun = $uploaddir.$datename;
				if(is_dir($tahun)){
				}else{
					mkdir($tahun, 0777, true);
					$fp = fopen($tahun.'/index.html', 'w');
					fwrite($fp , 'Bismillaah');
					fclose($fp);
				}


				$datename2 = date("m"); 
			
				$bulan = $uploaddir.$datename."/".$datename2;
				if(is_dir($bulan)){
				}else{
					mkdir($bulan, 0777, true);
					$fp = fopen($bulan.'/index.html', 'w');
					fwrite($fp , 'Bismillaah');
					fclose($fp);
				}


				$datename3 = date("d"); 
			
				$hari = $uploaddir.$datename."/".$datename2."/".$datename3;
				if(is_dir($hari)){
				}else{
					mkdir($hari, 0777, true);
					$fp = fopen($hari.'/index.html', 'w');
					fwrite($fp , 'Bismillaah');
					fclose($fp);
				}

			}

		
		
		if(!empty($_FILES['image']['name'])){
				$image_name =  str_replace(' ','_',date('Ymdhis').$_FILES['image']['name']);
				$config['upload_path'] 		= $hari ;
				$config['allowed_types'] 	= 'gif|jpg|png|jpeg';
				$config['file_name'] 		= $image_name;
				$config['min_width'] = '800';
				$config['min_height'] = '450';
				$this->upload->initialize($config);
				$this->upload->do_upload('image');
			}else{
				$image_name = $this->input->post('image');
			}

		if(!empty($_FILES['thumb']['name'])){
				$thumb_name =  str_replace(' ','_','thumb_'.date('Ymdhis').$_FILES['thumb']['name']);
				$config['upload_path'] 		= $hari ;
				$config['allowed_types'] 	= 'gif|jpg|png|jpeg';
				$config['file_name'] 		= $thumb_name;
				$this->upload->initialize($config);
				$this->upload->do_upload('thumb');
			}else{
				$thumb_name = '';
			}

		if(!empty($_FILES['slider']['name'])){
				$slider_name =  str_replace(' ','_','slider_'.date('Ymdhis').$_FILES['slider']['name']);
				$config['upload_path'] 		= $hari ;
				$config['allowed_types'] 	= 'gif|jpg|png|jpeg';
				$config['file_name'] 		= $slider_name;
				$config['min_width'] = '1900';
				$config['min_height'] = '800';
				$this->upload->initialize($config);
				$this->upload->do_upload('slider');
			}else{
				$slider_name = $this->input->post('slider');
			}
	
		$parent = $this->input->post('parent_id');

			if(!empty($this->input->post('editor')))
			{
				$editor = $this->input->post('editor');
			}
			else
			{
				$editor = '';
			}

			if(!empty($this->input->post('gagasan')))
			{
				$gagasan = $this->input->post('gagasan');
			}
			elseif(!empty($this->input->post('detail')))
			{
				$explode = explode('<p>', $this->input->post('detail'));
				$gagasan = $explode[1];
			}
			else
			{
				$gagasan = '';
			}

			if(!empty($this->input->post('mkey')))
			{
				$metakey = $this->input->post('mkey');
			}
			else
			{
				$metakey = $this->input->post('judul');
			}

			if(!empty($this->input->post('mtit')))
			{
				$metatit = $this->input->post('mtit');
			}
			else
			{
				$metatit = $this->input->post('judul');
			}


			$urltitle = url_title($this->input->post('judul'));
			$cek = $this->T_artikel->geturltitle($urltitle);
			
			if(!empty($cek))
			{
				$count = (int)count($cek) + (int)1;
				$judul = $this->input->post('judul').' '.$count;
			}
			else
			{
				$judul = $this->input->post('judul');
			}
		
		
			$data = array(
				'id_admin'    	  	  	=> $this->input->post('admin'),
				'judul_artikel'	    	=> $this->input->post('judul'),
				'isi_artikel'     		=> $this->input->post('detail'),
				'gagasan_utama'    	  	=> $gagasan,
				'kota'					=> $this->input->post('kota'),
				'tanggal'	    		=> $this->input->post('tgl'),
				'jam'     				=> $this->input->post('jam').":".$this->input->post('menit'),
				'tgl_pub'     			=> $this->input->post('tgl')." ".$this->input->post('jam').":".$this->input->post('menit').":00",
				'postdate'     			=> date('Y-m-d H:i:s'),
				'meta_key'    	  		=> $metakey,
				'meta_title'			=> $metatit,
				'meta_des'    	  	  	=> $this->input->post('mdesc'),
				'publish'	    		=> $this->input->post('publish'),
				'sponsored'				=> $this->input->post('sponsored'),
				'position'				=> $this->input->post('position'),
				'id_video'	    		=> $this->input->post('url'),
				'url'					=> $this->input->post('sumber'),
				'id_section'	    	=> $this->input->post('kategori'),
				'editor'				=> $editor,
				'thumbnail'     		=> $image_name,
				'thumbnail_watermark'	=>	$this->input->post('imagewatermark'),
				'ket_thumbnail'     	=> $this->input->post('caption'),
				'headline'     			=> 'N',
				'fokus'					=> $this->input->post('fokus'),
				//'kolom'					=> $this->input->post('kolom'),
				'urltitle'				=> url_title($judul),
				'daerah'				=> $parent
				
			);
		
			$this->T_artikel->add($data);
			$lastid = $this->db->insert_id();

			//insert to tbl relation

			$data2 = array(
				'id_object'    	  	  	=> $lastid,
				'id_object2'	    	=> $this->input->post('kategori'),
				'tipe'     				=> 'art_sec'
			);
		
			$this->T_relasi->add($data2);



		//input category    
					if(!empty($this->input->post('tag'))){
						$tag = $this->input->post('tag');
						//echo "<pre>";
						//print_r($tag);
						//die();
						$explode = explode(',', $this->input->post('tag'));	

						foreach($explode as $datatag=>$value){
							//search the tag open or not
							echo "ada value gak sih";
							echo $value;
							
							$cekcategori = $this->T_tag->cekcategory($value);
							//echo "<pre>";
							//print_r($cekcategori);
							//die();
							if(empty($cekcategori['id_tag'])){
								//kategori belum ada berarti insert kategori
								$datax = array(
									'id_supsection'  	=> 99,
									'nama_tag'    		=> $value,
									'seo_tag'     		=> url_title($value)
								);
								$this->T_tag->add($datax);
								$idcat = $this->db->insert_id();
								$kategoriID = $idcat;
							}else{
								//kategori sudah ada berarti tinggal masukkan ke assosiasi
								$kategoriID = $cekcategori['id_tag'];
							}
							
							//insert data to assosiate
							$data2 = array(
								'id_object'    	  	  	=> $lastid,
								'id_object2'	    	=> $kategoriID,
								'tipe'     				=> 'art_tag'
							);
							
							$this->T_relasi->add($data2);
						}
					}else{
						//insert data to assosiate
							$data2 = array(
								'id_object'    	  	  	=> $lastid,
								'id_object2'	    	=> 1,
								'tipe'     				=> 'art_tag'
							);
							
							$this->T_relasi->add($data2);
					}

			
				redirect('artikel/index/'.$this->input->post('parent_id')); 
		 
	}



	function edit($id=0,$parent=0)
	{
		$datacontent['data'] 	= $this->T_artikel->get($id);
		//get category
		$datacontent['catnya'] 	= $this->T_relasi->getobj2($datacontent['data']['id_artikel'],'art_sec');
		
		$datacontent['tag'] 	= $this->T_tag->select();
		$datacontent['tagnya'] 	= $this->T_relasi->getobj3($datacontent['data']['id_artikel'],'art_tag');
		$datacontent['fokus']	= $this->T_artikel->fokus($id);
		$datacontent['member'] 	= $this->T_admin->select($parent);

		$datacontent['editor']	= $this->T_admin->geteditor();
		//$datacontent['kolom']	= $this->T_artikel->kolom($id);
		//echo "<pre>";
		//print_r($datacontent['tagnya']);
		$datacontent['isi'][]="";
		foreach($datacontent['tagnya'] as $x){
			$datacontent['isi'][] = $x['id_object2'];
		}

		$datacontent['daerah']	= $parent;
			
			$this->load->view('gen_cnf/artikel/edit',$datacontent);
		
		
		
		
		
	}



	function actionedit()
	{
		

		if(!empty($_FILES)){

				//pengecekan folder ada apa tidak
				$uploaddir 		= $this->config->item('upload_images').'images/';
				

				$datapath = explode("/", $this->input->post('path'));

				$datename 		= $datapath[1]; 
			
				$tahun = $uploaddir.$datename;
				if(is_dir($tahun)){
				}else{
					mkdir($tahun, 0777, true);
					$fp = fopen($tahun.'/index.html', 'w');
					fwrite($fp , 'Bismillaah');
					fclose($fp);
				}


				$datename2 = $datapath[2]; 
			
				$bulan = $uploaddir.$datename."/".$datename2;
				if(is_dir($bulan)){
				}else{
					mkdir($bulan, 0777, true);
					$fp = fopen($bulan.'/index.html', 'w');
					fwrite($fp , 'Bismillaah');
					fclose($fp);
				}


				$datename3 = $datapath[3]; 
			
				$hari = $uploaddir.$datename."/".$datename2."/".$datename3;
				if(is_dir($hari)){
				}else{
					mkdir($hari, 0777, true);
					$fp = fopen($hari.'/index.html', 'w');
					fwrite($fp , 'Bismillaah');
					fclose($fp);
				}

			}


		if(!empty($_FILES['image']['name'])){
				$image_name =  str_replace(' ','_',date('Ymdhis').$_FILES['image']['name']);
				$config['upload_path'] 		= $hari;
				$config['allowed_types'] 	= 'gif|jpg|png|jpeg';
				$config['file_name'] 		= $image_name;
				$config['min_width'] = '800';
				$config['min_height'] = '450';
				$this->upload->initialize($config);
				$this->upload->do_upload('image');
			}else{
				$image_name = $this->input->post('image');
			}



		if(!empty($_FILES['thumb']['name'])){
				$thumb_name =  str_replace(' ','_','thumb_'.date('Ymdhis').$_FILES['thumb']['name']);
				$config['upload_path'] 		= $hari;
				$config['allowed_types'] 	= 'gif|jpg|png|jpeg';
				$config['file_name'] 		= $thumb_name;
				$this->upload->initialize($config);
				$this->upload->do_upload('thumb');
			}else{
				$thumb_name = $this->input->post('thumbx');
			}

		if(!empty($_FILES['slider']['name'])){
				$slider_name =  str_replace(' ','_','slider_'.date('Ymdhis').$_FILES['slider']['name']);
				$config['upload_path'] 		= $hari;
				$config['allowed_types'] 	= 'gif|jpg|png|jpeg';
				$config['file_name'] 		= $slider_name;
				$config['min_width'] = '1900';
				$config['min_height'] = '800';
				$this->upload->initialize($config);
				$this->upload->do_upload('slider');
			}else{
				$slider_name = $this->input->post('slider');
			}

		if(!empty($this->input->post('editor')))
		{
			$editor = $this->input->post('editor');
		}
		else
		{
			$editor = '';
		}

		if(!empty($this->input->post('gagasan')))
		{
			$gagasan = $this->input->post('gagasan');
		}
		elseif(!empty($this->input->post('detail')))
		{
			$explode = explode('<p>', $this->input->post('detail'));
			$gagasan = $explode[1];
		}
		else
		{
			$gagasan = '';
		}

		if(!empty($this->input->post('mkey')))
		{
			$metakey = $this->input->post('mkey');
		}
		else
		{
			$metakey = $this->input->post('judul');
		}

		if(!empty($this->input->post('mtit')))
		{
			$metatit = $this->input->post('mtit');
		}
		else
		{
			$metatit = $this->input->post('judul');
		}

	
	$parent = $this->input->post('parent_id');
	
	
		
		$data = array(
			'id_admin'    	  	  	=> $this->input->post('admin'),
			'judul_artikel'	    	=> $this->input->post('judul'),
			'isi_artikel'     		=> $this->input->post('detail'),
			'gagasan_utama'    	  	=> $gagasan,
			'kota'					=> $this->input->post('kota'),
			'tanggal'	    		=> $this->input->post('tgl'),
			'jam'     				=> $this->input->post('jam').":".$this->input->post('menit'),
			'tgl_pub'     			=> $this->input->post('tgl')." ".$this->input->post('jam').":".$this->input->post('menit').":00",
			'meta_key'    	  		=> $metakey,
			'meta_title'			=> $metatit,
			'meta_des'    	  	  	=> $this->input->post('mdesc'),
			'publish'	    		=> $this->input->post('publish'),
			'sponsored'				=> $this->input->post('sponsored'),
			'position'				=> $this->input->post('position'),
			'id_video'	    		=> $this->input->post('url'),
			'url'					=> $this->input->post('sumber'),
			'editor'				=> $editor,
			'thumbnail'     		=> $image_name,
			'thumbnail_watermark'	=>	$this->input->post('imagewatermark'),
			'id_section'	    	=> $this->input->post('kategori'),
			'ket_thumbnail'     	=> $this->input->post('caption'),
			'fokus'					=> $this->input->post('fokus'),
			//'kolom'					=> $this->input->post('kolom'),
			'urltitle'				=> url_title($this->input->post('judul'))
			
		);
		
		$this->T_artikel->update($this->input->post('id_artikel'),$data);
		$lastid = $this->input->post('id_artikel');

		//insert to tbl relation

		//hapus dulu relasinya
		if(!empty($this->input->post('kategori'))){

			$this->T_relasi->delete($this->input->post('id_artikel'),'art_sec');

			$data2 = array(
				'id_object'    	  	  	=> $lastid,
				'id_object2'	    	=> $this->input->post('kategori'),
				'tipe'     				=> 'art_sec'
			);
			
			$this->T_relasi->add($data2);

		}

		//input category    
					if(!empty($this->input->post('tag'))){

						//hapus dulu
						$this->T_relasi->delete($this->input->post('id_artikel'),'art_tag');

						$tag = $this->input->post('tag');
						$explode = explode(',', $this->input->post('tag'));	
						//echo "<pre>";
						//print_r($tag);
						//die();
						foreach($explode as $datatag=>$value){
							//search the tag open or not
							//echo "ada value gak sih";
							//echo $value;
							
							$cekcategori = $this->T_tag->cekcategory(strtolower($value));
							//echo "<pre>";
							//print_r($cekcategori);
							//die();
							if(empty($cekcategori['id_tag'])){
								//kategori belum ada berarti insert kategori
								$datax = array(
									'id_supsection'  	=> 99,
									'nama_tag'    		=> strtolower($value),
									'seo_tag'     		=> url_title(strtolower($value))
								);
								$this->T_tag->add($datax);
								$idcat = $this->db->insert_id();
								$kategoriID = $idcat;
							}else{
								//kategori sudah ada berarti tinggal masukkan ke assosiasi
								$kategoriID = $cekcategori['id_tag'];
							}
							
							//insert data to assosiate
							$data2 = array(
								'id_object'    	  	  	=> $lastid,
								'id_object2'	    	=> $kategoriID,
								'tipe'     				=> 'art_tag'
							);
							
							$this->T_relasi->add($data2);
						}
					}else{
						//insert data to assosiate
							$data2 = array(
								'id_object'    	  	  	=> $lastid,
								'id_object2'	    	=> 1,
								'tipe'     				=> 'art_tag'
							);
							
							$this->T_relasi->add($data2);
					}
					
		
		

				redirect('artikel/index/'.$this->input->post('parent_id')); 	
		 
	}


	function trash($id)
	{
	
		$data = $this->T_artikel->get($id);
		$notifikasi = "Artikel berjudul ".$data['judul_artikel']." telah dihapus ke trash.";
		$this->session->set_flashdata('notifikasidelete', strtoupper($notifikasi));
		$isi = array('publish' => 'T');
		$this->T_artikel->trash($id, $isi);
		redirect('artikel');  
	}

	function restore($id)
	{
	
		$isi = array('publish' => 'Y');
		$this->T_artikel->trash($id, $isi);
		redirect('artikel');  
	}

	function delete($id)
	{
	
		$data = $this->T_artikel->get($id);
		$notifikasi = "Artikel berjudul ".$data['judul_artikel']." telah dihapus <b>permanen</b>.";
		$this->session->set_flashdata('notifikasidelete', strtoupper($notifikasi));
		$this->T_artikel->delete($id);
		redirect('artikel');  
	}

	function hot($id,$daerah=0)
	{
	
		$datacontent['data'] 	= $this->T_artikel->get($id);


		if($datacontent['data']['hot']=='Y'){
			$d = 'N';
				
		}else{
			$d = 'Y';
		}


		$data = array(
			'hot'	    	=> $d
		);
		
		$this->T_artikel->update($id,$data);

		redirect('artikel/index/'.$daerah);  
	}
	

	function hl($id, $daerah=0)
	{
	
		$datacontent['data'] 	= $this->T_artikel->get($id);


		if($datacontent['data']['headline']=='Y'){
			$d = 'N';
				
		}else{
			$d = 'Y';
		}


		$data = array(
			'headline'	    	=> $d
		);
		
		$this->T_artikel->update($id,$data);

		redirect('artikel/index/'.$daerah);  
	}
	


	function ap($id)
	{
	
		$datacontent['data'] 	= $this->T_artikel->get($id);


		if($datacontent['data']['apps']==0){
			$d = 1;
				
		}else{
			$d = 0;
		}


		$data = array(
			'apps'	    	=> $d
		);
		
		$this->T_artikel->update($id,$data);

		redirect('artikel');  
	}

	function oldimage()
	{
		$data = $this->T_artikel->getold();
		foreach($data as $data) {
			$old = 'http://www.rilis.id/img/full_size/'.$data['oldimage'];
			$oldimage = array('oldimage'	=>	$old);
			$this->T_artikel->updateimage($data['id_artikel'], $oldimage);
		}
	}

	function rilizen($id=0,$pages=0)
	{

		//echo date('Y-m-d H:i:s');
		$datacontent['title']	= 'artikel';
		$viewperpage 	= 20;
		$datacontent['parent_id'] 	= $id;

		
		$datacontent['tag'] 	= $this->T_tag->select();

		if(!empty($this->input->post('search')))
		{
			$datacontent['data'] 	= 	$this->T_artikel->selectrilizensearch($pages, $viewperpage, $this->input->post('search'));
			$datacontent['count'] 	=  	$this->T_artikel->countrilizensearch($this->input->post('search'));
		}
		else
		{
			$datacontent['data'] 	= 	$this->T_artikel->selectrilizen($pages, $viewperpage);
			$datacontent['count'] 	=  	$this->T_artikel->countrilizen();
		}

		$datacontent['search'] = $this->input->post('search');
			

		 $datacontent['pages'] = (!empty($pages))?$pages:0;
		$this->load->library('pagination');
            $config['base_url'] 	= site_url('artikel/index/'.$id.'/');
            $config['total_rows'] 	= $datacontent['count'];
            $config['per_page'] 	= $viewperpage;
             $config['first_tag_open'] 	= '<div class="btn btn-info" type="button">';
            $config['first_tag_close'] 	= '</div>';
            $config['last_tag_open'] 	= '<div class="btn btn-info" type="button">';
            $config['last_tag_close'] 	= '</div>';
             $config['prev_tag_open'] 	= '<div class="btn btn-info" type="button">';
            $config['prev_tag_close'] 	= '</div>';
            $config['next_tag_open'] 	= '<div class="btn btn-info" type="button">';
            $config['next_tag_close'] 	= '</div>';
            $config['num_tag_open'] 	= '<div class="btn btn-info" type="button">';
            $config['num_tag_close'] 	= '</div>';
            $config['cur_tag_open'] 	= '<div class="btn btn-info active" type="button">';
            $config['cur_tag_close'] 	= '</div>';
			$this->pagination->initialize($config);
			$datacontent['tag'] 	= $this->T_tag->select();
			$datacontent['paging'] = $this->pagination->create_links();

			$datacontent['member'] 	= $this->T_admin->select();


		
		//echo "<pre>";
		//print_r($datacontent['data']);
		//echo "</pre>";
		$this->load->view('gen_cnf/artikel/indexzen',$datacontent);
	}

	function editzen($id=0)
	{
		$datacontent['data'] 	= $this->T_artikel->get($id);
		//get category
		$datacontent['catnya'] 	= $this->T_relasi->getobj2($datacontent['data']['id_artikel'],'art_sec');
		
		$datacontent['tag'] 	= $this->T_tag->select();
		$datacontent['tagnya'] 	= $this->T_relasi->getobj3($datacontent['data']['id_artikel'],'art_tag');
		$datacontent['fokus']	= $this->T_artikel->fokus($id);
		$datacontent['kolom']	= $this->T_artikel->kolom($id);
		//echo "<pre>";
		//print_r($datacontent['tagnya']);
		$datacontent['isi'][]="";
		foreach($datacontent['tagnya'] as $x){
			$datacontent['isi'][] = $x['id_object2'];
		}

		
			$this->load->view('gen_cnf/artikel/editzen',$datacontent);
		
		
		
	}



	function actioneditzen()
	{
		

		if(!empty($_FILES)){

				//pengecekan folder ada apa tidak
				$uploaddir 		= $this->config->item('upload_images').'images/';
				

				$datapath = explode("/", $this->input->post('path'));

				$datename 		= $datapath[1]; 
			
				$tahun = $uploaddir.$datename;
				if(is_dir($tahun)){
				}else{
					mkdir($tahun, 0777, true);
					$fp = fopen($tahun.'/index.html', 'w');
					fwrite($fp , 'Bismillaah');
					fclose($fp);
				}


				$datename2 = $datapath[2]; 
			
				$bulan = $uploaddir.$datename."/".$datename2;
				if(is_dir($bulan)){
				}else{
					mkdir($bulan, 0777, true);
					$fp = fopen($bulan.'/index.html', 'w');
					fwrite($fp , 'Bismillaah');
					fclose($fp);
				}


				$datename3 = $datapath[3]; 
			
				$hari = $uploaddir.$datename."/".$datename2."/".$datename3;
				if(is_dir($hari)){
				}else{
					mkdir($hari, 0777, true);
					$fp = fopen($hari.'/index.html', 'w');
					fwrite($fp , 'Bismillaah');
					fclose($fp);
				}

			}


		if(!empty($_FILES['image']['name'])){
				$image_name =  str_replace(' ','_',date('Ymdhis').$_FILES['image']['name']);
				$config['upload_path'] 		= $hari;
				$config['allowed_types'] 	= 'gif|jpg|png|jpeg';
				$config['file_name'] 		= $image_name;
				$config['min_width'] = '800';
				$config['min_height'] = '450';
				$this->upload->initialize($config);
				$this->upload->do_upload('image');
			}else{
				$image_name = $this->input->post('image');
			}



		if(!empty($_FILES['thumb']['name'])){
				$thumb_name =  str_replace(' ','_','thumb_'.date('Ymdhis').$_FILES['thumb']['name']);
				$config['upload_path'] 		= $hari;
				$config['allowed_types'] 	= 'gif|jpg|png|jpeg';
				$config['file_name'] 		= $thumb_name;
				$this->upload->initialize($config);
				$this->upload->do_upload('thumb');
			}else{
				$thumb_name = $this->input->post('thumbx');
			}

		if(!empty($_FILES['slider']['name'])){
				$slider_name =  str_replace(' ','_','slider_'.date('Ymdhis').$_FILES['slider']['name']);
				$config['upload_path'] 		= $hari;
				$config['allowed_types'] 	= 'gif|jpg|png|jpeg';
				$config['file_name'] 		= $slider_name;
				$config['min_width'] = '1900';
				$config['min_height'] = '800';
				$this->upload->initialize($config);
				$this->upload->do_upload('slider');
			}else{
				$slider_name = $this->input->post('slider');
			}

	
		$data = array(
			'judul_artikel'	    	=> $this->input->post('judul'),
			'isi_artikel'     		=> $this->input->post('detail'),
			'gagasan_utama'    	  	=> $this->input->post('gagasan'),
			'kota'					=> $this->input->post('kota'),
			'tanggal'	    		=> $this->input->post('tgl'),
			'jam'     				=> $this->input->post('jam').":".$this->input->post('menit'),
			'tgl_pub'     			=> $this->input->post('tgl')." ".$this->input->post('jam').":".$this->input->post('menit').":00",
			'meta_key'    	  		=> $this->input->post('mkey'),
			'meta_des'    	  	  	=> $this->input->post('mdesc'),
			'publish'	    		=> $this->input->post('publish'),
			'sponsored'				=> $this->input->post('sponsored'),
			'position'				=> $this->input->post('position'),
			'id_video'	    		=> $this->input->post('url'),
			'parent_id'	    		=> $this->input->post('parent_id'),
			'thumbnail'     		=> $image_name,
			'thumbnail_watermark'	=>	$this->input->post('imagewatermark'),
			'id_section'	    	=> $this->input->post('kategori'),
			'ket_thumbnail'     	=> $this->input->post('caption'),
			'fokus'					=> $this->input->post('fokus'),
			'kolom'					=> $this->input->post('kolom'),
			'urltitle'				=> url_title($this->input->post('judul'))
			
		);
		
		$this->T_artikel->update($this->input->post('id_artikel'),$data);
		$lastid = $this->input->post('id_artikel');

		//insert to tbl relation

		//hapus dulu relasinya
		if(!empty($this->input->post('kategori'))){

			$this->T_relasi->delete($this->input->post('id_artikel'),'art_sec');

			$data2 = array(
				'id_object'    	  	  	=> $lastid,
				'id_object2'	    	=> $this->input->post('kategori'),
				'tipe'     				=> 'art_sec'
			);
			
			$this->T_relasi->add($data2);

		}

		//input category    
					if(!empty($this->input->post('tag'))){

						//hapus dulu
						$this->T_relasi->delete($this->input->post('id_artikel'),'art_tag');

						$tag = $this->input->post('tag');
						$explode = explode(',', $this->input->post('tag'));	
						//echo "<pre>";
						//print_r($tag);
						//die();
						foreach($explode as $datatag=>$value){
							//search the tag open or not
							//echo "ada value gak sih";
							//echo $value;
							
							$cekcategori = $this->T_tag->cekcategory(strtolower($value));
							//echo "<pre>";
							//print_r($cekcategori);
							//die();
							if(empty($cekcategori['id_tag'])){
								//kategori belum ada berarti insert kategori
								$datax = array(
									'id_supsection'  	=> 99,
									'nama_tag'    		=> strtolower($value),
									'seo_tag'     		=> url_title(strtolower($value))
								);
								$this->T_tag->add($datax);
								$idcat = $this->db->insert_id();
								$kategoriID = $idcat;
							}else{
								//kategori sudah ada berarti tinggal masukkan ke assosiasi
								$kategoriID = $cekcategori['id_tag'];
							}
							
							//insert data to assosiate
							$data2 = array(
								'id_object'    	  	  	=> $lastid,
								'id_object2'	    	=> $kategoriID,
								'tipe'     				=> 'art_tag'
							);
							
							$this->T_relasi->add($data2);
						}
					}else{
						//insert data to assosiate
							$data2 = array(
								'id_object'    	  	  	=> $lastid,
								'id_object2'	    	=> 1,
								'tipe'     				=> 'art_tag'
							);
							
							$this->T_relasi->add($data2);
					}
					
		$parent = $this->input->post('parent_id');
		
		if($parent==0){
				redirect('artikel/rilizen'); 

			}else{

				redirect('artikel/index/'.$this->input->post('parent_id')); 

			}		
		 
	}

	function hotzen($id)
	{
	
		$datacontent['data'] 	= $this->T_artikel->get($id);


		if($datacontent['data']['hot']=='Y'){
			$d = 'N';
				
		}else{
			$d = 'Y';
		}


		$data = array(
			'hot'	    	=> $d
		);
		
		$this->T_artikel->update($id,$data);

		redirect('artikel/rilizen');  
	}

	function hlzen($id)
	{
	
		$datacontent['data'] 	= $this->T_artikel->get($id);


		if($datacontent['data']['headline']=='Y'){
			$d = 'N';
				
		}else{
			$d = 'Y';
		}


		$data = array(
			'headline'	    	=> $d
		);
		
		$this->T_artikel->update($id,$data);

		redirect('artikel/rilizen');  
	}

	function trashzen($id)
	{
	
		$data = $this->T_artikel->get($id);
		$notifikasi = "Artikel berjudul ".$data['judul_artikel']." telah dihapus ke trash.";
		$this->session->set_flashdata('notifikasidelete', strtoupper($notifikasi));
		$isi = array('publish' => 'T');
		$this->T_artikel->trash($id, $isi);
		redirect('artikel/rilizen');  
	}

	function restorezen($id)
	{
	
		$isi = array('publish' => 'Y');
		$this->T_artikel->trash($id, $isi);
		redirect('artikel/rilizen');  
	}

	function deletezen($id)
	{
	
		$data = $this->T_artikel->get($id);
		$notifikasi = "Artikel berjudul ".$data['judul_artikel']." telah dihapus <b>permanen</b>.";
		$this->session->set_flashdata('notifikasidelete', strtoupper($notifikasi));
		$this->T_artikel->delete($id);
		redirect('artikel/rilizen');  
	}
	
	

}
