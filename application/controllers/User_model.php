<?php

Class User_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function checkmail($email,$from)
    {
        $sql = "SELECT 1 FROM member WHERE email='$email' AND dari='$from'";
        $query = $this->db->query($sql);
        $row = $query->num_rows();
        return $row;
    }
	
    function checkavail($email,$from)
    {
        $sql = "SELECT id FROM member WHERE email='$email' AND dari='$from'";
        $query = $this->db->query($sql);
        $row = $query->row_array();
        return $row;
    }
	
    function get_from_mail($email,$from)
    {
        $sql = "SELECT salt FROM member WHERE email='$email' AND dari='$from'";
        $query = $this->db->query($sql);
        $row = $query->row_array();
        return $row;
    }
	
    function get_mailnpwd($email,$from,$pwd)
    {
        $sql = "SELECT id,name,image,dari FROM member WHERE email='$email' AND dari='$from' AND password='$pwd'";
        $query = $this->db->query($sql);
        $row = $query->row_array();
        return $row;
    }
	
	function add($data){
		$this->db->insert('member', $data);
		$insert_id = $this->db->insert_id();
		$sql = "SELECT id,name FROM member WHERE id='$insert_id'";
        $query = $this->db->query($sql);
        $row = $query->row_array();
		$hasil = array(
			'member_id' => $row['id'],
			'name'		=> $row['name'],
		);
        return $hasil;
	}
	
	function get_user_auth($id,$dari){
		$sql = "SELECT id,name FROM member WHERE auth_id='$id' AND dari='$dari'";
        $query = $this->db->query($sql);
        $row = $query->row_array();
        return $row;
	}
	
	function get_data($id){
		$sql = "SELECT m.image,m.id,m.name,m.email,m.gender,m.phone,m.address,m.dari,m.provinsi_id,m.city_id,
				(select p.name FROM provinsi p WHERE p.id=m.provinsi_id) as provinsi,
				(select c.city_name FROM city c WHERE c.id=m.city_id) as kota
		FROM member m WHERE m.id='$id'";
        $query = $this->db->query($sql);
        $row = $query->row_array();
        return $row;
	}
	
    function update($id, $update) {
        $this->db->where('id', $id);
        $data = $this->db->update('member', $update);
		//echo $this->db->last_query();
    }
	
	function get_salt($id){
		$sql = "SELECT m.salt,m.password
		FROM member m WHERE m.id='$id'";
        $query = $this->db->query($sql);
        $row = $query->row_array();
        return $row;
	}
	
	function get_article($id,$limit=6,$offset=0){
		$sql = "SELECT b.id,b.title,b.image,b.title,b.post_date,
			(SELECT c.nama FROM menu_article c WHERE c.id=b.id_menu) as nama_menu 
			FROM article_user a 
			INNER JOIN article b 
			ON a.article_id=b.id 
			WHERE a.user_id='$id' 
			GROUP BY a.article_id LIMIT $limit OFFSET $offset";
        $query = $this->db->query($sql);
        $row = $query->result_array();
        return $row;
	}
	
	function article_by_comment($id,$limit=6,$offset=0){
		$sql = "SELECT b.id,b.title,b.image,b.title,b.detail,b.post_date,
			(SELECT c.nama FROM menu_article c WHERE c.id=b.id_menu) as nama_menu 
			FROM comment a 
			INNER JOIN article b 
			ON a.article_id=b.id 
			WHERE a.member_id='$id' 
			GROUP BY a.article_id LIMIT $limit OFFSET $offset";
        $query = $this->db->query($sql);
        $row = $query->result_array();
        return $row;
	}
	
	function magazine_by_user($id,$limit=6,$offset=0){
		$sql = "SELECT b.id,b.datetime,b.title,b.content,b.thumbnail,a.status
			FROM epaper_user a 
			INNER JOIN epaper b 
			ON a.epaper_id=b.id 
			WHERE a.user_id='$id' ORDER BY a.order_date LIMIT $limit OFFSET $offset";
        $query = $this->db->query($sql);
        $row = $query->result_array();
        return $row;
	}
}