<?php
class Informasi extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('T_informasi');
		if(!$this->session->userdata('id_adm'))
			redirect('gen_cnf/auth');
	}
	
	function index($id)
	{
		$datacontent['title']	= 'informasi';
		$datacontent['url'] 	= "informasi/index";
		$datacontent['action'] 		= 'index';
		$this->load->helper('form');
		$datacontent['user'] 		= $this->T_informasi->get_informasi($id);
		
		//echo "<pre>";
		//print_r($datacontent['user']);
		//echo "</pre>";
		
		$this->load->view('gen_cnf/informasi/index',$datacontent);
	}
	
	function actionindex()
	{
	
		$datacontent = array(
			'penerbit'    		 	=> $this->input->post('penerbit'),
			'telepon'     			=> $this->input->post('number'),
			'fax'     				=> $this->input->post('fax'),
			'email'    	  		  	=> $this->input->post('email'),
			'alamat'    	  	  	=> $this->input->post('alamat'),
			'facebook'    	  	  	=> $this->input->post('fb'),
			'twitter'    	  	  	=> $this->input->post('tw'),
		);
		$this->T_informasi->get_informasi($this->input->post('id'),$datacontent);
		
		redirect('gen_cnf/informasi/'); 
	}
	
	
	
	function add()
	{
		$datacontent['title']	= 'administrator';
		$datacontent['url'] = "administrator/index";
		$this->load->helper('form');
		$datacontent['action'] 	= 'add';
		$datacontent['group'] 	= $this->model_group->select();
		$this->load->view('gen_cnf/administrator/add',$datacontent);
	}
	
	function actionadd()
	{
	if(!empty($_FILES['image']['name'])){
				$image_name =  str_replace(' ','_',date('Ymdhis').$_FILES['image']['name']);
				$config['upload_path'] 		= $this->config->item('upload_images');
				$config['allowed_types'] 	= 'gif|jpg|png';
				$config['file_name'] 		= $image_name;
				$this->upload->initialize($config);
				$this->upload->do_upload('image');
			}else{
				$image_name = '';
			}
		$salt = substr(md5($this->input->post('password')),0,6);
		
		$password = md5($this->input->post('password').$salt);
		$data = array(
			'username'    		 	=> $this->input->post('username'),
			'password'     			=> $password,
			'email'     			=> $this->input->post('email'),
			'phone'     			=> $this->input->post('phone'),
			'address'    	  	  	=> $this->input->post('address'),
			'group_id'    	  	  	=> $this->input->post('group_id'),
			'join_date'    	  	  	=> date("Y-m-d H:i:s"),
			'image'				=> $image_name,
			'salt'				=> $salt,
		);
		
		$this->model_administrator->add($data);
		
		//log admin
				$activity = "Add";
				$table = "user";
				$last = $this->db->insert_id();
				include_once dirname(__FILE__).'/log.php' ;
		//end log admin
				
		redirect('gen_cnf/administrator/');  
	}
	
	function edit($id)
	{
		$datacontent['title']	= 'administrator';
		$datacontent['url'] = "administrator/index";
		$this->load->helper('form');
		$datacontent['action'] 		= 'edit';
		$datacontent['group'] 	= $this->model_group->select();
		$datacontent['data'] 		= $this->model_administrator->get_administrator($id);
		//echo "<pre>";
		//print_r($datacontent['data']);
		//echo "</pre>";

		$this->load->view('gen_cnf/administrator/edit',$datacontent);
	}	
	
	function actionedit()
	{
	if(!empty($_FILES['image']['name'])){
				$image_name =  str_replace(' ','_',date('Ymdhis').$_FILES['image']['name']);
				$config['upload_path'] 		= $this->config->item('upload_images');
				$config['allowed_types'] 	= 'gif|jpg|png';
				$config['file_name'] 		= $image_name;
				$this->upload->initialize($config);
				$this->upload->do_upload('image');
			}else{
				$image_name = $this->input->post('imagex');
			}
		$data = array(
			'username'    		 	=> $this->input->post('username'),
			'email'     			=> $this->input->post('email'),
			'phone'     			=> $this->input->post('phone'),
			'address'    	  	  	=> $this->input->post('address'),
			'group_id'    	  	  	=> $this->input->post('group_id'),
			'join_date'    	  	  	=> date("Y-m-d H:i:s"),
			'image'					=> $image_name,
		);
		$this->model_administrator->update($this->input->post('id'),$data);
		//log admin
				$activity = "Edit";
				$table = "user";
				$last = $this->input->post('id');
				include_once dirname(__FILE__).'/log.php' ;
		//end log admin
		redirect('gen_cnf/administrator/'); 
	}
	
	
	function cpassword($id)
	{
		$datacontent['title']	= 'administrator';
		$datacontent['url'] = "administrator/index";
		$this->load->helper('form');
		$datacontent['action'] 		= 'cpassword';
		$datacontent['data'] 		= $this->model_administrator->get_administrator($id);
		$this->load->view('gen_cnf/administrator/cpassword',$datacontent);
	}	
	
	
	function actioncpassword()
	{
	
		$salt = substr(md5($this->input->post('password')),0,6);
		$password = md5($this->input->post('password').$salt);
		$data = array(
			'password'     		=> $password,
			'salt'				=> $salt,
		);			
		$this->model_administrator->update($this->input->post('id'),$data);
		//log admin
				$activity = "Change Password";
				$table = "user";
				$last = $this->input->post('id');
				include_once dirname(__FILE__).'/log.php' ;
		//end log admin
		redirect('gen_cnf/administrator/'); 
	}
	
	
	
	function actiondelete($id)
	{
	
		//log admin
				$activity = "Delete";
				$table = "user";
				$last = $id;
				include_once dirname(__FILE__).'/log.php' ;
		//end log admin
		$this->model_administrator->delete($id);
		redirect('gen_cnf/administrator/');  
	}

	function change($id)
	{
		$datacontent['url'] = "administrator/index";
		$this->load->helper('form');
		$datacontent['action'] 		= 'change password';
		$datacontent['data'] 		= $this->model_administrator->get_administrator($id);
		if(!empty($_POST)){
		
		$salt = substr(md5($this->input->post('newpas')),0,6);
		$password = md5($this->input->post('newpas').$salt);
		$data = array(
			'password '     		=> $password,
			'salt'				=> $salt,
		);
		$this->model_administrator->update($id,$data);
		//log activities
			$activity = "Change Password";
			$table = "User";
			$last = $id;
			include_once dirname(__FILE__).'/log.php' ;
			//end log activities
			//email
		$nama = $this->input->post('username');
		$email = $this->input->post('email');
		$kirimpass = $this->input->post('newpassord');
		//echo $databam['EMAIL'];
		$to      = $email;
		$subject = 'Change Password';
		$message = 'Username : '.$nama.'<br> Password : '.$kirimpass;
		'X-Mailer: PHP/' . phpversion();
					
		@mail($to, $subject, $message);
		redirect('gen_cnf/administrator/');
		}
		$this->load->view('gen_cnf/administrator/change',$datacontent);	 
	}
	
	function check(){
	if($_POST['oldpas'])
		{
		$salt = $this->session->userdata('garam');
			$password = md5($this->input->post('oldpas').$salt);
			$userlogin = $this->model_administrator->authenticate2($password);
			$user_id = (count($userlogin) > 0) ? $userlogin['id'] : 0;
			if( $user_id )
			{
			echo '<span class="success">Password Benar</span>';
		}else{
			echo '<span class="error" style="font-size:0.9em">Password Salah</span>';
		}
		}
	}
	
	
	function toexcel(){
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="Data-Administrator-'.date("Y-m-d/H-i-s").'.xls"');
        $data["rows"] = $this->model_administrator->select();
        $this->load->view("gen_cnf/administrator/xls", $data);
    }

}
