<?php
class Administrator extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('T_admin');
		$this->load->model('T_level');
		//$this->load->model('model_typeuser');
		//$this->load->model('model_group');
		if(!$this->session->userdata('id_adm'))
			redirect('gen_cnf/auth');
	}
	
	function index()
	{
		$datacontent['title']	= 'administrator';
		$datacontent['url'] = "administrator/index";
		$datacontent['data'] 	= $this->T_admin->select();
		//echo "<pre>";
		//print_r($datacontent['data']);
		//echo "</pre>";

		$this->load->view('gen_cnf/administrator/index',$datacontent);
	}
	
	function add()
	{
		$this->output->cache(60);
		$datacontent['title']	= 'administrator';
		$datacontent['url'] = "administrator/index";
		$this->load->helper('form');
		$datacontent['action'] 	= 'add';
		$datacontent['group'] 	= $this->T_level->select();
		
		//echo "<pre>";
		//print_r($datacontent['group']);
		//echo "</pre>";
		$this->load->view('gen_cnf/administrator/add',$datacontent);
	}
	
	function actionadd()
	{
		if(!empty($_FILES['image']['name'])){
				$image_name =  str_replace(' ','_',date('Ymdhis').$_FILES['image']['name']);
				$config['upload_path'] 		= $this->config->item('upload_images');
				$config['allowed_types'] 	= 'gif|jpg|png';
				$config['file_name'] 		= $image_name;
				$this->upload->initialize($config);
				$this->upload->do_upload('image');
			}else{
				$image_name = '';
			}
	
		$data = array(
			'level'    	  	  	=> $this->input->post('group_id'),
			'nama'	    		=> $this->input->post('name'),
			'email'     		=> $this->input->post('email'),
			'des_admin'     	=> $this->input->post('deskripsi'),
			'password'    	  	=> $this->input->post('password'),
			'foto_admin'		=> $image_name,
			
		);
		
		$this->T_admin->add($data);
		
		
				
		redirect('gen_cnf/administrator/');  
	}
	
	function cpassword($id)
	{
		$datacontent['title']	= 'administrator';
		$datacontent['url'] = "administrator/index";
		$this->load->helper('form');
		$datacontent['action'] 		= 'cpassword';
		$datacontent['data'] 		= $this->T_admin->get_tadmin($id);
		$this->load->view('gen_cnf/administrator/cpassword',$datacontent);
	}
	
	function actioncpassword()
	{
		$password = md5($this->input->post('password'));
		$data = array(
			'password'     		=> $password,
		);			
		$this->T_admin->update($this->input->post('id'),$data);
		//$this->db->cache_delete('gen_cnf', 'administrator');
		redirect('gen_cnf/administrator/'); 
	}
	
	
	function edit($id)
	{
		$datacontent['title']	= 'administrator';
		$datacontent['url'] = "administrator/index";
		$this->load->helper('form');
		$datacontent['action'] 		= 'edit';
		$datacontent['group'] 		= $this->T_level->select();
		$datacontent['data'] 		= $this->T_admin->get_tadmin($id);
		//echo "<pre>";
		//print_r($datacontent['data']);
		//echo "</pre>";

		$this->load->view('gen_cnf/administrator/edit',$datacontent);
	}	
	
	function actionedit()
	{
	if(!empty($_FILES['image']['name'])){
				$image_name =  str_replace(' ','_',date('Ymdhis').$_FILES['image']['name']);
				$config['upload_path'] 		= $this->config->item('upload_images');
				$config['allowed_types'] 	= 'gif|jpg|png';
				$config['file_name'] 		= $image_name;
				$this->upload->initialize($config);
				$this->upload->do_upload('image');
			}else{
				$image_name = '';
			}
	
		$data = array(
			'level'    	  	  	=> $this->input->post('group_id'),
			'nama'	    		=> $this->input->post('name'),
			'email'     		=> $this->input->post('email'),
			'des_admin'     	=> $this->input->post('deskripsi'),
			'password'    	  	=> $this->input->post('password'),
			'foto_admin'		=> $image_name,
			
		);
		$this->T_admin->update($this->input->post('id'),$data);
		
		redirect('gen_cnf/administrator/'); 
	}
	
	
	
	function actiondelete($id)
	{
	
		
		$this->T_admin->delete($id);
		redirect('gen_cnf/administrator/');  
	}
	
	
	function toexcel(){
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="Data-Administrator-'.date("Y-m-d/H-i-s").'.xls"');
        $data["rows"] = $this->model_administrator->select();
        $this->load->view("gen_cnf/administrator/xls", $data);
    }

}
