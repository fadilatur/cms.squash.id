<?php
class Topfigure extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('T_figure');
		$this->load->model('T_negara');
		$this->load->model('T_supsection');
		if(!$this->session->userdata('id_adm'))
			redirect('gen_cnf/auth');
	}
	
	function index($id)
	{
		$datacontent['title']	= 'figure';
		$datacontent['data'] 	= $this->T_figure->select_group($id);
		echo "<pre>";
		print_r($datacontent['data']);
		echo "</pre>";
		$datacontent['supsection']		= $this->T_supsection->select();
		$datacontent['negara'] 		= $this->T_negara->get_group($id);
		
		
		$this->load->view('gen_cnf/topfigure/index',$datacontent);
	}
	
	function add()
	{
		//$this->output->cache(60);
		//$datacontent['title']	= 'administrator';
		//$datacontent['url'] = "administrator/index";
		//$this->load->helper('form');
		//$datacontent['action'] 	= 'add';
		//$datacontent['group'] 	= $this->T_level->select();
		
		//echo "<pre>";
		//print_r($datacontent['group']);
		//echo "</pre>";
		$this->load->view('gen_cnf/artikel/add');
	}
	
	function actionadd()
	{
		if(!empty($_FILES['image']['name'])){
				$image_name =  str_replace(' ','_',date('Ymdhis').$_FILES['image']['name']);
				$config['upload_path'] 		= $this->config->item('upload_images');
				$config['allowed_types'] 	= 'gif|jpg|png';
				$config['file_name'] 		= $image_name;
				$this->upload->initialize($config);
				$this->upload->do_upload('image');
			}else{
				$image_name = '';
			}
	
		$data = array(
			'level'    	  	  	=> $this->input->post('group_id'),
			'nama'	    		=> $this->input->post('name'),
			'email'     		=> $this->input->post('email'),
			'des_admin'     	=> $this->input->post('deskripsi'),
			'password'    	  	=> $this->input->post('password'),
			'foto_admin'		=> $image_name,
			
		);
		
		$this->T_admin->add($data);
		
		
				
		redirect('gen_cnf/administrator/');  
	}
	
	

}
