<?php
class Level extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('T_level');
		$this->load->model('T_page');
		$this->load->model('T_aksespage');
		if(!$this->session->userdata('id_adm'))
			redirect('gen_cnf/auth');
	}
	
	function index($id=0)
	{
		$datacontent['title']	= 'level';
		$datacontent['url'] 	= "level/index";
		$datacontent['data'] 	= $this->T_level->select($id);
		//echo "<pre>";
		//print_r($datacontent['data']);
		//echo "</pre>";
		$this->load->view('gen_cnf/level/index',$datacontent);
	}
	
	function edit($id)
	{
			$datacontent['title']	= "level";
			$datacontent['url'] 		= "gen_cnf/level/index";
			$this->load->helper('form');
			$datacontent['action'] 		= 'edit';
			$datacontent['page']		= $this->T_page->select();
			//echo "<pre>";
		//print_r($datacontent['page']);
		//echo "</pre>";
			$datacontent['aksespage']	= $this->T_aksespage->select_group($id);
			//echo "<pre>";
		//print_r($datacontent['aksespage']);
		//echo "</pre>";
			$datacontent['data'] 		= $this->T_level->get_group($id);
		
			$this->load->view('gen_cnf/level/edit',$datacontent);
			
	}
	
	function actionedit()
		{
			$datacontent = array(
				'level_nama'    		=> $this->input->post('level_nama'),
			);
			
			$this->T_level->update($this->input->post('id'),$data);
			$this->T_aksespage->delete($this->input->post('id'));
			$ks = $this->input->post('aksespage');
			foreach($ks as $aksespage){
				$data2 = array(
					'id_aksespage'	=> $this->input->post('id'),
					'id_page'	=> $aksespage,
					'level'	=> $aksespage,
					
				);
				$this->T_aksespage->add($data2);
			}
			redirect('gen_cnf/level/'); 
		}
	
	function add()
	{
		$datacontent['title']	= 'administrator';
		$datacontent['url'] = "administrator/index";
		$this->load->helper('form');
		$datacontent['action'] 	= 'add';
		$datacontent['group'] 	= $this->model_group->select();
		$this->load->view('gen_cnf/administrator/add',$datacontent);
	}
	
	function actionadd()
	{
	if(!empty($_FILES['image']['name'])){
				$image_name =  str_replace(' ','_',date('Ymdhis').$_FILES['image']['name']);
				$config['upload_path'] 		= $this->config->item('upload_images');
				$config['allowed_types'] 	= 'gif|jpg|png';
				$config['file_name'] 		= $image_name;
				$this->upload->initialize($config);
				$this->upload->do_upload('image');
			}else{
				$image_name = '';
			}
		$salt = substr(md5($this->input->post('password')),0,6);
		
		$password = md5($this->input->post('password').$salt);
		$data = array(
			'username'    		 	=> $this->input->post('username'),
			'password'     			=> $password,
			'email'     			=> $this->input->post('email'),
			'phone'     			=> $this->input->post('phone'),
			'address'    	  	  	=> $this->input->post('address'),
			'group_id'    	  	  	=> $this->input->post('group_id'),
			'join_date'    	  	  	=> date("Y-m-d H:i:s"),
			'image'				=> $image_name,
			'salt'				=> $salt,
		);
		
		$this->model_administrator->add($data);
		
		//log admin
				$activity = "Add";
				$table = "user";
				$last = $this->db->insert_id();
				include_once dirname(__FILE__).'/log.php' ;
		//end log admin
				
		redirect('gen_cnf/administrator/');  
	}
	
	
	function cpassword($id)
	{
		$datacontent['title']	= 'administrator';
		$datacontent['url'] = "administrator/index";
		$this->load->helper('form');
		$datacontent['action'] 		= 'cpassword';
		$datacontent['data'] 		= $this->model_administrator->get_administrator($id);
		$this->load->view('gen_cnf/administrator/cpassword',$datacontent);
	}	
	
	
	function actioncpassword()
	{
	
		$salt = substr(md5($this->input->post('password')),0,6);
		$password = md5($this->input->post('password').$salt);
		$data = array(
			'password'     		=> $password,
			'salt'				=> $salt,
		);			
		$this->model_administrator->update($this->input->post('id'),$data);
		//log admin
				$activity = "Change Password";
				$table = "user";
				$last = $this->input->post('id');
				include_once dirname(__FILE__).'/log.php' ;
		//end log admin
		redirect('gen_cnf/administrator/'); 
	}
	
	
	
	function actiondelete($id)
	{
	
		//log admin
				$activity = "Delete";
				$table = "user";
				$last = $id;
				include_once dirname(__FILE__).'/log.php' ;
		//end log admin
		$this->model_administrator->delete($id);
		redirect('gen_cnf/administrator/');  
	}

	function change($id)
	{
		$datacontent['url'] = "administrator/index";
		$this->load->helper('form');
		$datacontent['action'] 		= 'change password';
		$datacontent['data'] 		= $this->model_administrator->get_administrator($id);
		if(!empty($_POST)){
		
		$salt = substr(md5($this->input->post('newpas')),0,6);
		$password = md5($this->input->post('newpas').$salt);
		$data = array(
			'password '     		=> $password,
			'salt'				=> $salt,
		);
		$this->model_administrator->update($id,$data);
		//log activities
			$activity = "Change Password";
			$table = "User";
			$last = $id;
			include_once dirname(__FILE__).'/log.php' ;
			//end log activities
			//email
		$nama = $this->input->post('username');
		$email = $this->input->post('email');
		$kirimpass = $this->input->post('newpassord');
		//echo $databam['EMAIL'];
		$to      = $email;
		$subject = 'Change Password';
		$message = 'Username : '.$nama.'<br> Password : '.$kirimpass;
		'X-Mailer: PHP/' . phpversion();
					
		@mail($to, $subject, $message);
		redirect('gen_cnf/administrator/');
		}
		$this->load->view('gen_cnf/administrator/change',$datacontent);	 
	}
	
	function check(){
	if($_POST['oldpas'])
		{
		$salt = $this->session->userdata('garam');
			$password = md5($this->input->post('oldpas').$salt);
			$userlogin = $this->model_administrator->authenticate2($password);
			$user_id = (count($userlogin) > 0) ? $userlogin['id'] : 0;
			if( $user_id )
			{
			echo '<span class="success">Password Benar</span>';
		}else{
			echo '<span class="error" style="font-size:0.9em">Password Salah</span>';
		}
		}
	}
	
	
	function toexcel(){
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="Data-Administrator-'.date("Y-m-d/H-i-s").'.xls"');
        $data["rows"] = $this->model_administrator->select();
        $this->load->view("gen_cnf/administrator/xls", $data);
    }

}
