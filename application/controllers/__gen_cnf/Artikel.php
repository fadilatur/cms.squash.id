<?php
class Artikel extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('T_artikel');
		$this->load->model('T_section');
		$this->load->model('T_supsection');
		$this->load->model('T_relasi');
		$this->load->model('T_tag');
		if(!$this->session->userdata('id_adm'))
			redirect('gen_cnf/auth');
	}
	
	function index($id=0)
	{

		$datacontent['title']	= 'artikel';
		$datacontent['data'] 	= $this->T_artikel->selectadmin($id);
		//echo "<pre>";
		//print_r($datacontent['data']);
		//echo "</pre>";
		$this->load->view('gen_cnf/artikel/index',$datacontent);
	}
	
	function add()
	{
		$this->output->cache(120);
		$datacontent['tag'] 	= $this->T_tag->select();
		$this->load->view('gen_cnf/artikel/add',$datacontent);
	}
	
	function actionadd()
	{
		
		if(!empty($_FILES['image']['name'])){
				$image_name =  str_replace(' ','_',date('Ymdhis').$_FILES['image']['name']);
				$config['upload_path'] 		= $this->config->item('upload_images');
				$config['allowed_types'] 	= 'gif|jpg|png';
				$config['file_name'] 		= $image_name;
				$this->upload->initialize($config);
				$this->upload->do_upload('image');
			}else{
				$image_name = '';
			}
	
		$data = array(
			'id_admin'    	  	  	=> $this->session->userdata('id_adm'),
			'judul_artikel'	    	=> $this->input->post('judul'),
			'isi_artikel'     		=> $this->input->post('detail'),
			'gagasan_utama'    	  	=> $this->input->post('gagasan'),
			'tanggal'	    		=> date('Y-m-d'),
			'jam'     				=> $this->input->post('jam').":".$this->input->post('menit'),
			'tgl_pub'     			=> date('Y-m-d H:i:s'),
			'meta_key'    	  		=> $this->input->post('mkey'),
			'meta_des'    	  	  	=> $this->input->post('mdesc'),
			'publish'	    		=> $this->input->post('publish'),
			'url'	    			=> $this->input->post('url'),
			'thumbnail'     		=> $image_name,
			'ket_thumbnail'     	=> $this->input->post('caption')
			
		);
		
		$this->T_artikel->add($data);
		$lastid = $this->db->insert_id();

		//insert to tbl relation

		$data2 = array(
			'id_object'    	  	  	=> $lastid,
			'id_object2'	    	=> $this->input->post('kategori'),
			'tipe'     				=> 'art_kat'
		);
		
		$this->T_relasi->add($data2);



		//input category    
					if($posted['tag']!=NULL){
						$tag = $posted['tag'];
						foreach($tag as $datatag=>$value){
							//search the tag open or not
							$cekcategori = $this->T_tag->cekcategory($value);
							if(empty($cekcategori['id_tag'])){
								//kategori belum ada berarti insert kategori
								$datax = array(
									'id_supsection'  	=> 99,
									'nama_tag'    		=> $value,
									'seo_tag'     		=> url_title($value)
								);
								$this->T_tag->add($datax);
								$idcat = $this->db->insert_id();
								$kategoriID = $idcat;
							}else{
								//kategori sudah ada berarti tinggal masukkan ke assosiasi
								$kategoriID = $cekcategori['id_tag'];
							}
							
							//insert data to assosiate
							$data2 = array(
								'id_object'    	  	  	=> $lastid,
								'id_object2'	    	=> $kategoriID,
								'tipe'     				=> 'art_tag'
							);
							
							$this->T_relasi->add($data2);
						}
					}else{
						//insert data to assosiate
							$data2 = array(
								'id_object'    	  	  	=> $lastid,
								'id_object2'	    	=> 1,
								'tipe'     				=> 'art_tag'
							);
							
							$this->T_relasi->add($data2);
					}
		
				
		redirect('gen_cnf/artikel/');  
	}



	function edit($id=0)
	{
		$datacontent['data'] 	= $this->T_artikel->get($id);
		//get category
		$datacontent['cat'] 	= $this->T_relasi->getobj2($datacontent['data']['id_artikel'],'art_kat');
		echo "<pre>";
		print_r($datacontent['cat']);
		$datacontent['tag'] 	= $this->T_tag->select();
		$this->load->view('gen_cnf/artikel/edit',$datacontent);
	}
	
	

}
