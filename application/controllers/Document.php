<?php
class Document extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('T_file');
		date_default_timezone_set('Asia/Jakarta');
		if(!$this->session->userdata('id_adm'))
			redirect('auth');
	}
	
	function index()
	{

		$datacontent['data'] 	= $this->T_file->select();
		
		//echo "<pre>";
		//print_r($datacontent['data']);
		//echo "</pre>";	
		$this->load->view('gen_cnf/document/index',$datacontent);
	}
	function actiondelete($id)
	{
		
		$this->T_file->delete($id);
		redirect('document');  
	}
	
	function add()
	{
		$this->load->view('gen_cnf/document/add');
		
	}
	
	function actionadd()
	{
		if(!empty($_FILES)){

				//pengecekan folder ada apa tidak
				$uploaddir 		= $this->config->item('upload_images').'document/';
				$datename 		= date("Y"); 
			
				$tahun = $uploaddir.$datename;
				if(is_dir($tahun)){
				}else{
					mkdir($tahun, 0777, true);
					$fp = fopen($tahun.'/index.html', 'w');
					fwrite($fp , 'Bismillaah');
					fclose($fp);
				}


				$datename2 = date("m"); 
			
				$bulan = $uploaddir.$datename."/".$datename2;
				if(is_dir($bulan)){
				}else{
					mkdir($bulan, 0777, true);
					$fp = fopen($bulan.'/index.html', 'w');
					fwrite($fp , 'Bismillaah');
					fclose($fp);
				}


				$datename3 = date("d"); 
			
				$hari = $uploaddir.$datename."/".$datename2."/".$datename3;
				if(is_dir($hari)){
				}else{
					mkdir($hari, 0777, true);
					$fp = fopen($hari.'/index.html', 'w');
					fwrite($fp , 'Bismillaah');
					fclose($fp);
				}

			}


		if(!empty($_FILES['image']['name'])){
				$image_name =  str_replace(' ','_',date('Ymdhis').$_FILES['image']['name']);
				$config['upload_path'] 		= $hari;
				$config['allowed_types'] 	= 'gif|jpg|png|jpeg|pdf|doc|docx';
				$config['file_name'] 		= $image_name;
				$this->upload->initialize($config);
				$this->upload->do_upload('image');
			}else{
				$image_name = '';
			}
			
		$data = array(
		'nama_file'			=>	$image_name,
		'post_by'			=>	$this->session->userdata('id_adm'),
		'createdate'		=>	date('Y-m-d H:i:s'),
		);
		$this->T_file->add($data);
		redirect('document');
		
	}
	
	function edit($id)
	{
		
		$datacontent['data'] =  $this->T_file->get_file($id);
		$this->load->view('gen_cnf/document/edit', $datacontent);
	}

	function actionedit()
	{
		if(!empty($_FILES['image']['name'])){
				$image_name =  str_replace(' ','_',date('Ymdhis').$_FILES['image']['name']);
				$config['upload_path'] 		= $this->config->item('document');
		$config['allowed_types'] 	= 'gif|jpg|png|jpeg|pdf|doc|docx';
				$config['file_name'] 		= $image_name;
				$this->upload->initialize($config);
				$this->upload->do_upload('image');
			}else{
				$image_name =  $this->input->post('imagex');
			}
		
		$data = array(
			'nama_file'    		=> $image_name
			
		);
		$this->T_file->update($this->input->post('file'),$data);
		redirect('document');
	}
		
}
