<?php
/**
* 
*/
class Banner extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('T_banner');
	}

	function index()
	{
		$data['banner']	=	$this->T_banner->get();
		$this->load->view('gen_cnf/banner/index', $data);
	}

	function add()
	{
		$this->load->view('gen_cnf/banner/add');
	}

	function addbanner()
	{
		$data = array(
			'id_group'		=>	'',
			'judul'			=>	$this->input->post('judul'),
			'keterangan'	=>	$this->input->post('keterangan'),
			'url'			=>	'',
			'gambar'		=>	'',
			'publish'		=>	'Y');
		$this->T_banner->add($data);
		redirect('banner');
	}

	function edit($id)
	{
		$data['banner']	=	$this->T_banner->getid($id);
		$this->load->view('gen_cnf/banner/edit', $data);
	}

	function editbanner()
	{
		$data = array(
			'judul'			=>	$this->input->post('judul'),
			'keterangan'	=>	$this->input->post('keterangan'));
		$this->T_banner->edit($data, $this->input->post('id'));
		redirect('banner');
	}

	function delete($id)
	{
		$this->T_banner->delete($id);
		redirect('banner');
	}

	function banner_rilizen()
	{
		$data['banner']	=	$this->T_banner->getbr();
		$this->load->view('gen_cnf/banner/banner_rilizen', $data);
	}

	function add_br()
	{
		$this->load->view('gen_cnf/banner/add_br');
	}

	function add_actionbr()
	{
		if(!empty($_FILES)){

				//pengecekan folder ada apa tidak
				$uploaddir 		= $this->config->item('upload_images');
				$datename 		= date("Y"); 
			
				$tahun = $uploaddir.$datename;
				if(is_dir($tahun)){
				}else{
					mkdir($tahun, 0777, true);
					$fp = fopen($tahun.'/index.html', 'w');
					fwrite($fp , 'Bismillaah');
					fclose($fp);
				}


				$datename2 = date("m"); 
			
				$bulan = $uploaddir.$datename."/".$datename2;
				if(is_dir($bulan)){
				}else{
					mkdir($bulan, 0777, true);
					$fp = fopen($bulan.'/index.html', 'w');
					fwrite($fp , 'Bismillaah');
					fclose($fp);
				}


				$datename3 = date("d"); 
			
				$hari = $uploaddir.$datename."/".$datename2."/".$datename3;
				if(is_dir($hari)){
				}else{
					mkdir($hari, 0777, true);
					$fp = fopen($hari.'/index.html', 'w');
					fwrite($fp , 'Bismillaah');
					fclose($fp);
				}

			}

			if(!empty($_FILES['image']['name'])){
				$image_name =  str_replace(' ','_',date('Ymdhis').$_FILES['image']['name']);
				$config['upload_path'] 		= $hari ;
				$config['allowed_types'] 	= 'gif|jpg|png|jpeg';
				$config['file_name'] 		= $image_name;
				//$config['min_width'] = '800';
				//$config['min_height'] = '450';
				$this->upload->initialize($config);
				$this->upload->do_upload('image');
			}else{
				$image_name = $this->input->post('image');
			}
		$data = array(
			'nama'			=>	$this->input->post('nama'),
			'postdate'			=>	date('Y-m-d H:i:s'),
			'image'			=>	$image_name);
		$this->T_banner->add_br($data);
		redirect('banner/banner_rilizen');
	}

	function edit_br($id)
	{
		$data['banner']	=	$this->T_banner->getid_br($id);
		$this->load->view('gen_cnf/banner/edit_br', $data);
	}

	function editaction_br()
	{
		if(!empty($_FILES)){

				//pengecekan folder ada apa tidak
				$uploaddir 		= $this->config->item('upload_images');
				$datename 		= date("Y"); 
			
				$tahun = $uploaddir.$datename;
				if(is_dir($tahun)){
				}else{
					mkdir($tahun, 0777, true);
					$fp = fopen($tahun.'/index.html', 'w');
					fwrite($fp , 'Bismillaah');
					fclose($fp);
				}


				$datename2 = date("m"); 
			
				$bulan = $uploaddir.$datename."/".$datename2;
				if(is_dir($bulan)){
				}else{
					mkdir($bulan, 0777, true);
					$fp = fopen($bulan.'/index.html', 'w');
					fwrite($fp , 'Bismillaah');
					fclose($fp);
				}


				$datename3 = date("d"); 
			
				$hari = $uploaddir.$datename."/".$datename2."/".$datename3;
				if(is_dir($hari)){
				}else{
					mkdir($hari, 0777, true);
					$fp = fopen($hari.'/index.html', 'w');
					fwrite($fp , 'Bismillaah');
					fclose($fp);
				}

			}

			if(!empty($_FILES['image']['name'])){
				$image_name =  str_replace(' ','_',date('Ymdhis').$_FILES['image']['name']);
				$config['upload_path'] 		= $hari ;
				$config['allowed_types'] 	= 'gif|jpg|png|jpeg';
				$config['file_name'] 		= $image_name;
				//$config['min_width'] = '800';
				//$config['min_height'] = '450';
				$this->upload->initialize($config);
				$this->upload->do_upload('image');
			}else{
				$image_name = $this->input->post('image');
			}
		$data = array(
			'nama'			=>	$this->input->post('nama'),
			'postdate'		=>	date('Y-m-d H:i:s'),
			'image'			=>	$image_name);
		$this->T_banner->edit_br($data);
		redirect('banner/banner_rilizen');
	}

	function delete_br($id)
	{
		$this->T_banner->delete_br($id);
		redirect('banner/banner_rilizen');
	}
}