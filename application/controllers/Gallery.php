<?php
class gallery extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('T_admin');
		$this->load->model('T_level');
		$this->load->model('T_section');
		$this->load->model('T_gallery');
		//$this->load->model('model_typeuser');
		//$this->load->model('model_group');
		if(!$this->session->userdata('id_adm'))
			redirect('auth');
	}
	
	function index()
	{
		$datacontent['title']	= 'administrator';
		$datacontent['url'] = "administrator/index";
		$datacontent['data'] 	= $this->T_gallery->select();
		//echo "<pre>";
		//print_r($datacontent['data']);
		//echo "</pre>";

		$this->load->view('gen_cnf/gallery/index', $datacontent);
	}

	function add($id=0)
	{
		//$this->output->cache(120);
		$datacontent['parent_id'] 	= $id;
		//$datacontent['tag'] 	= $this->T_tag->select();
		$this->load->view('gen_cnf/gallery/add',$datacontent);
		
	}
	

	function actionadd()
	{
		
		

		
			$data = array(
				'nama_gallery'	    		=> $this->input->post('judul'),
				'post_by'					=> $this->session->userdata('id_adm'),
				'create_date'     			=> date("Y-m-d H:i:s")
				
			);
		
			$this->T_gallery->add($data);
			$lastid = $this->db->insert_id();

			redirect('gallery'); 
				
		  
	}
	
	
	
	
	
	function edit($id=0)
	{
		$datacontent['data'] 	= $this->T_gallery->get($id);
		$datacontent['parent_id'] 	= $id;
		$this->load->view('gen_cnf/gallery/edit',$datacontent);
		
	}



	function actionedit()
	{
		
		$data = array(
			'nama_gallery'	  => $this->input->post('judul'),
		);
		$this->T_gallery->update($this->input->post('parent_id'), $data);
		redirect('gallery'); 
	}

	
	
	function actiondelete($data)
	{

		$this->T_gallery->delete($data);
		redirect('gallery');  
	}
	
	
	function toexcel(){
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="Data-Administrator-'.date("Y-m-d/H-i-s").'.xls"');
        $data["rows"] = $this->model_administrator->select();
        $this->load->view("gen_cnf/gallery/xls", $data);
    }

    function detail($id)
	{
		$data['detail'] = $this->T_gallery->detail($id);
		$this->load->view('gen_cnf/gallery/detail', $data);
	}

	function add_detail($id=0)
	{
		//$this->output->cache(120);
		$datacontent['parent_id'] 	= $id;
		//$datacontent['tag'] 	= $this->T_tag->select();
		$this->load->view('gen_cnf/gallery/add_detail',$datacontent);
		
	}

	function actionadd_detail()
	{
		
		if(!empty($_FILES['image']['name'])){

				//pengecekan folder ada apa tidak
				$uploaddir 		= $this->config->item('upload_images').'images/';
				$datename 		= date("Y"); 
			
				$tahun = $uploaddir.$datename;
				if(is_dir($tahun)){
				}else{
					mkdir($tahun, 0777, true);
					$fp = fopen($tahun.'/index.html', 'w');
					fwrite($fp , 'Bismillaah');
					fclose($fp);
				}


				$datename2 = date("m"); 
			
				$bulan = $uploaddir.$datename."/".$datename2;
				if(is_dir($bulan)){
				}else{
					mkdir($bulan, 0777, true);
					$fp = fopen($bulan.'/index.html', 'w');
					fwrite($fp , 'Bismillaah');
					fclose($fp);
				}


				$datename3 = date("d"); 
			
				$hari = $uploaddir.$datename."/".$datename2."/".$datename3;
				if(is_dir($hari)){
				}else{
					mkdir($hari, 0777, true);
					$fp = fopen($hari.'/index.html', 'w');
					fwrite($fp , 'Bismillaah');
					fclose($fp);
				}

			




				$image_name =  str_replace(' ','_','gallerydetail_'.date('Ymdhis').$_FILES['image']['name']);
				$config['upload_path'] 		= $hari;
				$config['allowed_types'] 	= 'gif|jpg|png|jpeg';
				$config['file_name'] 		= $image_name;
				$this->upload->initialize($config);
				$this->upload->do_upload('image');
			}else{
				$image_name = $this->input->post('imagebrowse');
			}

		
			$data = array(
				'id_detail_gallery'	    		=> $this->input->post('parent_id'),
				'image'							=> $image_name,
				'kredit'						=> $this->input->post('kredit'),
				'postdate'						=> date("Y-m-d H:i:s"),
				'summary'						=> $this->input->post('summary'),
				
			);
		
			$this->T_gallery->add_detail($data);
			$lastid = $this->db->insert_id();

			redirect('gallery/detail/'.$this->input->post('parent_id'), $data); 
				
		  
	}

	function editdetail($id=0,$oyeh)
	{
		$datacontent['detail'] 	= $this->T_gallery->detail_img($oyeh);
		$datacontent['parent_id'] 	= $id;
		//get category
		//echo "<pre>";
		//print_r($datacontent['tagnya']);
			$this->load->view('gen_cnf/gallery/edit_detail',$datacontent);
		
	}

	function actionedit_detail()
	{
		
		if(!empty($_FILES)){

				//pengecekan folder ada apa tidak
				$uploaddir 		= $this->config->item('upload_images').'images/';
				

				$datapath = explode("/", $this->input->post('path'));

				$datename 		= $datapath[1]; 
			
				$tahun = $uploaddir.$datename;
				if(is_dir($tahun)){
				}else{
					mkdir($tahun, 0777, true);
					$fp = fopen($tahun.'/index.html', 'w');
					fwrite($fp , 'Bismillaah');
					fclose($fp);
				}


				$datename2 = $datapath[2]; 
			
				$bulan = $uploaddir.$datename."/".$datename2;
				if(is_dir($bulan)){
				}else{
					mkdir($bulan, 0777, true);
					$fp = fopen($bulan.'/index.html', 'w');
					fwrite($fp , 'Bismillaah');
					fclose($fp);
				}


				$datename3 = $datapath[3]; 
			
				$hari = $uploaddir.$datename."/".$datename2."/".$datename3;
				if(is_dir($hari)){
				}else{
					mkdir($hari, 0777, true);
					$fp = fopen($hari.'/index.html', 'w');
					fwrite($fp , 'Bismillaah');
					fclose($fp);
				}

		}


		if(!empty($_FILES['image']['name'])){
				$image_name =  str_replace(' ','_','gallerydetail_'.date('Ymdhis').$_FILES['image']['name']);
				$config['upload_path'] 		= $hari;
				$config['allowed_types'] 	= 'gif|jpg|png|jpeg';
				$config['file_name'] 		= $image_name;
				$this->upload->initialize($config);
				$this->upload->do_upload('image');
			}else{
				$image_name = $this->input->post('imagex');
			}
	
		$data = array(
				'id_detail_gallery'	    		=> $this->input->post('parent_id'),
				'image'							=> $image_name,
				'kredit'						=>$this->input->post('kredit'),
				'summary'						=>$this->input->post('summary'),
				
			);
		
			$this->T_gallery->update_detail($this->input->post('parent_id2'), $data);

			redirect('gallery/detail/'.$this->input->post('parent_id'), $data); 
	}

	function actiondelete_detail($id,$data)
	{
	
		
		$this->T_gallery->delete_detail($id);
		redirect('gallery/detail/'.$data);  
	}

}
