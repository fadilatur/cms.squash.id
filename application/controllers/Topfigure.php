<?php
class Topfigure extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('T_figure');
		$this->load->model('T_negara');
		$this->load->model('T_supsection');
		$this->load->model('T_section');
		if(!$this->session->userdata('id_adm'))
			redirect('auth');
	}
	
	function index($id)
	{
		$datacontent['title']	= 'figure';
		$datacontent['data'] 	= $this->T_figure->select_group($id);
		//echo "<pre>";
		//print_r($datacontent['data']);
		//echo "</pre>";
		$datacontent['supsection']		= $this->T_supsection->select();
		$datacontent['negara'] 		= $this->T_negara->get_group($id);
		
		
		$this->load->view('gen_cnf/topfigure/index',$datacontent);
	}
	
	

}
