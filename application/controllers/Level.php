<?php
class Level extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('T_level');
		$this->load->model('T_page');
		$this->load->model('T_aksespage');
		$this->load->model('T_section');
		if(!$this->session->userdata('id_adm'))
			redirect('auth');
	}
	
	function index($id=0)
	{
		$datacontent['title']	= 'level';
		$datacontent['url'] 	= "level/index";
		$datacontent['data'] 	= $this->T_level->select($id);
		//echo "<pre>";
		//print_r($datacontent['data']);
		//echo "</pre>";
		$this->load->view('gen_cnf/level/index',$datacontent);
	}
	
	function edit($id)
	{
			$datacontent['title']	= "level";
			$datacontent['url'] 		= "gen_cnf/level/index";
			$this->load->helper('form');
			$datacontent['action'] 		= 'edit';
			$datacontent['page']		= $this->T_page->select();
			//echo "<pre>";
		//print_r($datacontent['page']);
		//echo "</pre>";
			$datacontent['aksespage']	= $this->T_aksespage->select_group($id);
			//echo "<pre>";
		//print_r($datacontent['aksespage']);
		//echo "</pre>";
			$datacontent['data'] 		= $this->T_level->get_group($id);
		
			$this->load->view('gen_cnf/level/edit',$datacontent);
			
	}
	
	function actionedit()
		{
			$datacontent = array(
				'level_nama'    		=> $this->input->post('level_nama'),
			);
			
			$this->T_level->update($this->input->post('id'),$data);
			$this->T_aksespage->delete($this->input->post('id'));
			$ks = $this->input->post('aksespage');
			foreach($ks as $aksespage){
				$data2 = array(
					'id_aksespage'	=> $this->input->post('id'),
					'id_page'	=> $aksespage,
					'level'	=> $aksespage,
					
				);
				$this->T_aksespage->add($data2);
			}
			redirect('level'); 
		}
	
	
	
	function actiondelete($id)
	{
	
		//log admin
				$activity = "Delete";
				$table = "user";
				$last = $id;
				include_once dirname(__FILE__).'/log.php' ;
		//end log admin
		$this->model_administrator->delete($id);
		redirect('level');  
	}

	
	
	
	function toexcel(){
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="Data-Administrator-'.date("Y-m-d/H-i-s").'.xls"');
        $data["rows"] = $this->model_administrator->select();
        $this->load->view("gen_cnf/administrator/xls", $data);
    }

}
