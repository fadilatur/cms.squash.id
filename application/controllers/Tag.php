<?php
class Tag extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('T_tag');
		$this->load->model('T_relasi');
		
		$this->load->model('T_section');
		if(!$this->session->userdata('id_adm'))
			redirect('auth');
	}
	
	function index($pages=0)
	{
		$datacontent['title']	= 'tag';
		$datacontent['url'] 	= "tag/index";
		
		$viewperpage 	= 100;
		$datacontent['data'] 	= $this->T_tag->select();
		$datacontent['data'] 	= $this->T_tag->select_list($pages,$viewperpage);
		$datacontent['pages'] = (!empty($pages))?$pages:0;
		$datacontent['count'] =  $this->T_tag->count();


		$this->load->library('pagination');
            $config['base_url'] 	= site_url('tag/index/');
            $config['total_rows'] 	= $datacontent['count'];
            $config['per_page'] 	= $viewperpage;
             $config['first_tag_open'] 	= '<div class="btn btn-info" type="button">';
            $config['first_tag_close'] 	= '</div>';
            $config['last_tag_open'] 	= '<div class="btn btn-info" type="button">';
            $config['last_tag_close'] 	= '</div>';
             $config['prev_tag_open'] 	= '<div class="btn btn-info" type="button">';
            $config['prev_tag_close'] 	= '</div>';
            $config['next_tag_open'] 	= '<div class="btn btn-info" type="button">';
            $config['next_tag_close'] 	= '</div>';
            $config['num_tag_open'] 	= '<div class="btn btn-info" type="button">';
            $config['num_tag_close'] 	= '</div>';
            $config['cur_tag_open'] 	= '<div class="btn btn-info active" type="button">';
            $config['cur_tag_close'] 	= '</div>';
			$this->pagination->initialize($config);
			$datacontent['paging'] = $this->pagination->create_links();

		$this->load->view('gen_cnf/tag/index',$datacontent);
	}


	function add()
	{
			$datacontent['title']	= "tag";
			$datacontent['url'] 		= "gen_cnf/tag/index";
			$this->load->helper('form');
			$datacontent['action'] 		= 'add';
			
		
			$this->load->view('gen_cnf/tag/add',$datacontent);
			
	}

	function actionadd()
	{
		$data = array(
			'nama_tag'   => $this->input->post('nama_tag'),
			'seo_tag'     => url_title($this->input->post('nama_tag')),
			'id_supsection' => 99
		);
		
		$this->T_tag->add($data);
		redirect('tag');  
	}

	
	function edit($id)
	{
			$datacontent['title']	= "tag";
			$datacontent['url'] 		= "tag/index";
			$this->load->helper('form');
			$datacontent['action'] 		= 'edit';
			
			$datacontent['data'] 		= $this->T_tag->get($id);
		
			$this->load->view('gen_cnf/tag/edit',$datacontent);
			
	}
	
	function actionedit()
		{
			$data = array(
				'nama_tag'    		=> $this->input->post('nama_tag'),
				'seo_tag'    		=> url_title($this->input->post('nama_tag'))
			);
			
			$this->T_tag->update($this->input->post('id'),$data);
			
			redirect('tag'); 
		}
	
	
	function actiondelete($id)
	{
	
		//log admin
			
		//end log admin
		$this->T_tag->delete($id);
		$this->T_relasi->delete2($id,'art_tag');
		redirect('tag');  
	}
	
	function toexcel(){
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="Data-Administrator-'.date("Y-m-d/H-i-s").'.xls"');
        $data["rows"] = $this->model_administrator->select();
        $this->load->view("gen_cnf/administrator/xls", $data);
    }

}
