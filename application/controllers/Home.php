<?php
class Home extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		//die();
		if(!$this->session->userdata('id_adm'))
			redirect('auth');
			$this->output->cache(120);
		$this->load->model('T_section');
	}
	
	function index()
	{
		$data['title']	= 'home';
		$this->load->view('gen_cnf/home/index',$data);
	}
		
}
