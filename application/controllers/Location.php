<?php
class Location extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('T_artikel');
		$this->load->model('T_section');
		if(!$this->session->userdata('id_adm'))
			redirect('auth');
	}
	
	function index()
	{

		$datacontent['data'] 	= $this->T_artikel->selectlocation();
		
		//echo "<pre>";
		//print_r($datacontent['data']);
		//echo "</pre>";	
		$this->load->view('gen_cnf/location/index',$datacontent);
	}
	
	

		
}
