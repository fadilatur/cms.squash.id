<?php
class Auth extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('T_admin');
		//$this->output->cache(120);
	}
	
	function index($msg='')
	{
		$datacontent['error'] = $msg;
		//echo md5('admin');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'name', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		if($this->form_validation->run() == FALSE)
		{
			$this->load->view('gen_cnf/auth/index',$datacontent);
		}
		else
		{
			
			$name = $this->input->post('name');
			//$password = md5($this->input->post('password'));
			
			$userlogin = $this->T_admin->authenticate($name);
			$user_id = (count($userlogin) > 0) ? $userlogin['id_adm'] : 0;
			if( $user_id )
			{
				
				$password = md5($this->input->post('password'));
				$userlogin2 = $this->T_admin->authenticate2($password);
				$user_id2 = (count($userlogin2) > 0) ? $userlogin2['id_adm'] : 0;
				if( $user_id2 ){
				$data_session = array(
									'email' 		=> $userlogin['email'],
									'id_adm'   => $userlogin['id_adm'],
									'nama'   => $userlogin['nama'],
									'foto'   => $userlogin['foto_admin'],
									'level'   => $userlogin['level'],
									'daerah'  => $userlogin['daerah']
									
								);
				//echo "<pre>";
		//print_r($data_session);
		//echo "</pre>";
		//die();
				$this->session->set_userdata($data_session);
				//log admin
				$activity = "Login";
				$table = "-";
				$last = "-";
				include_once dirname(__FILE__).'/log.php' ;
				//end log admin
				redirect('home');
				}else{
				$datacontent['error'] = 'error';
				$this->load->view('gen_cnf/auth/index',$datacontent);
				}
			}
			else
			{
				$datacontent['error'] = 'error';
				$this->load->view('gen_cnf/auth/index',$datacontent);
			}
		}
	}
	
	function logout()
	{
		$this->session->sess_destroy();
        redirect('auth');
	}
		
}
