<?php
class Contact extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('T_artikel');
		$this->load->model('T_section');
		$this->load->model('T_contact');
		if(!$this->session->userdata('id_adm'))
			redirect('auth');
	}
	
	function index()
	{

		$datacontent['data'] 	= $this->T_contact->select();
		$this->load->view('gen_cnf/contact/index',$datacontent);
	}
	
	function actiondelete($id)
	{
		
		$this->T_contact->delete($id);
		redirect('contact');  
	}

	function reply($id)
	{
	
		$datacontent['id'] = $id;
		$datacontent['reply'] =  $this->T_contact->get_contact($id);
		if(!empty($_POST)){	
			$asalemail = "rilisjakarta@gmail.com";
			$email = $this->input->post('email');
			$message = $this->input->post('detail');
			 /*//echo $databam['EMAIL'];
					$to      = $email;
					$subject = 'Mitra Netra';
					$message = 'Email : '.$asalemail.'<br> Subject : '.$subject1.'<br> Message : '.$message;
						'X-Mailer: PHP/' . phpversion();
					
					@mail($to, $subject, $message); */
					
					
			
		$config = Array(
						'protocol' => 'smtp',
						'charset'   => 'iso-8859-1',
						'mailtype'  => 'html'
					);
		$this->load->library('email', $config);
		$this->email->from($asalemail,'Male Indonesia ');
		$this->email->to($email);
		$this->email->subject('Reply From RILIS.ID - '.$datacontent['reply']['subject']);
		$isi = $message;
		$this->email->message($isi);
		$this->email->send();

			redirect('contact');  
		}
		$this->load->view('gen_cnf/contact/reply',$datacontent);
		
		
	}
		
}
