<?php
class TagTest extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('T_artikel');
		$this->load->model('T_admin');
		$this->load->model('T_section');
		$this->load->model('T_supsection');
		$this->load->model('T_relasi');
		$this->load->model('T_tag');
		date_default_timezone_set('Asia/Jakarta');
		if(!$this->session->userdata('id_adm'))
			redirect('auth');
	}
	
	function index($id=0,$pages=0)
	{

		//echo date('Y-m-d H:i:s');
		$datacontent['title']	= 'artikel';
		$viewperpage 	= 1000;
		$datacontent['parent_id'] 	= $id;

		
		$datacontent['tag'] 	= $this->T_tag->select();

		

		$datacontent['search'] = $this->input->post('search');
		$datacontent['status'] = $this->input->post('status');
		
	
		$datacontent['data'] 	= 	$this->T_artikel->selectstatus2($pages, $viewperpage);
		$datacontent['count'] 	=  	$this->T_artikel->countstatus2();


			
		

		 $datacontent['pages'] = (!empty($pages))?$pages:0;
		$this->load->library('pagination');
            $config['base_url'] 	= site_url('tagtest/index/'.$id.'/');
            $config['total_rows'] 	= $datacontent['count'];
            $config['per_page'] 	= $viewperpage;
             $config['first_tag_open'] 	= '<div class="btn btn-info" type="button">';
            $config['first_tag_close'] 	= '</div>';
            $config['last_tag_open'] 	= '<div class="btn btn-info" type="button">';
            $config['last_tag_close'] 	= '</div>';
             $config['prev_tag_open'] 	= '<div class="btn btn-info" type="button">';
            $config['prev_tag_close'] 	= '</div>';
            $config['next_tag_open'] 	= '<div class="btn btn-info" type="button">';
            $config['next_tag_close'] 	= '</div>';
            $config['num_tag_open'] 	= '<div class="btn btn-info" type="button">';
            $config['num_tag_close'] 	= '</div>';
            $config['cur_tag_open'] 	= '<div class="btn btn-info active" type="button">';
            $config['cur_tag_close'] 	= '</div>';
			$this->pagination->initialize($config);
			$datacontent['tag'] 	= $this->T_tag->select();
			$datacontent['paging'] = $this->pagination->create_links();

			$datacontent['member'] 	= $this->T_admin->select();


		echo $datacontent['paging'];
		//echo "<pre>";
		//print_r($datacontent['data']);
		//echo "</pre>";
		//$this->load->view('gen_cnf/artikel/index',$datacontent);
	}
	

}
