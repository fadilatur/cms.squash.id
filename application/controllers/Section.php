<?php
class Section extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('T_section');
		if(!$this->session->userdata('id_adm'))
			redirect('auth');
	}
	
	function index($id=0)
	{
		$datacontent['title']	= 'section';
		$datacontent['id'] = $id;
		$datacontent['url'] 	= "section/index/".$id;
		//echo "<pre>";
		//print_r($datacontent['url']);
		//echo "</pre>";
		//$datacontent['menuid'] 	= $menu;
		$datacontent['data'] 	= $this->T_section->selectadmin($id);
		
		$this->load->view('gen_cnf/subcategory/index',$datacontent);
	}
	
	function add($id=0)
	{
		$datacontent['title']	= 'section';
		$datacontent['url'] = "section/index".$id;
		$this->load->helper('form');
		$datacontent['action'] 	= 'add';
		$datacontent['id']	 = $id;
		$this->load->view('gen_cnf/subcategory/add',$datacontent);
	}
	
	function actionadd($id)
	{
		$data = array(
			'nama_section'   => $this->input->post('nama_section'),
			'id_supsection'    => $id,
			'status'    => $this->input->post('status')
		);
		
		$this->T_section->add($data);
		redirect('section/index/'.$id);  
	}


	function edit($id)
	{
		$datacontent['title']	= 'section';
		$datacontent['url'] = "section/index";
		$datacontent['action'] 		= 'edit';
		$datacontent['data'] 		= $this->T_section->get($id);
		$this->load->view('gen_cnf/subcategory/edit',$datacontent);
	}	


	function actionedit($id)
	{
	
		$data = array(
			'nama_section'   => $this->input->post('nama_supsection'),
			'status'     			=> $this->input->post('status')
			
		);
		$this->T_section->update($this->input->post('id'),$data);
		
		redirect('section/index/'.$id); 
	}
	
	function actiondelete($id)
	{
	
		
		$this->model_administrator->delete($id);
		redirect('administrator');  
	}
	
	function toexcel(){
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="Data-Administrator-'.date("Y-m-d/H-i-s").'.xls"');
        $data["rows"] = $this->model_administrator->select();
        $this->load->view("gen_cnf/administrator/xls", $data);
    }

}
