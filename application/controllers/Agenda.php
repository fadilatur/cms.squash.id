<?php
/**
* 
*/
class Agenda extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('T_agenda');
	}

	function index()
	{
		$data['banner']	=	$this->T_agenda->get();
		$this->load->view('gen_cnf/agenda/index', $data);
	}

	function add()
	{
		$this->load->view('gen_cnf/agenda/add');
	}

	function addaction()
	{
		$data = array(
			
			'tanggal'			=>	$this->input->post('tgl'),
			'waktu'				=>	$this->input->post('jam').":".$this->input->post('menit'),
			'lokasi'			=>	$this->input->post('lokasi'),
			'kegiatan'			=>	$this->input->post('kegiatan'),
			'status'			=>	$this->input->post('status'),
			'id_admin'    	  	=> $this->session->userdata('id_adm'),

			);
		$this->T_agenda->add($data);
		redirect('agenda');
	}

	function edit($id)
	{
		$data['banner']	=	$this->T_agenda->getid($id);
		$this->load->view('gen_cnf/agenda/edit', $data);
	}

	function editbanner()
	{
		$data = array(
			
			'tanggal'			=>	$this->input->post('tgl'),
			'waktu'				=>	$this->input->post('jam').":".$this->input->post('menit'),
			'lokasi'			=>	$this->input->post('lokasi'),
			'kegiatan'			=>	$this->input->post('kegiatan'),
			'status'			=>	$this->input->post('status'),
			'id_admin'    	  	=> $this->session->userdata('id_adm'),

			);
		$this->T_agenda->edit($data, $this->input->post('id'));
		redirect('agenda');
	}

	function delete($id)
	{
		$this->T_agenda->delete($id);
		redirect('agenda');
	}
}