<?php
/**
* 
*/
class Rilizen extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('T_artikel_rilizen');
		$this->load->model('T_message_rilizen');
		$this->load->model('T_artikel');
		$this->load->model('T_user');
		$this->load->model('T_user_detail');
	}

	function article()
	{
		$data['article'] = $this->T_artikel_rilizen->get();
		$this->load->view('gen_cnf/rilizen/index', $data);
	}

	function approve($id)
	{
		$data = $this->T_artikel_rilizen->getid($id);

		$data2 = array(
			'id_admin'			=>	$data['id_admin'],
			'id_section'		=>	$data['id_section'],
			'judul_artikel'		=> 	$data['judul_artikel'],
			'seo_artikel'		=>	$data['seo_artikel'],
			'isi_artikel'		=>	$data['isi_artikel'],
			'gagasan_utama'		=>	$data['gagasan_utama'],
			'kota'				=>	$data['kota'],
			'hari'				=>	$data['hari'],
			'tanggal'			=>	$data['tanggal'],
			'jam'				=>	$data['jam'],
			'tgl_pub'			=>	$data['tgl_pub'],
			'dibaca'			=>	$data['dibaca'],
			'meta_key'			=>	$data['meta_key'],
			'meta_des'			=>	$data['meta_des'],
			'headline'			=>	'N',
			'hot'				=>	'N',
			'publish'			=>	'Y',
			'sponsored'			=>	'N',
			'rilizen'			=>	'Y',
			'fokus'				=>	'',
			'kolom'				=>	'',
			'thumbnail'			=>	$data['thumbnail'],
			'ket_thumbnail'		=>	$data['ket_thumbnail'],
			'id_video'			=>	$data['id_video'],
			'comment'			=>	$data['comment'],
			'url'				=>	$data['url'],
			'parent_id'			=>	$data['parent_id'],
			'postdate'			=>	$data['postdate'],
			'apps'				=>	$data['apps'],
			'oldid'				=>	$data['oldid'],
			'oldpenulis'		=>	$data['oldpenulis'],
			'oldeditor'			=>	$data['oldeditor'],
			'oldtags'			=>	$data['oldtags'],
			'oldimage'			=>	$data['oldimage'],
			'thumb'				=>	$data['thumb'],
			'slider'			=>	$data['slider'],
			'position'			=>	$data['position'],
			'urltitle'			=>	$data['urltitle']
		);
		$this->T_artikel->add($data2);

		$data3 = array('publish' => 'Y');
		$this->T_artikel_rilizen->updatestatus($data3, $id);


		$message = array(
			'user_id'	=>	$data['id_admin'],
			'message'	=>	'Artikel anda <i><b>'.$data['judul_artikel'].'</b></i> sudah kami terima, untuk selanjutnya akan kami publikasikan.',
			'postdate'	=>	date('Y-m-d H:i:s'),
			'status'	=>	0);
		$this->T_message_rilizen->add($message);
		redirect('rilizen/article');
	}

	function deny($id)
	{
		$data = $this->T_artikel_rilizen->getid($id);
		$message = array(
			'user_id'	=>	$data['id_admin'],
			'message'	=>	'Artikel anda <i><b>'.$data['judul_artikel'].'</b></i> tidak lolos uji review sehingga tidak dapat kami publikasikan.',
			'postdate'	=>	date('Y-m-d H:i:s'),
			'status'	=>	0);
		$this->T_message_rilizen->add($message);

		$data2 = array('publish' => 'T');
		$this->T_artikel_rilizen->updatestatus($data2, $id);
		redirect('rilizen/article');
	}

	function user()
	{
		$data['user']	=	$this->T_user->get();
		$this->load->view('gen_cnf/rilizen/user', $data);
	}

	function add()
	{
		$this->load->view('gen_cnf/rilizen/add');
	}


	function addprofile()
	{
		if(!empty($_FILES['image']['name'])){
			$image_name =  str_replace(' ','_',date('Ymdhis').$_FILES['image']['name']);
			$config['upload_path'] 		= $this->config->item('upload_rilizen') ;
			$config['allowed_types'] 	= 'gif|jpg|png|jpeg';
			$config['file_name'] 		= $image_name;
			$this->upload->initialize($config);
			$this->upload->do_upload('image');
		}else{
			$image_name = $this->input->post('image');
		}

		if(!empty($_FILES['banner']['name'])){
			$image_name_banner =  str_replace(' ','_',date('Ymdhis').$_FILES['banner']['name']);
			$config['upload_path'] 		= $this->config->item('upload_rilizen') ;
			$config['allowed_types'] 	= 'gif|jpg|png|jpeg';
			$config['file_name'] 		= $image_name_banner;
			$this->upload->initialize($config);
			$this->upload->do_upload('banner');
		}else{
			$image_name_banner = $this->input->post('banner');
		}

		$data = array(
			'nama_lengkap'	=>	$this->input->post('nama'),
			'email'			=>	$this->input->post('email'));
		$this->T_user->add($data);
		$id = $this->db->insert_id();
		$data2 = array(
			'user_id'		=>	$id,
			'phone'			=>	'',
			'image'			=>	$image_name,
			'banner'		=>	$image_name_banner,
			'birthday'		=>	$this->input->post('birthday'),
			'place'			=>	$this->input->post('place'),
			'facebook'		=>	$this->input->post('facebook'),
			'twitter'		=>	$this->input->post('twitter'),
			'instagram'		=>	$this->input->post('instagram'),
			'gplus'			=>	$this->input->post('gplus'),
			'address'		=>	$this->input->post('address'),
			'description'	=>	$this->input->post('description')
		);
		$this->T_user_detail->add($data2);
		//redirect('rilizen/user');
	}


	function edit($id)
	{
		$data['user'] = $this->T_user->getid($id);
		$this->load->view('gen_cnf/rilizen/edit', $data);
	}

	function editprofile()
	{
		if(!empty($_FILES['image']['name'])){
			$image_name =  str_replace(' ','_',date('Ymdhis').$_FILES['image']['name']);
			$config['upload_path'] 		= $this->config->item('upload_rilizen') ;
			$config['allowed_types'] 	= 'gif|jpg|png|jpeg';
			$config['file_name'] 		= $image_name;
			$this->upload->initialize($config);
			$this->upload->do_upload('image');
		}else{
			$image_name = $this->input->post('image');
		}

		if(!empty($_FILES['banner']['name'])){
			$image_name_banner =  str_replace(' ','_',date('Ymdhis').$_FILES['banner']['name']);
			$config['upload_path'] 		= $this->config->item('upload_rilizen') ;
			$config['allowed_types'] 	= 'gif|jpg|png|jpeg';
			$config['file_name'] 		= $image_name_banner;
			$this->upload->initialize($config);
			$this->upload->do_upload('banner');
		}else{
			$image_name_banner = $this->input->post('banner');
		}

		$data = array(
			'nama_lengkap'	=>	$this->input->post('nama'),
			'email'			=>	$this->input->post('email'));
		$this->T_user->edit($data, $this->input->post('id'));

		$data2 = array(
			'phone'			=>	$this->input->post('phone'),
			'image'			=>	$image_name,
			'banner'		=>	$image_name_banner,
			'birthday'		=>	$this->input->post('birthday'),
			'place'			=>	$this->input->post('place'),
			'facebook'		=>	$this->input->post('facebook'),
			'twitter'		=>	$this->input->post('twitter'),
			'instagram'		=>	$this->input->post('instagram'),
			'gplus'			=>	$this->input->post('gplus'),
			'address'		=>	$this->input->post('address'),
			'description'	=>	$this->input->post('description')
		);
		$this->T_user_detail->edit($data2, $this->input->post('id'));
		redirect('rilizen/user');
	}

	function delete($id)
	{
		$this->T_user->delete($id);
		$this->T_user_detail->delete($id);
		redirect('rilizen/user');
	}
}