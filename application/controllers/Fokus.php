<?php 
class Fokus extends ci_controller{

	function __construct()
	{
		parent::__construct();
		$this->load->model('T_fokus');
		$this->load->model('T_admin');
		date_default_timezone_set('Asia/Jakarta');
		if(!$this->session->userdata('id_adm'))
			redirect('auth');
	}

	function index(){

		$data['data'] = $this->T_fokus->select();
		$this->load->view ('gen_cnf/fokus/index', $data);
	}

	function add(){
		$this->load->view ('gen_cnf/fokus/add');
	}

	function actionadd(){
		if(!empty($_FILES)){

				//pengecekan folder ada apa tidak
				$uploaddir 		= $this->config->item('upload_images').'images/';
				$datename 		= date("Y"); 
			
				$tahun = $uploaddir.$datename;
				if(is_dir($tahun)){
				}else{
					mkdir($tahun, 0777, true);
					$fp = fopen($tahun.'/index.html', 'w');
					fwrite($fp , 'Bismillaah');
					fclose($fp);
				}


				$datename2 = date("m"); 
			
				$bulan = $uploaddir.$datename."/".$datename2;
				if(is_dir($bulan)){
				}else{
					mkdir($bulan, 0777, true);
					$fp = fopen($bulan.'/index.html', 'w');
					fwrite($fp , 'Bismillaah');
					fclose($fp);
				}


				$datename3 = date("d"); 
			
				$hari = $uploaddir.$datename."/".$datename2."/".$datename3;
				if(is_dir($hari)){
				}else{
					mkdir($hari, 0777, true);
					$fp = fopen($hari.'/index.html', 'w');
					fwrite($fp , 'Bismillaah');
					fclose($fp);
				}

			}

			if(!empty($_FILES['image']['name'])){
				$image_name =  str_replace(' ','_',date('Ymdhis').$_FILES['image']['name']);
				$config['upload_path'] 		= $hari ;
				$config['allowed_types'] 	= 'gif|jpg|png|jpeg';
				$config['file_name'] 		= $image_name;
				$config['min_width'] = '800';
				$config['min_height'] = '450';
				$this->upload->initialize($config);
				$this->upload->do_upload('image');
			}else{
				$image_name = $this->input->post('image');
			}
		$data = array(
				'judul'	    			=> $this->input->post('judul'),
				'postdate'    	  		=> date('Y-m-d H:i:s'),
				'image'     			=> $image_name,
				'summary'				=> $this->input->post('summary'),
				'post_by'    	  	  	=> $this->session->userdata('id_adm')
				
			);
			$this->T_fokus->add($data);
			$lastid = $this->db->insert_id();
			redirect('fokus');
	}

	function edit($id){

		$data['data'] = $this->T_fokus->get_fokus($id);
		$this->load->view ('gen_cnf/fokus/edit', $data);
	}

	function actionedit(){
		if(!empty($_FILES)){

				//pengecekan folder ada apa tidak
				$uploaddir 		= $this->config->item('upload_images').'images/';
				

				$datapath = explode("/", $this->input->post('path'));

				$datename 		= $datapath[1]; 
			
				$tahun = $uploaddir.$datename;
				if(is_dir($tahun)){
				}else{
					mkdir($tahun, 0777, true);
					$fp = fopen($tahun.'/index.html', 'w');
					fwrite($fp , 'Bismillaah');
					fclose($fp);
				}


				$datename2 = $datapath[2]; 
			
				$bulan = $uploaddir.$datename."/".$datename2;
				if(is_dir($bulan)){
				}else{
					mkdir($bulan, 0777, true);
					$fp = fopen($bulan.'/index.html', 'w');
					fwrite($fp , 'Bismillaah');
					fclose($fp);
				}


				$datename3 = $datapath[3]; 
			
				$hari = $uploaddir.$datename."/".$datename2."/".$datename3;
				if(is_dir($hari)){
				}else{
					mkdir($hari, 0777, true);
					$fp = fopen($hari.'/index.html', 'w');
					fwrite($fp , 'Bismillaah');
					fclose($fp);
				}

			}


		if(!empty($_FILES['image']['name'])){
				$image_name =  str_replace(' ','_',date('Ymdhis').$_FILES['image']['name']);
				$config['upload_path'] 		= $hari;
				$config['allowed_types'] 	= 'gif|jpg|png|jpeg';
				$config['file_name'] 		= $image_name;
				$config['min_width'] = '800';
				$config['min_height'] = '450';
				$this->upload->initialize($config);
				$this->upload->do_upload('image');
			}else{
				$image_name = $this->input->post('image');
			}

		$data = array(
			'judul'		=>	$this->input->post('judul'),
			'image'     => $image_name,
			'summary'	=> $this->input->post('summary')

			);
		$this->T_fokus->update($this->input->post('id'),$data);
		
		redirect('fokus/index/');
	}

	function actiondelete($id)
	{
		
		$this->T_fokus->delete($id);
		redirect('fokus/index/');  
	}
}