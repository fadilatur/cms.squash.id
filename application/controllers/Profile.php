<?php
class profile extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('T_admin');
		$this->load->model('T_level');
		$this->load->model('T_section');
		$this->load->model('T_halamanstatis');
		date_default_timezone_set('Asia/Jakarta');
		//$this->load->model('model_typeuser');
		//$this->load->model('model_group');
		if(!$this->session->userdata('id_adm'))
			redirect('auth');
	}
	
	function index($daerah)
	{
		$datacontent['title']	= 'administrator';
		$datacontent['url'] = "administrator/index";
		$datacontent['data'] 	= $this->T_halamanstatis->select($daerah);
		// $datacontent['daerah']	= $daerah;
		$datacontent['parent_id'] 	= $daerah;
		//echo "<pre>";
		//print_r($datacontent['data']);
		//echo "</pre>";

		$this->load->view('gen_cnf/profile/index',$datacontent);
	}

	function add($id=0)
	{
		//$this->output->cache(120);
		$datacontent['parent_id'] 	= $id;
		//$datacontent['tag'] 	= $this->T_tag->select();
		$this->load->view('gen_cnf/profile/add',$datacontent);
		
	}
	

	function actionadd()
	{
		
		
		if(!empty($_FILES['image']['name'])){
				$image_name =  str_replace(' ','_',date('Ymdhis').$_FILES['image']['name']);
				$config['upload_path'] 		= $this->config->item('upload_images');
				$config['allowed_types'] 	= 'gif|jpg|png|jpeg';
				$config['file_name'] 		= $image_name;
				$this->upload->initialize($config);
				$this->upload->do_upload('image');
			}else{
				$image_name = '';
			}
	
		$parent = $this->input->post('parent_id');

		
			$data = array(
				'judul_hs'	    		=> $this->input->post('judul'),
				'konten_hs'     		=> $this->input->post('detail'),
				'seo_hs'    	  		=> $this->input->post('gagasan'),
				'meta_title'			=> $this->input->post('meta_title'),
				'meta_key'				=> $this->input->post('meta_key'),
				'meta_des'				=> $this->input->post('meta_des'),
				'tanggal_hs'     		=> $this->input->post('tgl')." ".$this->input->post('jam').":".$this->input->post('menit').":00",
				'titleurl'				=> url_title($this->input->post('judul')),
				'daerah'				=> $parent
				
			);
		
			$this->T_halamanstatis->add($data);
			$lastid = $this->db->insert_id();

			redirect('profile/index/'.$parent); 
	}
	
	
	function cpassword($id)
	{
		$datacontent['title']	= 'administrator';
		$datacontent['url'] = "administrator/index";
		$this->load->helper('form');
		$datacontent['action'] 		= 'cpassword';
		$datacontent['data'] 		= $this->T_admin->get_tadmin($id);
		$this->load->view('gen_cnf/administrator/cpassword',$datacontent);
	}
	
	function actioncpassword()
	{
		$password = md5($this->input->post('password'));
		$data = array(
			'password'     		=> $password,
		);			
		$this->T_admin->update($this->input->post('id'),$data);
		//$this->db->cache_delete('gen_cnf', 'administrator');
		redirect('administrator/'); 
	}
	
	
	function edit($id=0)
	{
		$datacontent['data'] 	= $this->T_halamanstatis->get($id);
		//get category
		//echo "<pre>";
		//print_r($datacontent['tagnya']);
			$this->load->view('gen_cnf/profile/edit',$datacontent);
		
	}



	function actionedit()
	{
		
		if(!empty($_FILES['image']['name'])){
				$image_name =  str_replace(' ','_',date('Ymdhis').$_FILES['image']['name']);
				$config['upload_path'] 		= $this->config->item('upload_images');
				$config['allowed_types'] 	= 'gif|jpg|png|jpeg';
				$config['file_name'] 		= $image_name;
				$this->upload->initialize($config);
				$this->upload->do_upload('image');
			}else{
				$image_name = '';
			}
	
		$parent = $this->input->post('parent_id');

		
			$data = array(
				'judul_hs'	    		=> $this->input->post('judul'),
				'konten_hs'     		=> $this->input->post('detail'),
				'seo_hs'    	  		=> $this->input->post('gagasan'),
				'meta_title'			=> $this->input->post('meta_title'),
				'meta_key'				=> $this->input->post('meta_key'),
				'meta_des'				=> $this->input->post('meta_des'),
				'tanggal_hs'     		=> $this->input->post('tgl')." ".$this->input->post('jam').":".$this->input->post('menit').":00",
				'titleurl'                      => url_title($this->input->post('judul')),
				'daerah'				=>$this->input->post('daerah')
			);
		
			$this->T_halamanstatis->update($this->input->post('id_hs'), $data);

			redirect('profile/index/'.$this->input->post('daerah')); 
}

	
	
	function actiondelete($data, $id)
	{
	
		
		$this->T_halamanstatis->delete($data);
		redirect('profile/index/'.$id);  
	}
	
	
	function toexcel(){
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="Data-Administrator-'.date("Y-m-d/H-i-s").'.xls"');
        $data["rows"] = $this->model_administrator->select();
        $this->load->view("gen_cnf/administrator/xls", $data);
    }

}
