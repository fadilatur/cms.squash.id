<?php
class Notifikasi extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('T_artikel');
		$this->load->model('T_notifikasi');
		$this->load->model('T_admin');
		$this->load->model('T_section');
		$this->load->model('T_supsection');
		date_default_timezone_set('Asia/Jakarta');
		
	}
	
	function index()
	{	if(!$this->session->userdata('id_adm'))
			redirect('auth');
		$datacontent['data']=$this->T_notifikasi->select('');
		$this->load->view('gen_cnf/notifikasi/index',$datacontent);
	}
	
	function add($id=0)
	{
		if(!$this->session->userdata('id_adm'))
			redirect('auth');
		$datacontent['id_notifikasi'] 	= $id;
		$this->load->view('gen_cnf/notifikasi/add',$datacontent);
		
	}
	
	function actionadd()
	{
		if(!$this->session->userdata('id_adm'))
			redirect('auth');
			$data = array(
				'title'	    			=> $this->input->post('title'),
				'url'    	  			=> $this->input->post('url'),
				'content '     			=> $this->input->post('content'),
				'tanggal'     			=> $this->input->post('tanggal'),
				'jam'     				=> $this->input->post('jam').":".$this->input->post('menit').":00",
				'status'	    		=> 0,
				
			);
			$this->db->insert('t_notifikasi',$data);
			redirect('notifikasi/index/');


			}
	function edit($id)
	{
		if(!$this->session->userdata('id_adm'))
			redirect('auth');
		$datacontent['data'] = $this->T_notifikasi->edits($id);
		$this->load->view('gen_cnf/notifikasi/edit',$datacontent);
	}
	function actionedit(){
		if(!$this->session->userdata('id_adm'))
			redirect('auth');
		$data = array(
				'title'	    			=> $this->input->post('title'),
				'url'    	  			=> $this->input->post('url'),
				'content '     			=> $this->input->post('content'),
				'tanggal'     			=> $this->input->post('tanggal'),
				'status'	    		=> $this->input->post('status'),
				'jam'     				=> $this->input->post('jam').":".$this->input->post('menit').":00"
				
			);
			$this->T_notifikasi->actionedit($this->input->post('id_notifikasi'),$data);
			redirect('notifikasi/index/');

	}
	

		function delete($id)
	{
		if(!$this->session->userdata('id_adm'))
			redirect('auth');
		$this->T_notifikasi->delete($id);
		redirect('notifikasi/index/');  
	}


	function pushnotif()
	{
		$data = $this->T_notifikasi->getpush();
		if(count($data) > 0){
			foreach($data as $data)
			{
				$heading = array(
					"en" => strip_tags($data['title']),
					);
				$content = array(
					"en" => strip_tags($data['content']),
					);
				
				$fields = array(
					'app_id' => "e4ea1e4b-2c60-4401-b958-db83b5573694",
					'included_segments' => array('All'),
					'headings' => $heading,
					'contents' => $content,
					"url" => $data['url'],
				);
				
				$fields = json_encode($fields);
				
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
				curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
														   'Authorization: Basic ZTU1YzEwMWUtOWMwMS00YTg3LTg0YmItNGIwYTE5Y2ZiMjlm'));
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
				curl_setopt($ch, CURLOPT_HEADER, FALSE);
				curl_setopt($ch, CURLOPT_POST, TRUE);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
	
				$response = curl_exec($ch);
				curl_close($ch);
				
				if($response){
					$this->T_notifikasi->updatepush($data['id_notifikasi']);
					$status = 'SUKSES push ke onesignal pada '.date("Y-m-d H:i:s").' - '.$data['title']."<br>\n";
				}else{
					$status = 'GAGAL push ke onesignal pada '.date("Y-m-d H:i:s").' - '.$data['title']."<br>\n";
				}
				error_log($status, 3, '/tmp/pushnotif.log');
			}
		}else{
			echo 'Tidak ada jadwal push';
		}
	}	
	

}
