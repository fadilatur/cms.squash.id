<?php
class Image extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('T_folder');
		$this->load->model('T_folder_image');
		$this->load->model('T_admin');
		$this->load->model('T_watermark');
		$this->load->library('image_lib');
		if(!$this->session->userdata('id_adm'))
			redirect('auth');
	}
	
	function index($id=0)
	{
		$data['identifier']	=	$id;
		$data['searchword']	=	$this->input->post('search');
		$data['title'] 		=	$this->T_folder->getfolder($id);
		if(!empty($this->input->post('search'))) 
		{
			$data['folder'] = $this->T_folder->getparentsearch($id, $this->input->post('search'));
			$data['image']	= $this->T_folder_image->getparentimagesearch($id, $this->input->post('search'));
		}
		else
		{
			$data['folder'] = $this->T_folder->getparent($id);
			$data['image']	= $this->T_folder_image->getparentimage($id);
		}

	
		$this->load->view('gen_cnf/image/index', $data);
	}

	function more($j, $id)
	{
		$data['no']		= $j;
		$data['image']	= $this->T_folder_image->getparentimagemore($j,$id);
		$this->load->view('gen_cnf/image/more', $data);
	}
	
	
	function add($id=0)
	{
		$this->load->view('gen_cnf/image/add');
	}	

	function actionadd()
	{
		$data	=	array(
			'nama'			=>	$this->input->post('judul'),
			'parent_id'		=>	$this->input->post('parent_id'),
			'admin_id'		=>	$this->session->userdata('id_adm'),
			'create_date'	=>	date('Y-m-d H:i:s'));
		$this->T_folder->add($data);
		redirect('image/index/'.$this->input->post('parent_id'));
	}

	function edit($id, $url)
	{
		$data['folder'] = $this->T_folder->getfolder($id);
		$this->load->view('gen_cnf/image/edit', $data);
	}

	function actionedit()
	{
		$data = array('nama' => $this->input->post('judul'));
		$this->T_folder->edit($data, $this->input->post('id'));
		redirect('image/index/'.$this->input->post('url'));
	}

	function delete($id, $url)
	{
		$this->T_folder->delete($id);
		redirect('image/index/'.$url);
	}

	function addimage($id)
	{
		$data['watermark']	=	$this->T_watermark->get();
		$this->load->view('gen_cnf/image/add_detail', $data);
	}

	function actionaddimage()
	{
		if(!empty($_FILES)){

			//pengecekan folder ada apa tidak
			$uploaddir 		= $this->config->item('upload_images');

			$datename 		= date("Y"); 
		
			$tahun = $uploaddir.$datename;
			if(is_dir($tahun)){
			}else{
				mkdir($tahun, 0777, true);
				$fp = fopen($tahun.'/index.html', 'w');
				fwrite($fp , 'Bismillaah');
				fclose($fp);
			}


			$datename2 = date("m"); 
		
			$bulan = $uploaddir.$datename."/".$datename2;
			if(is_dir($bulan)){
			}else{
				mkdir($bulan, 0777, true);
				$fp = fopen($bulan.'/index.html', 'w');
				fwrite($fp , 'Bismillaah');
				fclose($fp);
			}


			$datename3 = date('d'); 
		
			$hari = $uploaddir.$datename."/".$datename2."/".$datename3;
			if(is_dir($hari)){
			}else{
				mkdir($hari, 0777, true);
				$fp = fopen($hari.'/index.html', 'w');
				fwrite($fp , 'Bismillaah');
				fclose($fp);
			}

		}

		
		$getwatermark = $this->T_watermark->getid($this->input->post('watermark'));

		$explode = explode('-', $this->input->post('position'));
		
		if(!empty($this->input->post('watermark'))) {	

				if(!empty($_FILES['image']['name'])){
					$image_name =  str_replace(' ','_',date('Ymdhis').$_FILES['image']['name']);
					$config['upload_path'] 		= $hari;
					$config['allowed_types'] 	= 'gif|jpg|png|jpeg';
					$config['file_name'] 		= $image_name;
					$this->upload->initialize($config);
					$this->upload->do_upload('image');

					$image = 'watermark'.$image_name;

					$config['image_library'] 	= 'gd2';
					$config['source_image'] 	= $hari.'/'.$image_name;
					$config['new_image'] 		= $hari.'/'.$image;
					$config['wm_type'] 			= 'overlay';
					$config['wm_overlay_path'] 	= $this->config->item('watermark'). $getwatermark['gambar'];
					$config['wm_opacity'] = 50;
					$config['wm_vrt_alignment'] = $explode[0];
					$config['wm_hor_alignment'] = $explode[1];
					$config['wm_padding'] = '';
					$this->image_lib->initialize($config);

					$this->image_lib->watermark();

					$config['image_library'] = 'gd2';
					$config['source_image'] 	= $hari.'/'.$image;
					$config['create_thumb'] = FALSE;
					$config['maintain_ratio'] = TRUE;
					$config['width']         = 850;
					$config['height']       = auto;

					$this->image_lib->initialize($config);

					$this->image_lib->resize();

				}else{
					$image_name = $this->input->post('image');
				}

			$data = array(
				'folder_id'		=>	$this->input->post('parent_id'),
				'image'			=>	$image_name,
				'image_watermark'=>	$image,
				'watermark_id'	=>	$getwatermark['id'],
				'title'			=>	$this->input->post('title'),
				'caption'		=>	$this->input->post('caption'),
				'description'	=>	$this->input->post('description'),
				'postdate'		=>	date('Y-m-d H:i:s'),
				'position'		=>	$this->input->post('position'));
		} 
		else
		{
			if(!empty($_FILES['image']['name'])){
				$image_name =  str_replace(' ','_',date('Ymdhis').$_FILES['image']['name']);
				$config['upload_path'] 		= $hari;
				$config['allowed_types'] 	= 'gif|jpg|png|jpeg';
				$config['file_name'] 		= $image_name;
				$this->upload->initialize($config);
				$this->upload->do_upload('image');

				$config['image_library'] = 'gd2';
				$config['source_image'] 	= $hari.'/'.$image_name;
				$config['create_thumb'] = FALSE;
				$config['maintain_ratio'] = TRUE;
				$config['width']         = 850;
				$config['height']       = auto;

				$this->image_lib->initialize($config);

				$this->image_lib->resize();
			}else{
				$image_name = $this->input->post('image');
			}

			$data = array(
				'folder_id'		=>	$this->input->post('parent_id'),
				'image'			=>	$image_name,
				'image_watermark'=>	'',
				'watermark_id'	=>	'',
				'title'			=>	$this->input->post('title'),
				'caption'		=>	$this->input->post('caption'),
				'description'	=>	$this->input->post('description'),
				'postdate'		=>	date('Y-m-d H:i:s'),
				'position'		=>	'');
		}


		$this->T_folder_image->add($data);
		redirect('image/index/'.$this->input->post('parent_id'));
	}

	function editimage($id, $url)
	{
		$data['image']	=	$this->T_folder_image->getimage($id);
		$data['watermark']	=	$this->T_watermark->get();
		$this->load->view('gen_cnf/image/edit_detail', $data);
	}

	function actioneditimage()
	{
		if(!empty($_FILES)){

				//pengecekan folder ada apa tidak
				$uploaddir 		= $this->config->item('upload_images');
				

				$datapath = explode("/", $this->input->post('path'));

				$datename 		= $datapath[0]; 
			
				$tahun = $uploaddir.$datename;
				if(is_dir($tahun)){
				}else{
					mkdir($tahun, 0777, true);
					$fp = fopen($tahun.'/index.html', 'w');
					fwrite($fp , 'Bismillaah');
					fclose($fp);
				}


				$datename2 = $datapath[1]; 
			
				$bulan = $uploaddir.$datename."/".$datename2;
				if(is_dir($bulan)){
				}else{
					mkdir($bulan, 0777, true);
					$fp = fopen($bulan.'/index.html', 'w');
					fwrite($fp , 'Bismillaah');
					fclose($fp);
				}


				$datename3 = $datapath[2]; 
			
				$hari = $uploaddir.$datename."/".$datename2."/".$datename3;
				if(is_dir($hari)){
				}else{
					mkdir($hari, 0777, true);
					$fp = fopen($hari.'/index.html', 'w');
					fwrite($fp , 'Bismillaah');
					fclose($fp);
				}

		}

		$getwatermark = $this->T_watermark->getid($this->input->post('watermark'));

		$explode = explode('-', $this->input->post('position'));
			
		if(!empty($this->input->post('watermark'))) {
			
			if(!empty($_FILES['image']['name'])){

					$image_name =  str_replace(' ','_',date('Ymdhis').$_FILES['image']['name']);
					$config['upload_path'] 		= $hari;
					$config['allowed_types'] 	= 'gif|jpg|png|jpeg';
					$config['file_name'] 		= $image_name;
					$this->upload->initialize($config);
					$this->upload->do_upload('image');

					$image = 'watermark'.$image_name;

					$config['image_library'] 	= 'gd2';
					$config['source_image'] 	= $hari.'/'.$image_name;
					$config['new_image'] 		= $hari.'/'.$image;
					$config['wm_type'] 			= 'overlay';
					$config['wm_overlay_path'] 	= $this->config->item('watermark'). $getwatermark['gambar'];
					$config['wm_opacity'] = 50;
					$config['wm_vrt_alignment'] = $explode[0];
					$config['wm_hor_alignment'] = $explode[1];
					$config['wm_padding'] = '';
					$this->image_lib->initialize($config);

					$this->image_lib->watermark();
				
					
					$config['image_library'] = 'gd2';
					$config['source_image'] 	= $hari.'/'.$image;
					$config['create_thumb'] = FALSE;
					$config['maintain_ratio'] = TRUE;
					$config['width']         = 850;
					$config['height']       = auto;

					$this->image_lib->initialize($config);

					$this->image_lib->resize();

					$data = array(
					'image'			=>	$image_name,
					'image_watermark'=>	$image,
					'watermark_id'	=>	$getwatermark['id'],
					'title'			=>	$this->input->post('title'),
					'caption'		=>	$this->input->post('caption'),
					'description'	=>	$this->input->post('description'),
					'position'		=>	$this->input->post('position'));

			}
			else
			{
				$image_name = $this->input->post('imagex');
				$ex = explode('.', $image_name);
				$format = end($ex);
				$image = 'watermark'.date('Ymdhis').'.'.$format;
				$config['image_library'] 	= 'gd2';
				$config['source_image'] 	= $hari.'/'.$image_name;
				$config['new_image'] 		= $hari.'/'.$image;
				$config['wm_type'] 			= 'overlay';
				$config['wm_overlay_path'] 	= $this->config->item('watermark'). $getwatermark['gambar'];
				$config['wm_opacity'] = 50;
				$config['wm_vrt_alignment'] = $explode[0];
				$config['wm_hor_alignment'] = $explode[1];
				$config['wm_padding'] = '';
				$this->image_lib->initialize($config);

				$this->image_lib->watermark();

				$config['image_library'] = 'gd2';
				$config['source_image'] 	= $hari.'/'.$image;
				$config['create_thumb'] = FALSE;
				$config['maintain_ratio'] = TRUE;
				$config['width']         = 850;
				$config['height']       = auto;

				$this->image_lib->initialize($config);

				$this->image_lib->resize();

				$data = array(
				'image'			=>	$image_name,
				'image_watermark'=>	$image,
				'watermark_id'	=>	$getwatermark['id'],
				'title'			=>	$this->input->post('title'),
				'caption'		=>	$this->input->post('caption'),
				'description'	=>	$this->input->post('description'),
				'position'		=>	$this->input->post('position'));
			}
		}
		else
		{
			if(!empty($_FILES['image']['name'])){

					$image_name =  str_replace(' ','_',date('Ymdhis').$_FILES['image']['name']);
					$config['upload_path'] 		= $hari;
					$config['allowed_types'] 	= 'gif|jpg|png|jpeg';
					$config['file_name'] 		= $image_name;
					$this->upload->initialize($config);
					$this->upload->do_upload('image');

					$config['image_library'] = 'gd2';
					$config['source_image'] 	= $hari.'/'.$image_name;
					$config['create_thumb'] = FALSE;
					$config['maintain_ratio'] = TRUE;
					$config['width']         = 850;
					$config['height']       = auto;

					$this->image_lib->initialize($config);

					$this->image_lib->resize();

					$data = array(
					'image'			=>	$image_name,
					'image_watermark'=>	'',
					'watermark_id'	=>	'',
					'title'			=>	$this->input->post('title'),
					'caption'		=>	$this->input->post('caption'),
					'description'	=>	$this->input->post('description'),
					'position'		=>	'');

			}
			else
			{
				$image_name = $this->input->post('imagex');

				$config['image_library'] = 'gd2';
				$config['source_image'] 	= $hari.'/'.$image_name;
				$config['create_thumb'] = FALSE;
				$config['maintain_ratio'] = TRUE;
				$config['width']         = 850;
				$config['height']       = auto;

				$this->image_lib->initialize($config);

				$this->image_lib->resize();

				$data = array(
				'image'			=>	$image_name,
				'image_watermark'=>	'',
				'watermark_id'	=>	'',
				'title'			=>	$this->input->post('title'),
				'caption'		=>	$this->input->post('caption'),
				'description'	=>	$this->input->post('description'),
				'position'		=>	'');
			}
		}

		$this->T_folder_image->edit($data, $this->input->post('id'));
		redirect('image/index/'.$this->input->post('url'));
	}

	function deleteimage($id, $url)
	{
		$this->T_folder_image->delete($id);
		redirect('image/index/'.$url);
	}

	function browse($id=0)
	{
		//$data['title'] 		=	$this->T_folder->getfolder($id);

		$data['identifier']	=	$id;
		$data['searchword']	=	$this->input->post('search');
		$data['title'] 		=	$this->T_folder->getfolder($id);
		/*// $data['folder'] = $this->T_folder->getparentsearch($id, $this->input->post('search'));
		$data['image']	= $this->T_folder_image->getparentimagesearch($id, $this->input->post('search'));*/
		if(!empty($this->input->post('search'))) 
		{
			$data['folder'] = $this->T_folder->getparentsearch($id, $this->input->post('search'));
			$data['image']	= $this->T_folder_image->getparentimagesearch($id, $this->input->post('search'));
		}
		else
		{
			$data['folder'] = $this->T_folder->getparent($id);
			$data['image']	= $this->T_folder_image->getparentimage($id);
		}

		//$data['folder'] = $this->T_folder->getparent($id);
		//$data['image']	= $this->T_folder_image->getparentimage($id);
		$data['watermark']	=	$this->T_watermark->get();
		$this->load->view('gen_cnf/image/browse', $data);
	}

	function browsemore($j, $id)
	{
		$data['image']	= $this->T_folder_image->getparentimagemore($j,$id);
		$this->load->view('gen_cnf/image/browsemore', $data);
	}

	function actionaddimagebrowse()
	{
		if(!empty($_FILES)){

			//pengecekan folder ada apa tidak
			$uploaddir 		= $this->config->item('upload_images');

			$datename 		= date("Y"); 
		
			$tahun = $uploaddir.$datename;
			if(is_dir($tahun)){
			}else{
				mkdir($tahun, 0777, true);
				$fp = fopen($tahun.'/index.html', 'w');
				fwrite($fp , 'Bismillaah');
				fclose($fp);
			}


			$datename2 = date("m"); 
		
			$bulan = $uploaddir.$datename."/".$datename2;
			if(is_dir($bulan)){
			}else{
				mkdir($bulan, 0777, true);
				$fp = fopen($bulan.'/index.html', 'w');
				fwrite($fp , 'Bismillaah');
				fclose($fp);
			}


			$datename3 = date('d'); 
		
			$hari = $uploaddir.$datename."/".$datename2."/".$datename3;
			if(is_dir($hari)){
			}else{
				mkdir($hari, 0777, true);
				$fp = fopen($hari.'/index.html', 'w');
				fwrite($fp , 'Bismillaah');
				fclose($fp);
			}

		}

		
		$getwatermark = $this->T_watermark->getid($this->input->post('watermark'));

		$explode = explode('-', $this->input->post('position'));
		

		if(!empty($this->input->post('watermark'))) {	

				if(!empty($_FILES['image']['name'])){
					$image_name =  str_replace(' ','_',date('Ymdhis').$_FILES['image']['name']);
					$config['upload_path'] 		= $hari;
					$config['allowed_types'] 	= 'gif|jpg|png|jpeg';
					$config['file_name'] 		= $image_name;
					$this->upload->initialize($config);
					$this->upload->do_upload('image');

					$image = 'watermark'.$image_name;

					$config['image_library'] 	= 'gd2';
					$config['source_image'] 	= $hari.'/'.$image_name;
					$config['new_image'] 		= $hari.'/'.$image;
					$config['wm_type'] 			= 'overlay';
					$config['wm_overlay_path'] 	= $this->config->item('watermark'). $getwatermark['gambar'];
					$config['wm_opacity'] = 50;
					$config['wm_vrt_alignment'] = $explode[0];
					$config['wm_hor_alignment'] = $explode[1];
					$config['wm_padding'] = '';
					$this->image_lib->initialize($config);

					$this->image_lib->watermark();

					$config['image_library'] = 'gd2';
					$config['source_image'] 	= $hari.'/'.$image;
					$config['create_thumb'] = FALSE;
					$config['maintain_ratio'] = TRUE;
					$config['width']         = 850;
					$config['height']       = auto;

					$this->image_lib->initialize($config);

					$this->image_lib->resize();
				}else{
					$image_name = $this->input->post('image');
				}

			$data = array(
				'folder_id'		=>	25,
				'image'			=>	$image_name,
				'image_watermark'=>	$image,
				'watermark_id'	=>	$getwatermark['id'],
				'title'			=>	$this->input->post('title'),
				'caption'		=>	$this->input->post('caption'),
				'description'	=>	$this->input->post('description'),
				'postdate'		=>	date('Y-m-d H:i:s'),
				'position'		=>	$this->input->post('position'));
		} 
		else
		{
			if(!empty($_FILES['image']['name'])){
				$image_name =  str_replace(' ','_',date('Ymdhis').$_FILES['image']['name']);
				$config['upload_path'] 		= $hari;
				$config['allowed_types'] 	= 'gif|jpg|png|jpeg';
				$config['file_name'] 		= $image_name;
				$this->upload->initialize($config);
				$this->upload->do_upload('image');

				$config['image_library'] = 'gd2';
				$config['source_image'] 	= $hari.'/'.$image_name;
				$config['create_thumb'] = FALSE;
				$config['maintain_ratio'] = TRUE;
				$config['width']         = 850;
				$config['height']       = auto;

				$this->image_lib->initialize($config);

				$this->image_lib->resize();
				
			}else{
				$image_name = $this->input->post('image');
			}

			$data = array(
				'folder_id'		=>	25,
				'image'			=>	$image_name,
				'image_watermark'=>	'',
				'watermark_id'	=>	'',
				'title'			=>	$this->input->post('title'),
				'caption'		=>	$this->input->post('caption'),
				'description'	=>	$this->input->post('description'),
				'postdate'		=>	date('Y-m-d H:i:s'),
				'position'		=>	'');
		}

		$this->T_folder_image->add($data);
		redirect('image/browse/25');
	}

	function browseckeditor($id=0)
	{
		$data['identifier']	=	$id;
		$data['searchword']	=	$this->input->post('search');
		$data['title'] 		=	$this->T_folder->getfolder($id);
		if(!empty($this->input->post('search'))) 
		{
			$data['folder'] = $this->T_folder->getparentsearch($id, $this->input->post('search'));
			$data['image']	= $this->T_folder_image->getparentimagesearch($id, $this->input->post('search'));
		}
		else
		{
			$data['folder'] = $this->T_folder->getparent($id);
			$data['image']	= $this->T_folder_image->getparentimage($id);
		}

		//$data['folder'] = $this->T_folder->getparent($id);
		//$data['image']	= $this->T_folder_image->getparentimage($id);
		$data['watermark']	=	$this->T_watermark->get();
		$this->load->view('gen_cnf/image/browseckeditor', $data);
	}

	function browseckeditormore($j, $id)
	{
		$data['image']	= $this->T_folder_image->getparentimagemore($j,$id);
		$this->load->view('gen_cnf/image/browseckeditormore', $data);
	}

	function actionaddimagebrowseckeditor()
	{
		if(!empty($_FILES)){

			//pengecekan folder ada apa tidak
			$uploaddir 		= $this->config->item('upload_images');

			$datename 		= date("Y"); 
		
			$tahun = $uploaddir.$datename;
			if(is_dir($tahun)){
			}else{
				mkdir($tahun, 0777, true);
				$fp = fopen($tahun.'/index.html', 'w');
				fwrite($fp , 'Bismillaah');
				fclose($fp);
			}


			$datename2 = date("m"); 
		
			$bulan = $uploaddir.$datename."/".$datename2;
			if(is_dir($bulan)){
			}else{
				mkdir($bulan, 0777, true);
				$fp = fopen($bulan.'/index.html', 'w');
				fwrite($fp , 'Bismillaah');
				fclose($fp);
			}


			$datename3 = date('d'); 
		
			$hari = $uploaddir.$datename."/".$datename2."/".$datename3;
			if(is_dir($hari)){
			}else{
				mkdir($hari, 0777, true);
				$fp = fopen($hari.'/index.html', 'w');
				fwrite($fp , 'Bismillaah');
				fclose($fp);
			}

		}

		
		$getwatermark = $this->T_watermark->getid($this->input->post('watermark'));

		$explode = explode('-', $this->input->post('position'));
		

		if(!empty($this->input->post('watermark'))) {	

				if(!empty($_FILES['image']['name'])){
					$image_name =  str_replace(' ','_',date('Ymdhis').$_FILES['image']['name']);
					$config['upload_path'] 		= $hari;
					$config['allowed_types'] 	= 'gif|jpg|png|jpeg';
					$config['file_name'] 		= $image_name;
					$this->upload->initialize($config);
					$this->upload->do_upload('image');

					$image = 'watermark'.$image_name;

					$config['image_library'] 	= 'gd2';
					$config['source_image'] 	= $hari.'/'.$image_name;
					$config['new_image'] 		= $hari.'/'.$image;
					$config['wm_type'] 			= 'overlay';
					$config['wm_overlay_path'] 	= $this->config->item('watermark'). $getwatermark['gambar'];
					$config['wm_opacity'] = 50;
					$config['wm_vrt_alignment'] = $explode[0];
					$config['wm_hor_alignment'] = $explode[1];
					$config['wm_padding'] = '';
					$this->image_lib->initialize($config);

					$this->image_lib->watermark();

					$config['image_library'] = 'gd2';
					$config['source_image'] 	= $hari.'/'.$image;
					$config['create_thumb'] = FALSE;
					$config['maintain_ratio'] = TRUE;
					$config['width']         = 850;
					$config['height']       = auto;

					$this->image_lib->initialize($config);

					$this->image_lib->resize();

				}else{
					$image_name = $this->input->post('image');
				}

			$data = array(
				'folder_id'		=>	25,
				'image'			=>	$image_name,
				'image_watermark'=>	$image,
				'watermark_id'	=>	$getwatermark['id'],
				'title'			=>	$this->input->post('title'),
				'caption'		=>	$this->input->post('caption'),
				'description'	=>	$this->input->post('description'),
				'postdate'		=>	date('Y-m-d H:i:s'),
				'position'		=>	$this->input->post('position'));
		} 
		else
		{
			if(!empty($_FILES['image']['name'])){
				$image_name =  str_replace(' ','_',date('Ymdhis').$_FILES['image']['name']);
				$config['upload_path'] 		= $hari;
				$config['allowed_types'] 	= 'gif|jpg|png|jpeg';
				$config['file_name'] 		= $image_name;
				$this->upload->initialize($config);
				$this->upload->do_upload('image');

				$config['image_library'] = 'gd2';
				$config['source_image'] 	= $hari.'/'.$image_name;
				$config['create_thumb'] = FALSE;
				$config['maintain_ratio'] = TRUE;
				$config['width']         = 850;
				$config['height']       = auto;

				$this->image_lib->initialize($config);

				$this->image_lib->resize();

			}else{
				$image_name = $this->input->post('image');
			}

			$data = array(
				'folder_id'		=>	25,
				'image'			=>	$image_name,
				'image_watermark'=>	'',
				'watermark_id'	=>	'',
				'title'			=>	$this->input->post('title'),
				'caption'		=>	$this->input->post('caption'),
				'description'	=>	$this->input->post('description'),
				'postdate'		=>	date('Y-m-d H:i:s'),
				'position'		=>	'');
		}

		$this->T_folder_image->add($data);
		redirect('image/browseckeditor/25');
	}
}
