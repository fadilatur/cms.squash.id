<?php include_once dirname(__FILE__).'/../layouts/header.php';?>
<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Administrator</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                <div style='padding-bottom:10px'><button onclick="location.href='<?php echo site_url('administrator/add/');?>'" type="button" class="btn btn-info">Add</button>
									<button onclick="location.href='<?php echo site_url('administrator/toexcel');?>'" type="button" class="btn btn-info">Download to Excel</button>
									</div>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
						  <th>No</th>
                          <th>Nama</th>
                          <th>Email</th>
                          <th>Aktif</th>
						  <th>Level Nama</th>
						  <th>Level</th>
                          <th>Option</th>
                        </tr>
                      </thead>


                      <tbody>
					  <?php $i=1;
					  foreach($data as $user){?>
                        <tr>
						  <td><?php echo $i;?></td>
                          <td><?php echo $user['nama'];?></td>
                          <td><?php echo $user['email'];?></td>
						  <td><?php echo $user['aktif'];?></td>
						  <td><?php echo $user['level_nama'];?></td>
						  <td><?php echo $user['level'];?></td>
						  <td>
			
                            <div class="btn-group"><button onclick="location.href='<?php echo site_url('administrator/edit/'.$user['id_adm']);?>'" type="button" class="btn btn-info">Edit</button></div>
                            <div class="btn-group"><a data-href="<?php echo site_url('administrator/actiondelete/'.$user['id_adm']);?>" data-toggle="modal" data-target="#confirm-delete<?php echo $i;?>" href="#"><button type="button" class="btn btn-info">Delete</button></a></div>
                                                    
                            <div class="modal fade" id="confirm-delete<?php echo $i;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                   <div class="modal-content">                         
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                             <h4 class="modal-title" id="myModalLabel">Confirm Delete</h4>
                                        </div>             
                                        <div class="modal-body">
                                            <p>You are about to delete this data.</p>
                                            <p>Do you want to proceed?</p>
                                            <p class="debug-url"></p>
                                        </div>
                                                                
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
											<a href="<?php echo site_url('administrator/actiondelete/'.$user['idx']);?>" class="btn btn-danger danger">Delete</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
						  </td>
                        </tr>
						<?php $i++;}?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

              
              </div>

              

              

              

              
            </div>
          </div>
        </div>
        <!-- /page content -->
<?php include_once dirname(__FILE__).'/../layouts/footer.php';?>