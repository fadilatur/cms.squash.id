<?php include_once dirname(__FILE__).'/../layouts/header.php';?>
<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Artikel</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-6">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Default Buttons </h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                  </div>
                </div>


                <div class="x_panel">
                  <div class="x_title">
                    <h2>Rounded Button</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                  </div>
                </div>

                <div class="x_panel">
                  <div class="x_title">
                    <h2>Button Dropdown</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    
                  </div>
                </div>


                <div class="x_panel">
                  <div class="x_title">
                    <h2>Split Button Dropdown</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    


                  </div>
                </div>


                <div class="x_panel">
                  <div class="x_title">
                    <h2>Button App Design</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                  </div>
                </div>

              </div>

              <div class="col-md-6">

                <div class="x_panel">
                  <div class="x_title">
                    <h2>Button Sizes</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                  </div>

                </div>


                <div class="x_panel">
                  <div class="x_title">
                    <h2>Button Groups</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    

                    
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
<?php include_once dirname(__FILE__).'/../layouts/header.php';?>