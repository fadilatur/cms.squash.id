<?php include_once dirname(__FILE__).'/../layouts/header.php';?>
<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>SUB CATEGORY</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <ol class="breadcrumb">
                        <li><a href="<?php echo base_url('gen_cnf/home');?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="<?php echo site_url('gen_cnf/category');?>">Category</a></li>
                        <li class="active">Edit</li>
                    </ol>
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <form class="form-horizontal form-label-left" enctype="multipart/form-data" method="post" action="<?php echo site_url('section/actionedit/'.$data['id_supsection']);?>">
                      
                    <div class="box-body">
                    
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Nama section</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="text" name="nama_supsection" value="<?php echo $data['nama_section'];?>" class="form-control col-md-7 col-xs-12" style="width:350px;">
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Status</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                           <input type="radio"  <?php echo ($data['status']=='1')?"checked='checked'":"";?> name="status" value="1"  > YES  <input type="radio" name="status" <?php echo ($data['status']=='0')?"checked='checked'":"";?> value="0"> NO
                        </div>
                    </div>
                                        
                                    
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                          <input type="hidden" name="id" value="<?php echo $data['id_section'];?>"/>
                          <button type="submit" class="btn btn-primary">Submit</button>
                          <button onclick="location.href='<?php echo site_url('category');?>'" type="button" class="btn btn-info">Cancel</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
        
<?php include_once dirname(__FILE__).'/../layouts/footer.php';?>
