<?php include_once dirname(__FILE__).'/../layouts/header.php';?>
<script type="text/javascript" src="<?php echo $js.'ckeditor/ckeditor.js'; ?>"></script>
<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Liga</h3>
              </div>

            
            </div>

            <div class="clearfix"></div>

            <div class="row">
             <form id="demo-form" data-parsley-validate  enctype="multipart/form-data" method="post" action="<?php echo site_url('liga/actionadd');?>">
              
			  <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_content">
                    
                      <label for="fullname">Nama Liga :</label>
                      <input type="text" id="fullname" class="form-control" name="nama_liga" data-parsley-trigger="change" />
                      <br/>
					  <label for="fullname">Inisial Liga :</label>
                      <input type="text" id="fullname" class="form-control" name="nama_liga2" data-parsley-trigger="change" />
                      <br/>
					  
					  <label for="fullname">Negara :</label>
                      <select class="select2_group form-control" name="negara">
                      <?php
                            foreach($negara as $trans2){ 
                            ?>
                                  <option <?php echo (!empty($trans2['id_negara']))?(($trans['id_negara']==$trans2['id_negara'])?"selected='selected'":""):"";?> value="<?php echo $trans2['id_negara'];?>"><?php echo $trans2['nama_negara'];?></option>
                            <?php } ?>

                      </select>
					  <br />
					  <br />
					  
					  <label for="fullname">Logo :</label>
                      <input type="file" id="fullname" class="form-control" name="image"  />
                      <br/>
                      
					  <label for="fullname">Aktif :</label>
					  <input type="radio" name="status" value="1" checked="checked" > Ya  <input type="radio" name="status" value="0"> Tidak
                      <br/><br/>

                      <label for="email">Meta Keywords :</label>
                      <textarea id="email" class="form-control" name="meta_key" data-parsley-trigger="change"></textarea>
                      <br/>

                      <label for="email">Meta Descriptions :</label>
                      <textarea id="email" class="form-control" name="meta_des" data-parsley-trigger="change"></textarea>
                      <br/>

                  </div>
                </div>
              </div>

               <div  class="col-md-12">
                <input type="hidden" value="<?php //echo $parent_id;?>" name="id" />
                <button type="submit" class="btn btn-success">Submit</button>
			   </div>
               </form>
			   
            </div>

          </div>
        </div>
		
         <div style="clear:both"></div>
        <!-- /page content -->
<?php include_once dirname(__FILE__).'/../layouts/footer.php';?>