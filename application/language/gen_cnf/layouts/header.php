<?php 
$css 		= $this->config->item('css');
$images 	= $this->config->item('images');
$js 		= $this->config->item('js');
$upload 	= $this->config->item('images_upload');
$uploadall   = $this->config->item('images_all');

$tim    = $this->config->item('images_tim');

//set up user role
// $urole      = $this->model_role->select_group($this->session->userdata('role'));
//    foreach($urole as $urole){
//        $uarray[]   = $urole['menu_id'];
//    }
//$uri3 = $this->uri->segment(3);
	
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>TopSkor</title>

    <!-- Bootstrap -->
    <link href="<?php echo $css;?>bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo $css;?>font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo $css;?>nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="<?php echo $css;?>green.css" rel="stylesheet">
    <!-- bootstrap-progressbar -->
    <link href="<?php echo $css;?>bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="<?php echo $css;?>jqvmap.min.css" rel="stylesheet"/>

    <!-- Custom Theme Style -->
    <link href="<?php echo $css;?>custom.min.css" rel="stylesheet">

    <!-- Select2 -->
    <link href="<?php echo $css;?>/vendors/select2/dist/css/select2.min.css" rel="stylesheet">

     <script src="<?php echo $js;?>jquery.min.js"></script>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;background:#fff">
              <a href="index.html" class="site_title"><img src="<?php echo $images;?>logo.png" width="100%"/></a>
            </div>

            <div class="clearfix"></div>

            

            

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
            
                <ul class="nav side-menu">
                  <li><a href="<?php echo site_url('home');?>"><i class="fa fa-home"></i> Home </a>
                    
                  </li>
				  
				  <li class="treeview active">
                            <a href="#">
                                <i class="fa fa-user-secret"></i> <span>Administrator</span><i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
							
                               <li>
                                    <a href="<?php echo site_url('administrator');?>">
                                        <i class="fa fa-user-secret"></i> <span>Administrator</span>
                                    </a>
                                </li>
								<li >
                                    <a href="<?php echo site_url('level');?>">
                                        <i class="fa fa-cog"></i> <span>Level</span>
                                    </a>
                                </li>
							
                            </ul>
                   </li>
				   
				   	
            <li class="treeview">
              <a href="#">
                <i class="fa fa-file-text"></i> <span>Articles</span> <i class="fa fa-angle-left pull-right"></i>
              </a>		  
              <ul class="treeview-menu">
              <li><a  href="<?php echo site_url('artikel/add');?>"><i class="fa fa-plus-square-o"></i>Tambah Artikel</a></li>
              <li><a  href="<?php echo site_url('artikel');?>"><i class="fa fa-file-text"></i>Semua Artikel</a></li>
				<li><a  href="<?php echo site_url('category');?>"><i class="fa fa-th-large"></i>Category</a></li>
				
				<!--<li><a href="<?php //echo site_url('gen_cnf/topgrafik');?>"><i class="fa fa-desktop"></i> Top Grafik </a></li>-->
              </ul>
            </li>
		
					
                  <!--<li><a><i class="fa fa-edit"></i> Risk Profile Manag...<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="form.html">General Form</a></li>
                      <li><a href="form_advanced.html">Advanced Components</a></li>
                      <li><a href="form_validation.html">Form Validation</a></li>
                      
                    </ul>
                  </li>-->
            
			<li class="treeview">
              <a href="#">
                <i class="fa fa-file-text"></i> <span>Komponen</span> <i class="fa fa-angle-left pull-right"></i>
              </a>		  
              <ul class="treeview-menu">
			  
				<li><a  href="<?php echo site_url('paper/edit/484');?>"><i class="fa fa-dot-circle-o"></i>Data Paper</a></li>
				<li><a  href="<?php echo site_url('klub');?>"><i class="fa fa-dot-circle-o"></i>Klub</a></li>
				<li><a  href="<?php echo site_url('liga');?>"><i class="fa fa-dot-circle-o"></i>Liga</a></li>
				<li><a  href="<?php echo site_url('klasemen');?>"><i class="fa fa-dot-circle-o"></i>Klasemen</a></li>
				<li><a  href="<?php echo site_url('jadwal');?>"><i class="fa fa-dot-circle-o"></i>Jadwal</a></li>
				<li><a  href="<?php echo site_url('negara');?>"><i class="fa fa-dot-circle-o"></i>Negara</a></li>
				
              </ul>
            </li>			
				
				 
				  
			<li class="treeview">
              <a href="#">
                <i class="fa fa-file-text"></i> <span>Pengaturan</span> <i class="fa fa-angle-left pull-right"></i>
              </a>		  
              <ul class="treeview-menu">
			  
				<li><a  href="<?php echo site_url('profile');?>"><i class="fa fa-dot-circle-o"></i>Profile Website</a></li>
				<li><a  href="<?php echo site_url('informasi/index/1');?>"><i class="fa fa-dot-circle-o"></i>Informasi Website</a></li>
				<!--<div class="btn-group"><button onclick="location.href='<?php //echo site_url('gen_cnf/administrator/edit/'.$user['idx']);?>'" type="button" class="btn btn-info">Edit</button></div>-->
				
				  
                </ul>
			</li>
			<li class="">
              <a href="<?php echo site_url('polling');?>">
                <i class="fa fa-bar-chart"></i> <span>Polling</span> 
              </a>		  
              
			</li>
			<li class="">
              <a href="<?php echo site_url('member');?>">
                <i class="fa fa-user-secret"></i> <span>Member</span> 
              </a>		  
              
			</li>
      <li class="">
              <a href="<?php echo site_url('report');?>">
                <i class="fa fa-bar-chart"></i> <span>Report</span> 
              </a>      
              
      </li>
			</u>
              </div>
              

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
					<img src="<?php echo $tim.$upload.$this->session->userdata('foto');?>&w=100&h=100"/><?php echo $this->session->userdata('nama');?>
					<span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="<?php echo site_url('profile/edit/'.$this->session->userdata('id_adm'));?>">Profile</a></li>
                    
                    <li><a href="<?php echo site_url('auth/logout');?>"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>

                  </ul>
                </li>

                
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->