<html>
    <style>
        td {
            vertical-align: text-align;
            text-align: left;
            border: 1px solid black;
            backgound: none;
            font-family: helvetica, calibri, verdana;
            font-size: 10pt;
        }
        th {
            border: 1px solid black;
            backgound: #555;
            font-family: helvetica, calibri, verdana;
            font-size: 12pt;
            font-weight: bold;
        }
    </style>
<body>
    <h1>SANDIMAS</h1>
    <h2>Data Administrator</h2>
    <p />
    
    <table>
        <tr>
            <th>No.</th>
            <th>Username</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Address</th>
            <th>Image</th>
            <th>Join Date</th>
    <?php 
	$rx = 1;
	foreach($rows as $r): ?>
        <tr>
            <td><?php echo $rx;?></td>
            <td><?php echo $r['username'];?></td>
            <td><?php echo $r['email'];?></td>
            <td><?php echo $r['phone'];?></td>
            <td><?php echo $r['address'];?></td>
            <td><?php echo $r['image'];?></td>
            <td><?php echo $r['join_date'];?></td>
        </tr>
    <?php 
	$rx++;
	endforeach; ?>
    </table>
</body>
</html>