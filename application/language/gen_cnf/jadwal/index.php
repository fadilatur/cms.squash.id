<?php include_once dirname(__FILE__).'/../layouts/header.php';?>
<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Jadwal</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-4">
             <form id="demo-form" data-parsley-validate  enctype="multipart/form-data" method="post" action="<?php echo site_url('jadwal/index/');?><?php echo (!empty($detail['id_jadwal']))?$detail['id_jadwal']:0;?>">
             <div class="x_panel">
                  <div class="x_title">
                   <label for="fullname">Liga * :</label>
                      <select class="select2_group form-control" name="liga">
                      <?php
                            foreach($liga as $trans2){ 
                            ?>
                                  <option <?php echo (!empty($detail['id_liga']))?(($detail['id_liga']==$trans2['id_liga'])?"selected='selected'":""):"";?> value="<?php echo $trans2['id_liga'];?>"><?php echo $trans2['nama_liga'];?></option>
                            <?php } ?>

                           
                          </select>
                      <br/>
                       <br/>
                       <label for="fullname">Tanding * :</label>
                      <select class="select2_group form-control" name="klub1">
                      <?php

                            foreach($klub as $trans2){ 

                              $arrklub[$trans2['id_klub']] = $trans2['nama_klub'];
                            ?>
                                  <option <?php echo (!empty($detail['klub1']))?(($detail['klub1']==$trans2['id_klub'])?"selected='selected'":""):"";?> value="<?php echo $trans2['id_klub'];?>"><?php echo $trans2['nama_klub'];?></option>
                            <?php } ?>

                           
                          </select>
                      <br/>
                       <br/>
                       <label for="fullname">Melawan * :</label>
                      <select class="select2_group form-control" name="klub2">
                      <?php
                            foreach($klub as $trans2){ 
                            ?>
                                  <option <option <?php echo (!empty($detail['klub2']))?(($detail['klub2']==$trans2['id_klub'])?"selected='selected'":""):"";?> value="<?php echo $trans2['id_klub'];?>"><?php echo $trans2['nama_klub'];?></option>
                            <?php } ?>

                           
                          </select>
                      <br/>
                       <br/>
                       <label for="fullname">Waktu Pertandingan * :</label>
                      
                        <div >
                      <div class="col-md-12 xdisplay_inputx form-group has-feedback">
                                <input type="text" value="<?php echo (!empty($detail['tanggal']))?$detail['tanggal']:date('Y-m-d');?>" name="tgl" class="form-control has-feedback-left" id="single_cal3" placeholder="" aria-describedby="inputSuccess2Status3">
                                <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                <span id="inputSuccess2Status3" class="sr-only">(success)</span>
                              </div>
                        </div>
                         <div >
                           
                           <div class="row">
                        <div class="col-md-6">
                       <select id="heard" name="jam" class="form-control" >
                            <option value="00">Jam</option>
                            <?php 
                            if(!empty($detail['jam'])){
                                $ex = explode(":",$detail['jam']);
                            }

                            for($i=0;$i<25;$i++){ 
                                if($i<10){
                                  $nilai1 = "0".$i;
                                }else{
                                  $nilai1 = $i;
                                }

                            ?>
                             <option <?php echo (!empty($ex['0']))?($ex['0']==$i)?"selected='selected'":"":(date('H')==$i)?"selected='selected'":"";?> value="<?php echo $nilai1;?>"><?php echo $nilai1;?></option>
                            <?php } ?>
                            
                          
                          </select>
                          </div><div class="col-md-6">
                           <select name="menit" id="heard" class="form-control" >
                            <option value="">Menit</option>
                            <?php for($i=0;$i<61;$i++){ 
                                if($i<10){
                                  $nilai1 = "0".$i;
                                }else{
                                  $nilai1 = $i;
                                }

                            ?>
                            <option <?php echo (!empty($ex['1']))?($ex['1']==$i)?"selected='selected'":"":(date('i')==$i)?"selected='selected'":"";?> value="<?php echo $nilai1;?>"><?php echo $nilai1;?></option>
                            <?php } ?>
                          </select>
                          </div>
                      <br/>
                    
                  </div>

                         
                      </div>
                      <br/>
                       <label for="fullname">Channel * :</label>
                      <input type="text" id="fullname" class="form-control" value="<?php echo (!empty($detail['channel']))?$detail['channel']:"";?>" name="channel" required />
                      <br/>
                      <?php if(!empty($detail['id_jadwal'])){ ?>


                       <label for="fullname">Hasil Pertandingan  Klub1* :</label>
                      <input type="text" id="fullname" class="form-control" value="<?php echo (!empty($detail['HasilKlub1']))?$detail['HasilKlub1']:"";?>" name="hklub1" required /> <br/>
                       <label for="fullname">Hasil Pertandingan  Klub1 * :</label>
                      <input type="text" id="fullname" class="form-control" value="<?php echo (!empty($detail['HasilKlub2']))?$detail['HasilKlub2']:"";?>" name="hklub2" required />
 <br/>
                       <?php } ?> 


                       <button type="submit" class="btn btn-success">Submit</button>
                      
                  </div>
              </div>
              </form>
            </div>

              <div class="col-md-8 col-sm-8 col-xs-8">
                <div class="x_panel">
                  <div class="x_title">
				
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>

                  <div class="x_content">
                    
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                         <th>No</th>
						  <th>Liga</th>
                          <th>Tanding</th>
						  <th>Jadwal</th>
              <th>Channel</th>
              <th>Hasil</th>
                          <th>Option</th>
                        </tr>
                      </thead>


                      <tbody>
					  <?php $i=1;
            $pub = array('No','Yes');
					  foreach($data as $user){?>
                        <tr>
						  <td><?php echo $i;?></td>
						  <td><?php echo $user['nama_liga'];?></td>
						 <td><?php echo $arrklub[$user['klub1']];?> <strong>VS</strong> <?php echo $arrklub[$user['klub2']];?></td>
             <td><?php echo $user['tanggal'];?> <?php echo $user['jam'];?> WIB</td>
             <td><?php echo $user['channel'];?></td>
             <td><?php echo $user['HasilKlub1'];?> : <?php echo $user['HasilKlub2'];?></td>
						  <td>
							
						
                            <div class="btn-group"><button onclick="location.href='<?php echo site_url('jadwal/index/'.$user['id_jadwal']);?>'" type="button" class="btn btn-info">Edit</button></div>
                            <div class="btn-group"><a data-href="<?php echo site_url('jadwal/actiondelete/');?>" data-toggle="modal" data-target="#confirm-delete<?php echo $i;?>" href="#"><button type="button" class="btn btn-info">Delete</button></a></div>
                                                    
                            <div class="modal fade" id="confirm-delete<?php echo $i;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                   <div class="modal-content">                         
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                             <h4 class="modal-title" id="myModalLabel">Confirm Delete</h4>
                                        </div>             
                                        <div class="modal-body">
                                            <p>You are about to delete this data.</p>
                                            <p>Do you want to proceed?</p>
                                            <p class="debug-url"></p>
                                        </div>
                                                                
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
											<a href="<?php echo site_url('jadwal/actiondelete/'.$user['id_jadwal']);?>" class="btn btn-danger danger">Delete</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
						  </td>
                        </tr>
						<?php $i++;}?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

              
              </div>

              

              

              

              
            </div>
          </div>
        </div>
        <!-- /page content -->
<?php include_once dirname(__FILE__).'/../layouts/footer.php';?>