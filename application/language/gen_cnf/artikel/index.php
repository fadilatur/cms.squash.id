<?php include_once dirname(__FILE__).'/../layouts/header.php';?>

 <!-- Datatables -->
    <link href="<?php echo $css;?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $css;?>vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $css;?>vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $css;?>vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $css;?>vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="row top_tiles" style="margin: 10px 0;">
              <div class="col-md-3 col-sm-3 col-xs-6 tile">
                <h3>Artikel</h3>
               
              </div>
            </div>
            <br />


            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="dashboard_graph x_panel">
                  <div class="row x_title">
				  <div class="col-md-3">
				  <button onclick="location.href='<?php echo site_url('artikel/add/'.$parent_id);?>'" type="button" class="btn btn-info">Add</button>
				  <button onclick="location.href='<?php echo site_url('artikel/toexcel');?>'" type="button" class="btn btn-info">Download to Excel</button>
				  </div>
                    
                    <div class="col-md-9">
                    	<form method="POST" action="<?php echo site_url('artikel');?>">
                    	<div class="row">
                    	<div class="col-md-3">
                    		<input type="text" id="fullname" class="form-control" placeholder="Judul" name="judul" />
                    	</div>
                    	<div class="col-md-3">
                    		<select class="select2_group form-control" name="category">
                    		<option value="0">Category</option>
		                      <?php
		                      $cat = $this->T_supsection->select(0);
		                      foreach($cat as $trans){ 
		                      ?>
		                            <optgroup label="<?php echo $trans['nama_supsection'];?>">
		                             <?php
		                            $bcd = $this->T_section->select($trans['id_supsection']);
		                            foreach($bcd as $trans2){ 
		                            ?>
		                                  <option value="<?php echo $trans2['id_section'];?>"><?php echo $trans2['nama_section'];?></option>
		                            <?php } ?>
		                              
		                            </optgroup>
		                      <?php } ?>
                          </select>
                    	</div>
                    	<div class="col-md-3">
                    		<select class="select2_group form-control" name="admin">
		                      		<option value="0">Admin</option>
		                             <?php
		                           
		                            foreach($member as $trans2){ 
		                            ?>
		                                  <option value="<?php echo $trans2['id_adm'];?>"><?php echo $trans2['nama'];?></option>
		                            <?php } ?>
		                            
                          </select>
                    	</div>
                    	<div class="col-md-3">
                    	<button type="submit" class="btn btn-info">SEARCH</button>
                    	</div>
                    	</div>
                    	</form>
                      
                    </div>
                  </div>
                   <div><?php echo $paging;?></div>
                  <div class="x_content">
                    <table id="datatablesalah" class="table table-striped table-bordered">
                      <thead>
                        <tr>
						  <th>No</th>
                          <th>Judul Artikel</th>
                          
                          <th>Page View</th>
						  <th>Status</th>
						  <th>Tanggal Publis</th>
               <th>Admin</th>
                          <th>Option</th>
                        </tr>
                      </thead>
					  <tbody>
					  <?php $i=1;
					  foreach($data as $user){?>
                        <tr>
						  <td><?php echo $i+$pages;?></td>
                          <td><a style="color:#000" href="<?php echo 'http://www.topskor.id/detail/'.$user['id_artikel'].'/'.url_title($user['judul_artikel']);?>" target="_blank"><?php echo $user['judul_artikel'];?></a></td>
						  <td><?php echo $user['dibaca'];?></td>
						  <td><div style="float:left;border-radius:4px;color:#fff;font-weight:bold;padding:8px;background:#<?php echo ($user['publish']=='Y')?"4ce85a":"d30675";?>"><?php echo $user['publish'];?></div></td>
						  <td><?php echo $user['tgl_pub'];?></td>
              <td><?php echo $user['nama'];?></td>
						  <td>
                <div class="btn-group"><button onclick="location.href='<?php echo site_url('artikel/index/'.$user['id_artikel']);?>'" type="button" class="btn btn-info">Sub</button></div>
							<div class="btn-group"><button onclick="location.href='<?php echo site_url('artikel/edit/'.$user['id_artikel']);?>'" type="button" class="btn btn-info">Edit</button></div>
							<div class="btn-group"><button onclick="location.href='<?php echo site_url('artikel/delete/'.$user['id_artikel']);?>'" type="button" class="btn btn-info">Delete</button></div>
                            
						  </td>
						  
                        </tr>
						<?php $i++;}?>
                      </tbody>
					  
					  
					 </table> 
                  </div>
                  <div><?php echo $paging;?></div>
                </div>



              </div>
            </div>


            
          </div>
        </div>
        <!-- /page content -->
 <!-- Datatables -->
    <script src="<?php echo $css;?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-scroller/js/datatables.scroller.min.js"></script>
    <script src="<?php echo $css;?>vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo $css;?>vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo $css;?>vendors/pdfmake/build/vfs_fonts.js"></script>
    <script>
       $('#datatable').dataTable();
    </script>

<?php include_once dirname(__FILE__).'/../layouts/footer.php';?>