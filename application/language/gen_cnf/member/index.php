<?php include_once dirname(__FILE__).'/../layouts/header.php';?>
<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Member</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-4">
             <form id="demo-form" data-parsley-validate  enctype="multipart/form-data" method="post" action="<?php echo site_url('member/index/');?><?php echo (!empty($detail['id']))?$detail['id']:0;?>">
             <div class="x_panel">
                  <div class="x_title">
                      <br/>
                       <br/>
					   <label for="fullname">Nama * :</label>
                       <input type="text" id="fullname" class="form-control" value="<?php echo (!empty($detail['name']))?$detail['name']:"";?>" name="name" required />
					   <br/>
					   <label for="fullname">Password * :</label>
                       <input type="text" id="fullname" class="form-control" value="<?php echo (!empty($detail['password']))?$detail['password']:"";?>" name="password" required />
					   <br/>
					   <label for="fullname">Email * :</label>
                       <input type="text" id="fullname" class="form-control" value="<?php echo (!empty($detail['email']))?$detail['email']:"";?>" name="email" required />
					   <br/>
					   <label for="fullname">Phone * :</label>
                       <input type="text" id="fullname" class="form-control" value="<?php echo (!empty($detail['phone']))?$detail['phone']:"";?>" name="phone" required />
					   <br/>
					   <label for="fullname">Address* :</label>
                       <input type="text" id="fullname" class="form-control" value="<?php echo (!empty($detail['address']))?$detail['address']:"";?>" name="address" required />
					   <br/>
					   <label for="fullname">Image * :</label>
					   <?php if(!empty($detail['image'])){ ?>
                            <img src="<?php echo $detail['image'];?>"/>
                             <input type="hidden" id="fullname" class="form-control" name="image" value="<?php echo $detail['image'];?>"  />
                       <?php } ?>
					   <input type="file" id="fullname" class="form-control" name="image"  />
					   <br/>
					   <label for="fullname">Gender * :</label>
                       <div class="x_content">
                       <input type="radio" name="gender" value="1" checked="checked" > Laki-laki  <input type="radio" name="gender" value="1"> Perempuan
					   </div>
					   <br/>
                       <label for="fullname">Birthday* :</label>
                       <br/>
                        <div >
						<?php
						
						if(!empty($detail['publishdate'])){
							$a=explode(' ',$detail['publishdate']);
							
						}
						?>
                      <div class="col-md-12 xdisplay_inputx form-group has-feedback">
                                <input type="text" value="<?php echo (!empty($a[0]))?$a[0]:date('Y-m-d');?>" name="tgl" class="form-control has-feedback-left" id="single_cal3" placeholder="" aria-describedby="inputSuccess2Status3">
                                <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                <span id="inputSuccess2Status3" class="sr-only">(success)</span>
                              </div>
                        </div>
                         <div >
                           
                           <div class="row">
                        
                      <br/>
                    
                  </div>

                         
                      </div>
                      
                      <?php if(!empty($detail['id'])){ ?>


                       
 <br/>
                       <?php } ?> 


                       <button type="submit" class="btn btn-success">Submit</button>
                      
                  </div>
              </div>
              </form>
            </div>

              <div class="col-md-8 col-sm-8 col-xs-8">
                <div class="x_panel">
                  <div class="x_title">
				
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>

                  <div class="x_content">
                    
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                         <th>No</th>
						  <th>Name</th>
                          <th>Email</th>
						  <th>Address</th>
						  <th>Image</th>
						  <th>Option</th>
                        </tr>
                      </thead>

                      <tbody>
					  <?php $i=1;
            $pub = array('No','Yes');
					  foreach($data as $user){?>
                        <tr>
						  <td><?php echo $i;?></td>
						  <td><?php echo $user['name'];?></td>
						  <td><?php echo $user['email'];?></td>
						  <td><?php echo $user['address'];?></td>
						  <td>
						  <?php if(!empty($user['image'])){ ?>
						  <img src='<?php echo $user['image'];?>' width='50' height='50' />
						  <?php } ?>
						  </td>
						  
 
						  <td>
							
						
                            <div class="btn-group"><button onclick="location.href='<?php echo site_url('member/index/'.$user['id']);?>'" type="button" class="btn btn-info">Edit</button></div>
                            <div class="btn-group"><a data-href="<?php echo site_url('member/actiondelete/');?>" data-toggle="modal" data-target="#confirm-delete<?php echo $i;?>" href="#"><button type="button" class="btn btn-info">Delete</button></a></div>
                                                    
                            <div class="modal fade" id="confirm-delete<?php echo $i;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                   <div class="modal-content">                         
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                             <h4 class="modal-title" id="myModalLabel">Confirm Delete</h4>
                                        </div>             
                                        <div class="modal-body">
                                            <p>You are about to delete this data.</p>
                                            <p>Do you want to proceed?</p>
                                            <p class="debug-url"></p>
                                        </div>
                                                                
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
											<a href="<?php echo site_url('member/actiondelete/'.$user['id']);?>" class="btn btn-danger danger">Delete</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
						  </td>
                        </tr>
						<?php $i++;}?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

              </div>
              
            </div>
          </div>
        </div>
        <!-- /page content -->
<?php include_once dirname(__FILE__).'/../layouts/footer.php';?>