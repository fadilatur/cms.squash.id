<?php 
$css 	= $this->config->item('css');
$images = $this->config->item('images');
$js 	= $this->config->item('js');
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Gentellela Alela! | </title>

    <!-- Bootstrap -->
    <link href="<?php echo $css;?>bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo $css;?>font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo $css;?>nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="<?php echo $css;?>animate.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?php echo $css;?>custom.min.css" rel="stylesheet">
  </head>

  <body class="login">
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
            <form action="<?php echo site_url('auth');?>" method="post">
            <div style="background:#fff; border-radius:5px"><img src="<?php echo $images;?>logo.png" width="275" /></div><br />
              <!--<h1>Login Form</h1>-->
              <div class="name">
                <input type="text" name="name" class="form-control" placeholder="Username" required="" />
              </div>
              <div class="name">
                <input type="password" name="password" class="form-control" placeholder="Password" required="" />
              </div>
			  <div class="footer">
              <button type="submit" class="btn bg-olive btn-block">Log in</button> 
			  </div>
              <div class="clearfix"></div>

             
            </form>
          </section>
        </div>

        
      </div>
    </div>
  </body>
</html>