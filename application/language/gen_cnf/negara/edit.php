<?php include_once dirname(__FILE__).'/../layouts/header.php';?>
<script type="text/javascript" src="<?php echo $js.'ckeditor/ckeditor.js'; ?>"></script>
<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Negara</h3>
              </div>

            
            </div>

            <div class="clearfix"></div>

            <div class="row">
             <form id="demo-form" data-parsley-validate  enctype="multipart/form-data" method="post" action="<?php echo site_url('negara/actionedit');?>">
              
			  <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_content">
                    
                      <label for="fullname">Nama Negara :</label>
                      <input type="text" id="fullname" class="form-control" name="nama_negara" value="<?php echo $data['nama_negara'];?>" data-parsley-trigger="change" />
                      <br/>
					  <label for="fullname">Inisial Negara :</label>
                      <input type="text" name="nama_negara2" class="form-control" value="<?php echo $data['nama_negara2'];?>" id="nama_negara2" />
                      
					  <br/>
					  
              <label for="fullname">Bendera :</label>
               <?php if(!empty($data['logo'])){ ?>
                           <img src="<?php echo $tim.$uploadall.'vhliga/negara/'.$data['logo'];?>&w=20" />
                             <input type="hidden" id="fullname" class="form-control" name="imagex" value="<?php echo $data['logo'];?>"  />
                       <?php } ?>
                      <input type="file" id="fullname" class="form-control" name="image"  />
                      <br/>
                      
					  
					  <label for="fullname">Aktif :</label>
                      <input type="radio"  <?php echo ($data['aktif']=='Y')?"checked='checked'":"";?> name="status" value="Y"  > Ya  <input type="radio" name="status" <?php echo ($data['aktif']=='N')?"checked='checked'":"";?> value="N"> Tidak
					  <br/><br/>

                      <label for="fullname">Meta Keywords :</label>
                      <textarea id="fullname" class="form-control" name="metakey" data-parsley-trigger="change" /><?php echo $data['meta_key'];?></textarea>		
                      <br/>

                      <label for="fullname">Meta Descriptions :</label>
                      <textarea id="fullname" class="form-control" name="metades" data-parsley-trigger="change" /><?php echo $data['meta_des'];?></textarea>
					  <br/>

                  </div>
                </div>
			
				
              </div>

               <div  class="col-md-12">
                <input type="hidden" value="<?php //echo $parent_id;?>" name="id" />
				<input type="hidden" name="id" value="<?php echo $data['id_negara'];?>">
                <button type="submit" class="btn btn-success">Submit</button>
			   </div>
               </form>
			   
            </div>

          </div>
        </div>
		
         <div style="clear:both"></div>
        <!-- /page content -->
<?php include_once dirname(__FILE__).'/../layouts/footer.php';?>