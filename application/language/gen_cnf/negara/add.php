<?php include_once dirname(__FILE__).'/../layouts/header.php';?>
<script type="text/javascript" src="<?php echo $js.'ckeditor/ckeditor.js'; ?>"></script>
<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Negara</h3>
              </div>

            
            </div>

            <div class="clearfix"></div>

            <div class="row">
             <form id="demo-form" data-parsley-validate  enctype="multipart/form-data" method="post" action="<?php echo site_url('negara/actionadd');?>">
              
			  <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_content">
                    
                      <label for="fullname">Nama Negara :</label>
                      <input type="text" id="fullname" class="form-control" name="nama_negara" data-parsley-trigger="change" />
                      <br/>
					  <label for="fullname">Inisial Negara :</label>
                      <input type="text" id="fullname" class="form-control" name="nama_negara2" data-parsley-trigger="change" />
                      <br/>
					  
					  <label for="fullname">Bendera :</label>
                      <input type="file" id="fullname" class="form-control" name="image"  />
                      <br/>
                      
					  <label for="fullname">Aktif :</label>
					  <input type="radio" name="status" value="1" checked="checked" > Ya  <input type="radio" name="status" value="0"> Tidak
                      <br/><br/>

                      <label for="email">Meta Keywords :</label>
                      <textarea id="email" class="form-control" name="meta_key" data-parsley-trigger="change"></textarea>
                      <br/>

                      <label for="email">Meta Descriptions :</label>
                      <textarea id="email" class="form-control" name="meta_des" data-parsley-trigger="change"></textarea>
                      <br/>

                  </div>
                </div>
              </div>

               <div  class="col-md-12">
                <input type="hidden" value="<?php //echo $parent_id;?>" name="id" />
                <button type="submit" class="btn btn-success">Submit</button>
			   </div>
               </form>
			   
            </div>

          </div>
        </div>
		
         <div style="clear:both"></div>
        <!-- /page content -->
<?php include_once dirname(__FILE__).'/../layouts/footer.php';?>