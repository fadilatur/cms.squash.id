<?php include_once dirname(__FILE__).'/../layouts/header.php';?>

<!-- Datatables -->
    <link href="<?php echo $css;?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $css;?>vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $css;?>vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $css;?>vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $css;?>vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">


<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="row top_tiles" style="margin: 10px 0;">
              <div class="col-md-3 col-sm-3 col-xs-6 tile">
                <h3>Paper</h3>
                <span class="sparkline_one" style="height: 160px;">
                      <canvas width="200" height="60" style="display: inline-block; vertical-align: top; width: 94px; height: 30px;"></canvas>
                  </span>
              </div>
            </div>
            <br />


            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="dashboard_graph x_panel">
                  <div class="row x_title">
				  <div style='padding-bottom:10px'><button onclick="location.href='<?php echo site_url('paper/add/'.$id);?>'" type="button" class="btn btn-info">Add</button>
									
									<button onclick="location.href='<?php echo site_url('paper/toexcel');?>'" type="button" class="btn btn-info">Download to Excel</button>
									</div>
                    
                    <div class="col-md-6">
                      
                    </div>
                  </div>
                   <div><?php //echo $paging;?></div>
                  <div class="x_content">
                    <table id="datatablesalah" class="table table-striped table-bordered">
                      <thead>
                        <tr>
						  <th>ID Paper</th>
                          <th>Thumbnail</th>
						  <th>Tanggal</th>
                          <th>Option</th>
                        </tr>
                      </thead>
					  <tbody>
					  <?php $i=1;
					  foreach($data as $user){?>
                        <tr>
						  <td><?php echo $i;?></td>
                          <td><?php echo $user['thumbnail'];?></td>
						  <td><?php echo $user['tanggal'];?></td>
						  <td>
							<div class="btn-group"><button onclick="location.href='<?php echo site_url('paper/edit/'.$user['id_paper']);?>'" type="button" class="btn btn-info">Edit</button></div>
							
							
							
							<div class="btn-group"><button onclick="location.href='<?php echo site_url('paper/actiondelete/'.$user['id_paper']);?>'" type="button" class="btn btn-info">Dellete</button></div>
                            
						  </td>
						  
                        </tr>
						<?php $i++;}?>
                      </tbody>
					  
					  
					 </table> 
                  </div>
                  <div><?php //echo $paging;?></div>
                </div>



              </div>
            </div>


            
          </div>
        </div>
        <!-- /page content -->
		
		<!-- Datatables -->
    <script src="<?php echo $css;?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-scroller/js/datatables.scroller.min.js"></script>
    <script src="<?php echo $css;?>vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo $css;?>vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo $css;?>vendors/pdfmake/build/vfs_fonts.js"></script>
    <script>
       $('#datatable').dataTable();
    </script>
<?php include_once dirname(__FILE__).'/../layouts/footer.php';?>