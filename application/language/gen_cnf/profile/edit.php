<?php include_once dirname(__FILE__).'/../layouts/header.php';?>
<script type="text/javascript" src="<?php echo $js.'ckeditor/ckeditor.js'; ?>"></script>
<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>PROFILE</h3>
              </div>

            
            </div>

            <div class="clearfix"></div>

            <div class="row">
             <form id="demo-form" data-parsley-validate  enctype="multipart/form-data" method="post" action="<?php echo site_url('profile/actionedit');?>">
              
			  <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_content">
                      
					  <label for="fullname">Nama Lengkap :</label>
                      <input type="text" id="name" class="form-control" name="name" value="<?php echo $data['nama'];?>" data-parsley-trigger="change" />
                      <br/>
					  
					  <label for="fullname">Email :</label>
                      <input type="email" id="email" class="form-control" name="email" value="<?php echo $data['email'];?>" data-parsley-trigger="change" />
                      <br/>
					  
					  <label for="fullname">Deskripsi (285 karakter):</label>
                      <textarea id="fullname" class="form-control" name="deskripsi" data-parsley-trigger="change" /><?php echo $data['des_admin'];?></textarea>
					  <br/>

                  </div>
                </div>
				<div class="x_panel">
                  <div class="x_title">
                    <h2>Foto :</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                       <?php if(!empty($data['foto_admin'])){ ?>
                            <img src="<?php echo $upload;?><?php echo $data['foto_admin'];?>"/>
                             <input type="hidden" id="fullname" class="form-control" name="imagex" value="<?php echo $data['foto_admin'];?>"  />
                       <?php } ?>
                       <input type="file" id="fullname" class="form-control" name="image"  />
                  </div>

                </div>
              </div>

               <div  class="col-md-12">
                <input type="hidden" value="<?php echo $data['id_adm'];?>" name="id" />
                <button type="submit" class="btn btn-success">Submit</button>
			   </div>
               </form>
			   
            </div>

          </div>
        </div>
		
         <div style="clear:both"></div>
        <!-- /page content -->
<?php include_once dirname(__FILE__).'/../layouts/footer.php';?>