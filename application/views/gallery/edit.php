<?php include_once dirname(__FILE__).'/../layouts/header.php';?>
<script type="text/javascript" src="<?php echo $js.'ckeditor/ckeditor.js'; ?>"></script>
<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Edit</h3>
              </div>
        <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <ol class="breadcrumb">
                        <li><a style="color:#000" href="<?php echo site_url('home')?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a style="color:#000" href="<?php echo site_url('profile');?>">Gallery</a></li>
                        <li class="active">Edit</li>
                    </ol>
                  </div>
                </div>
              </div>
            
            </div>

            <div class="clearfix"></div>

            <div class="row">
             <form id="demo-form" data-parsley-validate  enctype="multipart/form-data" method="post" action="<?php echo site_url('gallery/actionedit');?>">
              <div class="col-md-9">
                <div class="x_panel">
                 
                  <div class="x_content">
                    
                      <label for="fullname">Nama Gallerry * :</label>
                      <input type="text" id="fullname" class="form-control" value="<?php echo $data['nama_gallery'];?>" name="judul" required />
                      <br/>


                     

                   
                  </div>
                </div>


               


                


               

              </div>

              <div class="col-md-3">


                <div class="x_panel">
                  <div class="x_title">
                    <h2>Tanggal</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                     <fieldset>
                          <div class="control-group">
                            <div class="controls">
                              <div class="col-md-11 xdisplay_inputx form-group has-feedback">
                                <input type="text" value="<?php echo date('Y-m-d');?>" name="tgl" class="form-control has-feedback-left" id="single_cal3" placeholder="" aria-describedby="inputSuccess2Status3">
                                <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                <span id="inputSuccess2Status3" class="sr-only">(success)</span>
                              </div>
                            </div>
                          </div>
                        </fieldset>

                        <label for="email">Pukul:</label>
                        <div class="row">
                        <div class="col-md-6">
                       <select id="heard" name="jam" class="form-control" >
                            <option value="00">Jam</option>
                            <?php for($i=0;$i<25;$i++){ 
                                if($i<10){
                                  $nilai1 = "0".$i;
                                }else{
                                  $nilai1 = $i;
                                }

                            ?>
                             <option <?php echo (date('H')==$i)?"selected='selected'":"";?> value="<?php echo $nilai1;?>"><?php echo $nilai1;?></option>
                            <?php } ?>
                            
                          
                          </select>
                          </div><div class="col-md-6">
                           <select name="menit" id="heard" class="form-control" >
                            <option value="">Menit</option>
                            <?php for($i=0;$i<61;$i++){ 
                                if($i<10){
                                  $nilai1 = "0".$i;
                                }else{
                                  $nilai1 = $i;
                                }

                            ?>
                            <option <?php echo (date('i')==$nilai1)?"selected='selected'":"";?> value="<?php echo $nilai1;?>"><?php echo $nilai1;?></option>
                            <?php } ?>
                          </select>
                          </div>
                      <br/>
                    
                  </div>
                </div>
              </div>


               <div class="x_panel">
                  <div class="x_title">
                    <h2>Publish</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                        <input type="radio" name="publish" value="Y" checked="checked" > YES  <input type="radio" name="publish" value="N"> NO
                      
                  </div>

                </div>

                </div>


                <div  class="col-md-12">
                <input type="text" value="<?php echo $parent_id;?>" name="parent_id" />
                 <button type="submit" class="btn btn-success">Submit</button></div>
               </form>
            </div>

          </div>
        </div>
         <div style="clear:both"></div>
        <!-- /page content -->
<?php include_once dirname(__FILE__).'/../layouts/footer.php';?>