<?php include_once dirname(__FILE__).'/../layouts/header.php';?>

 <!-- Datatables -->
    <link href="<?php echo $css;?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $css;?>vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $css;?>vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $css;?>vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $css;?>vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

<!-- page content -->
       <div class="right_col" role="main">
          <div class="">
            <div class="row top_tiles" style="margin: 10px 0;">
              <div class="col-md-3 col-sm-3 col-xs-6 tile">
                <h3>Detail</h3>
               
              </div>
            </div>
            <br />


            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="dashboard_graph x_panel">
                  <div class="row x_title">
          <div class="col-md-3">
          <button onclick="location.href='<?php echo site_url('gallery/add_detail/'.$this->uri->segment(3));?>'" type="button" class="btn btn-info">Add</button>
         
          </div>
                    
                   
                  </div>
                   <div><?php //echo $paging;?></div>
                  <div class="x_content">
                    <table id="datatablesalah" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                     
                    <th>Images</th>
                    <th>Kredit</th>
                    <th>Summary</th>
              

                          <th>Option</th>
                        </tr>
                      </thead>
                      <?php foreach ($detail as $detail){

                        $path = "images/".date('Y/m/d/', strtotime($detail['postdate']));
                        ?>
            <tbody>
                        <tr>
             
                          <td>
                            <?php
                              $mystring = $detail['image'];
                              $findme   = 'http';
                              $pos = strpos($mystring, $findme);

                              // Note our use of ===.  Simply == would not work as expected
                              // because the position of 'a' was the 0th (first) character.
                              if ($pos === false) {?>
                              <img  src="<?php echo $tim.$upload.$path.$detail['image'];?>">    
                              <?php } else {?>
                                  <img  src="<?php echo $tim.$detail['image'];?>">
                            <?php }?>
                          </td>
                          <td><?php echo $detail['kredit'];?></td>
                          <td><?php echo $detail['summary'];?></td>
              

              <td>
              <div class="btn-group"><button onclick="location.href='<?php echo site_url('gallery/editdetail/'.$detail['id_detail_gallery'].'/'.$detail['id']);?>'" type="button" class="btn btn-info">Edit</button></div>
              <div class="btn-group"><button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal<?php echo $detail['id'];?>">Delete</button></td>

                <div class="modal modal-info" id="myModal<?php echo $detail['id'];?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">×</span></button>
                        <h4 class="modal-title">Confirmation</h4>
                      </div>
                      <div class="modal-body">
                        Anda yakin akan menghapus data ini ?
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                        <a href="<?php echo site_url('gallery/actiondelete_detail/'.$detail['id'].'/'.$detail['id_detail_gallery']);?>" class="btn btn-outline">Delete</a>
                      </div>
                    </div>
                  
                  <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
              </div>  

              </td>
              <?php ;}?> 
                        </tr>
                      </tbody>
            
            
           </table> 
                  </div>
                  <div><?php //echo $paging;?></div>
                </div>



              </div>
            </div>


            
          </div>
        </div>
        <!-- /page content -->
 <!-- Datatables -->
    <script src="<?php echo $css;?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-scroller/js/datatables.scroller.min.js"></script>
    <script src="<?php echo $css;?>vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo $css;?>vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo $css;?>vendors/pdfmake/build/vfs_fonts.js"></script>
    <script>
       $('#datatable').dataTable();
    </script>

<?php include_once dirname(__FILE__).'/../layouts/footer.php';?>