<?php include_once dirname(__FILE__).'/../layouts/header.php';?>
<script type="text/javascript" src="<?php echo $js.'ckeditor/ckeditor.js'; ?>"></script>
<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Fokus</h3>
              </div>
				<div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <ol class="breadcrumb">
                        <li><a style="color:#000" href="<?php echo site_url('home')?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a style="color:#000" href="<?php echo site_url('fokus');?>">Fokus</a></li>
                        <li class="active">Edit</li>
                    </ol>
                  </div>
                </div>
              </div>
            
            </div>

            <div class="clearfix"></div>

            <div class="row">
             <form id="demo-form" data-parsley-validate  enctype="multipart/form-data" method="post" action="<?php echo site_url('fokus/actionedit');?>">
              <div class="col-md-12">
                <div class="x_panel">
                 
                  <div class="x_content">
                    
                      <label for="fullname">judul * :</label>
                      <input type="text" id="fullname" value="<?php echo $data['judul'];?>" class="form-control" name="judul" required />
					  <input type="hidden" id="fullname" value="<?php echo $data['id'];?>" class="form-control" name="id" required />
                      <br/>

                  </div>

                  <div class="x_content">
                  <?php 
                  //take folder for image
                  $path = "images/".date('Y/m/d/', strtotime($data['postdate']));?>
                      
                       
                        
                        <label for="browse">Image Thumbnail :</label><br>
                        <?php
                        $mystring = $data['image'];
                        $findme   = 'http';
                        $pos = strpos($mystring, $findme);
                        if ($pos === false) {?>
                          <img src="<?php echo $tim.$upload.$path.$data['image'];?>&w=188&h=125&zc=0" id="preview"/>
                        <?php } else {?>
                          <img src="<?php echo $tim.$data['image'];?>&w=188&h=125&zc=0" id="preview"/>
                      <?php }?>
                        <input type="hidden" name="image" id="imagethumb" value="<?php echo $data['image'];?>">
                        <input type="hidden" name="imagex" id="imagethumbwatermark">
                        <button type="button" id="browse" onclick="openMyKc()" style="margin-top: 5px">Pilih File</button><span id="warn2">Tidak ada file</span>
                  </div>

                  <div class="x_content">
                    
                      <label for="fullname">summary * :</label>
                      <input type="text" id="fullname" value="<?php echo $data['summary'];?>" class="form-control" name="summary" required />
            <input type="hidden" id="fullname" value="<?php echo $data['id'];?>" class="form-control" name="id" required />
                      <br/>

                  </div>

				  <div  class="col-md-12">
				  <input type='hidden' name="file" value="<?php //echo $data['id_file'];?>">
                 <button type="submit" class="btn btn-success">Submit</button></div>
               </form>
            </div>
                </div>
				
            </div>


                

          </div>
        </div>
         <div style="clear:both"></div>
        <!-- /page content -->
<script>
  function openMyKc() {

    window.open('http://cms.rilis.id/index.php/image/browse/1', 'kcfinder_single', "width=700, height=600, left=400; top=50");
}
</script>
<?php include_once dirname(__FILE__).'/../layouts/footer.php';?>