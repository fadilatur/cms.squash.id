<?php include_once dirname(__FILE__).'/../layouts/header.php';?>
<script type="text/javascript" src="<?php echo $js.'ckeditor/ckeditor.js'; ?>"></script>
<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Add Fokus</h3>
              </div>
				<div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <ol class="breadcrumb">
                        <li><a style="color:#000" href="<?php echo site_url('home')?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a style="color:#000" href="<?php echo site_url('fokus');?>">Fokus</a></li>
                        <li class="active">Add</li>
                    </ol>
                  </div>
                </div>
              </div>
            
            </div>

            <div class="clearfix"></div>

            <div class="row">
             <form id="demo-form" data-parsley-validate  enctype="multipart/form-data" method="post" action="<?php echo site_url('fokus/actionadd');?>">
              <div class="col-md-12">
                <div class="x_panel">
                 
                  <div class="x_content">
                    
                      <label for="fullname">judul * :</label>
                      <input type="text" id="fullname" class="form-control" name="judul" required />
                      <br/>
                  </div>

                 <div class="x_content">
                  
                  <label for="browse">Image* :</label><br>
                  <img id="preview">
                  <input type="hidden" name="image" id="imagethumb">
                  <input type="hidden" name="imagewatermark" id="imagethumbwatermark">
                  <button type="button" id="browse" onclick="openMyKc()" style="margin-top: 5px">Pilih File</button><span id="warn2">Tidak ada file</span>
                  </div>

                  <div class="x_content">
                    
                      <label for="fullname">summary * :</label>
                      <input type="text" id="fullname" class="form-control" name="summary" required />
                      <br/>
                  </div>

				  
				  <div  class="col-md-12">
				  
                 <button type="submit" class="btn btn-success">Submit</button></div>
               </form>
            </div>
                </div>
				
            </div>


                

          </div>
        </div>
         <div style="clear:both"></div>
        <!-- /page content -->
<script>
  function openMyKc() {

    window.open('http://cms.rilis.id/index.php/image/browse/1', 'kcfinder_single', "width=700, height=600, left=400; top=50");
}
</script>
<?php include_once dirname(__FILE__).'/../layouts/footer.php';?>