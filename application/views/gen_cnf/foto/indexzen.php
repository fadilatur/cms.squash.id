<?php include_once dirname(__FILE__).'/../layouts/header.php';?>

 <!-- Datatables -->
    <link href="<?php echo $css;?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $css;?>vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $css;?>vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $css;?>vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $css;?>vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="row top_tiles" style="margin: 10px 0;">
              <div class="col-md-3 col-sm-3 col-xs-6 tile">
                <h3>Artikel Rilizen</h3>  
              </div>
			  
            </div>
             <h4 style="color: #000"><?php echo $this->session->flashdata('notifikasidelete');?></h4>
            <br />

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="dashboard_graph x_panel">
                  <div class="row x_title">
				  <!--div class="col-md-3">

				  <button onclick="location.href='<?php echo site_url('artikel/add/'.$parent_id);?>'" type="button" class="btn btn-info">Add</button>
				  <<button onclick="location.href='<?php echo site_url('administrator/toexcel');?>'" type="button" class="btn btn-info">Download to Excel</button>>
				  </div-->
          <form action="<?php echo site_url('artikel/rilizen');?>" method="POST">
           <div class="col-md-3">
            <input type="text" name="search" class="form-control" value="<?php if(!empty($search)) {echo $search;} else {'';}?>">
          </div>
          <div class="col-md-1">
            <button class="btn btn-info" type="submit">Search</button>
          </div>
          <div class="col-md-8"></div>
          </form>
                  </div>
                  <div class="x_content">
                    <table id="datatablesalah" class="table table-striped table-bordered">
                      <thead>
                        <tr>
						  <th>No</th>
               
                    <th>Judul Artikel</th>
                    <th>Channel</th>      
                    <th>Status</th>
					<th>Tanggal Publish</th>
                    <th>Author</th>
                

                          <th width="250">Option</th>
                        </tr>
                      </thead>
					  <tbody>
					  <?php $i=1;
					  foreach($data as $user){
              $channel = $this->T_section->get($user['id_section']);
              ?>
                        <tr>
						  <td><?php echo $i+$pages;?></td>
               
                          <td><a style="color:#000" href="<?php echo 'http://dev.rilis.id/www.rilis.id/'.$user['urltitle'];?>" target="_blank"><?php echo $user['judul_artikel'];?></a></td>

                          <td><?php echo $channel['nama_section'];?></td>

						  <td><div style="float:left;border-radius:4px;color:#fff;font-weight:bold;padding:8px;background:#<?php if($user['publish'] == 'Y') {echo '4ce85a';} elseif($user['publish'] == 'N'){echo 'd30675';} elseif($user['publish'] == 'S'){echo 'ff1a1a';} else {echo '000000';}?>"><?php echo $user['publish'];?></div></td>

						  <td><?php echo $user['tgl_pub'];?></td>

              <td><?php echo $user['nama_lengkap'];?></td>

              
						  <td>

             
              <?php
              if($this->session->userdata('status') == 'T') {?>

              <div class="btn-group"><a href="<?php echo site_url('artikel/restore/'.$user['id_artikel']);?>"><button onclick="location.href='<?php echo site_url('artikel/edit/'.$user['id_artikel']);?>'" type="button" class="btn btn-info">Restore</button></a></div>
							<div class="btn-group"><button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal<?php echo $i;?>">Delete</button></div>
              <div class="modal modal-info" id="myModal<?php echo $i;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">×</span></button>
                        <h4 class="modal-title">Confirmation</h4>
                      </div>
                      <div class="modal-body">
                        Anda yakin akan menghapus data ini secara <b>PERMANEN</b>?
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                        <a href="<?php echo site_url('artikel/deletezen/'.$user['id_artikel']);?>" class="btn btn-danger">Delete</a>
                      </div>
                    </div>
                  
                  <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
              </div>
              <?php ;} else {?>

              <div class="btn-group"><a href="<?php echo site_url('artikel/editzen/'.$user['id_artikel']);?>"><button onclick="location.href='<?php echo site_url('artikel/editzen/'.$user['id_artikel']);?>'" type="button" class="btn btn-info">Edit</button></a></div>
              <div class="btn-group"><button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal<?php echo $i;?>">Delete</button></div>
              <div class="modal modal-info" id="myModal<?php echo $i;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">×</span></button>
                        <h4 class="modal-title">Confirmation</h4>
                      </div>
                      <div class="modal-body">
                        Anda yakin akan menghapus data ini ?
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                        <a href="<?php echo site_url('artikel/deletezen/'.$user['id_artikel']);?>" class="btn btn-danger">Delete</a>
                      </div>
                    </div>
                  
                  <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
              </div>

              <?php
              if($user['id_section'] == 2 || $user['id_section'] == 130) {?>
              <div class="btn-group"><button onclick="location.href='<?php echo site_url('artikel/index/'.$user['id_artikel']);?>'" type="button" class="btn btn-info">Sub</button></div>
              <?php ;} else {'';}?>
               
							<?php if ($user['headline']=='N'){?>
                <div class="btn-group"><button onclick="location.href='<?php echo site_url('artikel/hlzen/'.$user['id_artikel']);?>'" type="button" class="btn btn-info">HL</button></div>
               <?php }else{?>
                <div class="btn-group"><button style='background:#000' onclick="location.href='<?php echo site_url('artikel/hlzen/'.$user['id_artikel']);?>'" type="button" class="btn btn-info">unHL</button></div>
               
              
              <?php }  ?>

              <?php if ($user['hot']=='N'){?>
                <div class="btn-group"><button onclick="location.href='<?php echo site_url('artikel/hotzen/'.$user['id_artikel']);?>'" type="button" class="btn btn-info">PILIHAN</button></div>
               <?php }else{?>
                <div class="btn-group"><button style='background:#000' onclick="location.href='<?php echo site_url('artikel/hotzen/'.$user['id_artikel']);?>'" type="button" class="btn btn-info">BATAL PILIHAN</button></div>
               
              
              <?php }  ?>
              <?php } ?>
                            
						  </td>
						  
                        </tr>
						<?php $i++;}?>
                      </tbody>
					  
					  
					 </table> 
                  </div>
                  <div><?php echo $paging;?></div>
                </div>



              </div>
            </div>


            
          </div>
        </div>
        <!-- /page content -->
 <!-- Datatables -->
    <script src="<?php echo $css;?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-scroller/js/datatables.scroller.min.js"></script>
    <script src="<?php echo $css;?>vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo $css;?>vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo $css;?>vendors/pdfmake/build/vfs_fonts.js"></script>
    <script>
       $('#datatable').dataTable();
    </script>

<?php include_once dirname(__FILE__).'/../layouts/footer.php';?>