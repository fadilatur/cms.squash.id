<?php include_once dirname(__FILE__).'/../layouts/header.php';?>
<script type="text/javascript" src="<?php echo $js.'ckeditor/ckeditor.js'; ?>"></script>
<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Foto</h3>
              </div>
				<div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <ol class="breadcrumb">
                        <li><a style="color:#000" href="<?php echo site_url('home')?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a style="color:#000" href="<?php echo site_url('foto');?>">Foto</a></li>
                        <li class="active">Edit</li>
                    </ol>
                  </div>
                </div>
              </div>
            
            </div>

            <div class="clearfix"></div>

            <div class="row">
             <form id="demo-form" data-parsley-validate  enctype="multipart/form-data" method="post" action="<?php echo site_url('foto/actionedit');?>">
              <div class="col-md-9">
                <div class="x_panel">
                 
                  <div class="x_content">
                    
                      <label for="fullname">Judul (90 Karakter) * :</label> <span style="float: right;" id="countjudul">90</span>
                      <input type="text" id="judul" class="form-control" value="<?php echo $data['judul_artikel'];?>" name="judul" required maxlength="90"/>
                      <br/>
                      <label for="email">Lead (130 Karakter) * :</label> <span style="float: right;" id="countgagasan">130</span>
                      <input type="text" id="gagasan" class="form-control" value="<?php echo $data['gagasan_utama'];?>" name="gagasan"  maxlength="130"/>
                      
                      <br/>

                      <label for="email">Content * :</label>
                      <textarea id="email" class="form-control" name="detail" data-parsley-trigger="change"><?php echo $data['isi_artikel'];?></textarea>
                      <script type="text/javascript">
                                                var editor = CKEDITOR.replace("detail", {
                                                    filebrowserBrowseUrl    : '<?php echo site_url('filebrowser');?>',
                                                    filebrowserWindowWidth  : 1000,
                                                    filebrowserWindowHeight : 500,
                                                    height : 800
                                                });
                                            </script>
                      <br/>

                      <label for="fullname">Sumber *:</label>
                      <input type="text" id="fullname" class="form-control" name="sumber" value="<?php echo $data['url'];?>" />
                      <br/>

                      <label for="fullname">Video URL * :</label>
                      <input type="text" id="fullname" class="form-control" name="url" value="<?php echo $data['id_video'];?>" />
                      <br/>

                     <label for="keyword">Meta Keywords (140 Karakter) * :</label> <span style="float: right;" id="countkeyword">140</span>
                      <textarea id="keyword" class="form-control" name="mkey" data-parsley-trigger="change" maxlength="140"><?php echo $data['meta_key'];?></textarea>
                      <br/>

                      <label for="email">Meta Descriptions (150 Karakter) * :</label> <span style="float: right;" id="countdesc">150</span>
                      <textarea id="desc" class="form-control" name="mdesc" data-parsley-trigger="change" maxlength="150"><?php echo $data['meta_des'];?></textarea>
                      <br/>


                     

                   
                  </div>
                </div>


               


                


                <div class="x_panel">
                  <div class="x_title">
                    <h2>Tags</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <!--
                     <select class="select2_multiple form-control" name="tag[]" multiple="multiple">
                     <?php foreach($tag as $tg){  ?>
                            <option <?php echo (in_array($tg['id_tag'], $isi))?"selected='selected'":"";?> value="<?php echo $tg['nama_tag'];?>"><?php echo $tg['nama_tag'];?></option>
                     <?php } ?>
                            </select>
                      -->
                      <textarea class="form-control" name="tag"><?php $row = '';
    foreach($tagnya as $tagnya)
    {
      $tanda = $this->T_tag->get($tagnya['id_object2']);
      $row .= $tanda['nama_tag'].',';
    }
    $last = substr($row,0,-1);echo $last;?></textarea>
                  </div>
                </div>
                <!--
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Position</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="form-group">
                    <label for="sel1">Select one:</label>
                      <select class="form-control" id="sel1" name="position">
                        <option <?php echo ($data['position']== '0')?'selected':'0';?>>0</option>
                        <option <?php echo ($data['position']== '1')?'selected':'1';?>>1</option>
                        <option <?php echo ($data['position']== '2')?'selected':'2';?>>2</option>
                        <option <?php echo ($data['position']== '3')?'selected':'3';?>>3</option>
                        <option <?php echo ($data['position']== '4')?'selected':'4';?>>4</option>
                      </select>
                  </div>
                </div>  
                -->
              </div>

              <div class="col-md-3">

                <div class="x_panel">
                  <div class="x_title">
                    <h2>Kategori</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                      <select class="select2_group form-control" name="kategori">
                        <option value="2">Foto</option>
                      </select>
                  </div>

                </div>

                <!-- <div class="x_panel">
                  <div class="x_title">
                    <h2>Kolom</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                        <div class="form-group">
                          <label for="sel1">Select one:</label>
                            <select class="select2_group form-control" name="kolom">
                            <?php
                            /*if($data['kolom'] == 0) {?>
                            <option value="0" selected="selected">--</option>
                            <?php foreach($kolom as $a){ ?>
                              <option value="<?php echo $a['id'];?>"><?php echo $a['nama'];?></option>
                            <?php } ?>
                            <?php ;} else {?>
                            <option value="0" selected="selected">--</option>
                            <?php foreach($kolom as $a){ ?>
                              <option value=""<?php if($a['id'] == $data['kolom']) {echo 'Selected';} else { '' ;};?>><?php echo $a['nama'];?></option>
                            <?php } ?>
                            <?php ;}*/ ?>
                            <!-- </select>
                        </div>
                      
                  </div>

                </div> --> 
                <?php
                if($this->session->userdata('level') == 3) {?>
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Penulis</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                        <div class="form-group">
                          <label for="sel1">Select one:</label>
                            <select class="select2_group form-control" name="admin">
                         
                                              <option  value="<?php echo $this->session->userdata('id_adm');?>"><?php echo $this->session->userdata('nama');?></option>
                                        
                                        
                                </select>
                        </div>
                      
                  </div>

                </div>
                <?php ;}else{?>
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Penulis</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                        <div class="form-group">
                          <label for="sel1">Select one:</label>
                            <select class="select2_group form-control" name="admin">
                          <option value="0">--</option>
                                         <?php
                                       
                                        foreach($member as $trans2){ 
                                        ?>
                                              <option <?php echo ($trans2['id_adm']==$data['id_admin'])?'selected':'' ;?> value="<?php echo $trans2['id_adm'];?>"><?php echo $trans2['nama'];?></option>
                                        <?php } ?>
                                        
                                </select>
                        </div>
                      
                  </div>

                </div>
                <?php ;} ?>

                <?php
                if($this->session->userdata('level') == 1) {?>
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Editor</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                        <div class="form-group">
                          <label for="sel1">Select one:</label>
                            <select class="select2_group form-control" name="editor">
                                      <option value="0">--</option>
                                         <?php
                                       
                                        foreach($editor as $editor){ 
                                        ?>
                                              <option <?php echo ($editor['id_adm'] == $data['editor'])?'selected':'' ;?> value="<?php echo $editor['id_adm'];?>"><?php echo $editor['nama'];?></option>
                                        <?php } ?>
                                        
                                </select>
                        </div>
                      
                  </div>

                </div>
                <?php ;}elseif($this->session->userdata('level') == 2) {?>
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Editor</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                        <div class="form-group">
                          <label for="sel1">Select one:</label>
                            <select class="select2_group form-control" name="editor">
      
                                               <option  value="<?php echo $this->session->userdata('id_adm')?>"><?php echo $this->session->userdata('nama')?></option>
                                </select>
                        </div>
                      
                  </div>

                </div>
                <?php ;} ?>

                <div class="x_panel">
                  <div class="x_title">
                    <h2>Fokus</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                        <div class="form-group">
                          <label for="sel1">Select one:</label>
                            <select class="select2_group form-control" name="fokus">
                            <?php
                            if($data['fokus'] == 0) {?>
                            <option value="0" selected="selected">--</option>
                            <?php foreach($fokus as $a){ ?>
                              <option value="<?php echo $a['id'];?>"><?php echo $a['judul'];?></option>
                            <?php } ?>
                            <?php ;} else {?>
                            <option value="0" selected="selected">--</option>
                            <?php foreach($fokus as $a){ ?>
                              <option value=""<?php if($a['id'] == $data['fokus']) {echo 'Selected';} else { '' ;};?>><?php echo $a['judul'];?></option>
                            <?php } ?>
                            <?php ;} ?>
                            </select>
                        </div>
                      
                  </div>

                </div>

                <div class="x_panel">
                  <div class="x_title">
                    <h2>Foto</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                  <?php 
                  //take folder for image
                  $path = date('Y/m/d/', strtotime($data['postdate']));?>
                      
                       
                        
                        <label for="browse">Image Thumbnail :</label><br>
                        <?php
                        if(!empty($data['thumbnail'])) {
                        $mystring = $data['thumbnail'];
                        $findme   = 'http';
                        $pos = strpos($mystring, $findme);
                        if ($pos === false) {?>
                            
                            <?php
                            if(!empty($data['thumbnail_watermark'])) {?>
                            <img src="<?php echo $tim.$upload.$path.$data['thumbnail_watermark'];?>&w=188&h=125&zc=0" width="100%" id="preview"/>
                            <?php ;} else { ?>
                            <img src="<?php echo $tim.$upload.$path.$data['thumbnail'];?>&w=188&h=125&zc=0" width="100%" id="preview"/>
                            <?php ;} ?>

                          <?php } else {?>

                             <?php
                            if(!empty($data['thumbnail_watermark'])) {?>
                            <img src="<?php echo $tim.$data['thumbnail_watermark'];?>&w=188&h=125&zc=0" width="100%" id="preview"/>
                            <?php ;} else { ?>
                            <img src="<?php echo $tim.$data['thumbnail'];?>&w=188&h=125&zc=0" width="100%" id="preview"/>
                            <?php ;} ?>
                          <?php }?>
                        <?php ;} else { ?>
                        <img src="<?php echo $tim.$data['oldimage'];?>&w=188&h=125&zc=0" width="100%" id="preview"/>
                        <?php ;} ?>
                        <input type="hidden" name="image" id="imagethumb" value="<?php echo $data['thumbnail'];?>">
                        <input type="hidden" name="imagewatermark" id="imagethumbwatermark" value="<?php echo $data['thumbnail_watermark'];?>">
                        <button type="button" id="browse" onclick="openMyKc()" style="margin-top: 5px">Pilih File</button><span id="warn2">Tidak ada file</span>

                       <!--
                       <?php if(!empty($data['thumb'])){ ?>
                            <img src="<?php echo $tim.$upload.$path.$data['thumb'];?>&w=200" width="100%"/>
                             <input type="hidden" id="fullname" class="form-control" name="thumbx" value="<?php echo $data['thumb'];?>"  />
                       <?php } ?>
                        <label for="email">Image Detail (1265 x 507 ) :</label>
                       <input type="file" id="fullname" class="form-control" name="thumb"  />
                     -->
                      <br/>
                      <label for="email">Caption:</label>
                       <input type="text" id="fullname" class="form-control" name="caption" value="<?php echo $data['ket_thumbnail'];?>"  />
                       <input type="hidden" name="path" value="<?php echo $path;?>">
                      <br/>
                  </div>

                </div>


                <div class="x_panel">
                  <div class="x_title">
                    <h2>Tanggal</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                     <fieldset>
                          <div class="control-group">
                            <div class="controls">
                              <div class="col-md-15 xdisplay_inputx form-group has-feedback">
                                <input type="text" name="tgl" class="form-control has-feedback-left" id="single_cal3" placeholder="" aria-describedby="inputSuccess2Status3" value="<?php echo $data['tanggal'];?>">
                                <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                <span id="inputSuccess2Status3" class="sr-only">(success)</span>
                              </div>
                            </div>
                          </div>
                        </fieldset>

                        <label for="email">Jam:</label>
                        <div class="row">
                        <div class="col-md-6">
                        <?php 
                        $rt = explode(":",$data['jam']); ?>
                       <select id="heard" name="jam" class="form-control" >
                            <option value="00">Jam</option>

                           <?php for($i=0;$i<24;$i++){ 
                                if($i<10){
                                  $nilai1 = "0".$i;
                                }else{
                                  $nilai1 = $i;
                                }

                            ?>
                             <option <?php echo ($rt[0]==$i)?"selected='selected'":"";?> value="<?php echo $nilai1;?>"><?php echo $nilai1;?></option>
                            <?php } ?>
                          
                          </select>
                          </div><div class="col-md-6">
                           <select name="menit" id="heard" class="form-control" >
                            <option value="00">Menit</option>
                            <?php for($i=0;$i<61;$i++){ 
                                if($i<10){
                                  $nilai1 = "0".$i;
                                }else{
                                  $nilai1 = $i;
                                }

                            ?>
                            <option <?php echo ($rt[1]==$nilai1)?"selected='selected'":"";?> value="<?php echo $nilai1;?>"><?php echo $nilai1;?></option>
                            <?php } ?>
                          </select>
                          </div>
                      <br/>
                    
                  </div>
                </div>
              </div>


               <div class="x_panel">
                  <div class="x_title">
                    <h2>Publish</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                        
                        <?php 
                        if($this->session->userdata('level') == 3) {?>

                          <?php
                          if($data['publish'] == 'Y') {?>
                            <input type="radio"  <?php echo ($data['publish']=='Y')?"checked='checked'":"";?> name="publish" value="Y"> YES  
                          <?php ;} else { ?>
                            <input type="radio" name="publish" <?php echo ($data['publish']=='N')?"checked='checked'":"";?> value="N"> NO
                          <?php ;} ?>
                       
                        <?php } else {?>
                       
                        <input type="radio"  <?php echo ($data['publish']=='Y')?"checked='checked'":"";?> name="publish" value="Y"> YES  
                        <input type="radio" name="publish" <?php echo ($data['publish']=='N')?"checked='checked'":"";?> value="N"> NO
                        
                        <?php ;}?>

                        
                        <!--
                        <input type="radio" name="publish" <?php echo ($data['publish']=='S')?"checked='checked'":"";?> value="S"> SCHEDULING
                      -->
                      
                  </div>

                </div>

                <div class="x_panel">
                  <div class="x_title">
                    <h2>Kota</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                        <input type="text" name="kota" class="form-control" value="<?php echo $data['kota'];?>">
                      
                  </div>

                </div>

                <div class="x_panel">
                  <div class="x_title">
                    <h2>Sponsored Content</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                        <input type="radio" name="sponsored" value="Y" <?php echo ($data['sponsored']=='Y')?"checked='checked'":"";?>> YES  
                        <input type="radio" name="sponsored" value="N" <?php echo ($data['sponsored']=='N')?"checked='checked'":"";?>> NO
                      
                  </div>

                </div>

                <div class="x_panel">
                  <div class="x_title">
                    <h2>Position</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                        <div class="form-group">
                          <label for="sel1">Select one:</label>
                            <select class="form-control" id="sel1" name="position">
                              <option <?php echo ($data['position']== '0')?'selected':'0';?>>0</option>
                              <option <?php echo ($data['position']== '1')?'selected':'1';?>>1</option>
                              <option <?php echo ($data['position']== '2')?'selected':'2';?>>2</option>
                              <option <?php echo ($data['position']== '3')?'selected':'3';?>>3</option>
                              <option <?php echo ($data['position']== '4')?'selected':'4';?>>4</option>
                            </select>
                        </div>
                      
                  </div>

                </div>

                </div>


                <div  class="col-md-12"> 
                <input type="hidden" value="<?php echo $data['parent_id'];?>" name="parent_id" />
                <input type="hidden" value="<?php echo $data['id_artikel'];?>" name="id_artikel" />
                <input type="hidden" value="<?php echo $daerah;?>" name="daerah" />
                <?php
                if($this->session->userdata('level') == 3) {?>
                 <button type="submit" class="btn btn-success">Draft</button>
                 <?php ;} else {?>
                 <button type="submit" class="btn btn-success">Submit</button>
                 <?php ;}?>
                </div>
               </form>
            </div>

          </div>
        </div>
         <div style="clear:both"></div>
        <!-- /page content -->
<?php include_once dirname(__FILE__).'/../layouts/footer.php';?>
<script type="text/javascript">
  $('#judul').on('keyup', function() {
    console.log(this.value.length);
    var rescount = parseInt(55) - parseInt(this.value.length);
    $("#countjudul").text(rescount);
    //if(this.value.length == 55) {
      //alert('Teks yang anda masukkan sudah penuh');
    //}
  });

  $(window).on('load', function() {
    var judul = $("#judul").val().length;
    var rescount = parseInt(55) - parseInt(judul);
    $("#countjudul").text(rescount);
  })

  $('#gagasan').on('keyup', function() {
    console.log(this.value.length);
    var rescount = parseInt(130) - parseInt(this.value.length);
    $("#countgagasan").text(rescount);
    //if(this.value.length == 330) {
      //alert('Teks yang anda masukkan sudah penuh');
    //}
  });

  $(window).on('load', function() {
    var gagasan = $("#gagasan").val().length;
    var rescount = parseInt(130) - parseInt(gagasan);
    $("#countgagasan").text(rescount);
  })

  $('#keyword').on('keyup', function() {
    console.log(this.value.length);
    var rescount = parseInt(140) - parseInt(this.value.length);
    $("#countkeyword").text(rescount);
    //if(this.value.length == 140) {
      //alert('Teks yang anda masukkan sudah penuh');
    //}
  });

  $(window).on('load', function() {
    var keyword = $("#keyword").val().length;
    var rescount = parseInt(140) - parseInt(keyword);
    $("#countkeyword").text(rescount);
  })

  $('#desc').on('keyup', function() {
    console.log(this.value.length);
    var rescount = parseInt(150) - parseInt(this.value.length);
    $("#countdesc").text(rescount);
    //if(this.value.length == 150) {
      //alert('Teks yang anda masukkan sudah penuh');
    //}
  });

  $(window).on('load', function() {
    var desc = $("#desc").val().length;
    var rescount = parseInt(150) - parseInt(desc);
    $("#countdesc").text(rescount);
  })


  function openMyKc() {

    window.open('http://cms.rilis.id/index.php/image/browse/1', 'kcfinder_single', "width=700, height=600, left=400; top=50");
}
</script>