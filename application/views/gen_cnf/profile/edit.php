<?php include_once dirname(__FILE__).'/../layouts/header.php';?>
<script type="text/javascript" src="<?php echo $js.'ckeditor/ckeditor.js'; ?>"></script>
<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Profile</h3>
              </div>
        <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <ol class="breadcrumb">
                        <li><a style="color:#000" href="<?php echo site_url('home')?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a style="color:#000" href="<?php echo site_url('profile');?>">Profile</a></li>
                        <li class="active">edit</li>
                    </ol>
                  </div>
                </div>
              </div>
            
            </div>

            <div class="clearfix"></div>

            <div class="row">
             <form id="demo-form" data-parsley-validate  enctype="multipart/form-data" method="post" action="<?php echo site_url('profile/actionedit');?>">
              <div class="col-md-9">
                <div class="x_panel">
                 
                  <div class="x_content">
                      <label for="fullname">Judul * :</label>
                      <input type="text" id="fullname" class="form-control" value="<?php echo $data['judul_hs'];?>"  name="judul" required />
                      <br/>
                      <label for="email">Gagasan Utama (330 Karakter) * :</label>
                      <textarea id="email" class="form-control" name="gagasan" data-parsley-trigger="change"><?php echo $data['seo_hs'];?></textarea>
                      <br/>

                      <label for="email">Content * :</label>
                      <textarea id="email" class="form-control" name="detail" data-parsley-trigger="change"><?php echo $data['konten_hs'];?></textarea>
                      <script type="text/javascript">
                                                var editor = CKEDITOR.replace("detail", {
                                                    filebrowserBrowseUrl    : '<?php echo site_url('filebrowser');?>',
                                                    filebrowserWindowWidth  : 1000,
                                                    filebrowserWindowHeight : 500,
                                                    height : 800
                                                });
                                            </script>
                                            <br>
                      <label for="fullname">Meta Title</label>
                      <input type="text" id="fullname" class="form-control" name="meta_title" value="<?php echo $data['meta_title'];?>" />
                      <br>
                      <label for="fullname">Meta Keyword</label>
                      <input type="text" id="fullname" class="form-control" name="meta_key" value="<?php echo $data['meta_key'];?>" />
                      <br>
                      <label for="fullname">Meta Description</label>
                      <input type="text" id="fullname" class="form-control" name="meta_des" value="<?php echo $data['meta_des'];?>" />
                      <br/>


                     

                   
                  </div>
                </div>


               


                


               

              </div>

              <div class="col-md-3">

               
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Foto <span style="font-size:12px;color:#000">( 800 x 450 pixel)</span></h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                       <input type="file" id="fullname" class="form-control" name="image"  />
                      <br/>
                      <label for="email">Caption:</label>
                       <input type="text" id="fullname" class="form-control" name="caption"  />
                      <br/>
                  </div>

                </div>


                <div class="x_panel">
                  <div class="x_title">
                    <h2>Tanggal</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                     <fieldset>
                          <div class="control-group">
                            <div class="controls">
                              <div class="col-md-11 xdisplay_inputx form-group has-feedback">
                                <input type="text" value="<?php echo date('Y-m-d');?>" name="tgl" class="form-control has-feedback-left" id="single_cal3" placeholder="" aria-describedby="inputSuccess2Status3">
                                <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                <span id="inputSuccess2Status3" class="sr-only">(success)</span>
                              </div>
                            </div>
                          </div>
                        </fieldset>

                        <label for="email">Pukul:</label>
                        <div class="row">
                        <div class="col-md-6">
                       <select id="heard" name="jam" class="form-control" >
                            <option value="00">Jam</option>
                            <?php for($i=0;$i<25;$i++){ 
                                if($i<10){
                                  $nilai1 = "0".$i;
                                }else{
                                  $nilai1 = $i;
                                }

                            ?>
                             <option <?php echo (date('H')==$i)?"selected='selected'":"";?> value="<?php echo $nilai1;?>"><?php echo $nilai1;?></option>
                            <?php } ?>
                            
                          
                          </select>
                          </div><div class="col-md-6">
                           <select name="menit" id="heard" class="form-control" >
                            <option value="">Menit</option>
                            <?php for($i=0;$i<61;$i++){ 
                                if($i<10){
                                  $nilai1 = "0".$i;
                                }else{
                                  $nilai1 = $i;
                                }

                            ?>
                            <option <?php echo (date('i')==$nilai1)?"selected='selected'":"";?> value="<?php echo $nilai1;?>"><?php echo $nilai1;?></option>
                            <?php } ?>
                          </select>
                          </div>
                      <br/>
                    
                  </div>
                </div>
              </div>


               <div class="x_panel">
                  <div class="x_title">
                    <h2>Publish</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                        <input type="radio" name="publish" value="Y" checked="checked" > YES  <input type="radio" name="publish" value="N"> NO
                      
                  </div>

                </div>

                </div>


                <div  class="col-md-12">
                <input type="hidden" name="daerah" value="<?php echo $data['daerah'];?>">
                <input type="hidden" value="<?php echo $this->uri->segment(3);?>" name="id_hs" />
                 <button type="submit" class="btn btn-success">Submit</button></div>
               </form>
            </div>

          </div>
        </div>
         <div style="clear:both"></div>
        <!-- /page content -->
<?php include_once dirname(__FILE__).'/../layouts/footer.php';?>