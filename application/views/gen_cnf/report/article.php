<?php include_once dirname(__FILE__).'/../layouts/header.php';?>

 <!-- Datatables -->
    <link href="<?php echo $css;?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $css;?>vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $css;?>vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $css;?>vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $css;?>vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="row top_tiles" style="margin: 10px 0;">
              <div class="col-md-12 col-sm-12 col-xs-12 tile">
                <h3>Report Jumlah PageView Periode <?php echo date('d F Y', strtotime($tgl1));?> - <?php echo date('d F Y', strtotime($tgl2));?></h3>
               
              </div>
            </div>
            <br />


            <div class="row">




              <div class="col-md-12 col-sm-12 col-xs-12">


                 <div class="row">

                   <form id="demo-form" data-parsley-validate  enctype="multipart/form-data" method="post" action="<?php echo site_url('report/article');?>">
            
                <div class="col-md-1 col-sm-1 col-xs-1">
                  From 
                   </div>
                    <div class="col-md-4 col-sm-4 col-xs-4">
               <div class="col-md-11 xdisplay_inputx form-group has-feedback">
                                <input type="text" value="<?php echo $tgl1;?>" name="tgl1" class="form-control has-feedback-left" id="single_cal2" placeholder="" aria-describedby="inputSuccess2Status3">
                                <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                <span id="inputSuccess2Status3" class="sr-only">(success)</span>
                              </div>

                            </div>
<div class="col-md-1 col-sm-1 col-xs-1">
                 To 
                   </div>
<div class="col-md-4 col-sm-4 col-xs-4">
   
                               <div class="col-md-11 xdisplay_inputx form-group has-feedback">
                                <input type="text" value="<?php echo $tgl2;?>" name="tgl2" class="form-control has-feedback-left" id="single_cal3" placeholder="" aria-describedby="inputSuccess2Status3">
                                <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                <span id="inputSuccess2Status3" class="sr-only">(success)</span>
                              </div>

                              </div>
                              <div class="col-md-2 col-sm-2 col-xs-2">
                 <button type="submit" >SUBMIT</button> 
                   </div>
</form>
             </div>

                <div class="dashboard_graph x_panel">

                   <div><?php //echo $paging;?></div>
                  <div class="x_content">
                    <table id="datatablesalah" class="table table-striped table-bordered">
                      <thead>
                        <tr>
						              <th>No</th>
                          <th>Penulis</th>
                          <th>Editor</th>
                          <th>Judul</th>
                          <th>Hari/Tgl</th>
                          <th>Kategori</th>
                          <th>PageView</th>
                        </tr>
                      </thead>
					            <tbody>
                       <?php
                        $i=1;
                        foreach($laporan as $laporan) {
                          if(!empty($laporan['id_admin']))
                          {
                            $param = $this->T_admin->get_tadmin($laporan['id_admin']);
                            $admin = $param['nama'];
                          }
                          else
                          {
                            $admin = '--';
                          }

                          if(!empty($laporan['editor']))
                          {
                            $param = $this->T_admin->get_tadmin($laporan['editor']);
                            $editor = $param['nama'];
                          }
                          else
                          {
                            $editor = '--';
                          }

                          if(!empty($laporan['id_section']))
                          {
                            $param = $this->T_section->getsection($laporan['id_section']);
                            $section = $param['nama_section'];
                          }
                          else
                          {
                            $section = '--';
                          }
                          ?>
                        <tr>
                          <td><?php echo $i;?></td>
                          <td><?php echo $admin;?></td>
                          <td><?php echo $editor;?></td>
                          <td><?php echo $laporan['judul_artikel'];?></td>
                          <td><?php echo $laporan['tgl_pub'];?></td>
                          <td><?php echo $section;?></td>
                          <td><?php echo (int)$laporan['dibaca'] + (int)$laporan['dibaca_m'] + (int)$laporan['dibaca_a'] + (int)$laporan['dibaca_i'];?></td>
                        </tr>
                        <?php $i++;} ?>
             
                      </tbody>
					  
					  
					 </table> 
                  </div>
                  <div><?php  // echo $paging;?></div>
                </div>



              </div>
            </div>


            
          </div>
        </div>
        <!-- /page content -->
 <!-- Datatables -->
    <script src="<?php echo $css;?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-scroller/js/datatables.scroller.min.js"></script>
    <script src="<?php echo $css;?>vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo $css;?>vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo $css;?>vendors/pdfmake/build/vfs_fonts.js"></script>
    <script>
       $('#datatable').dataTable();
    </script>

<?php include_once dirname(__FILE__).'/../layouts/footer.php';?>