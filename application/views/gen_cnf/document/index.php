<?php include_once dirname(__FILE__).'/../layouts/header.php';?>

 <!-- Datatables -->
    <link href="<?php echo $css;?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $css;?>vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $css;?>vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $css;?>vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $css;?>vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="row top_tiles" style="margin: 10px 0;">
              <div class="col-md-3 col-sm-3 col-xs-6 tile">
                <h3>Document</h3>
               
              </div>
            </div>
            <br />


            <div class="row">




              <div class="col-md-12 col-sm-12 col-xs-12">


                 <div class="row">

                   <form id="demo-form" data-parsley-validate  enctype="multipart/form-data" method="post" action="">
            
                <div class="col-md-1 col-sm-1 col-xs-1">
                 <!-- From -->
                   </div>
                    <div class="col-md-4 col-sm-4 col-xs-4">
               <!--<div class="col-md-11 xdisplay_inputx form-group has-feedback">
                                <input type="text" value="<?php echo date('Y-m-d');?>" name="tgl" class="form-control has-feedback-left" id="single_cal2" placeholder="" aria-describedby="inputSuccess2Status3">
                                <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                <span id="inputSuccess2Status3" class="sr-only">(success)</span>
                              </div>-->

                            </div>
<div class="col-md-1 col-sm-1 col-xs-1">
                 <!--To -->
                   </div>
<div class="col-md-4 col-sm-4 col-xs-4">
   
                              <!-- <div class="col-md-11 xdisplay_inputx form-group has-feedback">
                                <input type="text" value="<?php echo date('Y-m-d');?>" name="tgl2" class="form-control has-feedback-left" id="single_cal3" placeholder="" aria-describedby="inputSuccess2Status3">
                                <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                <span id="inputSuccess2Status3" class="sr-only">(success)</span>
                              </div>-->

                              </div>
                              <div class="col-md-2 col-sm-2 col-xs-2">
							 
                 <!--<button type="submit" >SUBMIT</button>--> 
                   </div>
				</form>
             </div>


                <div class="dashboard_graph x_panel">

                   <div><?php //echo $paging;?></div>
                  <div class="x_content">
				 <button onclick="location.href='<?php echo site_url('document/add/');?>'" type="button" class="btn btn-info">Add</button>
                    <table id="datatablesalah" class="table table-striped table-bordered">
                      <thead>
                        <tr>
						  <th>No</th>
						  <th>Nama File</th>
						  <th>Url</th>
                        
						  <th>Option</th>
						  <!--<th>Total Dibaca</th>-->
                        
                        </tr>
                      </thead>
					  <tbody>
					  <?php $i=1; foreach($data as $data){
              $explode = explode(' ', $data['createdate']);
              $path = "document/".date('Y/m/d/', strtotime($explode[0]));
              ?>
                        <tr>
						<td><?php echo $i;?></td>
						  <td><?php echo $data['nama_file'];?><br/><?php echo $data['createdate'];?><br/><?php echo $data['nama'];?></td>
						<td>
							 <div class="btn-group"><input type="text" style="float: left; width: 100%"  value="<?php echo $upload.$path.$data['nama_file'];?>" id="myInput<?php echo $data['id_file'];?>" readonly><br/>
                <button style="height: 27px;padding-top: 2px;" onclick="myFunction('myInput<?php echo $data['id_file'];?>')" type="button" class="btn btn-info">Copy</button>
						</td>
						 
							<td>
							 <div><button onclick="location.href='<?php echo site_url('document/edit/'.$data['id_file']);?>'" type="button" class="btn btn-info">Edit</button></div>
							 <div class="btn-group"><a data-href="<?php echo site_url('gen_cnf/document/actiondelete/'.$data['id_file']);?>" data-toggle="modal" data-target="#confirm-delete<?php echo $i;?>" href="#"><button type="button" class="btn btn-info">Delete</button></a></div>
							 <div class="modal fade" id="confirm-delete<?php echo $i;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                        <h4 class="modal-title" id="myModalLabel">Confirm Delete</h4>
                                                                    </div>
                                                                
                                                                    <div class="modal-body">
                                                                        <p>You are about to delete this data.</p>
                                                                        <p>Do you want to proceed?</p>
                                                                        <p class="debug-url"></p>
                                                                    </div>
                                                                    
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                                                        <a href="<?php echo site_url('document/actiondelete/'.$data['id_file']);?>" class="btn btn-danger danger">Delete</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
							</td>
						  
                        </tr>
						<?php $i++;}?>
                      </tbody>
					  
					  
					 </table> 
                  </div>
                  <div><?php  // echo $paging;?></div>
                </div>



              </div>
            </div>


            
          </div>
        </div>
        <!-- /page content -->
 <!-- Datatables -->
    <script src="<?php echo $css;?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-scroller/js/datatables.scroller.min.js"></script>
    <script src="<?php echo $css;?>vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo $css;?>vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo $css;?>vendors/pdfmake/build/vfs_fonts.js"></script>
    <script>
       $('#datatable').dataTable();
    </script>
<script>
function myFunction(isi) {
  var copyText = document.getElementById(isi);
  copyText.select();
  document.execCommand("Copy");
  //alert("Copied the text: " + copyText.value);
}
</script>
<?php include_once dirname(__FILE__).'/../layouts/footer.php';?>