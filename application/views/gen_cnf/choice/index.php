<?php include_once dirname(__FILE__).'/../layouts/header.php';?>
<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Choice</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-4">
             <form id="demo-form" data-parsley-validate  enctype="multipart/form-data" method="post" action="<?php echo site_url('choice/index/'.$polling);?>/<?php echo (!empty($detail['id']))?$detail['id']:0;?>">
			 
			 <input type="hidden" name="polling" value="<?php echo $polling;?>">
             <div class="x_panel">
                  <div class="x_title">
               
                      <br/>
                       <label for="fullname">Choice * :</label>
                      <input type="text" id="fullname" class="form-control" value="<?php echo (!empty($detail['content']))?$detail['content']:"";?>" name="choice" required />
                      <br/>
                      


                       <button type="submit" class="btn btn-success">Submit</button>
                      
                  </div>
              </div>
              </form>
            </div>

              <div class="col-md-8 col-sm-8 col-xs-8">
                <div class="x_panel">
                  <div class="x_title">
				
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>

                  <div class="x_content">
                    
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                         <th>No</th>
						  <th>Choice</th>
						  <th>Option</th>
                        </tr>
                      </thead>

                      <tbody>
					  <?php $i=1;
            $pub = array('No','Yes');
					  foreach($data as $user){?>
                        <tr>
						  <td><?php echo $i;?></td>
						  <td><?php echo $user['content'];?></td>
 
						  <td>
							
                            <div class="btn-group"><button onclick="location.href='<?php echo site_url('choice/index/'.$polling.'/'.$user['id']);?>'" type="button" class="btn btn-info">Edit</button></div>
                            <div class="btn-group"><a data-href="<?php echo site_url('choice/actiondelete/');?>" data-toggle="modal" data-target="#confirm-delete<?php echo $i;?>" href="#"><button type="button" class="btn btn-info">Delete</button></a></div>
							
                                                    
                            <div class="modal fade" id="confirm-delete<?php echo $i;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                   <div class="modal-content">                         
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                             <h4 class="modal-title" id="myModalLabel">Confirm Delete</h4>
                                        </div>             
                                        <div class="modal-body">
                                            <p>You are about to delete this data.</p>
                                            <p>Do you want to proceed?</p>
                                            <p class="debug-url"></p>
                                        </div>
                                                                
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
											<a href="<?php echo site_url('choice/actiondelete/'.$polling.'/'.$user['id']);?>" class="btn btn-danger danger">Delete</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
						  </td>
                        </tr>
						<?php $i++;}?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

              </div>
              
            </div>
          </div>
        </div>
        <!-- /page content -->
<?php include_once dirname(__FILE__).'/../layouts/footer.php';?>