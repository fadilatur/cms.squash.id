<?php include_once dirname(__FILE__).'/../layouts/header.php';?>
            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        ADMINISTRATOR
                    </h1>
                    <ol class="breadcrumb">
                        <li><a style="color:#000" href="<?php echo base_url('gen_cnf/home');?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a style="color:#000" href="<?php echo site_url('gen_cnf/administrator');?>">Administrator</a></li>
                        <li class="active">Add</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-12">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">Administrator > Add</h3>
                                </div><!-- /.box-header -->
                                <!-- form start -->
                                <form role="form" enctype="multipart/form-data" method="post" action="<?php echo site_url('gen_cnf/administrator/actionadd');?>">
                                    <div class="box-body">
									 <div class="form-group">
                                            <label for="number">Group Role</label><br/>
                                            <select style="width:250px" name='group_id'>
                                            <?php foreach($group as $mnm):?>
                                            <option value='<?php echo $mnm['id'];?>' > <?php echo $mnm['group_name'];?></option>
                                            <?php endforeach;?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Username</label>
                                            <input type="username" name="username" class="form-control" id="exampleInputUsername1" placeholder="Enter username">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Password</label>
                                            <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Email address</label>
                                            <input type="email" name="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Phone Number</label>
                                            <input type="number" name="phone" class="form-control" id="exampleInputNumber1" placeholder="Enter Phone Number">
                                        </div>
                                        <div class="form-group">
                                            <label>Address</label>
                                            <textarea class="form-control" name="address" rows="3" placeholder="Enter ..."></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputFile">Input Image</label>
                                            <input type="file" name="image" id="exampleInputFile">
                                        </div>
                                    </div><!-- /.box-body -->

                                    <div class="box-footer">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </form>
                            </div><!-- /.box -->
                        </div><!--/.col (right) -->
                    </div>   <!-- /.row -->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->
        <!-- jQuery 2.0.2 -->
        <script src="<?php echo $js;?>jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="<?php echo $Js;?>bootstrap.min.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="<?php echo $Js;?>AdminLTE/app.js" type="text/javascript"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="<?php echo $Js;?>AdminLTE/demo.js" type="text/javascript"></script>  
<?php include_once dirname(__FILE__).'/../layouts/header.php';?>