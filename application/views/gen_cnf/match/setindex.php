<?php include_once dirname(__FILE__).'/../layouts/header.php';?>
<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>SET</h3>
      </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <button onclick="location.href='<?php echo site_url('match/setadd/'.$match);?>'" type="button" class="btn btn-info">Add</button>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <table id="datatable" class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Set</th>
                  <th>Status</th>
                  <th>Option</th>
                </tr>
              </thead>
              <tbody>
                <?php $i=1; foreach($set as $set) { ?>
                <tr>
                  <td><?php echo $i;?></td>
                  <td><?php echo $set['set'];?></td>
                  <td><?php if($set['status'] == 0) {echo '<strong>NON ACTIVE</strong>';} else {echo '<strong>ACTIVE</strong>';};?></td>
                  <td>
                    <div class="btn-group"><button onclick="location.href='<?php echo site_url('match/setactive/'.$match.'/'.$set['set']);?>'" type="button" class="btn btn-info">SET ACTIVE</button></div>
                    <div class="btn-group"><button onclick="location.href='<?php echo site_url('match/score/'.$match.'/'.$set['set']);?>'" type="button" class="btn btn-info">Score</button></div>
                    <div class="btn-group"><button onclick="location.href='<?php echo site_url('match/setedit/'.$match.'/'.$set['id']);?>'" type="button" class="btn btn-info">Edit</button></div>
                    <div class="btn-group"><button data-toggle="modal" data-target="#confirm-delete<?php echo $i;?>" type="button" class="btn btn-info">Delete</button></div>
                    <div class="modal fade" id="confirm-delete<?php echo $i;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel">Confirm Delete</h4>
                          </div>
                          <div class="modal-body">
                            <p>You are about to delete this data.</p>
                            <p>Do you want to proceed?</p>
                            <p class="debug-url"></p>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            <a href="<?php echo site_url('match/setdelete/'.$set['set'].'/'.$match.'/'.$set['id']);?>" class="btn btn-danger danger">Delete</a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </td>
                </tr>
                <?php $i++;} ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<!-- /page content -->
<?php include_once dirname(__FILE__).'/../layouts/footer.php';?>