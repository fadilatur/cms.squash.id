<?php include_once dirname(__FILE__).'/../layouts/header.php';?>
<script type="text/javascript" src="<?php echo $js.'ckeditor/ckeditor.js'; ?>"></script>
<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>MATCH</h3>
      </div>
      <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
          <div class="input-group">
            <ol class="breadcrumb">
              <li><a style="color:#000" href="<?php echo site_url('home')?>"><i class="fa fa-dashboard"></i> Home</a></li>
              <li><a style="color:#000" href="<?php echo site_url('score');?>">Match</a></li>
              <li class="active">Add</li>
            </ol>
          </div>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
      <form id="demo-form" data-parsley-validate  enctype="multipart/form-data" method="post" action="<?php echo site_url('match/playeractionadd/'.$match);?>">
        <div class="col-md-12">
          <div class="x_panel">
            <div class="x_content">
              <label for="fullname">Player Status :</label>
              <select class="form-control" name="status">
                <option selected disabled>Pilih status</option>
                <option value="1">Player 1</option>
                <option value="2">Player 2</option>
              </select>
              <br/>
              <label for="fullname">Player Name :</label>
              <input type="text" id="name" name="name" placeholder="Name" class="form-control">
              <br/>
              <label for="fullname">Player Country :</label>
              <input type="text" id="country" name="country" placeholder="Country" class="form-control">
              <br/>
              <label for="fullname">Player Flag :</label>
              <input type="file" id="flag" name="flag" class="form-control">
              <br/>
              <label for="fullname">Player Image :</label>
              <input type="file" id="image" name="image" class="form-control">
              <br/>
            </div>
          </div>
        </div>
        <div  class="col-md-12">
          <button type="submit" class="btn btn-success">Submit</button>
        </div>
      </form>
    </div>
  </div>
</div>
<div style="clear:both"></div>
<!-- /page content -->
<?php include_once dirname(__FILE__).'/../layouts/footer.php';?>