<?php include_once dirname(__FILE__).'/../layouts/header.php';?>
<script type="text/javascript" src="<?php echo $js.'ckeditor/ckeditor.js'; ?>"></script>
<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>MATCH</h3>
      </div>
      <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
          <div class="input-group">
            <ol class="breadcrumb">
              <li><a style="color:#000" href="<?php echo site_url('home')?>"><i class="fa fa-dashboard"></i> Home</a></li>
              <li><a style="color:#000" href="<?php echo site_url('score');?>">Match</a></li>
              <li class="active">Edit</li>
            </ol>
          </div>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
      <form id="demo-form" data-parsley-validate  enctype="multipart/form-data" method="post" action="<?php echo site_url('match/actionedit/'.$match['id'].'/'.$parent);?>">
        <div class="col-md-12">
          <div class="x_panel">
            <div class="x_content">
              <label for="fullname">Match Name :</label>
              <input type="text" id="name" name="name" placeholder="Match" class="form-control" value="<?php echo $match['name'];?>">
              <br/>
            </div>
          </div>
        </div>
        <div  class="col-md-12">
          <button type="submit" class="btn btn-success">Submit</button>
        </div>
      </form>
    </div>
  </div>
</div>
<div style="clear:both"></div>
<!-- /page content -->
<?php include_once dirname(__FILE__).'/../layouts/footer.php';?>