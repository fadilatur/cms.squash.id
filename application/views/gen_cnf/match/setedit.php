<?php include_once dirname(__FILE__).'/../layouts/header.php';?>
<script type="text/javascript" src="<?php echo $js.'ckeditor/ckeditor.js'; ?>"></script>
<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>SET</h3>
      </div>
      <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
          <div class="input-group">
            <ol class="breadcrumb">
              <li><a style="color:#000" href="<?php echo site_url('home')?>"><i class="fa fa-dashboard"></i> Home</a></li>
              <li><a style="color:#000" href="<?php echo site_url('score');?>">Set</a></li>
              <li class="active">Add</li>
            </ol>
          </div>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
      <form id="demo-form" data-parsley-validate  enctype="multipart/form-data" method="post" action="<?php echo site_url('match/setactionedit/'.$match.'/'.$set['id']);?>">
        <div class="col-md-12">
          <div class="x_panel">
            <div class="x_content">
              <label for="fullname">Set Number :</label>
              <select class="form-control" name="set">
                <option selected disabled>Pilih set</option>
                <option value="1" <?php if($set['set'] == 1) {echo "selected";};?>>1</option>
                <option value="2" <?php if($set['set'] == 2) {echo "selected";};?>>2</option>
                <option value="3" <?php if($set['set'] == 3) {echo "selected";};?>>3</option>
                <option value="4" <?php if($set['set'] == 4) {echo "selected";};?>>4</option>
                <option value="5" <?php if($set['set'] == 5) {echo "selected";};?>>5</option>
              </select>
              <br/>
            </div>
          </div>
        </div>
        <div  class="col-md-12">
          <input type="hidden" name="oldset" value="<?php echo $set['set'];?>"> 
          <button type="submit" class="btn btn-success">Submit</button>
        </div>
      </form>
    </div>
  </div>
</div>
<div style="clear:both"></div>
<!-- /page content -->
<?php include_once dirname(__FILE__).'/../layouts/footer.php';?>