<?php include_once dirname(__FILE__).'/../layouts/header.php';?>
<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>GROUP MATCH</h3>
      </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <button onclick="location.href='<?php echo site_url('match/groupadd');?>'" type="button" class="btn btn-info">Add</button>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <table id="datatable" class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Group Name</th>
                  <th>Created</th>
                  <th>Option</th>
                </tr>
              </thead>
              <tbody>
                <?php $i=1; foreach($group as $group) { ?>
                <tr>
                  <td><?php echo $i;?></td>
                  <td><?php echo $group['name'];?></td>
                  <td><?php echo $group['created'];?></td>
                  <td>
                    <div class="btn-group"><button onclick="location.href='<?php echo site_url('match/index/'.$group['id']);?>'" type="button" class="btn btn-info">Match</button></div>
                    <div class="btn-group"><button onclick="location.href='<?php echo site_url('match/groupedit/'.$group['id']);?>'" type="button" class="btn btn-info">Edit</button></div>
                    <div class="btn-group"><button data-toggle="modal" data-target="#confirm-delete<?php echo $i;?>" type="button" class="btn btn-info">Delete</button></div>
                    <div class="modal fade" id="confirm-delete<?php echo $i;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel">Confirm Delete</h4>
                          </div>
                          <div class="modal-body">
                            <p>You are about to delete this data.</p>
                            <p>Do you want to proceed?</p>
                            <p class="debug-url"></p>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            <a href="<?php echo site_url('match/groupdelete/'.$group['id']);?>" class="btn btn-danger danger">Delete</a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </td>
                </tr>
                <?php $i++;} ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<!-- /page content -->
<?php include_once dirname(__FILE__).'/../layouts/footer.php';?>