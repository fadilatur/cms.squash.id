<?php include_once dirname(__FILE__).'/../layouts/header.php';?>
<script type="text/javascript" src="<?php echo $js.'ckeditor/ckeditor.js'; ?>"></script>

<style>
/* The container */
.container-input {
    display: block;
    position: relative;
    padding-left: 35px;
    margin-bottom: 12px;
    cursor: pointer;
    font-size: 22px;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}

/* Hide the browser's default radio button */
.container-input input {
    position: absolute;
    opacity: 0;
    cursor: pointer;
}

/* Create a custom radio button */
.checkmark {
    position: absolute;
    top: 0;
    left: 0;
    height: 25px;
    width: 25px;
    background-color: #eee;
    border-radius: 50%;
}

/* On mouse-over, add a grey background color */
.container-input:hover input ~ .checkmark {
    background-color: #ccc;
}

/* When the radio button is checked, add a blue background */
.container-input input:checked ~ .checkmark {
    background-color: #2196F3;
}

/* Create the indicator (the dot/circle - hidden when not checked) */
.checkmark:after {
    content: "";
    position: absolute;
    display: none;
}

/* Show the indicator (dot/circle) when checked */
.container-input input:checked ~ .checkmark:after {
    display: block;
}

/* Style the indicator (dot/circle) */
.container-input .checkmark:after {
  top: 9px;
  left: 9px;
  width: 8px;
  height: 8px;
  border-radius: 50%;
  background: white;
}
</style>

<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>LIVE SCORE</h3>
      </div>
      <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
          <div class="input-group">
            <ol class="breadcrumb">
              <li><a style="color:#000" href="<?php echo site_url('home')?>"><i class="fa fa-dashboard"></i> Home</a></li>
              <li><a style="color:#000" href="<?php echo site_url('score');?>">Score</a></li>
              <li class="active">Edit</li>
            </ol>
          </div>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
      <div class="col-md-12" style="text-align: center; background-color: #eee; margin-bottom: 30px">
        <h1><strong>SET <?php echo $set;?></strong></h3>
      </div>
      <!-- <form id="demo-form" data-parsley-validate  enctype="multipart/form-data" method="post" action="<?php echo site_url('match/setactionadd/'.$match);?>"> -->
        <div class="col-md-6">
          <div class="x_panel">
            <div class="x_content">
              <div class="row">
                <div class="col-md-12" style="text-align: left;  background-color: #eee; margin-bottom: 20px">
                  <h3><strong>PLAYER 1</strong></h3>
                </div>
                <div class="col-md-12" style="margin-bottom: 20px">
                  <label class="container-input">
                    <input type="radio" name="radio" id="serve1" <?php if($player1['serve'] == 1) {echo 'checked';};?>>
                    <span class="checkmark"></span>
                  </label>
                </div>
                <div class="col-md-12" style="text-align: center;">
                  <img src="<?php echo $tim.$upload.'/player/'.$player1['image'];?>&w=220&h=220"> <br>
                  <h4><strong><?php echo $player1['name'];?></strong></h4>
                </div>
                <div class="col-md-12">
                  <div class="row">
                    <div class="col-md-3" style="text-align: right;">
                      <button type="button" class="btn btn-primary btn-lg" id="plus-score1">
                        <strong><i class="fa fa-plus"></i></strong>
                      </button>
                    </div>
                    <div class="col-md-6">
                      <input type="number" class="form-control" name="score1" id="score1" value="<?php echo $score1['score'];?>" style="text-align: center; font-size: 28px; padding: 22px; font-weight: bolder;">
                    </div>
                    <div class="col-md-3">
                      <button type="button" class="btn btn-primary btn-lg" id="minus-score1">
                        <strong><i class="fa fa-minus"></i></strong>
                      </button>
                    </div>
                  </div>
                </div>
              </div>
              <br/>
            </div>
          </div>
        </div>

        <div class="col-md-6">
          <div class="x_panel">
            <div class="x_content">
              <div class="row">
                <div class="col-md-12" style="text-align: left;  background-color: #eee; margin-bottom: 20px">
                  <h3><strong>PLAYER 2</strong></h3>
                </div>
                <div class="col-md-12" style="margin-bottom: 20px">
                  <label class="container-input">
                    <input type="radio" name="radio" id="serve2" <?php if($player2['serve'] == 1) {echo 'checked';};?>>
                    <span class="checkmark"></span>
                  </label>
                </div>
                <div class="col-md-12" style="text-align: center;">
                  <img src="<?php echo $tim.$upload.'/player/'.$player2['image'];?>&w=220&h=220"> <br>
                  <h4><strong><?php echo $player2['name'];?></strong></h4>
                </div>
                <div class="col-md-12">
                  <div class="row">
                    <div class="col-md-3" style="text-align: right;">
                      <button type="button" class="btn btn-primary btn-lg" id="plus-score2">
                        <strong><i class="fa fa-plus"></i></strong>
                      </button>
                    </div>
                    <div class="col-md-6">
                      <input type="number" class="form-control" name="score2" id="score2" value="<?php echo $score2['score'];?>" style="text-align: center; font-size: 28px; padding: 22px; font-weight: bolder;">
                    </div>
                    <div class="col-md-3">
                      <button type="button" class="btn btn-primary btn-lg" id="minus-score2">
                        <strong><i class="fa fa-minus"></i></strong>
                      </button>
                    </div>
                  </div>
                </div>
              </div>
              <br/>
            </div>
          </div>
        </div>


        <div class="col-md-12">
          <div class="x_panel">
            <div class="x_content">
              <div class="row">
                <div class="col-md-12" style="text-align: left;  background-color: #eee; margin-bottom: 20px">
                  <h3><strong>MATCH WINNER</strong></h3>
                </div>
                <div class="col-md-12">
                  <select class="form-control" id="winner">
                    <option selected disabled>Pilih pemenang</option>
                    <option value="<?php echo $player1['id'];?>"><?php echo $player1['name'];?></option>
                    <option value="<?php echo $player2['id'];?>"><?php echo $player2['name'];?></option>
                  </select>
                </div>
              </div>
              <br/>
            </div>
          </div>
        </div>

        <div  class="col-md-12">
          <a href="<?php echo site_url('match/set/'.$match);?>">
            <button type="button" class="btn btn-success btn-lg"><strong>SELESAI</strong></button>
          </a>
        </div>
      <!-- </form> -->
    </div>
  </div>
</div>
<div style="clear:both"></div>
<!-- /page content -->
<?php include_once dirname(__FILE__).'/../layouts/footer.php';?>
<script type="text/javascript">
  $(document).ready(function() {
    $("#plus-score1").click(function() {
      var score = $("#score1").val();
      var plus = parseInt(score) + 1;
      $("#score1").val(plus);
      $.ajax({
        url : "<?php echo site_url('match/updatescoreplayer/'.$match.'/'.$set.'/'.$player1['id']);?>",
        method : "POST",
        data : {'score' : plus},
        success : function(data) {
          console.log("oke");
        }
      });
    });

    $("#minus-score1").click(function() {
      var score = $("#score1").val();
      var minus = parseInt(score) - 1;
      if(score == 0) {
        $("#score1").val(0);
      } else {
        $("#score1").val(minus);
      }
      $.ajax({
        url : "<?php echo site_url('match/updatescoreplayer/'.$match.'/'.$set.'/'.$player1['id']);?>",
        method : "POST",
        data : {'score' : minus},
        success : function(data) {
          console.log("oke");
        }
      });
    });

    $("#plus-score2").click(function() {
      var score = $("#score2").val();
      var plus = parseInt(score) + 1;
      $("#score2").val(plus);
      $.ajax({
        url : "<?php echo site_url('match/updatescoreplayer/'.$match.'/'.$set.'/'.$player2['id']);?>",
        method : "POST",
        data : {'score' : plus},
        success : function(data) {
          console.log("oke");
        }
      });
    });

    $("#minus-score2").click(function() {
      var score = $("#score2").val();
      var minus = parseInt(score) - 1;
      if(score == 0) {
        $("#score2").val(0);
      } else {
        $("#score2").val(minus);
      }
      $.ajax({
        url : "<?php echo site_url('match/updatescoreplayer/'.$match.'/'.$set.'/'.$player2['id']);?>",
        method : "POST",
        data : {'score' : minus},
        success : function(data) {
          console.log("oke");
        }
      });
    });

    $("#winner").change(function() {
      var winner = $(this).val();
      $.ajax({
        url : "<?php echo site_url('match/setWinner/'.$match.'/'.$set);?>",
        method : "POST",
        data : {'winner' : winner},
        success : function(data) {
          console.log("oke");
        }
      });
    });

    $("#serve1").click(function() {
      $.ajax({
        url : "<?php echo site_url('match/setServe/'.$match.'/'.$player1['id']);?>",
        method : "GET",
        success : function(data) {
          console.log("oke");
        }
      });
    });

    $("#serve2").click(function() {
      $.ajax({
        url : "<?php echo site_url('match/setServe/'.$match.'/'.$player2['id']);?>",
        method : "GET",
        success : function(data) {
          console.log("oke");
        }
      });
    });
  });
</script>