<?php include_once dirname(__FILE__).'/../layouts/header.php';?>
<script type="text/javascript" src="<?php echo $js.'ckeditor/ckeditor.js'; ?>"></script>
<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Artikel</h3>
              </div>

            
            </div>

            <div class="clearfix"></div>

            <div class="row">
             <form id="demo-form" data-parsley-validate  enctype="multipart/form-data" method="post" action="<?php echo site_url('artikel/actionadd');?>">
              <div class="col-md-9">
                <div class="x_panel">
                 
                  <div class="x_content">
                    
                      <label for="fullname">Judul * :</label>
                      <input type="text" id="fullname" class="form-control" name="judul" required />
                      <br/>
                      <label for="email">Gagasan Utama (330 Karakter) * :</label>
                      <textarea id="email" class="form-control" name="gagasan" data-parsley-trigger="change"></textarea>
                      <br/>

                      <label for="email">Content * :</label>
                      <textarea id="email" class="form-control" name="detail" data-parsley-trigger="change"></textarea>
                      <script type="text/javascript">
                                                var editor = CKEDITOR.replace("detail", {
                                                    filebrowserBrowseUrl    : '<?php echo site_url('filebrowser');?>',
                                                    filebrowserWindowWidth  : 1000,
                                                    filebrowserWindowHeight : 500
                                                });
                                            </script>
                      <br/>

                      <label for="fullname">Video URL * :</label>
                      <input type="text" id="fullname" class="form-control" name="url"  />
                      <br/>

                      <label for="email">Meta Keywords * :</label>
                      <textarea id="email" class="form-control" name="mkey" data-parsley-trigger="change"></textarea>
                      <br/>

                      <label for="email">Meta Descriptions * :</label>
                      <textarea id="email" class="form-control" name="mdesc" data-parsley-trigger="change"></textarea>
                      <br/>


                     

                   
                  </div>
                </div>


               


                


                <div class="x_panel">
                  <div class="x_title">
                    <h2>Tags</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                     <select class="select2_multiple form-control" name="tag[]" multiple="multiple">
                     <?php foreach($tag as $tg){  ?>
                            <option value="<?php echo $tg['nama_tag'];?>"><?php echo $tg['nama_tag'];?></option>
                     <?php } ?>
                            </select>

                  </div>
                </div>

              </div>

              <div class="col-md-3">

                <div class="x_panel">
                  <div class="x_title">
                    <h2>Kategori</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                      <select class="select2_group form-control" name="kategori">
                      <?php
                      $cat = $this->T_supsection->select(0);
                      foreach($cat as $trans){ 
                      ?>
                            <optgroup label="<?php echo $trans['nama_supsection'];?>">
                             <?php
                            $bcd = $this->T_section->selectadmin($trans['id_supsection']);
                            foreach($bcd as $trans2){ 
                            ?>
                                  <option value="<?php echo $trans2['id_section'];?>"><?php echo $trans2['nama_section'];?></option>
                            <?php } ?>
                              
                            </optgroup>
                      <?php } ?>
                           
                          </select>
                  </div>

                </div>

                <div class="x_panel">
                  <div class="x_title">
                    <h2>Foto <span style="font-size:12px;color:#000">( 800 x 450 pixel)</span></h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                       <input type="file" id="fullname" class="form-control" name="image"  />
                      <br/>
                      <label for="email">Caption:</label>
                       <input type="text" id="fullname" class="form-control" name="caption"  />
                      <br/>
                  </div>

                </div>


                <div class="x_panel">
                  <div class="x_title">
                    <h2>Tanggal</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                     <fieldset>
                          <div class="control-group">
                            <div class="controls">
                              <div class="col-md-11 xdisplay_inputx form-group has-feedback">
                                <input type="text" value="<?php echo date('Y-m-d');?>" name="tgl" class="form-control has-feedback-left" id="single_cal3" placeholder="" aria-describedby="inputSuccess2Status3">
                                <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                <span id="inputSuccess2Status3" class="sr-only">(success)</span>
                              </div>
                            </div>
                          </div>
                        </fieldset>

                        <label for="email">Pukul:</label>
                        <div class="row">
                        <div class="col-md-6">
                       <select id="heard" name="jam" class="form-control" >
                            <option value="00">Jam</option>
                            <?php for($i=0;$i<25;$i++){ 
                                if($i<10){
                                  $nilai1 = "0".$i;
                                }else{
                                  $nilai1 = $i;
                                }

                            ?>
                             <option <?php echo (date('H')==$i)?"selected='selected'":"";?> value="<?php echo $nilai1;?>"><?php echo $nilai1;?></option>
                            <?php } ?>
                            
                          
                          </select>
                          </div><div class="col-md-6">
                           <select name="menit" id="heard" class="form-control" >
                            <option value="">Menit</option>
                            <?php for($i=0;$i<61;$i++){ 
                                if($i<10){
                                  $nilai1 = "0".$i;
                                }else{
                                  $nilai1 = $i;
                                }

                            ?>
                            <option <?php echo (date('i')==$nilai1)?"selected='selected'":"";?> value="<?php echo $nilai1;?>"><?php echo $nilai1;?></option>
                            <?php } ?>
                          </select>
                          </div>
                      <br/>
                    
                  </div>
                </div>
              </div>


               <div class="x_panel">
                  <div class="x_title">
                    <h2>Publish</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                        <input type="radio" name="publish" value="Y" checked="checked" > YES  <input type="radio" name="publish" value="N"> NO
                      
                  </div>

                </div>

                </div>


                <div  class="col-md-12">
                <input type="hidden" value="<?php echo $parent_id;?>" name="parent_id" />
                 <button type="submit" class="btn btn-success">Submit</button></div>
               </form>
            </div>

          </div>
        </div>
         <div style="clear:both"></div>
        <!-- /page content -->
<?php include_once dirname(__FILE__).'/../layouts/footer.php';?>