<?php include_once dirname(__FILE__).'/../layouts/header.php';?>
<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Profil</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-4">
             <form id="demo-form" data-parsley-validate  enctype="multipart/form-data" method="post" action="<?php echo site_url('profil/index/');?><?php echo (!empty($detail['id_profil']))?$detail['id_profil']:0;?>">
             <div class="x_panel">
                  <div class="x_title">
                      <br/>
                       <br/>
					   
                        <div >
						<?php
						if(!empty($detail['publishdate'])){
							$a=explode(' ',$detail['publishdate']);	
						}
						?>
                     
                        </div>
                         
                      <br/>
                       <label for="fullname">ID_Supsection * :</label>
					  <br/>
					    <select>
                        <?php foreach($supsection as $sc){?>
						<option value=<?php echo $sc['id_supsection'];?>"><?php echo $sc['nama_supsection'];?></option>
						<?php } ?>
						</select>
					  <br/>
                      <br/>
                      <label for="fullname">ID_Negara* :</label>
					  <br/>
                      <select>
			
						<?php foreach($negara as $ng){?>
						<option value=<?php echo $ng['id_negara'];?>"><?php echo $ng['nama_negara'];?></option>
						<?php } ?>
						
					</select>
                      <br/>
					  <br/>
					  <label for="fullname">Nama* :</label>
                      <input type="text" id="fullname" class="form-control" value="<?php echo (!empty($detail['nama']))?$detail['nama']:"";?>" name="nama" required />
                      <br/>
					  <label for="fullname">Atlet* :</label>
                      <input type="text" id="fullname" class="form-control" value="<?php echo (!empty($detail['atlet']))?$detail['atlet']:"";?>" name="atlet"/>
                      <br/>
					  <label for="fullname">ID_Klub* :</label>
					  <br/>
                      <select>
			
						<?php foreach($klub as $ng){?>
						<option value=<?php echo $ng['id_klub'];?>"><?php echo $ng['nama_klub'];?></option>
						<?php } ?>
						
					  </select>
					  <br/>
                      <br/>
					  <label for="fullname">No_Punggung* :</label>
                      <input type="text" id="fullname" class="form-control" value="<?php echo (!empty($detail['no_punggung']))?$detail['no_punggung']:"";?>" name="no_punggung" required />
                      <br/>
					  <label for="fullname">Tgl_Lahir* :</label>
                       <br/>
                        <div >
						<?php
						
						if(!empty($detail['publishdate'])){
							$a=explode(' ',$detail['publishdate']);
							
						}
						?>
                      <div class="col-md-12 xdisplay_inputx form-group has-feedback">
                                <input type="text" value="<?php echo (!empty($a[0]))?$a[0]:date('Y-m-d');?>" name="tgl" class="form-control has-feedback-left" id="single_cal3" placeholder="" aria-describedby="inputSuccess2Status3">
                                <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                <span id="inputSuccess2Status3" class="sr-only">(success)</span>
                              </div>
                        </div>
                      <br/>
					  <label for="fullname">keterangan* :</label>
                      <input type="text" id="fullname" class="form-control" value="<?php echo (!empty($detail['keterangan']))?$detail['keterangan']:"";?>" name="keterangan" required />
                      <br/>
					  <label for="fullname">Image * :</label>
					   <?php if(!empty($detail['foto'])){ ?>
                            <img src="<?php echo $detail['foto'];?>"/>
                             <input type="hidden" id="fullname" class="form-control" name="image" value="<?php echo $detail['foto'];?>"  />
                       <?php } ?>
					   <input type="file" id="fullname" class="form-control" name="image" />
					   <br/>
					   <label for="fullname">Aktif* :</label>
                      <input type="text" id="fullname" class="form-control" value="<?php echo (!empty($detail['aktif']))?$detail['aktif']:"";?>" name="aktif" required />
                      <br/>
					  <label for="fullname">Meta_key* :</label>
                      <input type="text" id="fullname" class="form-control" value="<?php echo (!empty($detail['meta_key']))?$detail['meta_key']:"";?>" name="meta_key" required />
                      <br/>
					  <label for="fullname">Meta_des* :</label>
                      <input type="text" id="fullname" class="form-control" value="<?php echo (!empty($detail['meta_des']))?$detail['meta_des']:"";?>" name="meta_des" required />
                      <br/>
					  <label for="fullname">Seo* :</label>
                      <input type="text" id="fullname" class="form-control" value="<?php echo (!empty($detail['seo']))?$detail['seo']:"";?>" name="seo" required />
                      <br/>
                      <?php if(!empty($detail['id_profil'])){ ?>


                       
                       <?php } ?> 


                       <button type="submit" class="btn btn-success">Submit</button>
                      
                  </div>
              </div>
              </form>
            </div>

              <div class="col-md-8 col-sm-8 col-xs-8">
                <div class="x_panel">
                  <div class="x_title">
				
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>

                  <div class="x_content">
                    
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                         <th>No</th>
						  <th>Nama</th>
                          <th>Atlet</th>
						  <th>Image</th>
						  <th>Tgl_Lahir</th>
                          <th>Option</th>
                        </tr>
                      </thead>

                      <tbody>
					  <?php $i=1;
            $pub = array('No','Yes');
					  foreach($data as $user){?>
                        <tr>
						  <td><?php echo $i;?></td>
						  <td><?php echo $user['nama'];?></td>
						  <td><?php echo $user['atlet'];?></td>
						  <td>
						  <?php if(!empty($user['foto'])){ ?>
						  <img src='<?php echo $user['foto'];?>' width='50' height='50' />
						  <?php } ?>
						  </td>
						  <td><?php echo $user['tgl_lahir'];?></td>
						 
						  <td>
						
                            <div class="btn-group"><button onclick="location.href='<?php echo site_url('profil/index/'.$user['id_profil']);?>'" type="button" class="btn btn-info">Edit</button></div>
                            <div class="btn-group"><a data-href="<?php echo site_url('profil/actiondelete/');?>" data-toggle="modal" data-target="#confirm-delete<?php echo $i;?>" href="#"><button type="button" class="btn btn-info">Delete</button></a></div>
							
                                                    
                            <div class="modal fade" id="confirm-delete<?php echo $i;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                   <div class="modal-content">                         
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                             <h4 class="modal-title" id="myModalLabel">Confirm Delete</h4>
                                        </div>             
                                        <div class="modal-body">
                                            <p>You are about to delete this data.</p>
                                            <p>Do you want to proceed?</p>
                                            <p class="debug-url"></p>
                                        </div>
                                                                
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
											<a href="<?php echo site_url('profil/actiondelete/'.$user['id_profil']);?>" class="btn btn-danger danger">Delete</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
						  </td>
                        </tr>
						<?php $i++;}?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

              </div>
              
            </div>
          </div>
        </div>
        <!-- /page content -->
<?php include_once dirname(__FILE__).'/../layouts/footer.php';?>