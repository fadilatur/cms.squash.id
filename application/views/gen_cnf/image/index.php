<?php include_once dirname(__FILE__).'/../layouts/header.php';?>

<script>
 $(document).ready(function(){
     i=0;
     $('#more').click(function(){
        i = i+20;
        j = i;
         $.ajax({
            url:'<?php echo site_url('image/more/');?>'+j+'/'+'<?php echo $identifier?>',
            method:'get',
             dataType:'text',
             success:function(data)
             {
               $('#result').append(data);
             }
         });
      
     });
   });
 </script>

<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Images & Folder</h3>
              </div>

              <div class="title_right">
                <div class="col-md-6 col-sm-6 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
          <form method="POST" action="<?php echo site_url('image/index/'.$identifier);?>">
                    <input type="text" class="form-control" style="width:165px;" name="search" placeholder="Search for..." value="<?php if(!empty($searchword)) {echo $searchword;} else {'';}?>">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="submit">Go!</button>
                    </span>
                    </form>
                    <ol class="breadcrumb" style="margin-top: 10px">
                        <li><a style="color:#000" href="<?php echo site_url('home')?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a style="color:#000" href="<?php echo site_url('category');?>">Folder</a></li>
                        <li class="active"><?php echo $title['nama'];?></li>
                    </ol>
                  </div>
                </div>
              </div>
            </div>

            <h4 style="color: red"><?php echo $this->session->flashdata('postwarning');?></h4>

            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
          <div style='padding-bottom:10px'><button onclick="location.href='<?php echo site_url('image/add/'.$this->uri->segment(3));?>'" type="button" class="btn btn-info">Add Folder</button>
          <h3><?php echo $title['nama'];?></h3>
          <!--<button onclick="location.href='<?php echo site_url('administrator/toexcel');?>'" type="button" class="btn btn-info">Download to Excel</button>-->
          </div>

                    <ul class="nav navbar-right panel_toolbox">
                      <!--<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>-->
                      </li>
                      <li class="dropdown">
                       <!-- <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>-->
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <!--<li><a class="close-link"><i class="fa fa-close"></i></a>-->
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Folder</th>
                          <th>Sub Folder</th>
                          <th>Option</th>
                        </tr>
                      </thead>


                      <tbody>
            <?php
            $i=1;
            foreach($folder as $folder) {
            $sub = $this->T_folder->getparent($folder['id']);
            $admin = $this->T_admin->get_tadmin($folder['admin_id']);
            ?>
                        <tr>
              <td><?php echo $i;?></td>
              <td><?php echo $folder['nama'];?></td>
              <td>
              <ul>
              <?php foreach($sub as $sub) {?>
                <li><?php echo $sub['nama'];?></li>
              <?php ;} ?>
             
              </ul>
              </td>
                                                    
                            
              </td>
              <td>
             <div class="btn-group"><button onclick="location.href='<?php echo site_url('image/index/'.$folder['id']);?>'" type="button" class="btn btn-info">Sub / Image</button></div>
            
                            <div class="btn-group"><button onclick="location.href='<?php echo site_url('image/edit/'.$folder['id'].'/'.$this->uri->segment(3));?>'" type="button" class="btn btn-info">Edit</button></div>
                            <div class="btn-group"><a data-href="<?php //echo site_url('category/actiondelete/');?>" data-toggle="modal" data-target="#confirm-delete<?php echo $i;?>" href="#"><button type="button" class="btn btn-info">Delete</button></a></div>
                            <div class="modal fade" id="confirm-delete<?php echo $i;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                   <div class="modal-content">                         
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                             <h4 class="modal-title" id="myModalLabel">Confirm Delete</h4>
                                        </div>             
                                        <div class="modal-body">
                                            <p>You are about to delete this data.</p>
                                            <p>Do you want to proceed?</p>
                                            <p class="debug-url"></p>
                                        </div>
                                                                
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                      <a href="<?php echo site_url('image/delete/'.$folder['id'].'/'.$this->uri->segment(3));?>" class="btn btn-danger danger">Delete</a>
                                        </div>
                                    </div>
                                </div>
                            </div>



            <!-- <div style="float:left;border-radius:4px;color:#fff;font-weight:bold;padding:8px;background:#<?php echo ($category['status']=='1')?"4ce85a":"d30675";?>"><?php echo $category['status'];?></div>-->
             </td>
                        </tr>
            <?php $i++ ;} ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

              
              </div>

              <?php
              if($this->uri->segment(3) != 1) {?>
              <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
          <div style='padding-bottom:10px'><button onclick="location.href='<?php echo site_url('image/addimage/'.$this->uri->segment(3));?>'" type="button" class="btn btn-info">Add Image</button>
          </div>

                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Image</th>
                          <th>Title</th>
                          <th>Caption</th>
                          <th>Posted By</th>
                          <th>Option</th>
                        </tr>
                      </thead>


                      <tbody id="result">
            <?php
            $i=1;
            foreach($image as $image) {
            $admin = $this->T_admin->get_tadmin($image['admin_id']);
            $path = date('Y/m/d/', strtotime($image['postdate']));
            ?>
                        <tr>
              <td><?php echo $i;?></td>
              <td>
                <?php
                if(!empty($image['image_watermark'])) {?>
                  <img  src="<?php echo $tim.$upload.$path.$image['image_watermark'];?>&w=300&h300&zc=1">
                <?php ;} else { ?>
                  <img  src="<?php echo $tim.$upload.$path.$image['image'];?>&w=300&h300&zc=1">
                <?php ;} ?>
              </td>
              <td><?php echo $image['title'];?></td>
              <td><?php echo $image['caption'];?></td>
              <td><?php echo $admin['nama'];?></td>
                                                    
                            
              </td>
              <td>
            
                            <div class="btn-group"><button onclick="location.href='<?php echo site_url('image/editimage/'.$image['id'].'/'.$image['folder_id']);?>'" type="button" class="btn btn-info">Edit</button></div>
                            <div class="btn-group"><a data-href="<?php //echo site_url('category/actiondelete/');?>" data-toggle="modal" data-target="#confirm-delete2<?php echo $i;?>" href="#"><button type="button" class="btn btn-info">Delete</button></a></div>
                            <div class="modal fade" id="confirm-delete2<?php echo $i;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                   <div class="modal-content">                         
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                             <h4 class="modal-title" id="myModalLabel">Confirm Delete</h4>
                                        </div>             
                                        <div class="modal-body">
                                            <p>You are about to delete this data.</p>
                                            <p>Do you want to proceed?</p>
                                            <p class="debug-url"></p>
                                        </div>
                                                                
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                      <a href="<?php echo site_url('image/deleteimage/'.$image['id'].'/'.$this->uri->segment(3));?>" class="btn btn-danger danger">Delete</a>
                                        </div>
                                    </div>
                                </div>
                            </div>



            <!-- <div style="float:left;border-radius:4px;color:#fff;font-weight:bold;padding:8px;background:#<?php echo ($category['status']=='1')?"4ce85a":"d30675";?>"><?php echo $category['status'];?></div>-->
             </td>
                        </tr>
                        <?php $i++ ;} ?>
                      </tbody>
                    </table>

                    <div class="row">
                <div class="col-md-12" style="text-align: center;">
                  <button class="btn btn-info" id="more">More</button>
                </div>
              </div>

                  </div>
                </div>
              </div>

              
              </div>
              
              <?php ;} ?>
              

              

              

              
            </div>
          </div>
        </div>
        <!-- /page content -->
<?php include_once dirname(__FILE__).'/../layouts/footer.php';?>