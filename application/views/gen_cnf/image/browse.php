<?php 
$css    = $this->config->item('css');
$images   = $this->config->item('images');
$js     = $this->config->item('js');
$upload   = $this->config->item('images_upload');
$uploadall   = $this->config->item('images_all');
$uploadlocal = $this->config->item('images_front');
$uploadpic = $this->config->item('gallery');
$uploaddoc = $this->config->item('document');

$tim    = $this->config->item('images_tim');

//set up user role
// $urole      = $this->model_role->select_group($this->session->userdata('role'));
//    foreach($urole as $urole){
//        $uarray[]   = $urole['menu_id'];
//    }
//$uri3 = $this->uri->segment(3);
  
?>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/jpg" href="<?php echo $images;?>logo.png"/>

    <title>CMS | Rilis.id</title>

    <!-- Bootstrap -->
    <link href="<?php echo $css;?>bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- NProgress -->
    <link href="<?php echo $css;?>nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="<?php echo $css;?>green.css" rel="stylesheet">
    <!-- bootstrap-progressbar -->
    <link href="<?php echo $css;?>bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="<?php echo $css;?>jqvmap.min.css" rel="stylesheet"/>

    

    <!-- Select2 -->
    <link href="<?php echo $css;?>/vendors/select2/dist/css/select2.min.css" rel="stylesheet">

     <script src="<?php echo $js;?>jquery.min.js"></script>

     <script>
 $(document).ready(function(){
     i=0;
     $('#more').click(function(){
        i = i+20;
        j = i;
         $.ajax({
            url:'<?php echo site_url('image/browsemore/');?>'+j+'/'+'<?php echo $identifier?>',
            method:'get',
             dataType:'text',
             success:function(data)
             {
               $('#result').append(data);
             }
         });
      
     });
   });
 </script>
        
            <div class="row" style="padding: 10px 10px">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
          

                    <ul class="nav navbar-right panel_toolbox">
                      <!--<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>-->
                      </li>
                      <li class="dropdown">
                       <!-- <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>-->
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <!--<li><a class="close-link"><i class="fa fa-close"></i></a>-->
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <form method="POST" action="<?php echo site_url('image/browse/'.$identifier);?>">
                      <div class="row">
                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4"> 
                          <input type="text" class="form-control" name="search" placeholder="Search for..." value="<?php if(!empty($searchword)) {echo $searchword;} else {'';}?>">
                        </div>
                        <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                            <button class="btn btn-default" type="submit">Go!</button>
                        </div>
                        <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7" style="text-align: right;">
                           
                          <a class="btn btn-info" onclick="goBack()">Back</a>
                          <script>
function goBack() {
    window.history.back();
}
</script>
              
                        </div>
                      </div>
                    </form>
                    <br>
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Folder</th>
                          <th>Created</th>
                          <th>Sub Folder</th>
                          <th>Option</th>
                        </tr>
                      </thead>


                      <tbody>
            <?php
            $i=1;
            foreach($folder as $folder) {
            $sub = $this->T_folder->getparent($folder['id']);
            $admin = $this->T_admin->get_tadmin($folder['admin_id']);
            ?>
                        <tr>
              <td><?php echo $i;?></td>
              <td><?php echo $folder['nama'];?></td>
              <td><?php echo $folder['create_date'];?></td>
              <td>
              <ul>
              <?php foreach($sub as $sub) {?>
                <li><?php echo $sub['nama'];?></li>
              <?php ;} ?>
             
              </ul>
              </td>
                                                    
                            
              </td>
              <td>
             <div class="btn-group"><button onclick="location.href='<?php echo site_url('image/browse/'.$folder['id']);?>'" type="button" class="btn btn-info">Sub / Image</button></div>
            
                      



            <!-- <div style="float:left;border-radius:4px;color:#fff;font-weight:bold;padding:8px;background:#<?php echo ($category['status']=='1')?"4ce85a":"d30675";?>"><?php echo $category['status'];?></div>-->
             </td>
                        </tr>
            <?php $i++ ;} ?>
                      </tbody>
                    </table>

                    <div class="row" style="padding: 15px 15px" id="result">
                      <?php
                      $i=1;
                      foreach($image as $image) {
                      $path = date('Y/m/d/', strtotime($image['postdate']));
                      ?>
                      <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="margin-bottom: 15px; text-align: center">
                        <?php
                        if(!empty($image['image_watermark'])) {?>

                          <img  src="<?php echo $tim.$upload.$path.$image['image_watermark'];?>&w=145&h200&zc=1" id="<?php echo $image['image'];?>" name="<?php echo $tim.$upload.$path.$image['image'];?>&w=145&h200&zc=1" onclick="getImage(this.src, this.id, this.name)">

                        <?php ;} else { ?>

                          <img  src="<?php echo $tim.$upload.$path.$image['image'];?>&w=145&h200&zc=1" id="<?php echo $image['image'];?>" name="<?php echo $tim.$upload.$path.$image['image'];?>&w=145&h200&zc=1" onclick="getImage2(this.src, this.id)">

                        <?php ;} ?>
                        
                        <span><?php echo $image['title'];?></span>
                      </div>
                      <?php $i++ ;} ?>
                    </div>

                    <?php if($this->uri->segment(3) != 1) {?>
                    <?php if(empty($searchword)) {?>
                    <div class="row">
                      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align: center;">
                        <button class="btn btn-info" id="more">More</button>
                      </div>
                    </div>
                    <?php ;} ?>
                    <?php ;} ?>

                    <?php 
                    if($this->uri->segment(3) == 1) {?>
                    <div class="x_content">
                       <form id="demo-form" data-parsley-validate  enctype="multipart/form-data" method="post" action="<?php echo site_url('image/actionaddimagebrowse');?>">
                      <div class="row">
                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                          <label for="browse">Image :</label><br>
                          <input type="file" name="image"><br>
                        </div>
                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                          <label for="sel1">Watermark :</label>
                          <select class="select2_group form-control" name="watermark">
                            <option value="">--</option> 
                            <?php foreach($watermark as $watermark) {?>
                              <option value="<?php echo $watermark['id'];?>"><?php echo $watermark['judul'];?></option> 
                            <?php ;} ?>
                          </select>
                          <br>
                        </div>
                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                          <label for="sel1">Position :</label>
                          <select class="select2_group form-control" name="position">
                            <option value="">--</option> 
                            <option value="bottom-right">Bottom-Right</option>
                            <option value="bottom-center">Bottom-Center</option> 
                            <option value="bottom-left">Bottom-Left</option>
                            <option value="middle-right">Middle-Right</option>
                            <option value="middle-center">Middle-Center</option>
                            <option value="middle-left">Middle-Left</option>
                            <option value="top-right">Top-Right</option>
                            <option value="top-center">Top-Center</option>
                            <option value="top-left">Top-Left</option>
                          </select>
                        </div>
                        
                      </div>
                      <div class="row">
                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                          <label for="fullname">Title :</label>
                          <input type="text" id="fullname" class="form-control" name="title" />
                          <br/>
                        </div>
                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                          <label for="fullname">Caption :</label>
                          <input type="text" id="fullname" class="form-control" name="caption" />
                          <br/>
                        </div>
                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                          <label for="fullname">Description :</label>
                          <textarea name="description" class="form-control"></textarea>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4"></div>
                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="text-align: center;">
                          <input type="hidden" value="<?php echo $this->uri->segment(3);?>" name="parent_id" />
                          <button class="btn btn-info" type="submit" style="margin-top: 10px">Submit</button></div>
                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4"></div>
                      </div>
                      </form>
                      </div>
                      <?php ;} ?>
                  </div>
                </div>
              </div>

              
              </div>

              <?php
              if($this->uri->segment(3) != 1 || !empty($searchword)) {?>
              <div class="row"  style="padding: 10px 10px">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
          

                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">


                    
                    
                    <!--table id="datatable" class="table table-striped table-bordered">

                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Image</th>
                          <th>Title</th>
                          <th>Caption</th>
                          <th>Description</th>
                          <th>Option</th>
                        </tr>
                      </thead>


                      <tbody>
            <?php
            $i=1;
            foreach($image as $image) {
            $path = date('Y/m/d/', strtotime($image['postdate']));
            ?>
                        <tr>
              <td><?php echo $i;?></td>
              <td><img  src="<?php echo $tim.$upload.$path.$image['image'];?>"></td>
              <td><?php echo $image['title'];?></td>
              <td><?php echo $image['caption'];?></td>
              <td><?php echo $image['description'];?></td>
              <td>
                <div class="btn-group"><button type="button" class="btn btn-info">Select</button></div>
              </td>                        
                            
              </td>
             
                        </tr>
                        <?php $i++ ;} ?>
                      </tbody>
                    </table-->
                  </div>
                </div>
              </div>

              
              </div>
              <?php ;} ?>
              

              

              

         

        <script src="<?php echo $js;?>bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo $js;?>fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo $js;?>nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo $js;?>Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="<?php echo $js;?>gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="<?php echo $js;?>bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="<?php echo $js;?>icheck.min.js"></script>
    <!-- Skycons -->
    <script src="<?php echo $js;?>skycons.js"></script>
    <!-- Flot -->
    <script src="<?php echo $js;?>jquery.flot.js"></script>
    <script src="<?php echo $js;?>jquery.flot.pie.js"></script>
    <script src="<?php echo $js;?>jquery.flot.time.js"></script>
    <script src="<?php echo $js;?>jquery.flot.stack.js"></script>
    <script src="<?php echo $js;?>jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="<?php echo $js;?>js/jquery.flot.orderBars.js"></script>
    <script src="<?php echo $js;?>jquery.flot.spline.min.js"></script>
    <script src="<?php echo $js;?>curvedLines.js"></script>
    <!-- DateJS -->
    <script src="<?php echo $js;?>build/date.js"></script>
    <!-- JQVMap -->
    <script src="<?php echo $js;?>jquery.vmap.js"></script>
    <script src="<?php echo $js;?>jquery.vmap.world.js"></script>
    <script src="<?php echo $js;?>jquery.vmap.sampledata.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="<?php echo $js;?>moment.min.js"></script>
    <script src="<?php echo $js;?>daterangepicker.js"></script>

     <!-- Select2 -->
    <script src="<?php echo $css;?>vendors/select2/dist/js/select2.full.min.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="<?php echo $js;?>custom.min.js"></script>
    <script type="text/javascript">
      function getImage(src, id, watermark)
      {
        var str = src;
        var res = str.split("?src=");
        var str2 = res[1];
        var res2 = str2.split("&");

        var strwatermark = watermark;
        var reswatermark = strwatermark.split("?src=");
        var str2watermark = reswatermark[1];
        var res2watermark = str2watermark.split("&");
         window.opener.document.getElementById('imagethumb').value = res2watermark[0];
        window.opener.document.getElementById('imagethumbwatermark').value = res2[0];
        window.opener.document.getElementById('warn2').innerHTML = id;
        window.opener.document.getElementById('preview').src = '<?php echo $tim;?>'+res2[0]+'&w=188&h=125&zc=0';
        window.close();
      }

      function getImage2(src, id)
      {
        var str = src;
        var res = str.split("?src=");
        var str2 = res[1];
        var res2 = str2.split("&");

        
        window.opener.document.getElementById('imagethumb').value = res2[0];
        window.opener.document.getElementById('preview').src = '<?php echo $tim;?>'+res2[0]+'&w=188&h=125&zc=0';
        window.opener.document.getElementById('imagethumbwatermark').value = '';
        window.opener.document.getElementById('warn2').innerHTML = id;
        window.close();
      }
    </script>