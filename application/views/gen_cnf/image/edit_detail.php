<?php include_once dirname(__FILE__).'/../layouts/header.php';?>
<script type="text/javascript" src="<?php echo $js.'ckeditor/ckeditor.js'; ?>"></script>
<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Edit</h3>
              </div>
        <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <ol class="breadcrumb">
                        <li><a style="color:#000" href="<?php echo site_url('home')?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a style="color:#000" href="<?php echo site_url('profile');?>">Gallery</a></li>
                        <li class="active">Edit</li>
                    </ol>
                  </div>
                </div>
              </div>
            
            </div>

            <div class="clearfix"></div>

            <div class="row">
             <form id="demo-form" data-parsley-validate  enctype="multipart/form-data" method="post" action="<?php echo site_url('image/actioneditimage');?>">
              <div class="col-md-9">
                <div class="x_panel">
                 
                  <div class="x_content">

  
                  
                      <?php  $path = date('Y/m/d/', strtotime($image['postdate']));?>  
                      <label for="browse">Image :</label><br>
                      <?php 
                      if(!empty($image['image_watermark'])) {?>
                      <img src="<?php echo $tim.$upload.$path.$image['image_watermark'];?>&w=600" id="img"/><br>
                      <?php ;} else {?>
                      <img src="<?php echo $tim.$upload.$path.$image['image'];?>&w=600" id="img"/><br>
                      <?php ;} ?>
                      <input type="file" name="image">
                      <br/>

                      <label for="sel1">Watermark :</label>
                      <select class="select2_group form-control" name="watermark">
                        <option value="">-No Watermark-</option> 
                        <?php foreach($watermark as $watermark) {?>
                          <option value="<?php echo $watermark['id'];?>" <?php if($image['watermark_id'] == $watermark['id']) {echo 'selected';} else {'';};?>><?php echo $watermark['judul'];?></option> 
                        <?php ;} ?>
                      </select>
                      <br><br>

                      <label for="sel1">Position :</label>
                      <select class="select2_group form-control" name="position">
                        <option value="">--</option> 
                        <option value="bottom-right" <?php if($image['position'] == 'bottom-right') {echo 'selected';} else {'';};?>>Bottom-Right</option>
                        <option value="bottom-center" <?php if($image['position'] == 'bottom-center') {echo 'selected';} else {'';};?>>Bottom-Center</option> 
                        <option value="bottom-left" <?php if($image['position'] == 'bottom-left') {echo 'selected';} else {'';};?>>Bottom-Left</option>
                        <option value="middle-right" <?php if($image['position'] == 'middle-right') {echo 'selected';} else {'';};?>>Middle-Right</option>
                        <option value="middle-center" <?php if($image['position'] == 'middle-Center') {echo 'selected';} else {'';};?>>Middle-Center</option>
                        <option value="middle-left" <?php if($image['position'] == 'middle-left') {echo 'selected';} else {'';};?>>Middle-Left</option>
                        <option value="top-right" <?php if($image['position'] == 'top-right') {echo 'selected';} else {'';};?>>Top-Right</option>
                        <option value="top-center" <?php if($image['position'] == 'top-center') {echo 'selected';} else {'';};?>>Top-Center</option>
                        <option value="top-left" <?php if($image['position'] == 'top-left') {echo 'selected';} else {'';};?>>Top-Left</option>
                      </select>
                      <br><br>

                      <label for="fullname">Title :</label>
                      <input type="text" id="fullname" class="form-control" name="title" value="<?php echo $image['title'];?>" />
                      <br/>

                       <label for="fullname">Caption :</label>
                      <input type="text" id="fullname" class="form-control" name="caption" value="<?php echo $image['caption'];?>"/>
                      <br/>

                      <label for="fullname">Description :</label>
                      <textarea name="description" class="form-control"><?php echo $image['description'];?></textarea>
                      <br/>


                     

                   
                  </div>
                </div>


               


                


               

              </div>

              


                <div  class="col-md-12">
                <input type="hidden" name="imagex" value="<?php echo $image['image'];?>">
                <input type="hidden" name="imagewatermark" value="<?php echo $image['image_watermark'];?>">
                <input type="hidden" name="id" value="<?php echo $image['id'];?>">
                 <input type="hidden" name="url" value="<?php echo $image['folder_id'];?>">
                 <input type="hidden" name="path" value="<?php echo $path;?>">
                 <button type="submit" class="btn btn-success">Submit</button></div>
               </form>
            </div>

          </div>
        </div>
         <div style="clear:both"></div>
        <!-- /page content -->
<?php include_once dirname(__FILE__).'/../layouts/footer.php';?>
<script>
$('#button2').click(function(e){    
  window.open("http://www.google.com", "yyyyy", "width=480,height=360,resizable=no,toolbar=no,menubar=no,location=no,status=no");
return false;
});


 function openKCFinder_singleFile() {
    window.KCFinder = {};
    window.KCFinder.callBack = function(url) {
        // Actions with url parameter here
        window.KCFinder = null;
        document.getElementById('image').value = url;
        //document.getElementById('image').value = 'http://localhost/' + url;
        var split = url.split('/');
        document.getElementById('warn').innerHTML = split[4];
    };
    //window.open('http://localhost/kcfinder/browse.php?type=images&lang=id&dir=images/public', 'kcfinder_single', "width=600, height=500, left=400; top=50");
    window.open('https://fe.male.co.id/public_assets/gen_cnf/js/kcfinder/browse.php?type=images&lang=id&dir=images/public', 'kcfinder_single', "width=600, height=500, left=400; top=50");
}
//https://fe.male.co.id/public_assets/gen_cnf/js/kcfinder/browse.php?type=images&lang=id&dir=images/public
</script>