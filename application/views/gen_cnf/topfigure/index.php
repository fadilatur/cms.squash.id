<?php include_once dirname(__FILE__).'/../layouts/header.php';?>
<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="row top_tiles" style="margin: 10px 0;">
              <div class="col-md-3 col-sm-3 col-xs-6 tile">
                <h3>Top Figure</h3>
                <span class="sparkline_one" style="height: 160px;">
                      <canvas width="200" height="60" style="display: inline-block; vertical-align: top; width: 94px; height: 30px;"></canvas>
                  </span>
              </div>
            </div>
            <br />


            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="dashboard_graph x_panel">
                  <div class="row x_title">
				  <div style='padding-bottom:10px'><button onclick="location.href='<?php echo site_url('gen_cnf/artikel/add/');?>'" type="button" class="btn btn-info">Add</button>
									<button onclick="location.href='<?php echo site_url('gen_cnf/artikel/toexcel');?>'" type="button" class="btn btn-info">Download to Excel</button>
									</div>
                    
                    <div class="col-md-6">
                      
                    </div>
                  </div>
                  <div class="x_content">
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
						  <th>No</th>
                          <th>Judul Artikel</th>
                          <th>Profesi/Jabatan</th>
						  <th>Negara</th>
						  <th>Super Section</th>
                          <th>Option</th>
                        </tr>
                      </thead>
					  <tbody>
					  <?php $i=1;
					  foreach($data as $user){?>
                        <tr>
						  <td><?php echo $i;?></td>
                          <td><?php echo $user['nama_lengkap'];?></td>
						  <td><?php echo $user['profesi'];?></td>
						  <td><?php echo $user['nama_negara'];?></td>
						  <td><?php echo $user['nama_supersection'];?></td>
						  <td>
							<div class="btn-group"><button onclick="location.href='<?php echo site_url('gen_cnf/topskor/edit/');?>'" type="button" class="btn btn-info">Edit</button></div>
							<div class="btn-group"><button onclick="location.href='<?php echo site_url('gen_cnf/topskor/delete/');?>'" type="button" class="btn btn-info">Dellete</button></div>
                            
						  </td>
						  
                        </tr>
						<?php $i++;}?>
                      </tbody>
					  
					  
					 </table> 
                  </div>
                </div>
              </div>
            </div>


            
          </div>
        </div>
        <!-- /page content -->
<?php include_once dirname(__FILE__).'/../layouts/footer.php';?>