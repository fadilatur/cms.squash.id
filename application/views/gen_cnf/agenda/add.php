<?php include_once dirname(__FILE__).'/../layouts/header.php';?>
<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Banner</h3>
              </div>

             
            </div>
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                   <h3><?php //echo $menu['nama_section'];?></h3>
                   <!--
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                        -->
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <form class="form-horizontal form-label-left" enctype="multipart/form-data" method="post" action="<?php echo site_url('agenda/addaction');?>">
                      
                    <div class="box-body">

                      <div class="item form-group">
                      <div class="x_title">
                    <h2>Tanggal</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                      <div class="x_content">
                    
                     <fieldset>
                          <div class="control-group">
                            <div class="controls">
                              <div class="col-md-15 xdisplay_inputx form-group has-feedback">
                                <input type="text" value="<?php echo date('Y-m-d');?>" name="tgl" class="form-control has-feedback-left" id="single_cal3" placeholder="" aria-describedby="inputSuccess2Status3">
                                <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                <span id="inputSuccess2Status3" class="sr-only">(success)</span>
                              </div>
                            </div>
                          </div>
                        </fieldset>

                        <label for="email">Jam:</label>
                        <div class="row">
                        <div class="col-md-6">
                       <select id="heard" name="jam" class="form-control" >
                            <option value="00">Jam</option>
                            <?php for($i=0;$i<25;$i++){ 
                                if($i<10){
                                  $nilai1 = "0".$i;
                                }else{
                                  $nilai1 = $i;
                                }

                            ?>
                             <option <?php echo (date('H')==$i)?"selected='selected'":"";?> value="<?php echo $nilai1;?>"><?php echo $nilai1;?></option>
                            <?php } ?>
                            
                          
                          </select>
                          </div><div class="col-md-6">
                           <select name="menit" id="heard" class="form-control" >
                            <option value="">Menit</option>
                            <?php for($i=0;$i<61;$i++){ 
                                if($i<10){
                                  $nilai1 = "0".$i;
                                }else{
                                  $nilai1 = $i;
                                }

                            ?>
                            <option <?php echo (date('i')==$nilai1)?"selected='selected'":"";?> value="<?php echo $nilai1;?>"><?php echo $nilai1;?></option>
                            <?php } ?>
                          </select>
                          </div>
                      <br/>
                    
                  </div>
                </div>
                </div>
                    
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Lokasi</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="text" name="lokasi" value="" class="form-control col-md-7 col-xs-12" style="width:350px;">
                        </div>
                    </div>
                    
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Kegiatan</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <textarea name="kegiatan" class="form-control col-md-7 col-xs-12" style="width:350px;"></textarea>
                        </div>
                    </div>
                    
                    <div class="x_panel">
                  <div class="x_title">
                    <h2>Sponsored Content</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                        <input type="radio" name="status" value="1"> YES  
                        <input type="radio" name="status" value="0" checked="checked"> NO
                      
                  </div>

                </div>
                                        
                                    
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                          
                          <button type="submit" class="btn btn-primary">Submit</button>
                          <button onclick="location.href='<?php echo site_url('agenda');?>'" type="button" class="btn btn-info">Cancel</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
        
<?php include_once dirname(__FILE__).'/../layouts/footer.php';?>
