<?php include_once dirname(__FILE__).'/../layouts/header.php';?>
<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Banner</h3>
              </div>

             
            </div>
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                   <h3><?php //echo $menu['nama_section'];?></h3>
                   <!--
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                        -->
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <form class="form-horizontal form-label-left" enctype="multipart/form-data" method="post" action="<?php echo site_url('banner/editbanner');?>">
                      
                    <div class="box-body">
                    
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Title</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="text" name="judul" value="<?php echo $banner['judul'];?>" class="form-control col-md-7 col-xs-12" style="width:350px;">
                        </div>
                    </div>
                    
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Content</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <textarea name="keterangan" class="form-control col-md-7 col-xs-12" style="width:350px;"><?php echo $banner['keterangan'];?></textarea>
                        </div>
                    </div>
                    
                                        
                                    
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                          <input type="hidden" name="id" value="<?php echo $banner['id_banner'];?>"/>
                          <button type="submit" class="btn btn-primary">Submit</button>
                          <button onclick="location.href='<?php echo site_url('category');?>'" type="button" class="btn btn-info">Cancel</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
        
<?php include_once dirname(__FILE__).'/../layouts/footer.php';?>
