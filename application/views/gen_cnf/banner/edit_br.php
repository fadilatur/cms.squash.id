<?php include_once dirname(__FILE__).'/../layouts/header.php';?>
<script type="text/javascript" src="<?php echo $js.'ckeditor/ckeditor.js'; ?>"></script>
<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Add Image</h3>
              </div>
        <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <ol class="breadcrumb">
                        <li><a style="color:#000" href="<?php echo site_url('home')?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a style="color:#000" href="<?php echo site_url('profile');?>">Images</a></li>
                        <li class="active">Add</li>
                    </ol>
                  </div>
                </div>
              </div>
            
            </div>

            <div class="clearfix"></div>

            <div class="row">
             <form id="demo-form" data-parsley-validate  enctype="multipart/form-data" method="post" action="<?php echo site_url('banner/editaction_br');?>">
              <div class="col-md-9">
                <div class="x_panel">
                 
                  <div class="x_content">
                      <input type="hidden" name="image" id="image" value="">
                      
                      
                       
                       
                      <br/>

                      <label for="browse">Image :</label><br>
                      <input type="file" name="image">
                      <br/>

                      <<!-- label for="sel1">Watermark :</label>
                      <select class="select2_group form-control" name="watermark">
                        <option value="">-No Watermark-</option> 
                        <?php foreach($watermark as $watermark) {?>
                          <option value="<?php echo $watermark['id'];?>"><?php echo $watermark['judul'];?></option> 
                        <?php ;} ?>
                      </select>
                      <br><br>

                      <label for="sel1">Position :</label>
                      <select class="select2_group form-control" name="position">
                        <option value="bottom-right">Bottom-Right</option>
                        <option value="bottom-center">Bottom-Center</option> 
                        <option value="bottom-left">Bottom-Left</option>
                        <option value="middle-right">Middle-Right</option>
                        <option value="middle-center">Middle-Center</option>
                        <option value="middle-left">Middle-Left</option>
                        <option value="top-right">Top-Right</option>
                        <option value="top-center">Top-Center</option>
                        <option value="top-left">Top-Left</option>
                      </select>
                      <br><br> -->

                      <label for="fullname">Title :</label>
                      <input type="text" id="fullname" class="form-control" name="nama" />
                      <br/>

                      <!--  <label for="fullname">Caption :</label>
                      <input type="text" id="fullname" class="form-control" name="caption" />
                      <br/>

                      <label for="fullname">Description :</label>
                      <textarea name="description" class="form-control"></textarea>
                      <br/> -->

                   
                  </div>
                </div>


              </div>

            


                <div  class="col-md-12">
                <input type="hidden" value="<?php echo $this->uri->segment(3);?>" name="parent_id" />
                 <button type="submit" class="btn btn-success">Submit</button></div>
               </form>
            </div>

          </div>
        </div>
         <div style="clear:both"></div>
        <!-- /page content -->
<?php include_once dirname(__FILE__).'/../layouts/footer.php';?>
<script>
$('#button2').click(function(e){    
  window.open("http://www.google.com", "yyyyy", "width=480,height=360,resizable=no,toolbar=no,menubar=no,location=no,status=no");
return false;
});


 function openKCFinder_singleFile() {
    window.KCFinder = {};
    window.KCFinder.callBack = function(url) {
        // Actions with url parameter here
        window.KCFinder = null;
        document.getElementById('image').value = url;
        //document.getElementById('image').value = 'http://localhost/' + url;
        var split = url.split('/');
        document.getElementById('warn').innerHTML = split[4];
    };
    //window.open('http://localhost/kcfinder/browse.php?type=images&lang=id&dir=images/public', 'kcfinder_single', "width=600, height=500, left=400; top=50");
    window.open('https://fe.male.co.id/public_assets/gen_cnf/js/kcfinder/browse.php?type=images&lang=id&dir=images/public', 'kcfinder_single', "width=600, height=500, left=400; top=50");
}
//https://fe.male.co.id/public_assets/gen_cnf/js/kcfinder/browse.php?type=images&lang=id&dir=images/public
</script>