<?php include_once dirname(__FILE__).'/../layouts/header.php';?>
<script type="text/javascript" src="<?php echo $js.'ckeditor/ckeditor.js'; ?>"></script>
<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Reply</h3>
              </div>
				<div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <ol class="breadcrumb">
                        <li><a style="color:#000" href="<?php echo site_url('home')?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a style="color:#000" href="<?php echo site_url('contact');?>">Message</a></li>
                        <li class="active">Reply</li>
                    </ol>
                  </div>
                </div>
              </div>
            
            </div>

            <div class="clearfix"></div>

            <div class="row">
             <form id="demo-form" data-parsley-validate  enctype="multipart/form-data" method="post" action="<?php echo site_url('contact/reply/'.$id);?>">
              <div class="col-md-12">
                <div class="x_panel">
                 
                  <div class="x_content">
                      <h3 class="box-title">Reply > Form</h3>
                     
						<table>
                                        <tr>
                                            <td style="padding:10px">Name</td>
                                            <td style="padding-left:5px; padding-right:5px">:</td>
                                            <td><?php echo $reply['nama_lengkap'];?></td>
                                        </tr>
                                        <tr>
                                            <td style="padding:10px">Email</td>
                                            <td style="padding-left:5px; padding-right:5px">:</td>
                                            <td><?php echo $reply['email'];?></td>
                                        </tr>
										 <tr>
                                            <td style="padding:10px">Subject</td>
                                            <td style="padding-left:5px; padding-right:5px">:</td>
                                            <td><?php echo $reply['subject'];?></td>
                                        </tr>
                                         <tr>
                                            <td style="padding:10px">Message</td>
                                            <td style="padding-left:5px; padding-right:5px">:</td>
                                            <td><?php echo $reply['pesan'];?></td>
                                        </tr>
						</table>
                      <label for="email">Reply a Message</label>
					  <div class="box-body">
                                        <div class="form-group">
                                            <label for="email">Email To</label>
                                            <td style="padding-left:5px; padding-right:5px">:</td>
                                            <td><input name="email" value="<?php echo $reply['email'];?>"></td>
                                        </div>
										 <div class="form-group">
                                            <label for="detail">Message</label>
                      <textarea id="email" class="form-control" name="detail" data-parsley-trigger="change"></textarea>
                      <script type="text/javascript">
                                                var editor = CKEDITOR.replace("detail", {
                                                    filebrowserBrowseUrl    : '<?php echo site_url('filebrowser');?>',
                                                    filebrowserWindowWidth  : 1000,
                                                    filebrowserWindowHeight : 500,
                                                    
                                                });
                                            </script>
                     

                  </div>
				   <div class="box-footer">
                                        <input type="hidden" name="id" value=""<?php echo $reply['id_contact'];?>">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                        <button onclick="jsvsdcript:history.back()" type="button" class="btn btn-info">Cancel</button>
                                        <div style='clear:both'></div>
                                    </div>
                </div>

              </div>

              

          </div>
        </div>
         <div style="clear:both"></div>
        <!-- /page content -->
<?php include_once dirname(__FILE__).'/../layouts/footer.php';?>