<?php include_once dirname(__FILE__).'/../layouts/header.php';?>

 <!-- Datatables -->
    <link href="<?php echo $css;?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $css;?>vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $css;?>vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $css;?>vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $css;?>vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="row top_tiles" style="margin: 10px 0;">
              <div class="col-md-3 col-sm-3 col-xs-6 tile">
                <h3>Artikel <?php if($daerah == 1) {echo 'Lampung';}?></h3>  
              </div>
			  
            </div>
             <h4 style="color: #000"><?php echo $this->session->flashdata('notifikasidelete');?></h4>
            <br />

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="dashboard_graph x_panel">
                  <div class="row x_title">
				  <div class="col-md-1">

				  <button onclick="location.href='<?php echo site_url('artikel/add/'.$parent_id);?>'" type="button" class="btn btn-info">Add</button>
				  <!--<button onclick="location.href='<?php echo site_url('administrator/toexcel');?>'" type="button" class="btn btn-info">Download to Excel</button>-->
				  </div>
          <div class="col-md-10">
            <form action="<?php echo site_url('artikel/index/'.$daerah);?>" method="POST">
           <div class="col-md-3">
            <input type="text" name="search" class="form-control" value="<?php if(!empty($search)) {echo $search;} else {'';}?>">
          </div>
          <div class="col-md-2">
            <select class="select2_group form-control" name="status">
              <option value="Y" <?php if($status == 'Y') {echo "Selected='selected'";} else {'';};?>>Publish</option>
              <option value="N" <?php if($status == 'N') {echo "Selected='selected'";} else {'';};?>>Un Publish</option>
              <!--
              <option value="S" <?php if($status == 'S') {echo "Selected='selected'";} else {'';};?>>Scheduling</option>
            -->
              <option value="T" <?php if($status == 'T') {echo "Selected='selected'";} else {'';};?>>Trash</option>
            </select>
          </div>
          <div class="col-md-1">
            <button class="btn btn-info" type="submit">Search</button>
          </div>
          <div class="col-md-6"></div>
          </form>
          </div>
          
                    <?php /* ?>
                    <div class="col-md-9">
                    	<form method="GET" action="<?php echo site_url('artikel');?>">
                      <div class="row">
                        <div class="col-md-3" style="margin-right: 5px">
                          <label>Dari</label>
                          <div class="row">
                            <div class="col-md-1">
                              <input type="checkbox" name="daricheck" checked value="1">
                            </div>
                            <div class="col-md-10">
                              <input type="text" name="dari" class="form-control" id="single_cal2" value="<?php echo $this->session->userdata('dari');?>">
                            </div>
                          </div>
                          
                        </div>
                        <div class="col-md-3" style="">
                          <label>Sampai</label>
                          <div class="row">
                            <div class="col-md-1">
                              <input type="checkbox" name="sampaicheck" checked  value="1">
                            </div>
                            <div class="col-md-10">
                              <input type="text" name="sampai" class="form-control" id="single_cal3"  value="<?php echo $this->session->userdata('sampai');?>">
                            </div>
                          </div>
                          
                        </div>
                        <div class="col-md-2" style="margin-top: 20px">
                          <div class="row">
                            <div class="col-md-1">
                              <input type="checkbox" name="statuscheck" checked  value="1">
                            </div>
                            <div class="col-md-10">
                              <select class="select2_group form-control" name="status">
                                <option value="Y" <?php if($this->session->userdata('status') == 'Y') {echo "Selected='selected'";} else {'';};?>>Publish</option>
                                <option value="N" <?php if($this->session->userdata('status') == 'N') {echo "Selected='selected'";} else {'';};?>>Un Publish</option>
                                <!--
                                <option value="S" <?php if($this->session->userdata('status') == 'S') {echo "Selected='selected'";} else {'';};?>>Scheduling</option>
                              -->
                                <option value="T" <?php if($this->session->userdata('status') == 'T') {echo "Selected='selected'";} else {'';};?>>Trash</option>
                              </select>
                            </div>
                          </div>
                          
                        </div>
                      </div>

                    	<div class="row" style="padding-top: 5px">
                    	<div class="col-md-3" style="margin-right: 5px">
                        <div class="row">
                            <div class="col-md-1">
                              <input type="checkbox" name="judulcheck" <?php if(!empty($this->session->userdata('judul'))) {echo "checked";} else {'';};?> value="1">
                            </div>
                            <div class="col-md-10">
                              <input value='<?php echo (!empty($this->session->userdata('judul')))?$this->session->userdata('judul'):'' ;?>' type="text" id="fullname" class="form-control" placeholder="Judul" name="judul"/>
                            </div>
                          </div>
                    		
                    	</div>
                    	<div class="col-md-3">
                    		
                          <div class="row">
                            <div class="col-md-1">
                              <input type="checkbox" name="categorycheck" <?php if(!empty($this->session->userdata('category'))) {echo "checked";} else {'';};?> value="1">
                            </div>
                            <div class="col-md-10">
                              <select class="select2_group form-control" name="category" >
                            <option value="0">Category</option>
                     

                          <?php 
                          $deepth = 0;
                          function category($id,$deepth) {
                          $deepth = $deepth + 1;
                          $ini = get_instance();
                          $ini->load->model('T_section');
                          $category = $ini->T_section->getcategory($id);
                          foreach($category as $category) {
                          if(!empty($category)) {

                            $rt = "";
                                    for($i=0;$i<$deepth-1;$i++){
                                        $rt .= "-";
                                    }
                          ?>
                            <option value="<?php echo $category['id_section'];?>" <?php if($category['id_section'] == $ini->session->userdata('category')) {echo  "Selected='selected'";} else {"";}?>> <?php echo $rt." ".$category['nama_section'];?></option>
                            <?php $category2 = $ini->T_section->getcategory($category['id_section']);
                                  if(!empty($category2)) {
                                    
                                  ?> 
                                 
                                   <?php category($category['id_section'],$deepth);?>
                                

                                  <?php ;}?>
                                  
                              
                            
                          <?php ;}?>
                          <?php ;}?>
                          <?php ;} category(0,0);?>
                           
                          </select>
                            </div>
                          </div>
                        

                          
                    	</div>
                      <?php ?>
                    	<div class="col-md-2">
                        <div class="row">
                            <div class="col-md-1">
                              <?php if($this->session->userdata('level') == 1 || $this->session->userdata('level') == 2 || $this->session->userdata('level') == 4) {?>
                              <input type="checkbox" name="admincheck" <?php if(!empty($this->session->userdata('admin'))) {echo "checked";} else {'';};?> value="1">
                              <?php ;}?>
                            </div>
                            <div class="col-md-10">
                              <?php if($this->session->userdata('level') == 1 || $this->session->userdata('level') == 2 || $this->session->userdata('level') == 4) {?>
                                <select class="select2_group form-control" name="admin">
                                      <option value="0">Admin</option>
                                         <?php
                                       
                                        foreach($member as $trans2){ 
                                        ?>
                                              <option <?php echo ($trans2['id_adm']==$this->session->userdata('admin'))?'selected':'' ;?> value="<?php echo $trans2['id_adm'];?>"><?php echo $trans2['nama'];?></option>
                                        <?php } ?>
                                        
                                </select>
                          <?php ;}?>
                            </div>
                          </div>

                        
                    	</div>
                    	<div class="col-md-1" style="margin-right: 20px">
                    	<button type="submit" class="btn btn-info">SEARCH</button>
                    	</div>
                    	
                    	
                      <div class="col-md-2">
                        <a href="<?php echo site_url('artikel/index/clear');?>">
                      <button type="button" class="btn btn-info">CLEAR</button>
                      </a>
                      </div>
                      </div>
                      </form>
                    </div>
                  <?php */ ?>
                  </div>
                  <div class="x_content">
                    <table id="datatablesalah" class="table table-striped table-bordered">
                      <thead>
                        <tr>
						  <th>No</th>
               
                    <th>Judul Artikel</th>
                    <th>Channel</th>      
                    <th>Status</th>
                   

					<th>Tanggal Publish</th>
                    <th>Author</th>
                

                          <th width="150">Option</th>
                        </tr>
                      </thead>
					  <tbody>
					  <?php $i=1;
					  foreach($data as $user){
              $channel = $this->T_section->get($user['id_section']);
              ?>
                        <tr>
						  <td><?php echo $i+$pages;?></td>
               
                          <td><a style="color:#000" href="<?php echo 'http://rilis.id/'.strtolower($user['urltitle']);?>" target="_blank"><?php echo $user['judul_artikel'];?></a>
 <br/>
                            D : <?php echo $user['dibaca'];?> |
              M : <?php echo $user['dibaca_m'];?> |
              A : <?php echo $user['dibaca_a'];?> |
              I : <?php echo $user['dibaca_i'];?> |
              T : <?php echo $user['dibaca']+$user['dibaca_m']+$user['dibaca_a']+$user['dibaca_i'];?>
                          </td>

                          <td><?php echo $channel['nama_section'];?></td>

						  <td><div style="float:left;border-radius:4px;color:#fff;font-weight:bold;padding:8px;background:#<?php if($user['publish'] == 'Y') {echo '4ce85a';} elseif($user['publish'] == 'N'){echo 'd30675';} elseif($user['publish'] == 'S'){echo 'ff1a1a';} else {echo '000000';}?>"><?php echo $user['publish'];?></div></td>

						  

               <td><?php echo $user['tgl_pub'];?></td>

              <td><?php echo $user['nama'];?></td>

              
						  <td>

             
              <?php
              if($this->session->userdata('status') == 'T') {?>

              <div class="btn-group"><a href="<?php echo site_url('artikel/restore/'.$user['id_artikel']);?>"><button onclick="location.href='<?php echo site_url('artikel/edit/'.$user['id_artikel'].'/'.$daerah);?>'" type="button" class="btn btn-info">Restore</button></a></div>
							<div class="btn-group"><button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal<?php echo $i;?>">Delete</button></div>
              <div class="modal modal-info" id="myModal<?php echo $i;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">×</span></button>
                        <h4 class="modal-title">Confirmation</h4>
                      </div>
                      <div class="modal-body">
                        Anda yakin akan menghapus data ini secara <b>PERMANEN</b>?
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                        <a href="<?php echo site_url('artikel/delete/'.$user['id_artikel']);?>" class="btn btn-danger">Delete</a>
                      </div>
                    </div>
                  
                  <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
              </div>
              <?php ;} else {?>

              <div class="btn-group"><a href="<?php echo site_url('artikel/edit/'.$user['id_artikel'].'/'.$daerah);?>"><button onclick="location.href='<?php echo site_url('artikel/edit/'.$user['id_artikel'].'/'.$daerah);?>'" type="button" class="btn btn-info">Edit</button></a></div>
              <div class="btn-group"><button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal<?php echo $i;?>">Delete</button></div>
              <div class="modal modal-info" id="myModal<?php echo $i;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">×</span></button>
                        <h4 class="modal-title">Confirmation</h4>
                      </div>
                      <div class="modal-body">
                        Anda yakin akan menghapus data ini ?
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                        <a href="<?php echo site_url('artikel/trash/'.$user['id_artikel']);?>" class="btn btn-danger">Delete</a>
                      </div>
                    </div>
                  
                  <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
              </div>

              <?php
              if($user['id_section'] == 2 || $user['id_section'] == 130) {?>
              <div class="btn-group"><button onclick="location.href='<?php echo site_url('artikel/index/'.$user['id_artikel']);?>'" type="button" class="btn btn-info">Sub</button></div>
              <?php ;} else {'';}?>
               
							<?php if ($user['headline']=='N'){?>
                <div class="btn-group"><button onclick="location.href='<?php echo site_url('artikel/hl/'.$user['id_artikel'].'/'.$daerah);?>'" type="button" class="btn btn-info">HL</button></div>
               <?php }else{?>
                <div class="btn-group"><button style='background:#000' onclick="location.href='<?php echo site_url('artikel/hl/'.$user['id_artikel'].'/'.$daerah);?>'" type="button" class="btn btn-info">unHL</button></div>
               
              
              <?php }  ?>

              <?php if ($user['hot']=='N'){?>
                <div class="btn-group"><button onclick="location.href='<?php echo site_url('artikel/hot/'.$user['id_artikel'].'/'.$daerah);?>'" type="button" class="btn btn-info">PILIHAN</button></div>
               <?php }else{?>
                <div class="btn-group"><button style='background:#000' onclick="location.href='<?php echo site_url('artikel/hot/'.$user['id_artikel'].'/'.$daerah);?>'" type="button" class="btn btn-info">BATAL PILIHAN</button></div>
               
              
              <?php }  ?>
              <?php } ?>
                            
						  </td>
						  
                        </tr>
						<?php $i++;}?>
                      </tbody>
					  
					  
					 </table> 
                  </div>
                  <div><?php echo $paging;?></div>
                </div>



              </div>
            </div>


            
          </div>
        </div>
        <!-- /page content -->
 <!-- Datatables -->
    <script src="<?php echo $css;?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-scroller/js/datatables.scroller.min.js"></script>
    <script src="<?php echo $css;?>vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo $css;?>vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo $css;?>vendors/pdfmake/build/vfs_fonts.js"></script>
    <script>
       $('#datatable').dataTable();
    </script>

<?php include_once dirname(__FILE__).'/../layouts/footer.php';?>