<?php include_once dirname(__FILE__).'/../layouts/header.php';?>
<script type="text/javascript" src="<?php echo $js.'ckeditor/ckeditor.js'; ?>"></script>
<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Artikel <?php if($daerah == 1) {echo 'Lampung';}?></h3>
              </div>
				<div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <ol class="breadcrumb">
                        <li><a style="color:#000" href="<?php echo site_url('home')?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a style="color:#000" href="<?php echo site_url('artikel');?>">Artikel</a></li>
                        <li class="active">Add</li>
                    </ol>
                  </div>
                </div>
              </div>
            
            </div>

            <div class="clearfix"></div>

            <div class="row">
             <form id="demo-form" data-parsley-validate  enctype="multipart/form-data" method="post" action="<?php echo site_url('artikel/actionadd');?>">
              <div class="col-md-9">
                <div class="x_panel">
                 
                  <div class="x_content">
                    
                      <label for="fullname">Judul (90 Karakter) * :</label> <span style="float: right;" id="countjudul">90</span>
                      <input type="text" id="judul" class="form-control" name="judul" required maxlength="90" />
                      <br/>
                      <label for="email">Lead (130 Karakter) * :</label> <span style="float: right;" id="countgagasan">130</span>
                      <input type="text" id="gagasan" class="form-control" name="gagasan" maxlength="130" />
                     
                      <br/>

                      <label for="email">Content * :</label>
                      <textarea id="email" class="form-control" name="detail" data-parsley-trigger="change"></textarea>
                      <script type="text/javascript">
                                                var editor = CKEDITOR.replace("detail", {
                                                    filebrowserBrowseUrl    : '<?php echo site_url('filebrowser');?>',
                                                    filebrowserWindowWidth  : 1000,
                                                    filebrowserWindowHeight : 500,
                                                    height : 800
                                                });
                                            </script>
                      <br/>

                      <label for="fullname">Sumber *:</label>
                      <input type="text" id="fullname" class="form-control" name="sumber"  />
                      <br/>

                      <label for="fullname">Video URL * :</label>
                      <input type="text" id="fullname" class="form-control" name="url"  />
                      <br/>

                      <label for="keyword">Meta Keywords (140 Karakter) * :</label> <span style="float: right;" id="countkeyword">140</span>
                      <textarea id="keyword" class="form-control" name="mkey" data-parsley-trigger="change"  maxlength="140"></textarea>
                      <br/>

                       <label for="keyword">Meta Title (140 Karakter) * :</label> <span style="float: right;" id="counttit">140</span>
                      <textarea id="metatitle" class="form-control" name="mtit" data-parsley-trigger="change"  maxlength="140"></textarea>
                      <br/>

                      <label for="email">Meta Descriptions (150 Karakter) * :</label> <span style="float: right;" id="countdesc">150</span>
                      <textarea id="desc" class="form-control" name="mdesc" data-parsley-trigger="change"  maxlength="150"></textarea>
                      <br/>




                     

                   
                  </div>
                </div>


               


                


                <div class="x_panel">
                  <div class="x_title">
                    <h2>Tags</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <!--
                     <select class="select2_multiple form-control" name="tag[]" multiple="multiple">
                     <?php foreach($tag as $tg){  ?>
                            <option value="<?php echo $tg['nama_tag'];?>"><?php echo $tg['nama_tag'];?></option>
                     <?php } ?>
                            </select>
                    -->
                    <textarea class="form-control" name="tag"></textarea>
                  </div>
                </div>

                <!--
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Position</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="form-group">
                    <label for="sel1">Select one:</label>
                      <select class="form-control" id="sel1" name="position">
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                      </select>
                  </div>
                </div>
                -->
              </div>

              <div class="col-md-3">

                <div class="x_panel">
                  <div class="x_title">
                    <h2>Kategori</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                      <select class="select2_group form-control" name="kategori">
                        <option value="0">--</option>
                      <?php
                      /*$cat = $this->T_supsection->select(0);
                      foreach($cat as $trans){ 
                      ?>
                            <optgroup label="<?php echo $trans['nama_supsection'];?>">
                             <?php
                            $bcd = $this->T_section->selectadmin($trans['id_supsection']);
                            foreach($bcd as $trans2){ 
                            ?>
                                  <option value="<?php echo $trans2['id_section'];?>"><?php echo $trans2['nama_section'];?></option>
                            <?php } ?>
                              
                            </optgroup>
                      <?php } */?>

                          <?php 
                          $deepth = 0;
                          function category($id,$deepth, $dae) {
                          $deepth = $deepth + 1;
                          $ini = get_instance();
                          $ini->load->model('T_section');
                          $category = $ini->T_section->getcategory($id, $dae);
                          foreach($category as $category) {
                          if(!empty($category)) {

                            $rt = "";
                                    for($i=0;$i<$deepth-1;$i++){
                                        $rt .= "-";
                                    }
                          ?>
                            <option value="<?php echo $category['id_section'];?>"> <?php echo $rt." ".$category['nama_section'];?></option>
                            <?php $category2 = $ini->T_section->getcategory($category['id_section'], $dae);
                                  if(!empty($category2)) {
                                    
                                  ?> 
                                 
                                   <?php category($category['id_section'],$deepth, $dae);?>
                                

                                  <?php ;}?>
                                  
                              
                            
                          <?php ;}?>
                          <?php ;}?>
                          <?php ;} category(0,0, $daerah);?>
                           
                          </select>
                  </div>

                </div>

                <!-- <div class="x_panel">
                  <div class="x_title">
                    <h2>Kolom</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                        <div class="form-group">
                          <label for="sel1">Select one:</label>
                            <select class="select2_group form-control" name="kolom">
                              <option>--</option>
                              <?php foreach($kolom as $a){ ?>
                              <option value="<?php echo $a['id'];?>"><?php echo $a['nama'];?></option>
                              <?php } ?>
                            </select>
                        </div>
                      
                  </div>

                </div> -->
                <?php
                if($this->session->userdata('level') == 3) {?>
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Penulis</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                        <div class="form-group">
                          <label for="sel1">Select one:</label>
                            <select class="select2_group form-control" name="admin">
                         
                                              <option  value="<?php echo $this->session->userdata('id_adm');?>"><?php echo $this->session->userdata('nama');?></option>
                                        
                                        
                                </select>
                        </div>
                      
                  </div>

                </div>
                <?php ;}else{?>
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Penulis</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                        <div class="form-group">
                          <label for="sel1">Select one:</label>
                            <select class="select2_group form-control" name="admin">
                                      <option value="0">--</option>
                                         <?php
                                       
                                        foreach($member as $trans2){ 
                                        ?>
                                              <option <?php echo ($trans2['id_adm']==$this->session->userdata('id_adm'))?'selected':'' ;?> value="<?php echo $trans2['id_adm'];?>"><?php echo $trans2['nama'];?></option>
                                        <?php } ?>
                                        
                                </select>
                        </div>
                      
                  </div>

                </div>
                <?php ;} ?>

                <?php
                if($this->session->userdata('level') == 1) {?>
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Editor</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                        <div class="form-group">
                          <label for="sel1">Select one:</label>
                            <select class="select2_group form-control" name="editor">
                                      <option value="0">--</option>
                                         <?php
                                       
                                        foreach($editor as $editor){ 
                                        ?>
                                              <option <?php echo ($editor['id_adm']==$this->session->userdata('id_adm'))?'selected':'' ;?> value="<?php echo $editor['id_adm'];?>"><?php echo $editor['nama'];?></option>
                                        <?php } ?>
                                        
                                </select>
                        </div>
                      
                  </div>

                </div>
                <?php ;}elseif($this->session->userdata('level') == 2) {?>
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Editor</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                        <div class="form-group">
                          <label for="sel1">Select one:</label>
                            <select class="select2_group form-control" name="editor">
                             <option value="">--</option>         
                                        
                                       <option  value="<?php echo $this->session->userdata('id_adm')?>"><?php echo $this->session->userdata('nama')?></option>
                                        
                                        
                                </select>
                        </div>
                      
                  </div>

                </div>
                <?php ;} ?>
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Fokus</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                        <div class="form-group">
                          <label for="sel1">Select one:</label>
                            <select class="select2_group form-control" name="fokus">
                              <option>--</option>
                              <?php foreach($fokus as $a){ ?>
                              <option value="<?php echo $a['id'];?>"><?php echo $a['judul'];?></option>
                              <?php } ?>
                            </select>
                        </div>
                      
                  </div>

                </div>

                <div class="x_panel">
                  <div class="x_title">
                    <h2>Foto <span style="font-size:12px;color:#000"></span></h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                  <!--label for="email">Image Slider (1900 x 800 ):</label>
                  <input type="file" id="fullname" class="form-control" name="slider"-->
                  
                  <!--label for="browse">Image Slider :</label><br>
                  <input type="hidden" name="slider" id="imageslider">
                  <button type="button" id="browse" onclick="openKCFinder_slider()" style="margin-top: 5px">Pilih File</button><span id="warn">Tidak ada file</span-->
                  
						      <!--label for="email">Image Thumbnail (800 x 450):</label>
                  <input type="file" id="fullname" class="form-control" name="image" -->
                  
                  <label for="browse">Image Thumbnail :</label><br>
                  <img id="preview">
                  <input type="hidden" name="image" id="imagethumb">
                  <input type="hidden" name="imagewatermark" id="imagethumbwatermark">
                  <button type="button" id="browse" onclick="openMyKc()" style="margin-top: 5px">Pilih File</button><span id="warn2">Tidak ada file</span>
                  
                       <!--
                       <label for="email">Image Detail (1265 x 507 ) :</label>
                       <input type="file" id="fullname" class="form-control" name="thumb"  />
                     -->
                      <br/>
                      <label for="email">Caption:</label>
                       <input type="text" id="fullname" class="form-control" name="caption"  />
                      <br/>
                  </div>

                </div>


                <div class="x_panel">
                  <div class="x_title">
                    <h2>Tanggal</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                     <fieldset>
                          <div class="control-group">
                            <div class="controls">
                              <div class="col-md-15 xdisplay_inputx form-group has-feedback">
                                <input type="text" value="<?php echo date('Y-m-d');?>" name="tgl" class="form-control has-feedback-left" id="single_cal3" placeholder="" aria-describedby="inputSuccess2Status3">
                                <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                <span id="inputSuccess2Status3" class="sr-only">(success)</span>
                              </div>
                            </div>
                          </div>
                        </fieldset>

                        <label for="email">Jam:</label>
                        <div class="row">
                        <div class="col-md-6">
                       <select id="heard" name="jam" class="form-control" >
                            <option value="00">Jam</option>
                            <?php for($i=0;$i<24;$i++){ 
                                if($i<10){
                                  $nilai1 = "0".$i;
                                }else{
                                  $nilai1 = $i;
                                }

                            ?>
                             <option <?php echo (date('H')==$i)?"selected='selected'":"";?> value="<?php echo $nilai1;?>"><?php echo $nilai1;?></option>
                            <?php } ?>
                            
                          
                          </select>
                          </div><div class="col-md-6">
                           <select name="menit" id="heard" class="form-control" >
                            <option value="">Menit</option>
                            <?php for($i=0;$i<61;$i++){ 
                                if($i<10){
                                  $nilai1 = "0".$i;
                                }else{
                                  $nilai1 = $i;
                                }

                            ?>
                            <option <?php echo (date('i')==$nilai1)?"selected='selected'":"";?> value="<?php echo $nilai1;?>"><?php echo $nilai1;?></option>
                            <?php } ?>
                          </select>
                          </div>
                      <br/>
                    
                  </div>
                </div>
              </div>


               <div class="x_panel">
                  <div class="x_title">
                    <h2>Publish</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                        <?php 
                        if($this->session->userdata('level') == 3) {?>

                        <input type="radio" name="publish" value="N" checked> NO
                        <!--
                        <input type="radio" name="publish" value="S"> SCHEDULING
                        -->
                        <?php } else {?>
                        <input type="radio" name="publish" value="Y" checked="checked" > YES  
                        <input type="radio" name="publish" value="N"> NO
                        <!--
                        <input type="radio" name="publish" value="S"> SCHEDULING
                        -->
                        <?php ;}?>
                      
                  </div>

                </div>


                <div class="x_panel">
                  <div class="x_title">
                    <h2>Kota</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                        <input type="text" name="kota" class="form-control">
                      
                  </div>

                </div>

                <div class="x_panel">
                  <div class="x_title">
                    <h2>Sponsored Content</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                        <input type="radio" name="sponsored" value="Y"> YES  
                        <input type="radio" name="sponsored" value="N" checked="checked"> NO
                      
                  </div>

                </div>

                <div class="x_panel">
                  <div class="x_title">
                    <h2>Position</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                        <div class="form-group">
                          <label for="sel1">Select one:</label>
                            <select class="form-control" id="sel1" name="position">
                              <option>0</option>
                              <option>1</option>
                              <option>2</option>
                              <option>3</option>
                              <option>4</option>
                            </select>
                        </div>
                      
                  </div>

                </div>

                </div>


                <div  class="col-md-12">
                <input type="hidden" value="<?php echo $daerah;?>" name="parent_id" />
                <?php
                if($this->session->userdata('level') == 3) {?>
                 <button type="submit" class="btn btn-success">Draft</button>
                 <?php ;} else {?>
                 <button type="submit" class="btn btn-success">Submit</button>
                 <?php ;}?>
                 </div>
               </form>
            </div>

          </div>
        </div>
         <div style="clear:both"></div>
        <!-- /page content -->
<?php include_once dirname(__FILE__).'/../layouts/footer.php';?>
<script type="text/javascript">
  $('#judul').on('keyup', function() {
    console.log(this.value.length);
    var rescount = parseInt(55) - parseInt(this.value.length);
    $("#countjudul").text(rescount);
    //if(this.value.length == 55) {
      //alert('Teks yang anda masukkan sudah penuh');
    //}
});

  $('#gagasan').on('keyup', function() {
    console.log(this.value.length);
    var rescount = parseInt(130) - parseInt(this.value.length);
    $("#countgagasan").text(rescount);
    //if(this.value.length == 330) {
      //alert('Teks yang anda masukkan sudah penuh');
    //}
});

  $('#keyword').on('keyup', function() {
    console.log(this.value.length);
    var rescount = parseInt(140) - parseInt(this.value.length);
    $("#countkeyword").text(rescount);
    //if(this.value.length == 140) {
      //alert('Teks yang anda masukkan sudah penuh');
    //}
});

  $('#desc').on('keyup', function() {
    console.log(this.value.length);
    var rescount = parseInt(150) - parseInt(this.value.length);
    $("#countdesc").text(rescount);
    //if(this.value.length == 150) {
      //alert('Teks yang anda masukkan sudah penuh');
    //}
});



function openMyKc() {

    window.open('<?php echo site_url();?>/image/browse/1', 'kcfinder_single', "width=700, height=600, left=400, top=50, toolbar=1");
}
//https://fe.male.co.id/public_assets/gen_cnf/js/kcfinder/browse.php?type=images&lang=id&dir=images/public
//http://localhost/kcfinder/browse.php?type=images&lang=id&dir=images/public
</script>