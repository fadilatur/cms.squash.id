<?php include_once dirname(__FILE__).'/../layouts/header.php';?>
<script type="text/javascript" src="<?php echo $js.'ckeditor/ckeditor.js'; ?>"></script>
<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Artikel</h3>
              </div>

            
            </div>

            <div class="clearfix"></div>

            <div class="row">
             <form id="demo-form" data-parsley-validate  enctype="multipart/form-data" method="post" action="<?php echo site_url('artikel/actionedit');?>">
              <div class="col-md-9">
                <div class="x_panel">
                 
                  <div class="x_content">
                    
                    <label for="email">Title</label>
                       <input type="text" id="fullname" class="form-control" name="judul"  value="<?php echo $data['judul_artikel'];?>" />
                      <br/>
                    

                      <label for="email">Content * :</label>
                      <textarea id="email" class="form-control" name="detail" data-parsley-trigger="change"><?php echo $data['isi_artikel'];?></textarea>
                      <script type="text/javascript">
                                                var editor = CKEDITOR.replace("detail", {
                                                    filebrowserBrowseUrl    : '<?php echo site_url('filebrowser');?>',
                                                    filebrowserWindowWidth  : 1000,
                                                    filebrowserWindowHeight : 500
                                                });
                                            </script>
                      <br/>

                      <label for="fullname">Foto * :</label>
                      <?php 
                  //take folder for image
                  $path = date('Y/m/d/', strtotime($data['postdate']));?>
                      
                       
                        
                        <label for="browse">Image Thumbnail :</label><br>
                        <?php
                        if(!empty($data['thumbnail'])) {
                        $mystring = $data['thumbnail'];
                        $findme   = 'http';
                        $pos = strpos($mystring, $findme);
                        if ($pos === false) {?>
                            
                            <?php
                            if(!empty($data['thumbnail_watermark'])) {?>
                            <img src="<?php echo $tim.$upload.$path.$data['thumbnail_watermark'];?>&w=188&h=125&zc=0" width="100%" id="preview"/>
                            <?php ;} else { ?>
                            <img src="<?php echo $tim.$upload.$path.$data['thumbnail'];?>&w=188&h=125&zc=0" width="100%" id="preview"/>
                            <?php ;} ?>

                          <?php } else {?>

                             <?php
                            if(!empty($data['thumbnail_watermark'])) {?>
                            <img src="<?php echo $tim.$data['thumbnail_watermark'];?>&w=188&h=125&zc=0" width="100%" id="preview"/>
                            <?php ;} else { ?>
                            <img src="<?php echo $tim.$data['thumbnail'];?>&w=188&h=125&zc=0" width="100%" id="preview"/>
                            <?php ;} ?>
                          <?php }?>
                        <?php ;} else { ?>
                        <img src="<?php echo $tim.$data['oldimage'];?>&w=188&h=125&zc=0" width="100%" id="preview"/>
                        <?php ;} ?>
                        <input type="hidden" name="image" id="imagethumb" value="<?php echo $data['thumbnail'];?>">
                        <input type="hidden" name="imagewatermark" id="imagethumbwatermark" value="<?php echo $data['thumbnail_watermark'];?>">
                        <button type="button" id="browse" onclick="openMyKc()" style="margin-top: 5px">Pilih File</button><span id="warn2">Tidak ada file</span>

                        <br><br>
                       
                      <label for="email">Caption:</label>
                       <input type="text" id="fullname" class="form-control" name="caption" value="<?php echo $data['ket_thumbnail'];?>"  />
                      <br/>


                     

                   
                  </div>
                </div>


               


                



              </div>

              <div class="col-md-3">

                

               


               


           

                </div>


                <div  class="col-md-12"> 
                <input type="hidden" value="<?php echo $data['parent_id'];?>" name="parent_id" />
                <input type="hidden" value="<?php echo $data['id_artikel'];?>" name="id_artikel" />
                <button type="submit" class="btn btn-success">Submit</button></div>
               </form>
            </div>

          </div>
        </div>
         <div style="clear:both"></div>
        <!-- /page content -->
<?php include_once dirname(__FILE__).'/../layouts/footer.php';?>

<script type="text/javascript">
  
function openMyKc() {

    window.open('http://cms.rilis.id/index.php/image/browse/1', 'kcfinder_single', "width=700, height=600, left=400, top=50, toolbar=1");
}
</script>