<?php include_once dirname(__FILE__).'/../layouts/header.php';?>
            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        ADMINISTRATOR
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?php echo site_url('gen_cnf/home');?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="<?php echo site_url('gen_cnf/administrator');?>">Administrator</a></li>
                        <li class="active">Edit</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-12">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">Administrator > Change Password</h3>
                                </div><!-- /.box-header -->
                                <!-- form start -->
                                <form role="form" enctype="multipart/form-data" method="post" action="<?php echo site_url('gen_cnf/administrator/actioncpassword');?>">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">New Password</label>
                                            <input type="password" name="password" class="form-control" value="" id="exampleInputUsername1" placeholder="Enter New Password">
                                        </div>
                                      
                                    </div><!-- /.box-body -->

                                    <div class="box-footer">
                                    	<input type="hidden" name="id" value="<?php echo $data['id'];?>"/>
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </form>
                            </div><!-- /.box -->
                        </div><!--/.col (right) -->
                    </div>   <!-- /.row -->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->
        <!-- jQuery 2.0.2 -->
        <script src="<?php echo $js;?>jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="<?php echo $Js;?>bootstrap.min.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="<?php echo $Js;?>AdminLTE/app.js" type="text/javascript"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="<?php echo $Js;?>AdminLTE/demo.js" type="text/javascript"></script>  
<?php include_once dirname(__FILE__).'/../layouts/header.php';?>