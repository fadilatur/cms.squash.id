<?php include_once dirname(__FILE__).'/../layouts/header.php';?>
<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Category</h3>
              </div>

              <div class="title_right">
                <div class="col-md-6 col-sm-6 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
				  <form method="POST" action="<?php echo site_url('category');?>">
                    <input type="text" class="form-control" style="width:165px;" name="search" placeholder="Search for..." >
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="submit">Go!</button>
                    </span>
                    </form>
                    <ol class="breadcrumb" style="margin-top: 10px">
                        <li><a style="color:#000" href="<?php echo site_url('home')?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a style="color:#000" href="<?php echo site_url('category');?>">Category</a></li>
                        <li class="active"><?php echo $title['nama_section'];?></li>
                    </ol>
                  </div>
                </div>
              </div>
            </div>

            <h4 style="color: red"><?php echo $this->session->flashdata('postwarning');?></h4>

            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
					<div style='padding-bottom:10px'><button onclick="location.href='<?php echo site_url('category/add/'.$this->uri->segment(3).'/'.$daerah);?>'" type="button" class="btn btn-info">Add</button>
					<h3><?php echo $title['nama_section'];?></h3>
					<!--<button onclick="location.href='<?php echo site_url('administrator/toexcel');?>'" type="button" class="btn btn-info">Download to Excel</button>-->
					</div>

                    <ul class="nav navbar-right panel_toolbox">
                      <!--<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>-->
                      </li>
                      <li class="dropdown">
                       <!-- <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>-->
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <!--<li><a class="close-link"><i class="fa fa-close"></i></a>-->
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
						              <th>Position</th>
                          <th>Subcategory</th>
                          <th>Keywords</th>
                          <th>Description</th>
                          <th>Children</th>
                          <th>Option</th>
                        </tr>
                      </thead>


                      <tbody>
					  <?php $i=1;
            
					  foreach($category as $category){
						  $children = $this->T_section->getcategory($category['id_section'], $daerah);
						  ?>
                        <tr>
						  <td><?php echo $category['position'];?></td>
						  <td><?php echo $category['nama_section'];?></td>
              <td><?php echo $category['keyword'];?></td>
              <td><?php echo $category['description'];?></td>
						  <td>
							<ul>
							<?php foreach($children as $children){?>
								<li><?php echo $children['nama_section']?></li>
							<?php ;}?>
							</ul>
							</td>
                                                    
                            
						  </td>
              <td>
             <div class="btn-group"><button onclick="location.href='<?php echo site_url('category/index/'.$category['id_section'].'/'.$daerah);?>'" type="button" class="btn btn-info">Sub Category</button></div>
            
                            <div class="btn-group"><button onclick="location.href='<?php echo site_url('category/edit/'.$category['id_section'].'/'.$this->uri->segment(3).'/'.$daerah);?>'" type="button" class="btn btn-info">Edit</button></div>
                            <div class="btn-group"><a data-href="<?php echo site_url('category/actiondelete/');?>" data-toggle="modal" data-target="#confirm-delete<?php echo $i;?>" href="#"><button type="button" class="btn btn-info">Delete</button></a></div>
                            <div class="modal fade" id="confirm-delete<?php echo $i;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                   <div class="modal-content">                         
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                             <h4 class="modal-title" id="myModalLabel">Confirm Delete</h4>
                                        </div>             
                                        <div class="modal-body">
                                            <p>You are about to delete this data.</p>
                                            <p>Do you want to proceed?</p>
                                            <p class="debug-url"></p>
                                        </div>
                                                                
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                      <a href="<?php echo site_url('category/actiondelete/'.$category['id_section'].'/'.$this->uri->segment(3).'/'.$daerah);?>" class="btn btn-danger danger">Delete</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="btn-group"><a data-href="<?php echo site_url('category/actiondelete/');?>" data-toggle="modal" data-target="#confirm-position<?php echo $i;?>" href="#"><button type="button" class="btn btn-info">Position</button></a></div>
                            <div class="modal fade" id="confirm-position<?php echo $i;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                   <div class="modal-content">                         
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                             <h4 class="modal-title" id="myModalLabel">Set Position</h4>
                                        </div>             
                                        <div class="modal-body">
                                            <form action="<?php echo site_url('category/position/');?>" method="post">
                                            <div class="form-group">
                                              
                                                <label>Postition</label>
                                                <input type="text" name="position" class="form-control" style="width: 200px" value="<?php echo $category['position'];?>">
                                                <input type="hidden" name="positionold" value="<?php echo $category['position'];?>">
                                                <input type="hidden" name="section" value="<?php echo $category['id_section'];?>">
                                                <input type="hidden" name="supsection" value="<?php echo $category['id_supsection'];?>">
                                            </div>
                                        </div>
                                                                
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                            <button type="submit" class="btn btn-primary">Change</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>


            <!-- <div style="float:left;border-radius:4px;color:#fff;font-weight:bold;padding:8px;background:#<?php echo ($category['status']=='1')?"4ce85a":"d30675";?>"><?php echo $category['status'];?></div>-->
             </td>
                        </tr>
						<?php $i++;}?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

              
              </div>

              

              

              

              
            </div>
          </div>
        </div>
        <!-- /page content -->
<?php include_once dirname(__FILE__).'/../layouts/footer.php';?>