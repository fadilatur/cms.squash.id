<?php include_once dirname(__FILE__).'/../layouts/header.php';?>
<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Watermark</h3>
              </div>

              

            <h4 style="color: red"><?php echo $this->session->flashdata('postwarning');?></h4>

            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
					<div style='padding-bottom:10px'><button onclick="location.href='<?php echo site_url('Watermark/add/');?>'" type="button" class="btn btn-info">Add</button>
					<h3><?php //echo $title['nama_section'];?></h3>
					<!--<button onclick="location.href='<?php echo site_url('administrator/toexcel');?>'" type="button" class="btn btn-info">Download to Excel</button>-->
					</div>

                    <ul class="nav navbar-right panel_toolbox">
                      <!--<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>-->
                      </li>
                      <li class="dropdown">
                       <!-- <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>-->
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <!--<li><a class="close-link"><i class="fa fa-close"></i></a>-->
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
    
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
						              <th>No</th>
                          <th>Title</th>
                          <th>Description</th>
                          <th>Watermark</th>
                          <th>Option</th>
                        </tr>
                      </thead>


                      <tbody>
					  <?php 
            
            $i=1;
            
					  foreach($watermark as $watermark){
						  ?>
                        <tr>
						  <td><?php echo $i;?></td>
						  <td><?php echo $watermark['judul'];?></td>
              <td><?php echo $watermark['keterangan'];?></td>          
              <td><img src="<?php echo $tim.$waterpath.$watermark['gambar'];?>&w=300&h=100&zc=0"></td>
						  <td>
                   <div class="btn-group">

                    <button onclick="location.href='<?php echo site_url('watermark/edit/'.$watermark['id']);?>'" type="button" class="btn btn-info">Edit</button></div>
                   
                    <div class="btn-group"><button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal2<?php echo $i;?>">Delete</button>
                </div>      
              </td>
              <div class="modal modal-info" id="myModal2<?php echo $i;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">×</span></button>
                        <h4 class="modal-title">Confirmation</h4>
                      </div>
                      <div class="modal-body">
                        Anda yakin akan menolak artikel ini ?
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                        <a href="<?php echo site_url('watermark/delete/'.$watermark['id']);?>" class="btn btn-danger">Delete</a>
                      </div>
                    </div>
                  
                  <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
              </div> 
            </tr>
						<?php $i++;} ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

              
              </div>

              

              

              

              
            </div>
          </div>
        </div>
        <!-- /page content -->
<?php include_once dirname(__FILE__).'/../layouts/footer.php';?>