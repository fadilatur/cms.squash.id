<?php include_once dirname(__FILE__).'/../layouts/header.php';?>
<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Kolom</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                <div style='padding-bottom:10px'><button onclick="location.href='<?php echo site_url('kolom/add/'.$daerah);?>'" type="button" class="btn btn-info">Add</button>
        <!--<button onclick="location.href='<?php echo site_url('administrator/toexcel');?>'" type="button" class="btn btn-info">Download to Excel</button>-->
          </div>
                    <ul class="nav navbar-right panel_toolbox">
                      <!--<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>-->
                      </li>
                      <li class="dropdown">
                       <!-- <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>-->
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <!--<li><a class="close-link"><i class="fa fa-close"></i></a>-->
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Image</th>
                          <th>Kolumnis</th>
                          <th>Kolom</th>
                          <th>Description</th>
                          <th style="width: 140px">Option</th>
                        </tr>
                      </thead>


                      <tbody>
            <?php $i=1;
              foreach($data as $a){
                $kolom = $this->T_section->get($a['category_id']);
                ?>
                        <tr>
                          <td><?php echo $i;?></td>
                          <td><img src="<?php echo $tim.$a['image'];?>&w=100&h=100&cz=1"></td>
                          <td><?php echo $a['nama'];?></td>
                          <td><?php echo $kolom['nama_section'];?></td>
                          <td><?php echo $a['keterangan'];?></td>
                          <td>
      
                            
                            <div><button onclick="location.href='<?php echo site_url('kolom/edit/'.$a['id'].'/'.$daerah);?>'" type="button" class="btn btn-info">Edit</button></div>


                            <div><button onclick="location.href='<?php echo site_url('kolom/artikel/'.$kolom['id_section']);?>'" type="button" class="btn btn-info">Pilihan</button></div>

               <div class="btn-group"><a data-href="<?php echo site_url('kolom/actiondelete/'.$a['id']);?>" data-toggle="modal" data-target="#confirm-delete<?php echo $i;?>" href="#"><button type="button" class="btn btn-info">Delete</button></a></div>
               <div class="modal fade" id="confirm-delete<?php echo $i;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                        <h4 class="modal-title" id="myModalLabel">Confirm Delete</h4>
                                                                    </div>
                                                                
                                                                    <div class="modal-body">
                                                                        <p>You are about to delete this data.</p>
                                                                        <p>Do you want to proceed?</p>
                                                                        <p class="debug-url"></p>
                                                                    </div>
                                                                    
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                                                        <a href="<?php echo site_url('kolom/actiondelete/'.$a['id'].'/'.$daerah);?>" class="btn btn-danger danger">Delete</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
              </td>
                        </tr>
            <?php $i++;}?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

              
              </div>

              

              

              

              
            </div>
          </div>
        </div>
        <!-- /page content -->
<?php include_once dirname(__FILE__).'/../layouts/footer.php';?>