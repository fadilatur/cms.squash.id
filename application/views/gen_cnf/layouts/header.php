<?php 
$css 		= $this->config->item('css');
$images 	= $this->config->item('images');
$js 		= $this->config->item('js');
$upload 	= $this->config->item('images_upload');
$uploadall   = $this->config->item('images_all');
$uploadlocal = $this->config->item('images_front');
$uploadpic = $this->config->item('gallery');
$uploaddoc = $this->config->item('document');

$tim    = $this->config->item('images_tim');
$rilizen    = $this->config->item('rilizen');
$waterpath   = $this->config->item('waterpath');

//set up user role
// $urole      = $this->model_role->select_group($this->session->userdata('role'));
//    foreach($urole as $urole){
//        $uarray[]   = $urole['menu_id'];
//    }
//$uri3 = $this->uri->segment(3);
	
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/jpg" href="<?php echo $images;?>logo.png"/>

    <title>CMS | squash.id</title>

    <!-- Bootstrap -->
    <link href="<?php echo $css;?>bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- NProgress -->
    <link href="<?php echo $css;?>nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="<?php echo $css;?>green.css" rel="stylesheet">
    <!-- bootstrap-progressbar -->
    <link href="<?php echo $css;?>bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="<?php echo $css;?>jqvmap.min.css" rel="stylesheet"/>

    <!-- Custom Theme Style -->
    <link href="<?php echo $css;?>custom.min.css" rel="stylesheet">

    <!-- Select2 -->
    <link href="<?php echo $css;?>/vendors/select2/dist/css/select2.min.css" rel="stylesheet">

     <script src="<?php echo $js;?>jquery.min.js"></script>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;background:#fff">
              <a href="<?php echo site_url('home')?>" class="site_title"><img style="margin-top: -10px; margin-left: 10px" src="<?php echo $tim.$images;?>logo.png&w=200&h=40&zc=0" /></a>
            </div>

            <div class="clearfix"></div>

            

            

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            
                <ul class="nav side-menu">
                  <li><a href="<?php echo site_url('home');?>"><i class="fa fa-home"></i> Home </a>
                    
                  </li>
				  <?php
          if($this->session->userdata('level') == 1 || $this->session->userdata('level') == 4) {?>
				  <li class="treeview active">
                            <a href="#">
                                <i class="fa fa-user-secret"></i> <span>Administrator</span><i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
							                 <?php if($this->session->userdata('daerah') == 0) {?>
                                <li>
                                    <a href="<?php echo site_url('administrator/index/0');?>">
                                        <i class="fa fa-user-secret"></i> <span>Utama</span>
                                    </a>
                                </li>
                                <?php ;} else {?>
                                <li>
                                    <a href="<?php echo site_url('administrator/index/1');?>">
                                        <i class="fa fa-user-secret"></i> <span>Administrator</span>
                                    </a>
                                </li>
                                <?php ;} ?>
                                <!--
								                <li >
                                    <a href="<?php echo site_url('level');?>">
                                        <i class="fa fa-cog"></i> <span>Level</span>
                                    </a>
                                </li>
							                 -->
                            </ul>
                   </li>
				   <?php ;}?>

            <li class="treeview">
              <a href="#">
                <i class="fa fa-file-text"></i> <span>Articles</span> <i class="fa fa-angle-left pull-right"></i>
              </a>		  
              <ul class="treeview-menu">
              <!--li><a  href="<?php echo site_url('artikel/add');?>"><i class="fa fa-plus-square-o"></i>Add Article</a></li-->
              <?php
              if($this->session->userdata('daerah') == 0) {?>
              <li><a  href=""><i class="fa fa-file-text"></i>All Article</a></li>
              <li><a  href="<?php echo site_url('artikel/index/0');?>">- <i class="fa fa-file-text"></i>Utama</a></li>
              <br>
              <?php ;} else {?>
              <li><a  href="<?php echo site_url('artikel/add/1');?>"><i class="fa fa-plus-square-o"></i>Add Artikel</a></li>
              <li><a  href="<?php echo site_url('artikel/index/1');?>"><i class="fa fa-file-text"></i>All Artikel</a></li>
              <?php ;} ?>

              <!-- <?php /*if($this->session->userdata('daerah') == 0)*/ {?>
              <li><a  href=""><i class="fa fa-file-text"></i>Foto</a></li>
              <li><a  href="<?php /*echo site_url('foto/index/0')*/;?>">- <i class="fa fa-file-text"></i>Utama</a></li>
              <br>
              <?php ;} /*else*/ {?>
              <li><a  href="<?php /*echo site_url('foto/index/1')*/;?>"><i class="fa fa-file-text"></i>Foto</a></li>
              <?php ;} ?> -->

              <li class=""><a href="<?php echo site_url('image/index/1');?>"><i class="fa fa-file-image-o"></i> <span>Images</span></a></li> 
              <li class=""><a href="<?php echo site_url('gallery');?>"><i class="fa fa-picture-o"></i> <span>Gallery</span></a></li> 
                 <!-- <li class="">
              <a href="<?php /*echo site_url('document')*/;?>">
                <i class="fa fa-file-o"></i> <span>Document</span> 
              </a>      
              
      </li> -->
              <?php if($this->session->userdata('level') == 1 || $this->session->userdata('level') == 2 || $this->session->userdata('level') == 4) {?>
              <?php if($this->session->userdata('daerah') == 0) {?>
              <br>
      				<li><a  href=""><i class="fa fa-th-large"></i>Category </a></li>
              <li><a  href="<?php echo site_url('category/index/0/0');?>">- <i class="fa fa-th-large"></i>Utama </a></li>
              <br>
              <?php ;} else { ?>
              <li><a  href="<?php echo site_url('category/index/0/1');?>"><i class="fa fa-th-large"></i>Category </a></li>
              <?php ;} ?>
              <?php ;}?>
              <!-- <li><a  href="<?php /*echo site_url('tag')*/;?>"><i class="fa fa-hashtag"></i>Tag</a></li> -->
              <!-- <li class=""><a href="<?php /*echo site_url('fokus')*/;?>"><i class="fa fa-tripadvisor"></i> <span>Fokus</span></a></li> -->
              <?php if($this->session->userdata('daerah') == 0) {?>
              <li class=""><a href="<?php echo site_url('');?>"><i class="fa fa-dashcube"></i> <span>Kolom</span></a></li>
              <li class=""><a href="<?php echo site_url('kolom/index/0');?>">- <i class="fa fa-dashcube"></i> <span>Utama</span></a></li>
              <br>
              <?php ;} else {?>
              <li class=""><a href="<?php echo site_url('kolom/index/1');?>"><i class="fa fa-dashcube"></i> <span>Kolom</span></a></li>
              <?php ;} ?>
              <!-- <li class=""><a href="<?php /*echo site_url('banner')*/;?>"><i class="fa fa-buysellads"></i> <span>Banner</span></a></li>
              <li class=""><a href="<?php /*echo site_url('watermark')*/;?>"><i class="fa fa-copyright"></i> <span>Watermark</span></a></li> -->
              <!-- <li class=""><a href="<?php /*echo site_url('banner/banner_rilizen')*/;?>"><i class="fa fa-buysellads"></i> <span>Banner Rilizen</span></a></li> -->
				
				<!--<li><a href="<?php //echo site_url('gen_cnf/topgrafik');?>"><i class="fa fa-desktop"></i> Top Grafik </a></li>-->
              </ul>
            </li>
		
					
                  <!--<li><a><i class="fa fa-edit"></i> Risk Profile Manag...<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="form.html">General Form</a></li>
                      <li><a href="form_advanced.html">Advanced Components</a></li>
                      <li><a href="form_validation.html">Form Validation</a></li>
                      
                    </ul>
                  </li>-->
      <!--
			<li class="treeview">
              <a href="#">
                <i class="fa fa-file-text"></i> <span>Komponen</span> <i class="fa fa-angle-left pull-right"></i>
              </a>		  
              <ul class="treeview-menu">
			  
				<li><a  href="<?php echo site_url('paper/edit/484');?>"><i class="fa fa-dot-circle-o"></i>Data Paper</a></li>
        <li><a  href="<?php echo site_url('pelatih');?>"><i class="fa fa-dot-circle-o"></i>Pelatih</a></li>
         <li><a  href="<?php echo site_url('stadion');?>"><i class="fa fa-dot-circle-o"></i>Stadion</a></li>
				<li><a  href="<?php echo site_url('klub');?>"><i class="fa fa-dot-circle-o"></i>Klub</a></li>
				<li><a  href="<?php echo site_url('liga');?>"><i class="fa fa-dot-circle-o"></i>Liga</a></li>
				li><a  href="<?php echo site_url('klasemen');?>"><i class="fa fa-dot-circle-o"></i>Klasemen</a></li
				<li><a  href="<?php echo site_url('jadwal');?>"><i class="fa fa-dot-circle-o"></i>Jadwal</a></li>
				<li><a  href="<?php echo site_url('negara');?>"><i class="fa fa-dot-circle-o"></i>Negara</a></li>
				
              </ul>
            </li>			
				-->
				 
			<?php if($this->session->userdata('level') == 1 || $this->session->userdata('level') == 4) {?> 

      <li class="treeview">
          <a href="#">
            <i class="fa fa-desktop"></i> <span>Live Scoring</span> <i class="fa fa-angle-left pull-right"></i>
          </a>      
          <ul class="treeview-menu">
            <li>
              <a  href="<?php echo site_url('match');?>"><i class="fa fa-user"></i>Single Match</a>
            </li>
            <li>
              <a  href="<?php echo site_url('match/group');?>"><i class="fa fa-users"></i>Group Match</a>
            </li>
          </ul>
      </li>

      <br>

			<li class="treeview">
              <a href="#">
                <i class="fa fa-gears"></i> <span>Setting</span> <i class="fa fa-angle-left pull-right"></i>
              </a>		  
              <ul class="treeview-menu">
			  
          <li><a  href="<?php echo site_url('profile/index/0');?>"><i class="fa fa-dot-circle-o"></i>Website Profile</a></li>
          <li><a  href="<?php echo site_url('informasi/index/0');?>"><i class="fa fa-dot-circle-o"></i>Website Info</a></li>
				<!--<div class="btn-group"><button onclick="location.href='<?php //echo site_url('gen_cnf/administrator/edit/'.$user['idx']);?>'" type="button" class="btn btn-info">Edit</button></div>-->
				
				  
                </ul>
			</li>


      <?php if($this->session->userdata('level') == 1 || $this->session->userdata('level') == 4){ ?>
        <li class="treeview">
              <a href="#">
                <i class="fa fa-users"></i> <span>Rilizen</span> <i class="fa fa-angle-left pull-right"></i>
              </a>      
              <ul class="treeview-menu">
        
                  <li><a  href="<?php echo site_url('artikel/rilizen');?>"><i class="fa fa-file-text"></i>Article Approved</a></li>
                  <li><a  href="<?php echo site_url('rilizen/article');?>"><i class="fa fa-file-text"></i>Article Waiting</a></li>
                  <li><a  href="<?php echo site_url('rilizen/user');?>"><i class="fa fa-user-o"></i>Users</a></li>
                  <!--<div class="btn-group"><button onclick="location.href='<?php //echo site_url('gen_cnf/administrator/edit/'.$user['idx']);?>'" type="button" class="btn btn-info">Edit</button></div>-->          
              </ul>
        </li>
      <?php }else{'';} ?>
      <?php ;}?>
      <!--
			<li class="">
              <a href="<?php echo site_url('polling');?>">
                <i class="fa fa-bar-chart"></i> <span>Polling</span> 
              </a>		  
              
			</li>
			<li class="">
              <a href="<?php echo site_url('member');?>">
                <i class="fa fa-user-secret"></i> <span>Member</span> 
              </a>		  
              
			</li>
      -->
      <?php if($this->session->userdata('level') == 1 || $this->session->userdata('level') == 2 || $this->session->userdata('level') == 4) {?>

      <li class="treeview">
              <a href="#">
                <i class="fa fa-bar-chart"></i> <span>Report</span> <i class="fa fa-angle-left pull-right"></i>
              </a>      
              <ul class="treeview-menu">
        
        <li><a  href="<?php echo site_url('report');?>"><i class="fa fa-dot-circle-o"></i>Author</a></li>
        <li><a  href="<?php echo site_url('report/article');?>"><i class="fa fa-dot-circle-o"></i>Article</a></li>
        <!--<div class="btn-group"><button onclick="location.href='<?php //echo site_url('gen_cnf/administrator/edit/'.$user['idx']);?>'" type="button" class="btn btn-info">Edit</button></div>-->
        
          
                </ul>
      </li>
     
       <li class="">
              <a href="<?php echo site_url('notifikasi');?>">
                <i class="fa fa-bell"></i> <span>Notification</span> 
              </a>      
              
      </li>

      

      

      <li class="">
              <a href="<?php echo site_url('location');?>">
                <i class="fa fa-map-marker"></i> <span>Location</span> 
              </a>      
              
      </li>

      <?php ;} ?>
      <?php if($this->session->userdata('level') == 1 || $this->session->userdata('level') == 4) {?>

	

	  <li class="">
              <a href="<?php echo site_url('contact');?>">
                <i class="fa fa-envelope"></i> <span>Message</span> 
              </a>      
              
      </li>

      <li class="">
              <a href="<?php echo site_url('subscriber');?>">
                <i class="fa fa-envelope"></i> <span>Subscriber</span> 
              </a>      
              
      </li>

       <li class="">
              <a href="<?php echo site_url('agenda');?>">
                <i class="fa fa-calendar"></i> <span>Agenda</span> 
              </a>      
              
      </li>
	    
	 <?php ;}?>
			</div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>
              <?php
              $admin = $this->T_admin->get_tadmin($this->session->userdata('id_adm'));?>
              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
					<img src="<?php echo $upload.'mimin/'.$admin['foto_admin'];?>"/><?php echo $this->session->userdata('nama');?>
					<span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="<?php echo site_url('administrator/edit/'.$this->session->userdata('id_adm'));?>">Profile</a></li>
                    
                    <li><a href="<?php echo site_url('auth/logout');?>"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>

                  </ul>
                </li>

                
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->