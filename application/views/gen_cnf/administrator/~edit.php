<?php include_once dirname(__FILE__).'/../layouts/header.php';?>
            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        ADMINISTRATOR
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?php echo site_url('gen_cnf/home');?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="<?php echo site_url('gen_cnf/administrator');?>">Administrator</a></li>
                        <li class="active">Edit</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-12">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">Administrator > Edit</h3>
                                </div><!-- /.box-header -->
                                <!-- form start -->
                                <form role="form" enctype="multipart/form-data" method="post" action="<?php echo site_url('gen_cnf/administrator/actionedit');?>">
                                    <div class="box-body">
										 <div class="form-group">
                                            <label for="number">Group Role</label><br/>
                                            <select style="width:250px" name='group_id'>
                                            <?php foreach($group as $mnm):?>
                                            <option <?php echo ($data['group_id']==$mnm['id'])?'selected':'';?> value='<?php echo $mnm['id'];?>' > <?php echo $mnm['group_name'];?></option>
                                            <?php endforeach;?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Username</label>
                                            <input type="username" name="username" class="form-control" value="<?php echo $data['username'];?>" id="exampleInputUsername1" placeholder="Enter username">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Email address</label>
                                            <input type="email" name="email" value="<?php echo $data['email'];?>" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Phone Number</label>
                                            <input type="number" name="phone" class="form-control" value="<?php echo $data['phone'];?>" id="exampleInputNumber1" placeholder="Enter Phone Number">
                                        </div>
                                        <div class="form-group">
                                            <label>Address</label>
                                            <textarea class="form-control" name="address" rows="3" placeholder="Enter ..."><?php echo $data['address'];?></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputFile">Input Image</label>
                                            <?php if(!empty($data['image'])){?>
                                            	<div>
                                                	<img src="<?php echo $upload;?><?php echo $data['image'];?>" alt="User Image" />
                                                	<input type="hidden" name="imagex" value="<?php echo $data['image'];?>"/>
                                                </div>
                                            <?php }?>
                                            <input type="file" name="image" id="exampleInputFile">
                                        </div>
                                    </div><!-- /.box-body -->

                                    <div class="box-footer">
                                    	<input type="hidden" name="id" value="<?php echo $data['id'];?>"/>
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </form>
                            </div><!-- /.box -->
                        </div><!--/.col (right) -->
                    </div>   <!-- /.row -->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->
        <!-- jQuery 2.0.2 -->
        <script src="<?php echo $js;?>jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="<?php echo $Js;?>bootstrap.min.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="<?php echo $Js;?>AdminLTE/app.js" type="text/javascript"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="<?php echo $Js;?>AdminLTE/demo.js" type="text/javascript"></script>  
<?php include_once dirname(__FILE__).'/../layouts/header.php';?>