<?php include_once dirname(__FILE__).'/../layouts/header.php';?>
<script type="text/javascript" src="<?php echo $js.'ckeditor/ckeditor.js'; ?>"></script>
<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>ADMINISTRATOR</h3>
              </div>
				<div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <ol class="breadcrumb">
                        <li><a style="color:#000" href="<?php echo site_url('home')?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a style="color:#000" href="<?php echo site_url('administrator');?>">Administrator</a></li>
                        <li class="active">Add</li>
                    </ol>
                  </div>
                </div>
              </div>
            
            </div>

            <div class="clearfix"></div>

            <div class="row">
             <form id="demo-form" data-parsley-validate  enctype="multipart/form-data" method="post" action="<?php echo site_url('administrator/actionadd');?>">
              
			  <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_content">
                    
                      <label for="fullname">Level :</label>
                      <select style="width:250px" name='group_id'>
						    <?php foreach($group as $mnm):?>
							<option value='<?php echo $mnm['level'];?>' > <?php echo $mnm['level_nama'];?></option>
                            <?php endforeach;?>
                      </select>
                      <br/><br/>
					  
					  <label for="fullname">Nama Lengkap :</label>
                      <input type="text" id="name" class="form-control" placeholder="Enter Nama" name="name" data-parsley-trigger="change" />
                      <br/>
					  
					  <label for="fullname">Email :</label>
                      <input type="email" id="email" class="form-control" placeholder="Enter Email" name="email" data-parsley-trigger="change" />
                      <br/>
					  
					  <label for="fullname">Deskripsi :</label>
					  <textarea id="deskripsi" name="deskripsi" placeholder="Enter deskripsi" class="form-control"></textarea>
                      <br/>
                      
					  <label for="fullname">Password :</label>
                      <input type="password" id="password" name="password" placeholder="Password" class="form-control">
					  <br/><br/>
					  
					  <label for="fullname">Ulangi Password :</label>
                      <input type="password" id="password" name="password" placeholder="Password" class="form-control">
					  <br/><br/>

                      <label for="email">Foto :</label>
					  <input type="file" id="foto" name="image" placeholder="Foto" class="form-control">
                      <br/>

                  </div>
                </div>
              </div>

               <div  class="col-md-12">
                <input type="hidden" value="<?php //echo $parent_id;?>" name="id" />
                <input type="hidden" name="daerah" value="<?php echo $daerah;?>">
                <button type="submit" class="btn btn-success">Submit</button>
			   </div>
               </form>
			   
            </div>

          </div>
        </div>
		
         <div style="clear:both"></div>
        <!-- /page content -->
<?php include_once dirname(__FILE__).'/../layouts/footer.php';?>