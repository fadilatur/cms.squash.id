<?php include_once dirname(__FILE__).'/../layouts/header.php';?>
<script type="text/javascript" src="<?php echo $js.'ckeditor/ckeditor.js'; ?>"></script>
<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>ADMINISTRATOR</h3>
              </div>
				<div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <ol class="breadcrumb">
                        <li><a style="color:#000" href="<?php echo site_url('home')?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a style="color:#000" href="<?php echo site_url('administrator');?>">Administrator</a></li>
                        <li class="active">Edit</li>
                    </ol>
                  </div>
                </div>
              </div>
            
            </div>
            <h4 style="color: red"><?php echo $this->session->flashdata('warning');?></h4>
            <div class="clearfix"></div>

            <div class="row">
             <form id="demo-form" data-parsley-validate  enctype="multipart/form-data" method="post" action="<?php echo site_url('administrator/actionedit');?>">
              
			  <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_content">
                    <?php if($this->session->userdata('level') == 2 || $this->session->userdata('level') == 3) {?>
                    <input type="hidden" name="group_id" value="<?php echo $data['level'];?>">
                  <?php } else {?>
                      <label for="fullname">Level :</label>
                      <select style="width:250px" name='group_id'>
						    <?php foreach($group as $mnm):?>
							<option <?php echo ($data['level']==$mnm['level'])?'selected':'';?> value='<?php echo $mnm['level'];?>' > <?php echo $mnm['level_nama'];?></option>
                            <?php endforeach;?>
                        </select>
                      <br/><br/>
					           <?php ;}?>
					  <label for="fullname">Nama Lengkap :</label>
                      <input type="text" id="name" class="form-control" name="name" value="<?php echo $data['nama'];?>" data-parsley-trigger="change" />
                      <br/>
					  
					  <label for="fullname">Email :</label>
                      <input type="email" id="email" class="form-control" name="email" value="<?php echo $data['email'];?>" data-parsley-trigger="change" />
                      <br/>
					  
					  <label for="fullname">Deskripsi :</label>
                      <textarea id="fullname" class="form-control" name="deskripsi" data-parsley-trigger="change" /><?php echo $data['des_admin'];?></textarea>
					  <br/>
            <!--label for="fullname">Old Password :</label>
                      <input type="password" id="old_password" class="form-control" name="old_password" value="<?php echo $data['password'];?>" data-parsley-trigger="change" /-->
            <br/>
					  <label for="fullname">New Password :</label>
                      <input type="password" id="password" class="form-control" name="password" value="" data-parsley-trigger="change" />
					  <br/>
            <label for="fullname">Re-Type Password :</label>
                      <input type="password" id="password" class="form-control" name="repassword" value="" data-parsley-trigger="change" />
            <br/>
                  </div>
                </div>
				<div class="x_panel">
                  <div class="x_title">
                    <h2>Foto :</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                       <?php if(!empty($data['foto_admin'])){ ?>
                            <img src="<?php echo $upload;?>mimin/<?php echo $data['foto_admin'];?>" width="100"/>
                             <input type="hidden" id="fullname" class="form-control" name="imagex" value="<?php echo $data['foto_admin'];?>"  />
                       <?php } ?>
                       <input type="file" id="fullname" class="form-control" name="image"  />
                  </div>

                </div>
              </div>

               <div  class="col-md-12">
                <input type="hidden" value="<?php echo $data['id_adm'];?>" name="id" />
                
                <button type="submit" class="btn btn-success">Submit</button>
			   </div>
               </form>
			   
            </div>

          </div>
        </div>
		
         <div style="clear:both"></div>
        <!-- /page content -->
<?php include_once dirname(__FILE__).'/../layouts/footer.php';?>