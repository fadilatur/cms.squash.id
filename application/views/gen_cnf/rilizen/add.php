<?php include_once dirname(__FILE__).'/../layouts/header.php';?>
<script type="text/javascript" src="<?php echo $js.'ckeditor/ckeditor.js'; ?>"></script>
<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Rilizen</h3>
              </div>
        <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <ol class="breadcrumb">
                        <li><a style="color:#000" href="<?php echo site_url('home')?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a style="color:#000" href="<?php echo site_url('notifikasi');?>">Rilizen</a></li>
                        <li class="active">Add</li>
                    </ol>
                  </div>
                </div>
              </div>
            
            </div>

            <div class="clearfix"></div>

            <div class="row">
             <form id="demo-form" data-parsley-validate  enctype="multipart/form-data" method="post" action="<?php echo site_url('rilizen/addprofile');?>">
              <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_content">
                      <label for="fullname">Image* :</label><br>
                      <input type="file" id="fullname" name="image"><br>

                      <label for="fullname">Banner* :</label><br>
                      <input type="file" id="fullname" name="banner"><br>

                      <label for="fullname">Name* :</label>
                      <input type="text" id="fullname" class="form-control" value="" name="nama" required />
                      <br/>

                      <label for="fullname">Proffesion * :</label>
                      <input type="text" id="fullname" class="form-control" value="" name="url"  />
                      <br/>

                      <label for="fullname">Email * :</label>
                      <input type="text" id="fullname" class="form-control" value="" name="email"  />
                      <br/>

                      <label for="fullname">Born * :</label>
                      <input type="text" id="fullname" class="form-control" value="" name="place"  />
                      <br/>

                      <label for="fullname">Birthday * :</label>
                      <input type="text" id="fullname" class="form-control" value="" name="birthday"  />
                      <br/>

                      <label for="fullname">Facebook * :</label>
                      <input type="text" id="fullname" class="form-control" value="" name="facebook"  />
                      <br/>

                      <label for="fullname">Twitter * :</label>
                      <input type="text" id="fullname" class="form-control" value="" name="twitter"  />
                      <br/>

                      <label for="fullname">Instagram * :</label>
                      <input type="text" id="fullname" class="form-control" value="" name="instagram"  />
                      <br/>


                      <label for="fullname">Gplus * :</label>
                      <input type="text" id="fullname" class="form-control" value="" name="gplus"  />
                      <br/>

                      <label for="fullname">Address * :</label>
                      <textarea class="form-control" name="address"></textarea>
                      <br/>

                      <label for="fullname">About Me * :</label>
                      <textarea class="form-control" name="description"></textarea>
                      <br/>

                      
  
                  </div>
                 <button type="submit" class="btn btn-success">Submit</button>
                </div>

                  </div>

               </form>
              </div>

              
              </div>


               

                </div>


               
               
            </div>

          </div>
        </div>
         <div style="clear:both"></div>
        <!-- /page content -->
<?php include_once dirname(__FILE__).'/../layouts/footer.php';?>