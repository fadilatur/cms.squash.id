<?php include_once dirname(__FILE__).'/../layouts/header.php';?>

 <!-- Datatables -->
    <link href="<?php echo $css;?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $css;?>vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $css;?>vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $css;?>vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $css;?>vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="row top_tiles" style="margin: 10px 0;">
              <div class="col-md-3 col-sm-3 col-xs-6 tile">
                <h3>Rilizen</h3>
               
              </div>
            </div>
            <br />


            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="dashboard_graph x_panel">
                  
                  <div class="x_content">
                    <table id="datatablesalah" class="table table-striped table-bordered">
                      <thead>
                        <tr>
					<th>No</th>
                    <th>Title</th>      
                    <th>Category</th>
					<th>Postdate</th>
					<th>User</th>
                    <th>Option</th>
                        </tr>
                      </thead>
					  <tbody>
					  <?php  $i=1;
					  foreach($article as $article){?>
      				<tr>
      				<td><?php echo $i++;?></td>
      				<td width="330"><?php echo $article['judul_artikel'];?></td>
      				<td width="270"><?php echo $article['nama_section'];?></td>
      				<td width="88"><?php echo $article['postdate'];?></td>
      				<td width="74"><?php echo $article['nama_lengkap'];?></td>
      				<td width="290">
                <?php
                if($article['publish'] == 'N') {?>
                <div class="btn-group">
                <a href="http://dev.rilis.id/www.rilis.id/rilizen/artikel/<?php echo $article['urltitle'];?>" class="btn btn-info" target="__blank">Live Preview</a>
                </div>
        				<div class="btn-group"><button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal<?php echo $i;?>">Approve</button></div>
                <div class="btn-group"><button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal2<?php echo $i;?>">Deny</button></div>
                <?php ;} elseif($article['publish'] == 'Y') { echo 'Approved';} elseif($article['publish'] == 'T') {echo 'Denied';}?>
        				</td>
            </tr>

            <div class="modal modal-info" id="myModal<?php echo $i;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">×</span></button>
                        <h4 class="modal-title">Confirmation</h4>
                      </div>
                      <div class="modal-body">
                        Anda yakin akan menerima artikel ini ?
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                        <a href="<?php echo site_url('rilizen/approve/'.$article['id_artikel']);?>" class="btn btn-primary">Approve</a>
                      </div>
                    </div>
                  
                  <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
              </div>

              <div class="modal modal-info" id="myModal2<?php echo $i;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">×</span></button>
                        <h4 class="modal-title">Confirmation</h4>
                      </div>
                      <div class="modal-body">
                        Anda yakin akan menolak artikel ini ?
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                        <a href="<?php echo site_url('rilizen/deny/'.$article['id_artikel']);?>" class="btn btn-danger">Deny</a>
                      </div>
                    </div>
                  
                  <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
              </div>
                <?php } ?>
						  
						  
                  
            </tbody>
					  
					  
					 </table> 
                  </div>
                </div>



              </div>
            </div>


            
          </div>
        </div>
        <!-- /page content -->
 <!-- Datatables -->
    <script src="<?php echo $css;?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo $css;?>vendors/datatables.net-scroller/js/datatables.scroller.min.js"></script>
    <script src="<?php echo $css;?>vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo $css;?>vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo $css;?>vendors/pdfmake/build/vfs_fonts.js"></script>
    <script>
       $('#datatable').dataTable();
    </script>

<?php include_once dirname(__FILE__).'/../layouts/footer.php';?>