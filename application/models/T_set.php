<?php 
/**
 * 
 */
class T_set extends CI_Model
{
	protected $table = 't_set';

	function getAllByMatch($id) {
		$this->db->where('match_id', $id);
		return $this->db->get($this->table)->result_array();
	}

	function add($data) {
		$this->db->insert($this->table, $data);
	}

	function getById($id) {
		$this->db->where('id', $id);
		return $this->db->get($this->table)->row_array();
	}

	function update($data, $id) {
		$this->db->where('id', $id);
		$this->db->update($this->table, $data);;
	}

	function delete($id) {
		$this->db->where('id', $id);
		$this->db->delete($this->table);
	}

	function updateByMatch($data, $match) {
		$this->db->where('match_id', $match);
		$this->db->update($this->table, $data);;
	}

	function updateBySetMatch($data, $match, $set) {
		$this->db->where('match_id', $match);
		$this->db->where('set', $set);
		$this->db->update($this->table, $data);;
	}
}