<?php
/**
* 
*/
class T_folder_image extends CI_Model
{
	
	protected $_table = 't_folder_image';

	function getparentimage($id)
	{
		$this->db->where('folder_id', $id);
		$this->db->order_by('postdate', 'DESC');
		$this->db->limit(20);
		return $this->db->get($this->_table)->result_array();
	}

	function getparentimagemore($offset,$id)
	{
		$this->db->where('folder_id', $id);
		$this->db->order_by('postdate', 'DESC');
		$this->db->limit(20);
		$this->db->offset($offset);
		return $this->db->get($this->_table)->result_array();
	}

	function getparentimagesearch($id, $search)
	{
		$this->db->like('title', $search);
		//$this->db->where('folder_id', $id);
		$this->db->order_by('postdate', 'DESC');
		return $this->db->get($this->_table)->result_array();
	}

	function getimage($id)
	{
		$this->db->where('id', $id);
		return $this->db->get($this->_table)->row_array();
	}

	function add($data)
	{
		$this->db->insert($this->_table, $data);
	}

	function edit($data, $id)
	{
		$this->db->where('id', $id);
		$this->db->update($this->_table, $data);
	}

	function delete($id)
	{
		$this->db->delete($this->_table, array('id' => $id));
	}
}