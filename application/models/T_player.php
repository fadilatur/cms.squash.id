<?php 
/**
 * 
 */
class T_player extends CI_Model
{
	protected $table = 't_player';

	function getAllByMatch($id) {
		$this->db->where('match_id', $id);
		return $this->db->get($this->table)->result_array();
	}

	function add($data) {
		$this->db->insert($this->table, $data);
	}

	function getById($id) {
		$this->db->where('id', $id);
		return $this->db->get($this->table)->row_array();
	}

	function update($data, $id) {
		$this->db->where('id', $id);
		$this->db->update($this->table, $data);
	}

	function delete($id) {
		$this->db->where('id', $id);
		$this->db->delete($this->table);
	}

	function getAllByMatchPlayer($id,$player) {
		$this->db->where('match_id', $id);
		$this->db->where('status', $player);
		return $this->db->get($this->table)->row_array();
	}

	function updateByMatch($match, $data) {
		$this->db->where('match_id', $match);
		$this->db->update($this->table, $data);
	}
}