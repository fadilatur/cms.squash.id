<?php
class T_section extends CI_Model 
{
	protected $_table = 't_section';
	function __construct()
	{
		parent::__construct();
	}
	 
	
	
	
	function select($id){
		$this->db->where('id_supsection', $id);
		
		$query = $this->db->get('t_section');

		return $query->result_array();
	}

	function selectadmin($id){
		$this->db->where('id_supsection', $id);
		$this->db->where('status', 1);
		$query = $this->db->get('t_section');

		return $query->result_array();
	}
	
	function add($data)
	    {
	        $this->db->insert($this->_table, $data);
	    }
		
	function search($keyword)
	{
		$this->db->like('nama_section', $keyword);
		$query = $this->db->get('t_section');
		return $query->result_array();
	}
	 
	function get($adminid)
	{
		$query   = $this->db->get_where($this->_table, array('id_section' => $adminid));
		return $query->row_array();
	}
	
	 
	function update($id, $data)
    {
		$this->db->where('id_section', $id);
		$this->db->update($this->_table, $data); 
	}

	function getcategory($id, $daerah)
	{
		$this->db->order_by('position', 'ASC');
		$this->db->where('daerah', $daerah);
		$this->db->where_not_in('id_section', array(17,8,172,153,154,155,156,157,158,159,160,161,162,172,));
		
		return $this->db->get_where($this->_table, array('id_supsection' => $id))->result_array();
	}
	
	function getcategory1($id, $daerah)
	{
		$section = array('2','126','38','23', '29', '8');
		$this->db->where('daerah', 0);
		$this->db->order_by('position', 'ASC');
		$this->db->where_in('t_section.id_section', array(25,153,154,155,156,157,125,124,162,158,122,123,132,159,160,24,161,172));
		$this->db->where_not_in('t_section.id_section', $section);
		return $this->db->get_where($this->_table)->result_array();
	}

	function delete($data)
	{
		$this->db->delete($this->_table, $data);
	}
	function getsection($id)
	{
		return $this->db->get_where($this->_table, array('id_section' => $id))->row_array();
	}

	function count($id){
		$this->db->where('id_supsection', $id);
		$query = $this->db->count_all_results($this->_table); 
		return $query;
	}

	function checkpos($position, $supsection)
	{
		return $this->db->get_where($this->_table, array('position' => $position, 'id_supsection' => $supsection))->row_array();
	}


	function updatepos($id, $data)
	{
		$this->db->where('id_section', $id);
		$this->db->update($this->_table, $data);
	}

	
	
}


?>
