<?php
class T_notifikasi extends CI_Model 
{
	protected $_table = 't_notifikasi';
	function __construct()
	{
		parent::__construct();
	}
	function select(){
		$this->db->order_by('id_notifikasi', 'desc');
		$this->db->select('id_notifikasi,title,content,tanggal,jam,status');
		$query = $this->db->get('t_notifikasi');
		
		return $query->result_array();
	}
	
	function add($data)
	    {
	        $this->db->insert($this->_table, $data);
	    }

	function actionadd(){
		$query = $this->db->get('t_notifikasi');
	    }
	
	function edits($id){
		$query   = $this->db->get_where('t_notifikasi', array('id_notifikasi' => $id));
		return $query->row_array();
	}
	

	function actionedit($id, $data)
    {

	$this->db->where('id_notifikasi', $id);
	$this->db->update('t_notifikasi', $data); 
	}
	
	function delete($data)
	{
		$this->db->delete('t_notifikasi', array('id_notifikasi' => $data)); 
	}

	function getpush()
	{
		$this->db->limit(1);
		$this->db->order_by('id_notifikasi', 'desc');
		return $this->db->get_where($this->_table, array('status' => 0))->result_array();
	}

	function updatepush($id)
	{
		$this->db->where('id_notifikasi', $id);
		$this->db->update($this->_table, array('status' => 1));
	}
	
}

/*echo '<pre>';
print_r($this->db->last_query());
echo '</pre>';*/

//echo "<pre>";
		//print_r($datacontent['data']);
		//echo "</pre>";
?>
