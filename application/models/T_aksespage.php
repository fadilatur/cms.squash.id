<?php
	class T_aksespage extends CI_Model 
	{
		protected $_table = 't_aksespage';
		function __construct()
		{
			parent::__construct();
		}
		 
		function select()
	    {
			$query = $this->db->get($this->_table);
			return $query->result_array();
		}

		function select_group($id)
	    {
	    	$this->db->select('t_page.page,t_aksespage.*');
	    	$this->db->join('t_page','t_page.id_page = t_aksespage.level');
	    	$this->db->where('level',$id);
			$query = $this->db->get($this->_table);
			//echo '<pre>';
//print_r($this->db->last_query());
//echo '</pre>';
			return $query->result_array();
			
		}
		
		function delete($data)
		{
			$this->db->delete($this->_table, array('group_id' => $data)); 
		}

		function add($data)
	    {
	        $this->db->insert($this->_table, $data);
	    }
	}
?>
