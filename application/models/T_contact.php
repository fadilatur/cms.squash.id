<?php
class T_contact extends CI_Model 
{
	protected $_table = 't_contact';
	function __construct()
	{
		parent::__construct();
	}
	 
	function select(){
	
		$query = $this->db->get('t_contact');
		return $query->result_array();
	
	}
	
	function get_contact($id)
	{
	
		$query   = $this->db->get_where('t_contact', array('id_contact' => $id));
		return $query->row_array();
	
	}
	
	function delete($data)
	{
		$this->db->delete('t_contact', array('id_contact' => $data)); 
	}
	
	
	
}

?>
