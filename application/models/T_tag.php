<?php
class T_tag extends CI_Model 
{
	protected $_table = 't_tag';
	function __construct()
	{
		parent::__construct();
	}
	
	function select_group($id)
	    {
	    	$this->db->select('t_supsection.id_supsection,t_tag.*');
	    	$this->db->join('t_supsection,t_supsection.id_supsection = t_tag.id_tag');
	    	$this->db->where('id_tag',$id);
			$query = $this->db->get($this->_table);
			
			return $query->result_array();
			
		}


	function get($id)
		{
			$query   = $this->db->get_where($this->_table, array('id_tag' => $id));
			return $query->row_array();
		}


		function cekcategory($id)
	    {

	    	$this->db->where('nama_tag',$id);
			$query = $this->db->get($this->_table);
			
			return $query->row_array();
			
		}


		function select()
	    {
	    	
			$query = $this->db->get($this->_table);
			return $query->result_array();
			
		}
	
	 function add($data)
	    {
	        $this->db->insert($this->_table, $data);
	    }



	function select_list($pages,$viewperpage){
		
		$query = $this->db->get($this->_table,$viewperpage,$pages);
		return $query->result_array();
	}

	function count()
	{
		return $this->db->count_all_results($this->_table);
	}

	function delete($data)
	{
		$this->db->delete($this->_table, array('id_tag' => $data)); 
	}
	


	function update($id, $data)
    {
		$this->db->where('id_tag', $id);
		$this->db->update($this->_table, $data); 
	}

	
}

/*echo '<pre>';
print_r($this->db->last_query());
echo '</pre>';*/

//echo "<pre>";
		//print_r($datacontent['data']);
		//echo "</pre>";
?>
