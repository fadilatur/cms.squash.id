<?php
class T_kolom extends CI_Model 
{
	protected $_table = 't_kolom';
	function __construct()
	{
		parent::__construct();
	}
	 
	function select($daerah){
	
		$this->db->select('*');
		$this->db->where('daerah', $daerah);
		$this->db->order_by('postdate', 'DESC');
		$query = $this->db->get($this->_table);
		return $query->result_array();
	
	}
	function delete($id)
	{
		$this->db->delete($this->_table, array('id' => $id)); 
	}
	
	function add($data)
    {
		
        $this->db->insert($this->_table, $data);
    }
	
	function update($id, $data)
    {
		$this->db->where('id', $id);
		$this->db->update($this->_table, $data); 
	}

	function get_kolom($id)
	{
		
		$query   = $this->db->get_where($this->_table, array('id' => $id));
		return $query->row_array();
	}
	
}

?>
