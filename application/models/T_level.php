<?php
class T_level extends CI_Model 
{
	protected $_table = 't_level';
	function __construct()
	{
		parent::__construct();
	}
	 
	 
	function authenticate($name) 
	{
		$query    = $this->db->get_where($this->_table, array('email' => $name));
		$result   = $query->row_array();
		
		return $result;
	}
	
	function authenticate2($password) 
	{
		$query    = $this->db->get_where($this->_table, array('password' => $password));
		$result   = $query->row_array();
		
		return $result;
	}
	
	function select(){
		$query = $this->db->get($this->_table);
		return $query->result_array();
	}

	function selectin($data){
		$this->db->where_in('level', $data);
		$query = $this->db->get($this->_table);
		return $query->result_array();
	}
	
	function get_group($adminid)
		{
			$query   = $this->db->get_where($this->_table, array('level' => $adminid));
			return $query->row_array();
		}
	
	
	
	//function select(){
		//$this->db->select('t_admin.id_adm as idx, t_admin.*, t_level.*');
		//$this->db->join('t_level', 't_admin.level = t_level.level');
		//$query = $this->db->get('t_admin');
		
		//return $query->result_array();
	//}
	
}

/*echo '<pre>';
print_r($this->db->last_query());
echo '</pre>';*/

//echo "<pre>";
		//print_r($datacontent['data']);
		//echo "</pre>";
?>
