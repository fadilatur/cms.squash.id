<?php
/**
* 
*/
class T_folder extends CI_Model
{
	protected $_table = 't_folder';


	function getfolder($id)
	{
		$this->db->where('id', $id);
		return $this->db->get($this->_table)->row_array();
	}

	function getparent($id)
	{
		$this->db->where('parent_id', $id);
		return $this->db->get($this->_table)->result_array();
	}

	function getparentsearch($id, $search)
	{
		$this->db->like('nama', $search);
		//$this->db->where('parent_id', $id);
		return $this->db->get($this->_table)->result_array();
	}

	function add($data)
	{
		$this->db->insert($this->_table, $data);
	}

	function edit($data, $id)
	{
		$this->db->where('id', $id);
		$this->db->update($this->_table, $data);
	}

	function delete($id)
	{
		$this->db->delete($this->_table, array('id' => $id));
	}
}