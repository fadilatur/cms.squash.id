<?php
class T_gallery extends CI_Model 
{
	protected $_table = 't_gallery';
	function __construct()
	{
		parent::__construct();
	}
	 
	 
	
	
	function select(){
		$this->db->select('*');
		$this->db->join('t_admin', 't_gallery.post_by = t_admin.id_adm');
		$this->db->order_by('t_gallery.id_gallery', 'DESC');
		$query = $this->db->get($this->_table);
		return $query->result_array();
	}

	function count()
	{
		return $this->db->count_all_results($this->_table);
	}
	

	function selectadmin($id,$pages,$viewperpage){
		
		//if($id!=0){
				$this->db->where('parent_id', $id);
		//}
		$this->db->order_by('tgl_pub', 'DESC');
		$this->db->join('t_admin', 't_admin.id_adm = t_artikel.id_admin');
		$query = $this->db->get($this->_table,$viewperpage,$pages);
		return $query->result_array();
	}



	function selectadminsub($id,$pages,$viewperpage){
		
		//if($id!=0){
				$this->db->where('parent_id', $id);
		//}
		$this->db->order_by('id_artikel', 'ASC');
		$this->db->join('t_admin', 't_admin.id_adm = t_artikel.id_admin');
		$query = $this->db->get($this->_table,$viewperpage,$pages);
		return $query->result_array();
	}



	function selectsection($id,$pages,$viewperpage){
		$this->db->where('t_relasi.id_object2', $id);
		$this->db->order_by('id_artikel', 'DESC');
		$this->db->join('t_admin', 't_admin.id_adm = t_artikel.id_admin');
		$this->db->join('t_relasi', 't_relasi.id_object = t_artikel.id_artikel');
		$query = $this->db->get($this->_table,$viewperpage,$pages);
		return $query->result_array();
	}

	function countsection($id){
		$this->db->where('t_relasi.id_object2', $id);
		$this->db->order_by('id_artikel', 'DESC');
		$this->db->join('t_admin', 't_admin.id_adm = t_artikel.id_admin');
		$this->db->join('t_relasi', 't_relasi.id_object = t_artikel.id_artikel');
		$query = $this->db->count_all($this->_table); 
		return $query;
	}



	function selectbyadmin($id,$pages,$viewperpage){
		$this->db->where('id_admin', $id);
		$this->db->order_by('id_artikel', 'DESC');
		$this->db->join('t_admin', 't_admin.id_adm = t_artikel.id_admin');
		
		$query = $this->db->get($this->_table,$viewperpage,$pages);
		return $query->result_array();
	}

	function countbyadmin($admin){
		$this->db->where('id_admin', $admin);
		$this->db->order_by('id_artikel', 'DESC');
		$this->db->join('t_admin', 't_admin.id_adm = t_artikel.id_admin');
		$this->db->join('t_relasi', 't_relasi.id_object = t_artikel.id_artikel');
		$query = $this->db->count_all($this->_table); 
		return $query;
	}



	function selectlengkap($id,$admin,$judul,$pages,$viewperpage){
		$this->db->where('t_relasi.id_object2', $id);
		$this->db->where('id_admin', $admin);
		$this->db->like('judul_artikel', $judul);
		$this->db->order_by('id_artikel', 'DESC');
		$this->db->join('t_admin', 't_admin.id_adm = t_artikel.id_admin');
		$this->db->join('t_relasi', 't_relasi.id_object = t_artikel.id_artikel');
		$query = $this->db->get($this->_table,$viewperpage,$pages);
		return $query->result_array();
	}


	function countlengkap($id,$admin,$judul){
		$this->db->where('t_relasi.id_object2', $id);
		$this->db->where('id_admin', $admin);
		$this->db->like('judul_artikel', $judul);
		$this->db->order_by('id_artikel', 'DESC');
		$this->db->join('t_admin', 't_admin.id_adm = t_artikel.id_admin');
		$this->db->join('t_relasi', 't_relasi.id_object = t_artikel.id_artikel');
		$query = $this->db->count_all($this->_table); 
		return $query;
	}



	function selectlengkap1($id,$admin,$pages,$viewperpage){
		$this->db->where('t_relasi.id_object2', $id);
		$this->db->where('id_admin', $admin);
		$this->db->order_by('id_artikel', 'DESC');
		$this->db->join('t_admin', 't_admin.id_adm = t_artikel.id_admin');
		$this->db->join('t_relasi', 't_relasi.id_object = t_artikel.id_artikel');
		$query = $this->db->get($this->_table,$viewperpage,$pages);
		return $query->result_array();
	}


	function countlengkap1($id,$admin){
		$this->db->where('t_relasi.id_object2', $id);
		$this->db->where('id_admin', $admin);
		$this->db->order_by('id_artikel', 'DESC');
		$this->db->join('t_admin', 't_admin.id_adm = t_artikel.id_admin');
		$this->db->join('t_relasi', 't_relasi.id_object = t_artikel.id_artikel');
		$query = $this->db->count_all($this->_table); 
		return $query;
	}


	function selectlengkap2($id,$judul,$pages,$viewperpage){
		$this->db->where('t_relasi.id_object2', $id);
		
		$this->db->like('judul_artikel', $judul);
		$this->db->order_by('id_artikel', 'DESC');
		$this->db->join('t_admin', 't_admin.id_adm = t_artikel.id_admin');
		$this->db->join('t_relasi', 't_relasi.id_object = t_artikel.id_artikel');
		$query = $this->db->get($this->_table,$viewperpage,$pages);
		return $query->result_array();
	}

	function countlengkap2($id,$judul){
		$this->db->where('t_relasi.id_object2', $id);
		$this->db->like('judul_artikel', $judul);
		$this->db->order_by('id_artikel', 'DESC');
		$this->db->join('t_admin', 't_admin.id_adm = t_artikel.id_admin');
		$this->db->join('t_relasi', 't_relasi.id_object = t_artikel.id_artikel');
		$query = $this->db->count_all($this->_table); 
		return $query;
	}


	function selectlengkap3($admin,$judul,$pages,$viewperpage){
		
		$this->db->where('id_admin', $admin);
		$this->db->like('judul_artikel', $judul);
		$this->db->order_by('id_artikel', 'DESC');
		$this->db->join('t_admin', 't_admin.id_adm = t_artikel.id_admin');
		$query = $this->db->get($this->_table,$viewperpage,$pages);
		return $query->result_array();
	}

	function countlengkap3($admin,$judul){
		$this->db->where('id_admin', $admin);
		$this->db->like('judul_artikel', $judul);
		$this->db->order_by('id_artikel', 'DESC');
		$this->db->join('t_admin', 't_admin.id_adm = t_artikel.id_admin');
		$this->db->join('t_relasi', 't_relasi.id_object = t_artikel.id_artikel');
		$query = $this->db->count_all($this->_table); 
		return $query;
	}



	function selectjudul($id,$pages,$viewperpage){
		$this->db->like('judul_artikel', $id);
		$this->db->order_by('id_artikel', 'DESC');
		$this->db->join('t_admin', 't_admin.id_adm = t_artikel.id_admin');
		
		$query = $this->db->get($this->_table,$viewperpage,$pages);
		return $query->result_array();
	}



	function countjudul($judul){
		$this->db->like('judul_artikel', $judul);
		$this->db->order_by('id_artikel', 'DESC');
		$this->db->join('t_admin', 't_admin.id_adm = t_artikel.id_admin');
		$this->db->join('t_relasi', 't_relasi.id_object = t_artikel.id_artikel');
		$query = $this->db->count_all($this->_table); 
		return $query;
	}
	 
	
	
	function get_group($adminid)
		{
			$query   = $this->db->get_where($this->_table, array('level' => $adminid));
			return $query->row_array();
		}


	function get($id)
		{
			$query   = $this->db->get_where($this->_table, array('id_gallery' => $id));
			return $query->row_array();
		}

	function add($data)
	    {
	        $this->db->insert($this->_table, $data);
	    }

	function add_detail($data)
	{
		$this->db->insert('t_detail_gallery', $data);
	}

	
	function update($id, $data)
    {
		$this->db->where('id_gallery', $id);
		$this->db->update($this->_table, $data);
	}

	function update_detail($id, $data)
    {
		$this->db->where('id', $id);
		$this->db->update('t_detail_gallery', $data);
	}


	function delete($data)
	{
		$this->db->delete($this->_table, array('id_gallery' => $data)); 
	}

	function selectreport($tgl1,$tgl2){
		$query  = $this->db->query('SELECT COUNT(id_artikel) as totnya,t_admin.*  from t_artikel  JOIN t_admin ON t_artikel.id_admin = t_admin.id_adm   where tgl_pub >="'.$tgl1.' 00:00:00" AND tgl_pub <="'.$tgl2.' 24:60:60"  group by id_adm order by totnya desc');
        return $query->result_array();
	}

	function detail($id)
	{
		$query = $this->db->get_where('t_detail_gallery', array('id_detail_gallery' => $id));
		return $query->result_array();
	}

	function detail_img($id)
	{
		$query = $this->db->get_where('t_detail_gallery', array('id' => $id));
		return $query->row_array();
	}

	function delete_detail($data)
	{
		$this->db->delete('t_detail_gallery', array('id' => $data)); 
	}
	
	
	
}
?>