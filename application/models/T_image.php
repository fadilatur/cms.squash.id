<?php
class T_image extends CI_Model 
{
	protected $_table = 'image';
	function __construct()
	{
		parent::__construct();
	}
	 
	function select(){
		$query = $this->db->get($this->_table);
		return $query->result_array();
	}
	
	function add($data)
	{
	    $this->db->insert($this->_table, $data);
	}
	function delete($data)
	{
		$this->db->delete($this->_table, array('id' => $data)); 
	}
	
	function get($adminid)
	{
		$query   = $this->db->get_where($this->_table, array('id' => $adminid));
		return $query->row_array();
	}
	
	function update($id, $data)
    {
		$this->db->where('id', $id);
		$this->db->update($this->_table, $data); 
	}
}	
?>
