<?php
	class T_page extends CI_Model 
	{
		protected $_table = 't_page';
		function __construct()
		{
			parent::__construct();
		}
		 
		function select()
	    {
			$this->db->order_by("page", "nama", "asc"); 
			$query = $this->db->get($this->_table);
			return $query->result_array();
		}
	}
?>
