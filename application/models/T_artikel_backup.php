<?php
class T_artikel extends CI_Model 
{
	protected $_table = 't_artikel';
	function __construct()
	{
		parent::__construct();
	}
	 
	 
	
	
	function select(){
		$query = $this->db->get($this->_table);
		$this->db->order_by('id_artikel', 'DESC');
		return $query->result_array();
	}

	function count()
	{
		return $this->db->count_all_results($this->_table);
	}
	

	function selectadmin($id,$pages,$viewperpage, $status){
		
		//if($id!=0){
				$this->db->where('parent_id', $id);
		//$this->db->where('postdate >=',$dari.' 00:00:00');
		//$this->db->where('postdate <=',$sampai.' 23:59:59');
		$this->db->where('publish',$status);
		//}
		$this->db->order_by('tgl_pub', 'DESC');
		$this->db->join('t_admin', 't_admin.id_adm = t_artikel.id_admin');
		$query = $this->db->get($this->_table,$viewperpage,$pages);
		return $query->result_array();
	}



	function selectadminsub($id,$pages,$viewperpage,  $dari, $sampai, $status){
		
		//if($id!=0){
				$this->db->where('parent_id', $id);
		$this->db->where('postdate >=',$dari.' 00:00:00');
		$this->db->where('postdate <=',$sampai.' 23:59:59');
		$this->db->where('publish',$status);
		//}
		$this->db->order_by('tgl_pub', 'DESC');
		$this->db->join('t_admin', 't_admin.id_adm = t_artikel.id_admin');
		$query = $this->db->get($this->_table,$viewperpage,$pages);
		return $query->result_array();
	}

	function selectadminauthor($id,$pages,$viewperpage,$dari, $sampai, $status, $author){
		
		//if($id!=0){
		$this->db->where('parent_id', $id);
		$this->db->where('id_admin', $author);
		$this->db->where('postdate >=',$dari.' 00:00:00');
		$this->db->where('postdate <=',$sampai.' 23:59:59');
		$this->db->where('publish',$status);
		//}
		$this->db->order_by('tgl_pub', 'DESC');
		$this->db->join('t_admin', 't_admin.id_adm = t_artikel.id_admin');
		$query = $this->db->get($this->_table,$viewperpage,$pages);
		return $query->result_array();
	}

	function countauthor($id,  $dari, $sampai, $status, $author){
		$this->db->where('t_relasi.id_object2', $id);
		$this->db->where('postdate >=',$dari.' 00:00:00');
		$this->db->where('postdate <=',$sampai.' 23:59:59');
		$this->db->where('publish',$status);
		$this->db->where('id_admin', $author);
		$this->db->order_by('tgl_pub', 'DESC');
		$this->db->join('t_admin', 't_admin.id_adm = t_artikel.id_admin');
		$query = $this->db->count_all_results($this->_table); 
		return $query;
	}



	function selectadminsubauthor($id,$pages,$viewperpage,  $dari, $sampai, $status, $author){
		
		//if($id!=0){
		$this->db->where('parent_id', $id);
		$this->db->where('id_admin', $author);
		$this->db->where('postdate >=',$dari.' 00:00:00');
		$this->db->where('postdate <=',$sampai.' 23:59:59');
		$this->db->where('publish',$status);
		//}
		$this->db->order_by('tgl_pub', 'DESC');
		$this->db->join('t_admin', 't_admin.id_adm = t_artikel.id_admin');
		$query = $this->db->get($this->_table,$viewperpage,$pages);
		return $query->result_array();
	}



	function selectsection($id,$pages,$viewperpage,  $dari, $sampai, $status){
		$this->db->where('id_section', $id);
		$this->db->where('postdate >=',$dari.' 00:00:00');
		$this->db->where('postdate <=',$sampai.' 23:59:59');
		$this->db->where('publish',$status);
		
		$this->db->order_by('tgl_pub', 'DESC');
		$this->db->join('t_admin', 't_admin.id_adm = t_artikel.id_admin');
		$query = $this->db->get($this->_table,$viewperpage,$pages);
		return $query->result_array();
	}

	function countsection($id,  $dari, $sampai, $status){
		$this->db->where('id_section', $id);
		$this->db->where('postdate >=',$dari.' 00:00:00');
		$this->db->where('postdate <=',$sampai.' 23:59:59');
		$this->db->where('publish',$status);
		
		$this->db->order_by('tgl_pub', 'DESC');
		$this->db->join('t_admin', 't_admin.id_adm = t_artikel.id_admin');
		$query = $this->db->count_all_results($this->_table); 
		return $query;
	}

	function selectsectionauthor($id,$pages,$viewperpage,  $dari, $sampai, $status, $author){
		$this->db->where('id_section', $id);
		$this->db->where('postdate >=',$dari.' 00:00:00');
		$this->db->where('postdate <=',$sampai.' 23:59:59');
		$this->db->where('publish',$status);
		$this->db->where('id_admin', $author);
		
		$this->db->order_by('tgl_pub', 'DESC');
		$this->db->join('t_admin', 't_admin.id_adm = t_artikel.id_admin');
		$query = $this->db->get($this->_table,$viewperpage,$pages);
		return $query->result_array();
	}

	function countsectionauthor($id,  $dari, $sampai, $status, $author){
		$this->db->where('id_section', $id);
		$this->db->where('postdate >=',$dari.' 00:00:00');
		$this->db->where('postdate <=',$sampai.' 23:59:59');
		$this->db->where('publish',$status);
		$this->db->where('id_admin', $author);
		
		$this->db->order_by('tgl_pub', 'DESC');
		$this->db->join('t_admin', 't_admin.id_adm = t_artikel.id_admin');
		$query = $this->db->count_all_results($this->_table); 
		return $query;
	}



	function selectbyadmin($id,$pages,$viewperpage,  $dari, $sampai, $status){
		$this->db->where('id_admin', $id);
		$this->db->where('postdate >=',$dari.' 00:00:00');
		$this->db->where('postdate <=',$sampai.' 23:59:59');
		$this->db->where('publish',$status);
		$this->db->order_by('tgl_pub', 'DESC');
		$this->db->join('t_admin', 't_admin.id_adm = t_artikel.id_admin');
		
		$query = $this->db->get($this->_table,$viewperpage,$pages);
		return $query->result_array();
	}

	function countbyadmin($admin,  $dari, $sampai, $status){
		$this->db->where('id_admin', $admin);
		$this->db->where('postdate >=',$dari.' 00:00:00');
		$this->db->where('postdate <=',$sampai.' 23:59:59');
		$this->db->where('publish',$status);
		$this->db->order_by('tgl_pub', 'DESC');
		$this->db->join('t_admin', 't_admin.id_adm = t_artikel.id_admin');
		$query = $this->db->count_all_results($this->_table); 
		return $query;
	}



	function selectlengkap($id,$admin,$judul,$pages,$viewperpage, $dari, $sampai, $status){
		$this->db->where('id_section', $id);
		$this->db->where('id_admin', $admin);
		$this->db->where('postdate >=',$dari.' 00:00:00');
		$this->db->where('postdate <=',$sampai.' 23:59:59');
		$this->db->where('publish',$status);
		$this->db->like('judul_artikel', $judul);
		$this->db->order_by('tgl_pub', 'DESC');
		$this->db->join('t_admin', 't_admin.id_adm = t_artikel.id_admin');
		$query = $this->db->get($this->_table,$viewperpage,$pages);
		return $query->result_array();
	}


	function countlengkap($id,$admin,$judul,  $dari, $sampai, $status){
		$this->db->where('id_section', $id);
		$this->db->where('id_admin', $admin);
		$this->db->where('postdate >=',$dari.' 00:00:00');
		$this->db->where('postdate <=',$sampai.' 23:59:59');
		$this->db->where('publish',$status);
		$this->db->like('judul_artikel', $judul);
		$this->db->order_by('tgl_pub', 'DESC');
		$this->db->join('t_admin', 't_admin.id_adm = t_artikel.id_admin');
		$query = $this->db->count_all_results($this->_table); 
		return $query;
	}



	function selectlengkap1($id,$admin,$pages,$viewperpage, $dari, $sampai, $status){
		$this->db->where('id_section', $id);
		$this->db->where('id_admin', $admin);
		$this->db->where('postdate >=',$dari.' 00:00:00');
		$this->db->where('postdate <=',$sampai.' 23:59:59');
		$this->db->where('publish',$status);
		$this->db->order_by('tgl_pub', 'DESC');
		$this->db->join('t_admin', 't_admin.id_adm = t_artikel.id_admin');
		$query = $this->db->get($this->_table,$viewperpage,$pages);
		return $query->result_array();
	}


	function countlengkap1($id,$admin, $dari, $sampai, $status){
		$this->db->where('id_section', $id);
		$this->db->where('id_admin', $admin);
		$this->db->where('postdate >=',$dari.' 00:00:00');
		$this->db->where('postdate <=',$sampai.' 23:59:59');
		$this->db->where('publish',$status);
		$this->db->order_by('tgl_pub', 'DESC');
		$this->db->join('t_admin', 't_admin.id_adm = t_artikel.id_admin');
		$query = $this->db->count_all_results($this->_table); 
		return $query;
	}


	function selectlengkap2($id,$judul,$pages,$viewperpage, $dari, $sampai, $status){
		$this->db->where('id_section', $id);
		$this->db->where('postdate >=',$dari.' 00:00:00');
		$this->db->where('postdate <=',$sampai.' 23:59:59');
		$this->db->where('publish',$status);
		
		$this->db->like('judul_artikel', $judul);
		$this->db->order_by('tgl_pub', 'DESC');
		$this->db->join('t_admin', 't_admin.id_adm = t_artikel.id_admin');
		$query = $this->db->get($this->_table,$viewperpage,$pages);
		return $query->result_array();
	}

	function countlengkap2($id,$judul, $dari, $sampai, $status){
		$this->db->where('id_section', $id);
		$this->db->where('postdate >=',$dari.' 00:00:00');
		$this->db->where('postdate <=',$sampai.' 23:59:59');
		$this->db->where('publish',$status);
		$this->db->like('judul_artikel', $judul);
		$this->db->order_by('tgl_pub', 'DESC');
		$this->db->join('t_admin', 't_admin.id_adm = t_artikel.id_admin');
		$query = $this->db->count_all_results($this->_table); 
		return $query;
	}

	function selectlengkap2author($id,$judul,$pages,$viewperpage, $dari, $sampai, $status, $author){
		$this->db->where('id_section', $id);
		$this->db->where('postdate >=',$dari.' 00:00:00');
		$this->db->where('postdate <=',$sampai.' 23:59:59');
		$this->db->where('publish',$status);
		$this->db->where('id_admin', $author);
		$this->db->like('judul_artikel', $judul);
		$this->db->order_by('tgl_pub', 'DESC');
		$this->db->join('t_admin', 't_admin.id_adm = t_artikel.id_admin');
		$query = $this->db->get($this->_table,$viewperpage,$pages);
		return $query->result_array();
	}

	function countlengkap2author($id,$judul, $dari, $sampai, $status, $author){
		$this->db->where('id_section', $id);
		$this->db->where('postdate >=',$dari.' 00:00:00');
		$this->db->where('postdate <=',$sampai.' 23:59:59');
		$this->db->where('publish',$status);
		$this->db->where('id_admin', $author);
		$this->db->like('judul_artikel', $judul);
		$this->db->order_by('tgl_pub', 'DESC');
		$this->db->join('t_admin', 't_admin.id_adm = t_artikel.id_admin');
		$query = $this->db->count_all_results($this->_table); 
		return $query;
	}


	function selectlengkap3($admin,$judul,$pages,$viewperpage, $dari, $sampai, $status){
		
		$this->db->where('id_admin', $admin);
		$this->db->where('postdate >=',$dari.' 00:00:00');
		$this->db->where('postdate <=',$sampai.' 23:59:59');
		$this->db->where('publish',$status);
		$this->db->like('judul_artikel', $judul);
		$this->db->order_by('tgl_pub', 'DESC');
		$this->db->join('t_admin', 't_admin.id_adm = t_artikel.id_admin');
		$query = $this->db->get($this->_table,$viewperpage,$pages);
		return $query->result_array();
	}

	function countlengkap3($admin,$judul, $dari, $sampai, $status){
		$this->db->where('id_admin', $admin);
		$this->db->where('postdate >=',$dari.' 00:00:00');
		$this->db->where('postdate <=',$sampai.' 23:59:59');
		$this->db->where('publish',$status);
		$this->db->like('judul_artikel', $judul);
		$this->db->order_by('tgl_pub', 'DESC');
		$this->db->join('t_admin', 't_admin.id_adm = t_artikel.id_admin');
		$query = $this->db->count_all_results($this->_table); 
		return $query;
	}



	function selectjudul($id,$pages,$viewperpage, $dari, $sampai, $status){
		$this->db->where('postdate >=',$dari.' 00:00:00');
		$this->db->where('postdate <=',$sampai.' 23:59:59');
		$this->db->where('publish',$status);
		$this->db->like('judul_artikel', $id);
		$this->db->order_by('tgl_pub', 'DESC');
		$this->db->join('t_admin', 't_admin.id_adm = t_artikel.id_admin');
		
		$query = $this->db->get($this->_table,$viewperpage,$pages);
		return $query->result_array();
	}



	function countjudul($judul, $dari, $sampai, $status){
		$this->db->where('postdate >=',$dari.' 00:00:00');
		$this->db->where('postdate <=',$sampai.' 23:59:59');
		$this->db->where('publish',$status);
		$this->db->like('judul_artikel', $judul);
		$this->db->order_by('tgl_pub', 'DESC');
		$this->db->join('t_admin', 't_admin.id_adm = t_artikel.id_admin');
		$query = $this->db->count_all_results($this->_table); 
		return $query;
	}

	function selectjudul2($id, $status, $pages,$viewperpage){
		$this->db->where('publish',$status);
		$this->db->like('judul_artikel', $id);
		$this->db->order_by('tgl_pub', 'DESC');
		$this->db->join('t_admin', 't_admin.id_adm = t_artikel.id_admin');
		
		$query = $this->db->get($this->_table,$viewperpage,$pages);
		return $query->result_array();
	}



	function countjudul2($judul, $status){
		$this->db->where('publish',$status);
		$this->db->like('judul_artikel', $judul);
		$this->db->order_by('tgl_pub', 'DESC');
		$this->db->join('t_admin', 't_admin.id_adm = t_artikel.id_admin');
		$query = $this->db->count_all_results($this->_table); 
		return $query;
	}

	function selectjudulauthor($id,$pages,$viewperpage, $dari, $sampai, $status, $author){
		$this->db->where('postdate >=',$dari.' 00:00:00');
		$this->db->where('postdate <=',$sampai.' 23:59:59');
		$this->db->where('publish',$status);
		$this->db->where('id_admin', $author);
		$this->db->like('judul_artikel', $id);
		$this->db->order_by('tgl_pub', 'DESC');
		$this->db->join('t_admin', 't_admin.id_adm = t_artikel.id_admin');
		
		$query = $this->db->get($this->_table,$viewperpage,$pages);
		return $query->result_array();
	}



	function countjudulauthor($judul, $dari, $sampai, $status, $author){
		$this->db->where('postdate >=',$dari.' 00:00:00');
		$this->db->where('postdate <=',$sampai.' 23:59:59');
		$this->db->where('publish',$status);
		$this->db->where('id_admin', $author);
		$this->db->like('judul_artikel', $judul);
		$this->db->order_by('tgl_pub', 'DESC');
		$this->db->join('t_admin', 't_admin.id_adm = t_artikel.id_admin');
		$query = $this->db->count_all_results($this->_table); 
		return $query;
	}

	function selectstatus($pages,$viewperpage, $dari, $sampai, $status){
		$this->db->where('postdate >=',$dari.' 00:00:00');
		$this->db->where('postdate <=',$sampai.' 23:59:59');
		$this->db->where('publish',$status);
		$this->db->where('parent_id', 0);
		$this->db->order_by('tgl_pub', 'DESC');
		$this->db->join('t_admin', 't_admin.id_adm = t_artikel.id_admin');
		
		$query = $this->db->get($this->_table,$viewperpage,$pages);
		
		return $query->result_array();
		//print_r($this->db->last_query());
		//die();
	}



	function countstatus($dari, $sampai, $status){
		$this->db->where('postdate >=',$dari.' 00:00:00');
		$this->db->where('postdate <=',$sampai.' 23:59:59');
		$this->db->where('publish',$status);
		$this->db->where('parent_id', 0);
		$this->db->order_by('tgl_pub', 'DESC');
		$this->db->join('t_admin', 't_admin.id_adm = t_artikel.id_admin');
		$query = $this->db->count_all_results($this->_table); 
		return $query;
	}

	function selectstatus2($pages,$viewperpage){
		$this->db->where('publish','Y');
		$this->db->where('parent_id', 0);
		$this->db->order_by('tgl_pub', 'DESC');
		$this->db->join('t_admin', 't_admin.id_adm = t_artikel.id_admin');
		
		$query = $this->db->get($this->_table,$viewperpage,$pages);
		
		return $query->result_array();
		//print_r($this->db->last_query());
		//die();
	}


	function selectstatus2child($id,$pages,$viewperpage){
		$this->db->where('publish','Y');
		$this->db->where('parent_id', $id);
		$this->db->order_by('tgl_pub', 'DESC');
		$this->db->join('t_admin', 't_admin.id_adm = t_artikel.id_admin');
		
		$query = $this->db->get($this->_table,$viewperpage,$pages);
		
		return $query->result_array();
		//print_r($this->db->last_query());
		//die();
	}



	function countstatus2(){
		$this->db->where('publish','Y');
		$this->db->where('parent_id', 0);
		$this->db->order_by('tgl_pub', 'DESC');
		$this->db->join('t_admin', 't_admin.id_adm = t_artikel.id_admin');
		$query = $this->db->count_all_results($this->_table); 
		return $query;
	}

	function countstatus2child($id){
		$this->db->where('publish','Y');
		$this->db->where('parent_id', $id);
		$this->db->order_by('tgl_pub', 'DESC');
		$this->db->join('t_admin', 't_admin.id_adm = t_artikel.id_admin');
		$query = $this->db->count_all_results($this->_table); 
		return $query;
	}

	function selectstatusauthor($pages,$viewperpage, $dari, $sampai, $status, $author){
		$this->db->where('postdate >=',$dari.' 00:00:00');
		$this->db->where('postdate <=',$sampai.' 23:59:59');
		$this->db->where('publish',$status);
		$this->db->where('id_admin', $author);
		$this->db->where('parent_id', 0);
		$this->db->order_by('tgl_pub', 'DESC');
		$this->db->join('t_admin', 't_admin.id_adm = t_artikel.id_admin');
		
		$query = $this->db->get($this->_table,$viewperpage,$pages);
		return $query->result_array();
	}



	function countstatusauthor($dari, $sampai, $status, $author){
		$this->db->where('postdate >=',$dari.' 00:00:00');
		$this->db->where('postdate <=',$sampai.' 23:59:59');
		$this->db->where('publish',$status);
		$this->db->where('id_admin', $author);
		$this->db->where('parent_id', 0);
		$this->db->order_by('tgl_pub', 'DESC');
		$this->db->join('t_admin', 't_admin.id_adm = t_artikel.id_admin');
		$query = $this->db->count_all_results($this->_table); 
		return $query;
	}

	function selectstatusauthor2($pages,$viewperpage,$author){
		$this->db->where('publish','Y');
		$this->db->where('id_admin', $author);
		$this->db->where('parent_id', 0);
		$this->db->order_by('tgl_pub', 'DESC');
		$this->db->join('t_admin', 't_admin.id_adm = t_artikel.id_admin');
		
		$query = $this->db->get($this->_table,$viewperpage,$pages);
		return $query->result_array();
	}



	function countstatusauthor2($author){
		$this->db->where('publish','Y');
		$this->db->where('id_admin', $author);
		$this->db->where('parent_id', 0);
		$this->db->order_by('tgl_pub', 'DESC');
		$this->db->join('t_admin', 't_admin.id_adm = t_artikel.id_admin');
		$query = $this->db->count_all_results($this->_table); 
		return $query;
	}

	function selectstatusauthor2search($pages,$viewperpage,$author,$id,$status){
		$this->db->where('publish',$status);
		$this->db->like('judul_artikel', $id);
		$this->db->where('id_admin', $author);
		$this->db->where('parent_id', 0);
		$this->db->order_by('tgl_pub', 'DESC');
		$this->db->join('t_admin', 't_admin.id_adm = t_artikel.id_admin');
		
		$query = $this->db->get($this->_table,$viewperpage,$pages);
		return $query->result_array();
	}



	function countstatusauthor2search($author,$id,$status){
		$this->db->where('publish',$status);
		$this->db->like('judul_artikel', $id);
		$this->db->where('publish','Y');
		$this->db->where('id_admin', $author);
		$this->db->where('parent_id', 0);
		$this->db->order_by('tgl_pub', 'DESC');
		$this->db->join('t_admin', 't_admin.id_adm = t_artikel.id_admin');
		$query = $this->db->count_all_results($this->_table); 
		return $query;
	}

	function selectstatusparent($parent, $pages,$viewperpage, $dari, $sampai, $status){
		$this->db->where('postdate >=',$dari.' 00:00:00');
		$this->db->where('postdate <=',$sampai.' 23:59:59');
		$this->db->where('publish',$status);
		$this->db->where('parent_id', $parent);
		$this->db->order_by('tgl_pub', 'DESC');
		$this->db->join('t_admin', 't_admin.id_adm = t_artikel.id_admin');
		
		$query = $this->db->get($this->_table,$viewperpage,$pages);
		
		return $query->result_array();
		//print_r($this->db->last_query());
		//die();
	}



	function countstatusparent($parent, $dari, $sampai, $status){
		$this->db->where('postdate >=',$dari.' 00:00:00');
		$this->db->where('postdate <=',$sampai.' 23:59:59');
		$this->db->where('publish',$status);
		$this->db->where('parent_id', $parent);
		$this->db->order_by('tgl_pub', 'DESC');
		$this->db->join('t_admin', 't_admin.id_adm = t_artikel.id_admin');
		$query = $this->db->count_all_results($this->_table); 
		return $query;
	}

	function selectstatusauthorparent($parent, $pages,$viewperpage, $dari, $sampai, $status, $author){
		$this->db->where('postdate >=',$dari.' 00:00:00');
		$this->db->where('postdate <=',$sampai.' 23:59:59');
		$this->db->where('publish',$status);
		$this->db->where('id_admin', $author);
		$this->db->where('parent_id', $parent);
		$this->db->order_by('tgl_pub', 'DESC');
		$this->db->join('t_admin', 't_admin.id_adm = t_artikel.id_admin');
		
		$query = $this->db->get($this->_table,$viewperpage,$pages);
		return $query->result_array();
	}



	function countstatusauthorparent($parent, $dari, $sampai, $status, $author){
		$this->db->where('postdate >=',$dari.' 00:00:00');
		$this->db->where('postdate <=',$sampai.' 23:59:59');
		$this->db->where('publish',$status);
		$this->db->where('id_admin', $author);
		$this->db->where('parent_id', $parent);
		$this->db->order_by('tgl_pub', 'DESC');
		$this->db->join('t_admin', 't_admin.id_adm = t_artikel.id_admin');
		$query = $this->db->count_all_results($this->_table); 
		return $query;
	}


	function selectrilizen($pages,$viewperpage){
		//$this->db->where('publish','Y');
		$this->db->where('parent_id', 0);
		$this->db->where('rilizen', 'Y');
		$this->db->order_by('tgl_pub', 'DESC');
		$this->db->join('t_user', 't_user.id = t_artikel.id_admin');
		$query = $this->db->get($this->_table,$viewperpage,$pages);
		return $query->result_array();
		//print_r($this->db->last_query());
		//die();
	}



	function countrilizen(){
		//$this->db->where('publish','Y');
		$this->db->where('parent_id', 0);
		$this->db->where('rilizen', 'Y');
		$this->db->order_by('tgl_pub', 'DESC');
		$this->db->join('t_user', 't_user.id = t_artikel.id_admin');
		$query = $this->db->count_all_results($this->_table); 
		return $query;
	}


	function selectrilizensearch($pages,$viewperpage, $search){
		//$this->db->where('publish','Y');
		$this->db->where('parent_id', 0);
		$this->db->where('rilizen', 'Y');
		$this->db->like('judul_artikel', $search);
		$this->db->order_by('tgl_pub', 'DESC');
		$this->db->join('t_user', 't_user.id = t_artikel.id_admin');
		$query = $this->db->get($this->_table,$viewperpage,$pages);
		return $query->result_array();
		//print_r($this->db->last_query());
		//die();
	}



	function countrilizensearch($search){
		//$this->db->where('publish','Y');
		$this->db->where('parent_id', 0);
		$this->db->where('rilizen', 'Y');
		$this->db->like('judul_artikel', $search);
		$this->db->order_by('tgl_pub', 'DESC');
		$this->db->join('t_user', 't_user.id = t_artikel.id_admin');
		$query = $this->db->count_all_results($this->_table); 
		return $query;
	}
	
	
	
	function get_group($adminid)
		{
			$query   = $this->db->get_where($this->_table, array('level' => $adminid));
			return $query->row_array();
		}


	function get($id)
		{
			$query   = $this->db->get_where($this->_table, array('id_artikel' => $id));
			return $query->row_array();
		}

	function add($data)
	    {
	        $this->db->insert($this->_table, $data);
	    }
	
	function update($id, $data)
    {
		$this->db->where('id_artikel', $id);
		$this->db->update($this->_table, $data); 
	}

	function trash($id, $data)
	{
		$this->db->where('id_artikel', $id);
		$this->db->update($this->_table, $data);
	}

	function delete($data)
	{
		$this->db->delete($this->_table, array('id_artikel' => $data)); 
	}

	function selectreport($tgl1,$tgl2){
		$query  = $this->db->query('SELECT COUNT(id_artikel) as totnya,SUM(dibaca) as jumdibaca,t_admin.*  from t_artikel  JOIN t_admin ON t_artikel.id_admin = t_admin.id_adm   where tgl_pub >="'.$tgl1.' 00:00:00" AND tgl_pub <="'.$tgl2.' 24:60:60"  group by id_adm order by totnya desc');
        return $query->result_array();
	}
	
	function selectlocation(){
	
		$query = $this->db->get('location');
		return $query->result_array();
	
		
	}
	
	function selectcontact(){
	
		$query = $this->db->get('t_contact');
		return $query->result_array();
	
	}
	
	function deletee($data)
	{
		$this->db->delete('t_contact', array('id' => $data)); 
	}

	function countartauthor($id, $dari, $sampai)
	{
		//$query = $this->db->query("SELECT count(*) AS jumlah FROM t_artikel where id_admin='$id' AND postdate >= '$dari 00:00:00' AND postdate<='$sampai 23:59:59' ");
		$this->db->where('id_admin', $id);
		$this->db->where('postdate >=',$dari.' 00:00:00');
		$this->db->where('postdate <=',$sampai.' 23:59:59');
		//print_r($this->db->last_query());
		//die();
		return $this->db->count_all_results($this->_table); 
		//return $query->row_array();
	}

	function fokus()
	{
		$query   = $this->db->get('t_fokus');
		return $query->result_array();
	}

	function kolom()
	{
		$query   = $this->db->get('t_kolom');
		return $query->result_array();
	}

	function getold()
	{
		return $this->db->get($this->_table)->result_array();
	}

	function updateimage($id, $data)
	{
		$this->db->where('id_artikel', $id);
		$this->db->update($this->_table, $data);
	}

	function getartikelkolom($id)
	{
		$this->db->join('t_section', 't_section.id_section = t_artikel.id_section');
		$this->db->where('t_artikel.id_section', $id);
		return $this->db->get($this->_table)->result_array();
	}

	function updatehlkolom($data, $id)
	{
		$this->db->where('id_artikel', $id);
		$this->db->update($this->_table, $data);
	}

	
	//function select(){
		//$this->db->select('t_admin.id_adm as idx, t_admin.*, t_level.*');
		//$this->db->join('t_level', 't_admin.level = t_level.level');
		//$query = $this->db->get('t_admin');
		
		//return $query->result_array();
	//}
	
}

/*echo '<pre>';
print_r($this->db->last_query());
echo '</pre>';*/

//echo "<pre>";
		//print_r($datacontent['data']);
		//echo "</pre>";
?>
