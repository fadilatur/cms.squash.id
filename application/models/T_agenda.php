<?php

/**
* 
*/
class T_agenda extends CI_Model
{
	
	protected $_table = 't_agenda';

	function get()
	{
		return $this->db->get($this->_table)->result_array();
	}

	function getid($id)
	{
		$this->db->where('id', $id);
		return $this->db->get($this->_table)->row_array();
	}

	function add($data)
	{
		$this->db->insert($this->_table, $data);
	}

	function edit($data, $id)
	{
		$this->db->where('id', $id);
		$this->db->update($this->_table, $data);
	}

	function delete($id)
	{
		$this->db->delete($this->_table, array('id'	=>	$id));
	}
}