<?php
/**
* 
*/
class T_user_detail extends CI_Model
{
	protected $_table = 't_user_detail';

	function add($data)
	{
		$this->db->insert($this->_table, $data);
	}

	function edit($data, $id)
	{
		$this->db->where('user_id', $id);
		$this->db->update($this->_table, $data);
	}

	function delete($id)
	{
		$this->db->delete($this->_table, array('user_id' => $id));
	}
}