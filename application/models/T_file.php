<?php
class T_file extends CI_Model 
{
	protected $_table = 't_file';
	function __construct()
	{
		parent::__construct();
	}
	 
	function select(){
	
		$this->db->select('*');
		$this->db->join('t_admin','t_file.post_by = t_admin.id_adm');
		$query = $this->db->get('t_file');
		return $query->result_array();
	
	}
	function delete($data)
	{
		$this->db->delete('t_file', array('id_file' => $data)); 
	}
	
	function add($data)
    {
		
        $this->db->insert($this->_table, $data);
    }
	
	function get_file($id)
	{
		
		$query   = $this->db->get_where('t_file', array('id_file' => $id));
		return $query->row_array();
	}
	
	function update($identitas, $data)
    {
		$this->db->where('id_file', $identitas);
		$this->db->update($this->_table, $data); 
	}
	
}

?>
