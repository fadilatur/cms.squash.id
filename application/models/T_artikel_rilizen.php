<?php

/**
* 
*/
class T_artikel_rilizen extends CI_Model
{
	protected $_table = 't_artikel_rilizen';

	function get()
	{
		$this->db->join('t_user', 't_user.id = t_artikel_rilizen.id_admin');
		$this->db->join('t_section', 't_section.id_section = t_artikel_rilizen.id_section');
		$this->db->order_by('postdate', 'DESC');
		return $this->db->get($this->_table)->result_array();
	}

	function getid($id)
	{
		$this->db->where('id_artikel', $id);
		return $this->db->get($this->_table)->row_array();
	}

	function updatestatus($data, $id)
	{
		$this->db->where('id_artikel', $id);
		$this->db->update($this->_table, $data);
	}
}