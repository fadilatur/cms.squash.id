<?php 
/**
 * 
 */
class T_match extends CI_Model
{
	protected $table = 't_match';

	function getAll() {
		$this->db->where('parent_id', 0);
		return $this->db->get($this->table)->result_array();
	}

	function add($data) {
		$this->db->insert($this->table, $data);
	}

	function getById($id) {
		$this->db->where('id', $id);
		return $this->db->get($this->table)->row_array();
	}

	function update($data, $id) {
		$this->db->where('id', $id);
		$this->db->update($this->table, $data);;
	}

	function delete($id) {
		$this->db->where('id', $id);
		$this->db->delete($this->table);
	}

	function getAllByParent($parent) {
		$this->db->where('parent_id', $parent);
		return $this->db->get($this->table)->result_array();
	}
}