<?php
class T_relasi extends CI_Model 
{
	protected $_table = 't_relasi';
	function __construct()
	{
		parent::__construct();
	}
	 
	 function add($data)
	    {
	        $this->db->insert($this->_table, $data);
	    }


	 function getobj2($id,$type)
	    {
	    	$this->db->where('id_object', $id);
	    	$this->db->where('tipe', $type);
	    	$query = $this->db->get($this->_table);
	       return $query->row_array();
	    }


	   function getobj3($id,$type)
	    {
	    	$this->db->where('id_object', $id);
	    	$this->db->where('tipe', $type);
	    	$query = $this->db->get($this->_table);
	       return $query->result_array();
	    }


	    function delete($data,$type)
	{
		$this->db->delete($this->_table, array('id_object' => $data,'tipe' => $type)); 
	}

	 function delete2($data,$type)
	{
		$this->db->delete($this->_table, array('id_object2' => $data,'tipe' => $type)); 
	}
	
}

?>
