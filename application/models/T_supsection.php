<?php
class T_supsection extends CI_Model 
{
	protected $_table = 't_supsection';
	function __construct()
	{
		parent::__construct();
	}
	 
	
	function select(){
		
		$query = $this->db->get('t_supsection');

		return $query->result_array();
	}


	function selectadmin(){
		$query = $this->db->get('t_supsection');

		return $query->result_array();
	}
	
	function add($data)
	    {
	        $this->db->insert($this->_table, $data);
	    }


	 function get($adminid)
	{
		$query   = $this->db->get_where($this->_table, array('id_supsection' => $adminid));
		return $query->row_array();
	}
	
	 
	function update($id, $data)
    {
		$this->db->where('id_supsection', $id);
		$this->db->update($this->_table, $data); 
	}
	
	function delete($data)
	{
		$this->db->delete($this->_table, array('id_supsection' => $data)); 
	}
	
}

/*echo '<pre>';
print_r($this->db->last_query());
echo '</pre>';*/

//echo "<pre>";
		//print_r($datacontent['data']);
		//echo "</pre>";
?>
