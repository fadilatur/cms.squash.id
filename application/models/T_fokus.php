<?php
class T_fokus extends CI_Model 
{
	protected $_table = 't_fokus';
	function __construct()
	{
		parent::__construct();
	}
	 
	function select(){
	
		$this->db->select('*');
		//$this->db->join('t_admin', 't_admin.id_adm = t_fokus.post_by');
		$query = $this->db->get($this->_table);
		return $query->result_array();
	
	}
	function delete($id)
	{
		$this->db->delete($this->_table, array('id' => $id)); 
	}
	
	function add($data)
    {
		
        $this->db->insert($this->_table, $data);
    }
	
	function update($id, $data)
    {
		$this->db->where('id', $id);
		$this->db->update($this->_table, $data); 
	}

	function get_fokus($id)
	{
		
		$query   = $this->db->get_where($this->_table, array('id' => $id));
		return $query->row_array();
	}
	
}

?>
