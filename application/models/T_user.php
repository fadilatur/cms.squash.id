<?php
/**
* 
*/
class T_user extends CI_Model
{
	
	protected $_table = 't_user';

	function add($data)
	{
		$this->db->insert($this->_table, $data);
	}

	function get()
	{
		$this->db->order_by('t_user.id','desc');
		$this->db->join('t_user_detail', 't_user_detail.user_id = t_user.id');
		return $this->db->get($this->_table)->result_array();
	}

	function getid($id)
	{
		$this->db->join('t_user_detail', 't_user_detail.user_id = t_user.id');
		$this->db->where('t_user.id', $id);
		return $this->db->get($this->_table)->row_array();
	}

	function edit($data, $id)
	{
		$this->db->where('id', $id);
		$this->db->update($this->_table, $data);
	}

	function delete($id)
	{
		$this->db->delete($this->_table, array('id' => $id));
	}
}