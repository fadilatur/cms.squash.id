<?php
class Newsletter extends CI_Model
{
	protected $_table = 'newsletter';
	function __construct()
	{
		parent::__construct();
	}
	 
	function select(){
	
		$this->db->group_by('email');
		$this->db->order_by('datetime', 'desc');
		$query = $this->db->get('newsletter');
		return $query->result_array();
	
	}

	function delete($data)
	{
		$this->db->delete('newsletter', array('id' => $data)); 
	}

}