<?php

/**
* 
*/
class T_banner extends CI_Model
{
	
	protected $_table = 't_banner';

	function get()
	{
		return $this->db->get($this->_table)->result_array();
	}

	function getid($id)
	{
		$this->db->where('id_banner', $id);
		return $this->db->get($this->_table)->row_array();
	}

	function add($data)
	{
		$this->db->insert($this->_table, $data);
	}

	function edit($data, $id)
	{
		$this->db->where('id_banner', $id);
		$this->db->update($this->_table, $data);
	}

	function delete($id)
	{
		$this->db->delete($this->_table, array('id_banner'	=>	$id));
	}

	function add_br($data)
	{
		$this->db->insert('t_banner_rilizen', $data);
	}

	function getbr()
	{
		return $this->db->get('t_banner_rilizen')->result_array();
	}

	function getid_br($id)
	{
		$this->db->where('id', $id);
		return $this->db->get('t_banner_rilizen')->row_array();
	}

	function delete_br($id)
	{
		$this->db->delete('t_banner_rilizen', array('id'=>	$id));
	}

	function edit_br($data, $id)
	{
		$this->db->where('id', $id);
		$this->db->update('t_banner_rilizen', $data);
	}
}