<?php 
/**
 * 
 */
class T_score extends CI_Model
{
	protected $table = 't_score';

	function getAllByMatch($id) {
		$this->db->where('match_id', $id);
		return $this->db->get($this->table)->result_array();
	}

	function add($data) {
		$this->db->insert($this->table, $data);
		print_r($this->db->last_query());
	}

	function getById($id) {
		$this->db->where('id', $id);
		return $this->db->get($this->table)->row_array();
	}

	function update($data, $id) {
		$this->db->where('id', $id);
		$this->db->update($this->table, $data);;
	}

	function delete($id) {
		$this->db->where('id', $id);
		$this->db->delete($this->table);
	}

	function deleteBySetMatch($set,$match) {
		$this->db->where('set_id', $set);
		$this->db->where('match_id', $match);
		$this->db->delete($this->table);
		print_r($this->db->last_query());
	}

	function getByeSetMatch($set,$match,$player) {
		$this->db->where('set_id', $set);
		$this->db->where('match_id', $match);
		$this->db->where('player_id', $player);
		return $this->db->get($this->table)->row_array();
	}

	function updateScorePlayer($match,$set,$player,$data) {
		$this->db->where('set_id', $set);
		$this->db->where('match_id', $match);
		$this->db->where('player_id', $player);
		$this->db->update($this->table,$data);
	}
}