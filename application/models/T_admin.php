<?php
class T_admin extends CI_Model 
{
	protected $_table = 't_admin';
	function __construct()
	{
		parent::__construct();
	}

	function get()
	{
		return $this->db->get($this->_table)->result_array();
	}
	 
	 
	function authenticate($name)  
	{
		$query    = $this->db->get_where($this->_table, array('email' => $name));
		$result   = $query->row_array();
		return $result;
		/*print_r($this->db->last_query());
		die();*/
	}
	
	function authenticate2($password) 
	{
		$query    = $this->db->get_where($this->_table, array('password' => $password));
		$result   = $query->row_array();
		
		return $result;
	}
	
	function select($daerah){
		$this->db->select('t_admin.id_adm as idx, t_admin.*, t_level.*');
		$this->db->join('t_level', 't_admin.level = t_level.level');
		$this->db->where('t_admin.daerah', $daerah);
		$query = $this->db->get('t_admin');
		
		return $query->result_array();
	}
	
	function add($data)
	    {
	        $this->db->insert($this->_table, $data);
	    }
		
	function get_tadmin($adminid)
	{
		$query   = $this->db->get_where('t_admin', array('id_adm' => $adminid));
		return $query->row_array();
	}
	
	function update($id, $data)
    {
		$this->db->where('id_adm', $id);
		$this->db->update($this->_table, $data); 
	}
	
	function delete($data)
	{
		$this->db->delete('t_admin', array('id_adm' => $data)); 
	}

	function getauthor()
	{
		return $this->db->get_where($this->_table, array('level' => 3))->result_array();
	}

	function geteditor()
	{
		return $this->db->get_where($this->_table, array('level' => 2))->result_array();
	}
	
}

/*echo '<pre>';
print_r($this->db->last_query());
echo '</pre>';*/

//echo "<pre>";
		//print_r($datacontent['data']);
		//echo "</pre>";
?>
