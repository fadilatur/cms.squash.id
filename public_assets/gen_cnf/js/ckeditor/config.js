/**
 * @license Copyright (c) 2003-2016, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

//CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	
//};


CKEDITOR.editorConfig = function( config ) {

	 	config.filebrowserBrowseUrl = 'http://cms.rilis.id/index.php/image/browseckeditor/1';
	 	config.filebrowserImageBrowseUrl = 'http://cms.rilis.id/index.php/image/browseckeditor/1';
	 	config.filebrowserFlashBrowseUrl = 'http://cms.rilis.id/index.php/image/browseckeditor/1';
	 	config.filebrowserUploadUrl = 'http://cms.rilis.id/index.php/image/browseckeditor/1';
	 	config.filebrowserImageUploadUrl = 'http://cms.rilis.id/index.php/image/browseckeditor/1';
	 	config.filebrowserFlashUploadUrl = 'http://cms.rilis.id/index.php/image/browseckeditor/1';


	config.toolbarGroups = [
		{ name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
		{ name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
		{ name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
		{ name: 'forms', groups: [ 'forms' ] },
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
		{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
		{ name: 'links', groups: [ 'links' ] },
		{ name: 'insert', groups: [ 'insert' ] },
		{ name: 'styles', groups: [ 'styles' ] },
		{ name: 'colors', groups: [ 'colors' ] },
		{ name: 'tools', groups: [ 'tools' ] },
		{ name: 'others', groups: [ 'others' ] },
		{ name: 'about', groups: [ 'about' ] }
	];
	
	config.removeButtons = 'Save,NewPage,Preview,Print,Templates,Cut,Copy,Undo,Redo,Find,Replace,SelectAll,Scayt,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,Outdent,Indent,Blockquote,CreateDiv,BidiLtr,BidiRtl,Language,Flash,HorizontalRule,Smiley,SpecialChar,PageBreak,Styles,Format,Font,FontSize,Maximize,ShowBlocks,About';
};


CKEDITOR.on( 'dialogDefinition', function( ev )
	{		
		var dialogName = ev.data.name;
		var dialogDefinition = ev.data.definition;
		
		if ( dialogName == 'link' || dialogName == 'table' || dialogName == 'tableProperties' || dialogName == 'image')
		{			
			dialogDefinition.removeContents( 'advanced' );
			dialogDefinition.removeContents('Upload');		
		}
	});


